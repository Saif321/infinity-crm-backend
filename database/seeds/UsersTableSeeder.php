<?php

use Illuminate\Database\Seeder;
use \App\Repositories\UserRepository;
use \Spatie\Permission\Models\Role;
use \Spatie\Permission\Models\Permission;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(UserRepository $userRepository)
    {
        $rootRole = Role::create(['guard_name' => 'web', 'name' => 'root']);
        $adminRole = Role::create(['guard_name' => 'web', 'name' => 'administrator']);
        $customerRole = Role::create(['guard_name' => 'api', 'name' => 'customer']);

        $user = $userRepository->create([
            'first_name' => 'Administrator',
            'last_name' => 'Administrator',
            'email' => 'administrator@telego.rs',
            'phone' => '38169153117712',
            'is_active' => 1,
            'password' => 'testeras',
            'image_id' => 1
        ]);

        $user->assignRole($rootRole);
        $user->assignRole($adminRole);

        $user = $userRepository->create([
            'first_name' => 'Infinityfit',
            'last_name' => 'Administrator',
            'email' => 'administrator@infinityfit.io',
            'phone' => '38169153117713',
            'is_active' => 1,
            'password' => 'testeras',
            'image_id' => 1
        ]);

        $user->assignRole($rootRole);
        $user->assignRole($adminRole);

        $user = $userRepository->create([
            'first_name' => 'Drugi',
            'last_name' => 'Administrator',
            'email' => 'admin@telego.rs',
            'is_active' => 1,
            'password' => 'testeras',
            'image_id' => 1
        ]);

        $user->assignRole($adminRole);

        $user = $userRepository->create([
            'first_name' => 'Test Probic',
            'last_name' => 'testeras',
            'email' => 'test@server.com',
            'phone' => '3816415311771',
            'is_active' => 1,
            'password' => 'testeras',
            'image_id' => 1
        ]);

        $user->assignRole($customerRole);

        $user = $userRepository->create([
            'first_name' => 'Rista',
            'last_name' => 'Testeras',
            'email' => 'ristivojevicnikola@yahoo.com',
            'phone' => '0642231',
            'is_active' => 1,
            'password' => 'Test@123',
            'image_id' => 1
        ]);

        $user->assignRole($customerRole);

        $user = $userRepository->create([
            'first_name' => 'Stevica',
            'last_name' => 'Testeras',
            'email' => 'stevan.ivankovic@telego.rs',
            'phone' => '381652629948',
            'is_active' => 1,
            'password' => 'testeras',
            'image_id' => 1
        ]);

        $user->assignRole($customerRole);

        $user = $userRepository->create([
            'first_name' => 'Emin',
            'last_name' => 'Grbo',
            'email' => 'emin.grbo@telego.rs',
            'phone' => '381631028881',
            'is_active' => 1,
            'password' => 'testeras',
            'image_id' => 1
        ]);

        $user->assignRole($customerRole);

        $user = $userRepository->create([
            'first_name' => 'Vukasin',
            'last_name' => 'Stankovic',
            'email' => 'vukasins@gmail.com',
            'phone' => '381691531177',
            'is_active' => 1,
            'password' => 'testeras',
            'image_id' => 1
        ]);

        $user->assignRole($customerRole);
    }
}
