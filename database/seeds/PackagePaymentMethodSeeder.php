<?php

use App\Models\PackagePaymentMethod;
use Illuminate\Database\Seeder;

class PackagePaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = date('Y-m-d H:i:s');
        \Illuminate\Support\Facades\DB::table('package_payment_methods')->insert([
            [
                'payment_method' => PackagePaymentMethod::PAYMENT_METHOD_CREDIT_CARD, 'package_id' => 1,
                'price' => 599, 'currency_code' => 'RSD',
                'created_at' => $time, 'updated_at' => $time
            ],
            [
                'payment_method' => PackagePaymentMethod::PAYMENT_METHOD_SMS, 'package_id' => 1, 'created_at' => $time,
                'price' => 599, 'currency_code' => 'RSD',
                'updated_at' => $time
            ],
            [
                'payment_method' => PackagePaymentMethod::PAYMENT_METHOD_APPLE_PAY, 'package_id' => 1,
                'price' => 599, 'currency_code' => 'RSD',
                'created_at' => $time, 'updated_at' => $time
            ],
            [
                'payment_method' => PackagePaymentMethod::PAYMENT_METHOD_GOOGLE_PAY, 'package_id' => 1,
                'price' => 599, 'currency_code' => 'RSD',
                'created_at' => $time, 'updated_at' => $time
            ],

            [
                'payment_method' => PackagePaymentMethod::PAYMENT_METHOD_CREDIT_CARD, 'package_id' => 2,
                'price' => 5999, 'currency_code' => 'RSD',
                'created_at' => $time, 'updated_at' => $time
            ],
            [
                'payment_method' => PackagePaymentMethod::PAYMENT_METHOD_APPLE_PAY, 'package_id' => 2,
                'price' => 5999, 'currency_code' => 'RSD',
                'created_at' => $time, 'updated_at' => $time
            ],
            [
                'payment_method' => PackagePaymentMethod::PAYMENT_METHOD_GOOGLE_PAY, 'package_id' => 2,
                'price' => 5999, 'currency_code' => 'RSD',
                'created_at' => $time, 'updated_at' => $time
            ],
        ]);
    }
}
