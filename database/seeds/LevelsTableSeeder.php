<?php

use Illuminate\Database\Seeder;

class LevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $descriptions = [
            'I have recently started to workout.',
            'In workout process less then 1 year',
            'In workout process more then 1 year',
            'Pro'
        ];
        foreach(['Basic', 'Intermediate', 'Advanced', 'Pro'] as $index => $name){
            $levelId = DB::table('levels')->insertGetId([
                'name' => $name,
                'description' => $descriptions[$index],
            ]);

            DB::table('__levels')->insert([
                'object_id' => $levelId,
                'language_id' => 1,
                'name' => $name,
                'description' => $descriptions[$index],
            ]);
        }
    }
}
