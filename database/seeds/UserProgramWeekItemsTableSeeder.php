<?php

use Illuminate\Database\Seeder;

class UserProgramWeekItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(range(1, 10) as $weekId){
            foreach(range(1, rand(3, 6)) as $day){
                DB::table('user_program_week_items')->insert([
                    'user_program_week_id' => $weekId,
                    'order_day' => $day,
                ]);
            }
        }
    }
}
