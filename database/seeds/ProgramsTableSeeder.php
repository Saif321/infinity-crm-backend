<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use \Illuminate\Support\Facades\DB;

class ProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $programs = [
            'Get Fitter Program',
            'Ultimate Strength Program',
            'Gain Muscle Program',
            'High Intensity Cardio',

            'Get Fitter Program',
            'Ultimate Strength Program',
            'Gain Muscle Program',
            'High Intensity Cardio',

            'Get Fitter Program',
            'Ultimate Strength Program',
            'Gain Muscle Program',
            'High Intensity Cardio',

            'Get Fitter Program',
            'Ultimate Strength Program',
            'Gain Muscle Program',
            'High Intensity Cardio',

            'Get Fitter Program',
            'Ultimate Strength Program',
            'Gain Muscle Program',
            'High Intensity Cardio',

            'Get Fitter Program',
            'Ultimate Strength Program',
            'Gain Muscle Program',
            'High Intensity Cardio',

            'Get Fitter Program',
        ];

        foreach($programs as $index => $name){
            $levelId = rand(1, 4);
            $days = rand(3, 6);
            $weeks = rand(3,8);
            $imageId = 1;
            $videoId = 3 + $index;
            $reviewCount = rand(100, 5000);
            $avgRating = rand(1, 5);

            $programId = DB::table('programs')->insertGetId([
                'level_id' => $levelId,
                'name' => $name,
                'description' => $faker->text(),
                'program_length_in_weeks' => $weeks,
                'workout_days_per_week' => $days,
                'video_id' => $videoId,
                'image_id' => $imageId,
                'like_count' => 0,
                'review_count' => $reviewCount,
                'avg_rating' => $avgRating,
                'is_published' => 1,
                'is_free' => 1,
                'exercise_environment_id' => 1,
            ]);

            DB::table('__programs')->insert([
                'object_id' => $programId,
                'language_id' => 1,
                'level_id' => $levelId,
                'name' => $name,
                'description' => $faker->text(),
                'program_length_in_weeks' => $weeks,
                'workout_days_per_week' => $days,
                'video_id' => $videoId,
                'image_id' => $imageId,
                'like_count' => 0,
                'review_count' => $reviewCount,
                'avg_rating' => $avgRating,
                'is_published' => 1,
                'exercise_environment_id' => 1,
            ]);

            for($j = 0; $j < 12; $j++) {
                $weekId = DB::table('program_weeks')->insertGetId([
                    'program_id' => $programId,
                    'order' => $j + 1
                ]);

                for($k = 0; $k < $days; $k++) {
                    $weekItemId = DB::table('week_items')->insertGetId([
                        'program_week_id' => $weekId,
                        'training_id' => 1, // definisano na osnovu broj generisanih treninga
                        'is_locked' => rand(0, 1),
                        'order' => $k+1
                    ]);
                }
            }

            foreach (range(1,10) as $indexComments) {
                DB::table('comments')->insert([
                    'user_id' => 1,
                    'object_id' => $programId,
                    'object_type' => \App\Models\Program::class,
                    'comment' => $faker->text(),
                    'is_published' => true,
                    'published_at' => date('Y-m-d H:i:s')
                ]);
            }

            // todo: uraditi dobro racunanje broja komentara
            foreach (range(1,10) as $indexComments) {
                DB::table('reviews')->insert([
                    'user_id' => 1,
                    'object_id' => $programId,
                    'object_type' => \App\Models\Program::class,
                    'description' => $faker->text(),
                    'rating' => rand(1, 5),
//                    'is_published' => true,
//                    'published_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
    }
}
