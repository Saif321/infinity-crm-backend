<?php

use Illuminate\Database\Seeder;

class UserProgramWeeksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(range(1, 4) as $week){
            DB::table('user_program_weeks')->insert([
                'user_program_id' => 1,
                'order' => $week,
            ]);
        }

        foreach(range(1, 6) as $week){
            DB::table('user_program_weeks')->insert([
                'user_program_id' => 2,
                'order' => $week,
            ]);
        }
    }
}
