<?php

use Illuminate\Database\Seeder;
use App\Repositories\MuscleGroupRepository;

class MuscleGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(MuscleGroupRepository $muscleGroupRepository)
    {
        //
        $muscleGroupList = ['Chest', 'Deltoid', 'Biceps', 'Abs', 'Quadriceps', 'Obliques', 'Abductor', 'Adductor', 'Hamstring', 'Calves', 'Glutes', 'Lats', 'Triceps', 'Infraspinatus', 'Trapezius', 'Rear deltoid', 'Forearm', 'Cardio'];
        foreach($muscleGroupList as $muscleGroupItem) {
            $insertId = DB::table('muscle_groups')->insertGetId([
                'name' => $muscleGroupItem,
                'slug' => \Illuminate\Support\Str::slug($muscleGroupItem)
            ]);
            DB::table('__muscle_groups')->insertGetId([
                'object_id' => $insertId,
                'language_id' => 1,
                'name' => $muscleGroupItem,
                'slug' => \Illuminate\Support\Str::slug($muscleGroupItem)
            ]);

            DB::table('__muscle_groups')->insertGetId([
                'object_id' => $insertId,
                'language_id' => 2,
                'name' => $muscleGroupItem,
                'slug' => \Illuminate\Support\Str::slug($muscleGroupItem)
            ]);
        }
    }
}
