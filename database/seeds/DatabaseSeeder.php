<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //todo: execute seed based on env

        $this->call(UsersTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);
//        $this->call(BlogsTableSeeder::class);
//        $this->call(LikesTableSeeder::class);
//        $this->call(BlogTagsTableSeeder::class);
        $this->call(LevelsTableSeeder::class);
//        $this->call(ProgramsTableSeeder::class);
//        $this->call(UserProgramsTableSeeder::class);
//        $this->call(PurchasedProgramsTableSeeder::class);
//        $this->call(UserProgramWeeksTableSeeder::class);
//        $this->call(UserProgramWeekItemsTableSeeder::class);
        $this->call(MuscleGroupsTableSeeder::class);
        $this->call(ExerciseEnvironmentsTableSeeder::class);
//        $this->call(ExercisesTableSeeder::class);
//        $this->call(MealsTableSeeder::class);
        $this->call(GoalsSeeder::class);
        $this->call(MealGroupsTableSeeder::class);
//        $this->call(NutritionProgramsTableSeeder::class);
        $this->call(FruitsTableSeeder::class);
        $this->call(VegetablesTableSeeder::class);
        $this->call(MeatsTableSeeder::class);
        $this->call(PackageSeeder::class);
        $this->call(OauthClientsSeeder::class);
//        $this->call(GalleriesTableSeeder::class);
//        $this->call(TrainingsTableSeeder::class);
    }
}
