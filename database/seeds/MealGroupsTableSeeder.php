<?php

use Illuminate\Database\Seeder;

class MealGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('meal_groups')->insertGetId([
            'name' => 'Breakfast',
            'eating_time' => '8am - 11am',
//            'order' => '1',
        ]);
        DB::table('__meal_groups')->insertGetId([
            'object_id' => 1,
            'language_id' => 1,
            'name' => 'Breakfast',
            'eating_time' => '8am - 11am',
//            'order' => '1',
        ]);
        DB::table('__meal_groups')->insertGetId([
            'object_id' => 1,
            'language_id' => 2,
            'name' => 'Doručak',
            'eating_time' => '8am - 11am',
//            'order' => '1',
        ]);

        DB::table('meal_groups')->insertGetId([
            'name' => 'Lunch',
            'eating_time' => '2pm - 4pm',
//            'order' => '2',
        ]);
        DB::table('__meal_groups')->insertGetId([
            'object_id' => 2,
            'language_id' => 1,
            'name' => 'Lunch',
            'eating_time' => '2pm - 4pm',
//            'order' => '2',
        ]);
        DB::table('__meal_groups')->insertGetId([
            'object_id' => 2,
            'language_id' => 2,
            'name' => 'Ručak',
            'eating_time' => '2pm - 4pm',
//            'order' => '2',
        ]);

        DB::table('meal_groups')->insertGetId([
            'name' => 'Dinner',
            'eating_time' => '7pm - 9pm',
//            'order' => '3',
        ]);
        DB::table('__meal_groups')->insertGetId([
            'object_id' => 3,
            'language_id' => 1,
            'name' => 'Dinner',
            'eating_time' => '7pm - 9pm',
//            'order' => '3',
        ]);
        DB::table('__meal_groups')->insertGetId([
            'object_id' => 3,
            'language_id' => 2,
            'name' => 'Večera',
            'eating_time' => '7pm - 9pm',
//            'order' => '3',
        ]);


    }
}
