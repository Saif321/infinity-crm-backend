<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OauthClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->insert([
            ['id' => 1, 'name' => 'Android', 'secret' => 'hIPfzsXdd2VaBkAVKxsrdB126UG9snwbPFrMNSXK', 'password_client' => 1],
            ['id' => 2, 'name' => 'iOS', 'secret' => '20OAK2sN7yg4Xz1iQbpQQvKUpiArnBwU2jU0FG6U', 'password_client' => 1],
            ['id' => 3, 'name' => 'CMS', 'secret' => 'J9mvGV3iVcN2NHWqKEb3yOVFsZIokZRzk1SzOxZY', 'password_client' => 1],
        ]);
    }
}
