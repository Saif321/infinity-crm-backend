<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExerciseEnvironmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('exercise_environments')->insertGetId([
            'name' => 'Gym',
            'order' => '2'
        ]);

        DB::table('__exercise_environments')->insertGetId([
            'object_id' => 1,
            'language_id' => 1,
            'name' => 'Gym',
            'order' => '2'
        ]);

        DB::table('__exercise_environments')->insertGetId([
            'object_id' => 1,
            'language_id' => 2,
            'name' => 'Teretana',
            'order' => '2'
        ]);




        DB::table('exercise_environments')->insertGetId([
            'name' => 'Home',
            'order' => '3'
        ]);

        DB::table('__exercise_environments')->insertGetId([
            'object_id' => 2,
            'language_id' => 1,
            'name' => 'Home',
            'order' => '3'
        ]);

        DB::table('__exercise_environments')->insertGetId([
            'object_id' => 2,
            'language_id' => 2,
            'name' => 'Kuća',
            'order' => '3'
        ]);



        DB::table('exercise_environments')->insertGetId([
            'name' => 'Outdoor',
            'order' => '5'
        ]);

        DB::table('__exercise_environments')->insertGetId([
            'object_id' => 3,
            'language_id' => 1,
            'name' => 'Outdoor',
            'order' => '5'
        ]);

        DB::table('__exercise_environments')->insertGetId([
            'object_id' => 3,
            'language_id' => 2,
            'name' => 'U Prirodi',
            'order' => '5'
        ]);
    }
}
