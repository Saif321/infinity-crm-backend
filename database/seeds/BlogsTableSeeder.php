<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use \Illuminate\Support\Facades\DB;

class BlogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create();
        foreach (range(1,50) as $index) {
            $title = $faker->name;
            $description = $faker->paragraphs(6, true);
            $publishedAt = date('Y-m-d H:i:s');

            $blogId = DB::table('blogs')->insertGetId([
                'user_id' => 1,
                'title' => $title,
                'description' => $description,
                'is_published' => true,
                'published_at' => $publishedAt,
                'image_id' => 1
            ]);

            DB::table('__blogs')->insert([
                'user_id' => 1,
                'object_id' => $blogId,
                'language_id' => 1,
                'title' => $title,
                'description' => $description,
                'is_published' => true,
                'published_at' => $publishedAt,
                'image_id' => 1
            ]);

            DB::table('__blogs')->insert([
                'user_id' => 1,
                'object_id' => $blogId,
                'language_id' => 2,
                'title' => $title,
                'description' => $description,
                'is_published' => true,
                'published_at' => $publishedAt,
                'image_id' => 1
            ]);

            $commentCount = 0;
            foreach (range(20,30) as $indexComments) {
                DB::table('comments')->insert([
                    'user_id' => 1,
                    'object_id' => $blogId,
                    'object_type' => \App\Models\Blog::class,
                    'comment' => $faker->text(),
                    'is_published' => true,
                    'published_at' => date('Y-m-d H:i:s')
                ]);
                $commentCount++;
            }

            DB::update("UPDATE blogs SET comment_count = ? WHERE id = ?", [$commentCount, $blogId]);
            DB::update("UPDATE __blogs SET comment_count = ? WHERE object_id = ?", [$commentCount, $blogId]);

            /*
            foreach (range(1,100) as $indexLikes) {
                DB::table('likes')->insert([
                    'user_id' => 1,
                    'object_id' => $blogId,
                    'object_type' => \App\Models\Blog::class,
                ]);
            }
            */

            $likeCount = 0;
            foreach (range(1,2) as $userId) {
                DB::table('likes')->insert([
                    'user_id' => $userId,
                    'object_id' => $blogId,
                    'object_type' => \App\Models\Blog::class,
                ]);

                $likeCount++;
            }

            DB::update("UPDATE blogs SET like_count = ? WHERE id = ?", [$likeCount, $blogId]);
            DB::update("UPDATE __blogs SET like_count = ? WHERE object_id = ?", [$likeCount, $blogId]);
        }
    }
}
