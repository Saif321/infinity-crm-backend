<?php

use Illuminate\Database\Seeder;

class PurchasedProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('purchased_programs')->insert([
            'user_id' => 2,
            'user_program_id' => 1,
            'source_program_id' => 1,
        ]);

        DB::table('purchased_programs')->insert([
            'user_id' => 2,
            'user_program_id' => 2,
            'source_program_id' => 3,
        ]);
    }
}
