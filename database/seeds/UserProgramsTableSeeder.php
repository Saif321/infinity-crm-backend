<?php

use Illuminate\Database\Seeder;

class UserProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $programs = [
            'Get Fitter Program',
            'Gain Muscle Program',
        ];

        foreach($programs as $name){
            $levelId = rand(1, 4);
            $days = rand(3, 6);

            $programId = DB::table('user_programs')->insertGetId([
                'level_id' => $levelId,
                'name' => $name,
                'workout_days_per_week' => $days,
                'video_id' => 1,
                'image_id' => 1,
                'is_published' => 1,
            ]);

            DB::table('__user_programs')->insert([
                'object_id' => $programId,
                'language_id' => 1,
                'level_id' => $levelId,
                'name' => $name,
                'workout_days_per_week' => $days,
                'video_id' => 1,
                'image_id' => 1,
                'is_published' => 1,
            ]);
        }
    }
}
