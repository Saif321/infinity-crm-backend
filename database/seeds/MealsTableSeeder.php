<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use \Illuminate\Support\Facades\DB;

class MealsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,50) as $index) {
            //
            $name = $faker->name;
            $description = $faker->paragraphs(6, true);

            $mealId = DB::table('meals')->insertGetId([
                'name' => $name,
                'description' => $description,
                'preparation_time_in_minutes' => rand(40, 160),
                'number_of_calories' => rand(350, 700),
                'fat' => rand(10, 60),
                'proteins' => rand(10, 100),
                'carbohydrates' => rand(10, 60),
                'image_id' => 1,
                'video_id' => 2,
                'is_published' => true,
            ]);
            DB::table('__meals')->insertGetId([
                'object_id' => $mealId,
                'language_id' => 1,
                'name' => $name,
                'description' => $description,
                'preparation_time_in_minutes' => rand(40, 160),
                'number_of_calories' => rand(350, 700),
                'fat' => rand(10, 60),
                'proteins' => rand(10, 100),
                'carbohydrates' => rand(10, 60),
                'image_id' => 1,
                'video_id' => 2,
                'is_published' => true,
            ]);

            for($j = 0; $j < 5; $j++) {
                $ingredientName = $faker->name;
                $mealIngredientId = DB::table('meal_ingredients')->insert([
                    'meal_id' => $mealId,
                    'name' => $ingredientName
                ]);
                DB::table('__meal_ingredients')->insert([
                    'language_id' => 1,
                    'object_id' => $mealIngredientId,
                    'meal_id' => $mealId,
                    'name' => $ingredientName
                ]);
            }

            for($j = 0; $j < 5; $j++) {
                $recepieStep = $faker->paragraphs(6, true);
                $recepieStepId = DB::table('meal_recipe_steps')->insert([
                    'meal_id' => $mealId,
                    'description' => $recepieStep
                ]);
                DB::table('__meal_recipe_steps')->insert([
                    'language_id' => 1,
                    'object_id' => $recepieStepId,
                    'meal_id' => $mealId,
                    'description' => $recepieStep
                ]);
            }
        }
    }
}
