<?php

use Illuminate\Database\Seeder;

class GoalsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $goalId = DB::table('goals')->insertGetId([
            'name' => 'Loose Weight',
            'description' => 'This goal is about loose weight method.',
            'order' => 1,
        ]);
        DB::table('__goals')->insert([
            'object_id' => $goalId,
            'language_id' => 1,
            'name' => 'Loose Weight',
            'description' => 'This goal is about loose weight method.',
            'order' => 1,
        ]);
        DB::table('__goals')->insert([
            'object_id' => $goalId,
            'language_id' => 2,
            'name' => 'Skinuti Kilažu',
            'description' => 'Ovim ciljem postižete željeni efekat skidanja kilaže.',
            'order' => 1,
        ]);

        $goalId = DB::table('goals')->insertGetId([
            'name' => 'Get Muscles',
            'description' => 'This goal is about get weight and muscles method.',
            'order' => 2,
        ]);
        DB::table('__goals')->insert([
            'object_id' => $goalId,
            'language_id' => 1,
            'name' => 'Get Muscles',
            'description' => 'This goal is about get weight and muscles method.',
            'order' => 2,
        ]);
        DB::table('__goals')->insert([
            'object_id' => $goalId,
            'language_id' => 2,
            'name' => 'Dobiti na mišićnoj masi',
            'description' => 'Ovim ciljem postižete željeni efekat dobijanja na snazi i mišićnoj masi.',
            'order' => 2,
        ]);

        $goalId = DB::table('goals')->insertGetId([
            'name' => 'Get fitter',
            'description' => 'This goal is about ...',
            'order' => 3,
        ]);
        DB::table('__goals')->insert([
            'object_id' => $goalId,
            'language_id' => 1,
            'name' => 'Get fitter',
            'description' => 'This goal is about ...',
            'order' => 3,
        ]);
        DB::table('__goals')->insert([
            'object_id' => $goalId,
            'language_id' => 2,
            'name' => 'Get fitter',
            'description' => 'This goal is about ...',
            'order' => 3,
        ]);
    }
}
