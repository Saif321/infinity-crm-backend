<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Repositories\ExerciseRepository;

class ExercisesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(ExerciseRepository $exerciseRepository)
    {
        $faker = Faker::create();
        for($i = 0; $i <= 50; $i++) {
            $name = $faker->name;
            $description = $faker->paragraphs(6, true);
            $publishedAt = date('Y-m-d H:i:s');

            $exerciseId = DB::table('exercises')->insertGetId([
                'name' => $name,
                'description' => $description,
                'image_id' => 1,
                'video_id' => 2,
                'level_id' => 1,
                'is_published' => true,
                'published_at' => $publishedAt,
                'is_free' => true,
            ]);
            DB::table('__exercises')->insertGetId([
                'object_id' => $exerciseId,
                'language_id' => 1,
                'name' => $name,
                'description' => $description,
                'image_id' => 1,
                'video_id' => 2,
                'level_id' => 1,
                'is_published' => true,
                'published_at' => $publishedAt,
                'is_free' => true,
            ]);

            DB::table('exercise_environment_items')->insertGetId([
                'exercise_id' => $exerciseId,
                'exercise_environment_id' => 1
            ]);
            DB::table('exercise_environment_items')->insertGetId([
                'exercise_id' => $exerciseId,
                'exercise_environment_id' => 2
            ]);

            DB::table('exercise_muscle_group_items')->insertGetId([
                'exercise_id' => $exerciseId,
                'muscle_group_id' => 1
            ]);
            DB::table('exercise_muscle_group_items')->insertGetId([
                'exercise_id' => $exerciseId,
                'muscle_group_id' => 2
            ]);
        }
    }
}
