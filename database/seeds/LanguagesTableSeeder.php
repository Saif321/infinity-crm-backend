<?php

use Illuminate\Database\Seeder;
use \App\Repositories\LanguageRepository;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(LanguageRepository $languageRepository)
    {
        //
        $languageRepository->create([
            'language' => 'English',
            'iso' => 'en',
            'is_default' => true
        ]);

        $languageRepository->create([
            'language' => 'Srpski',
            'iso' => 'sr',
            'is_default' => false
        ]);
    }
}
