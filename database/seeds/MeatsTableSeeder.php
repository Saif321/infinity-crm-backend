<?php

use Illuminate\Database\Seeder;

class MeatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('meats')->insertGetId([
            'name' => 'Chicken',
            'image' => 'images/nutrition-icons/ic_chicken.png',
            'order' => '1',
        ]);
        DB::table('__meats')->insertGetId([
            'object_id' => 1,
            'language_id' => 1,
            'name' => 'Chicken',
            'image' => 'images/nutrition-icons/ic_chicken.png',
            'order' => '1',
        ]);
        DB::table('__meats')->insertGetId([
            'object_id' => 1,
            'language_id' => 2,
            'name' => 'Piletina',
            'image' => 'images/nutrition-icons/ic_chicken.png',
            'order' => '1',
        ]);

        DB::table('meats')->insertGetId([
            'name' => 'Beef',
            'image' => 'images/nutrition-icons/ic_beef.png',
            'order' => '2',
        ]);
        DB::table('__meats')->insertGetId([
            'object_id' => 2,
            'language_id' => 1,
            'name' => 'Beef',
            'image' => 'images/nutrition-icons/ic_beef.png',
            'order' => '2',
        ]);
        DB::table('__meats')->insertGetId([
            'object_id' => 2,
            'language_id' => 2,
            'name' => 'Govedina',
            'image' => 'images/nutrition-icons/ic_beef.png',
            'order' => '2',
        ]);

        DB::table('meats')->insertGetId([
            'name' => 'Pork',
            'image' => 'images/nutrition-icons/ic_pork.png',
            'order' => '3',
        ]);
        DB::table('__meats')->insertGetId([
            'object_id' => 3,
            'language_id' => 1,
            'name' => 'Pork',
            'image' => 'images/nutrition-icons/ic_pork.png',
            'order' => '3',
        ]);
        DB::table('__meats')->insertGetId([
            'object_id' => 3,
            'language_id' => 2,
            'name' => 'Svinjetina',
            'image' => 'images/nutrition-icons/ic_pork.png',
            'order' => '3',
        ]);

        DB::table('meats')->insertGetId([
            'name' => 'Turkey',
            'image' => 'images/nutrition-icons/ic_turkey.png',
            'order' => '3',
        ]);
        DB::table('__meats')->insertGetId([
            'object_id' => 4,
            'language_id' => 1,
            'name' => 'Turkey',
            'image' => 'images/nutrition-icons/ic_turkey.png',
            'order' => '3',
        ]);
        DB::table('__meats')->insertGetId([
            'object_id' => 4,
            'language_id' => 2,
            'name' => 'Ćuretina',
            'image' => 'images/nutrition-icons/ic_turkey.png',
            'order' => '3',
        ]);

        DB::table('meats')->insertGetId([
            'name' => 'Fish',
            'image' => 'images/nutrition-icons/ic_fish.png',
            'order' => '3',
        ]);
        DB::table('__meats')->insertGetId([
            'object_id' => 5,
            'language_id' => 1,
            'name' => 'Fish',
            'image' => 'images/nutrition-icons/ic_fish.png',
            'order' => '3',
        ]);
        DB::table('__meats')->insertGetId([
            'object_id' => 5,
            'language_id' => 2,
            'name' => 'Riba',
            'image' => 'images/nutrition-icons/ic_fish.png',
            'order' => '3',
        ]);

        DB::table('meats')->insertGetId([
            'name' => 'No Meat',
            'image' => 'images/nutrition-icons/ic_no_meat.png',
            'order' => '3',
        ]);
        DB::table('__meats')->insertGetId([
            'object_id' => 6,
            'language_id' => 1,
            'name' => 'No Meat',
            'image' => 'images/nutrition-icons/ic_no_meat.png',
            'order' => '3',
        ]);
        DB::table('__meats')->insertGetId([
            'object_id' => 6,
            'language_id' => 2,
            'name' => 'Bez mesa',
            'image' => 'images/nutrition-icons/ic_no_meat.png',
            'order' => '3',
        ]);

    }
}
