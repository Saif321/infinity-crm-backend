<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class NutritionProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i = 0; $i <= 50; $i++) {
            $name = $faker->name;
            $description = $faker->paragraphs(6, true);

            $programLength = rand(1,30);
            $mealsPerDay = rand(1,8);
            $reviewCount = rand(100, 500);
            $avgRating = rand(1, 5);

            $nutritionProgramId = DB::table('nutrition_programs')->insertGetId([
                'name' => $name,
                'description' => $description,
                'comment_count' => 0,
                'like_count' => 0,
                'review_count' => $reviewCount,
                'avg_rating' => $avgRating,
                'image_id' => 1,
                'video_id' => 2,
                'program_length' => $programLength,
                'meals_per_day' => $mealsPerDay,
                'is_published' => true,
                'is_free' => true
            ]);
            DB::table('__nutrition_programs')->insertGetId([
                'object_id' => $nutritionProgramId,
                'language_id' => 1,
                'name' => $name,
                'description' => $description,
                'comment_count' => 0,
                'like_count' => 0,
                'review_count' => $reviewCount,
                'avg_rating' => $avgRating,
                'image_id' => 1,
                'video_id' => 2,
                'program_length' => $programLength,
                'meals_per_day' => $mealsPerDay,
                'is_published' => true,
                'is_free' => true
            ]);

            $nutritionProgramMealGroups = [];
            for($k = 0; $k < 3; $k++) {
                $nutritionProgramMealGroups[] = DB::table('nutrition_program_meal_groups')->insertGetId([
                    'nutrition_program_id' => $nutritionProgramId,
                    'meal_group_id' => rand(1, 3),
                    'order' => $k
                ]);
            }

            for($j = 0; $j < 30; $j++) {
                DB::table('nutrition_program_items')->insertGetId([
                    'nutrition_program_meal_group_id' => $nutritionProgramMealGroups[rand(0, 2)],
                    'meal_id' => rand(1, 50),
                    'order' => $j
                ]);
            }

            DB::table('goal_nutrition_program_items')->insertGetId([
                'nutrition_program_id' => $nutritionProgramId,
                'goal_id' => 1
            ]);
        }
    }
}
