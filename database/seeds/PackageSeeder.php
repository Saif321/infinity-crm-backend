<?php

use Illuminate\Database\Seeder;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = date('Y-m-d H:i:s');
        \Illuminate\Support\Facades\DB::table('packages')->insert([
            [
                'id' => 1, 'name' => 'Basic', 'number_of_programs' => 2, 'number_of_nutrition_programs' => 2,
                'recurring_period_in_days' => 30, 'free_trial_in_days' => 14, 'android_store_package_id' => 'basic_599',
                'created_at' => $time, 'updated_at' => $time
            ],
            [
                'id' => 2, 'name' => 'Premium', 'number_of_programs' => 20, 'number_of_nutrition_programs' => 20,
                'recurring_period_in_days' => 365, 'free_trial_in_days' => 14,
                'android_store_package_id' => 'premium_5999',
                'created_at' => $time, 'updated_at' => $time
            ],
        ]);

        $this->call(PackagePaymentMethodSeeder::class);
    }
}
