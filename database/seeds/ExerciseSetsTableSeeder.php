<?php

use Illuminate\Database\Seeder;

class ExerciseSetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('exercise_sets')->insertGetId([
            'training_id' => 1,
            'name' => 'Home',
            'description' => 'Home',
            'type' => 'Home',
            'duration_in_seconds' => 22,
            'order' => 1
        ]);

        DB::table('__exercise_sets')->insertGetId([
            'object_id' => 1,
            'language_id' => 1,
            'training_id' => 1,
            'name' => 'Home',
            'description' => 'Home',
            'type' => 'Home',
            'duration_in_seconds' => 22,
            'order' => 1
        ]);

        DB::table('__exercise_sets')->insertGetId([
            'object_id' => 1,
            'language_id' => 2,
            'training_id' => 1,
            'name' => 'Home',
            'description' => 'Home',
            'type' => 'Home',
            'duration_in_seconds' => 22,
            'order' => 1
        ]);




        DB::table('exercise_sets')->insertGetId([
            'training_id' => 2,
            'name' => 'Home',
            'description' => 'Home',
            'type' => 'Home',
            'duration_in_seconds' => 33,
            'order' => 3
        ]);

        DB::table('__exercise_sets')->insertGetId([
            'object_id' => 2,
            'language_id' => 1,
            'training_id' => 2,
            'name' => 'Home',
            'description' => 'Home',
            'type' => 'Home',
            'duration_in_seconds' => 'Home',
            'order' => 3
        ]);

        DB::table('__exercise_sets')->insertGetId([
            'object_id' => 2,
            'language_id' => 2,
            'training_id' => 2,
            'name' => 'Home',
            'description' => 'Home',
            'type' => 'Home',
            'duration_in_seconds' => 33,
            'order' => 3
        ]);



        DB::table('exercise_sets')->insertGetId([
            'training_id' => 6,
            'name' => 'Home',
            'description' => 'Home',
            'type' => 'Home',
            'duration_in_seconds' => 11,
            'order' => 4
        ]);

        DB::table('__exercise_sets')->insertGetId([
            'object_id' => 3,
            'language_id' => 1,
            'training_id' => 6,
            'name' => 'Home',
            'description' => 'Home',
            'type' => 'Home',
            'duration_in_seconds' => 11,
            'order' => 4
        ]);

        DB::table('__exercise_sets')->insertGetId([
            'object_id' => 3,
            'language_id' => 2,
            'training_id' => 6,
            'name' => 'Home',
            'description' => 'Home',
            'type' => 'Home',
            'duration_in_seconds' => 11,
            'order' => 4
        ]);
    }
}
