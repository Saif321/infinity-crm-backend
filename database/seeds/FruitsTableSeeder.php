<?php

use Illuminate\Database\Seeder;

class FruitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('fruits')->insertGetId([
            'name' => 'Apple',
            'image' => 'images/nutrition-icons/ic_apple.png',
            'order' => '1',
        ]);
        DB::table('__fruits')->insertGetId([
            'object_id' => 1,
            'language_id' => 1,
            'name' => 'Apple',
            'image' => 'images/nutrition-icons/ic_apple.png',
            'order' => '1',
        ]);
        DB::table('__fruits')->insertGetId([
            'object_id' => 1,
            'language_id' => 2,
            'name' => 'Jabuka',
            'image' => 'images/nutrition-icons/ic_apple.png',
            'order' => '1',
        ]);

        DB::table('fruits')->insertGetId([
            'name' => 'Banana',
            'image' => 'images/nutrition-icons/ic_banana.png',
            'order' => '2',
        ]);
        DB::table('__fruits')->insertGetId([
            'object_id' => 2,
            'language_id' => 1,
            'name' => 'Banana',
            'image' => 'images/nutrition-icons/ic_banana.png',
            'order' => '2',
        ]);
        DB::table('__fruits')->insertGetId([
            'object_id' => 2,
            'language_id' => 2,
            'name' => 'Banana',
            'image' => 'images/nutrition-icons/ic_banana.png',
            'order' => '2',
        ]);

        DB::table('fruits')->insertGetId([
            'name' => 'Grape',
            'image' => 'images/nutrition-icons/ic_grape.png',
            'order' => '3',
        ]);
        DB::table('__fruits')->insertGetId([
            'object_id' => 3,
            'language_id' => 1,
            'name' => 'Grape',
            'image' => 'images/nutrition-icons/ic_grape.png',
            'order' => '3',
        ]);
        DB::table('__fruits')->insertGetId([
            'object_id' => 3,
            'language_id' => 2,
            'name' => 'Grožđe',
            'image' => 'images/nutrition-icons/ic_grape.png',
            'order' => '3',
        ]);

        DB::table('fruits')->insertGetId([
            'name' => 'Strawberry',
            'image' => 'images/nutrition-icons/ic_strawberry.png',
            'order' => '4',
        ]);
        DB::table('__fruits')->insertGetId([
            'object_id' => 4,
            'language_id' => 1,
            'name' => 'Strawberry',
            'image' => 'images/nutrition-icons/ic_strawberry.png',
            'order' => '4',
        ]);
        DB::table('__fruits')->insertGetId([
            'object_id' => 4,
            'language_id' => 2,
            'name' => 'Jagoda',
            'image' => 'images/nutrition-icons/ic_strawberry.png',
            'order' => '4',
        ]);
        DB::table('fruits')->insertGetId([
            'name' => 'Watermelon',
            'image' => 'images/nutrition-icons/ic_watermelon.png',
            'order' => '5',
        ]);
        DB::table('__fruits')->insertGetId([
            'object_id' => 5,
            'language_id' => 1,
            'name' => 'Watermelon',
            'image' => 'images/nutrition-icons/ic_watermelon.png',
            'order' => '5',
        ]);
        DB::table('__fruits')->insertGetId([
            'object_id' => 5,
            'language_id' => 2,
            'name' => 'Lubenica',
            'image' => 'images/nutrition-icons/ic_watermelon.png',
            'order' => '5',
        ]);
        DB::table('fruits')->insertGetId([
            'name' => 'No fruit',
            'image' => 'images/nutrition-icons/ic_no_fruits.png',
            'order' => '6',
        ]);
        DB::table('__fruits')->insertGetId([
            'object_id' => 3,
            'language_id' => 1,
            'name' => 'No fruit',
            'image' => 'images/nutrition-icons/ic_no_fruits.png',
            'order' => '6',
        ]);
        DB::table('__fruits')->insertGetId([
            'object_id' => 3,
            'language_id' => 2,
            'name' => 'Ne jedem voće',
            'image' => 'images/nutrition-icons/ic_no_fruits.png',
            'order' => '6',
        ]);

    }
}
