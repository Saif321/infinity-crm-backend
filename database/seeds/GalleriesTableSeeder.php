<?php

use Illuminate\Database\Seeder;

class GalleriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('galleries')->insertGetId([
            'name' => 'Test image 1',
            'mime_type' => 'image/jpeg',
            'extension' => 'jpeg',
            'size' => '18.49 KB',
            'url' => 'http://fit-api.telego.rs/storage/5/0/8/508d01f4ca41e65fd6817f5261c492d8.jpeg',
            'is_user_avatar' => false,
            'type' => 'image',
        ]);

        DB::table('galleries')->insertGetId([
            'name' => 'Test video 1',
            'mime_type' => 'video/mp4',
            'extension' => 'mp4',
            'size' => '2.01 MB',
            'url' => 'http://fit-api.telego.rs/storage/c/0/5/c050568d26ece0a0f22f6ad69113d9c3.mp4',
            'is_user_avatar' => false,
            'type' => 'video',
        ]);

        $videos = [70, 292, 151, 407, 150, 415, 72, 409, 414, 291, 251, 252, 413, 406, 115, 152, 117, 116, 71, 408, 416, 293, 289, 290, 250];
        foreach($videos as $video) {
            DB::table('galleries')->insertGetId([
                'name' => 'Test video ' . $video,
                'mime_type' => 'video/mp4',
                'extension' => 'mp4',
                'size' => '2.01 MB',
                'url' => 'http://fit-api.telego.rs/storage/videos/' . $video . '.mp4',
                'is_user_avatar' => false,
                'type' => 'video',
            ]);
        }
    }
}
