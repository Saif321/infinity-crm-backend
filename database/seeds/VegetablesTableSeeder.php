<?php

use Illuminate\Database\Seeder;

class VegetablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('vegetables')->insertGetId([
            'name' => 'Tomato',
            'image' => 'images/nutrition-icons/ic_tomato.png',
            'order' => '1',
        ]);
        DB::table('__vegetables')->insertGetId([
            'object_id' => 1,
            'language_id' => 1,
            'name' => 'Tomato',
            'image' => 'images/nutrition-icons/ic_tomato.png',
            'order' => '1',
        ]);
        DB::table('__vegetables')->insertGetId([
            'object_id' => 1,
            'language_id' => 2,
            'name' => 'Paradajz',
            'image' => 'images/nutrition-icons/ic_tomato.png',
            'order' => '1',
        ]);

        DB::table('vegetables')->insertGetId([
            'name' => 'Rice',
            'image' => 'images/nutrition-icons/ic_rice.png',
            'order' => '2',
        ]);
        DB::table('__vegetables')->insertGetId([
            'object_id' => 2,
            'language_id' => 1,
            'name' => 'Rice',
            'image' => 'images/nutrition-icons/ic_rice.png',
            'order' => '2',
        ]);
        DB::table('__vegetables')->insertGetId([
            'object_id' => 2,
            'language_id' => 2,
            'name' => 'Pirinač',
            'image' => 'images/nutrition-icons/ic_rice.png',
            'order' => '2',
        ]);

        DB::table('vegetables')->insertGetId([
            'name' => 'Potato',
            'image' => 'images/nutrition-icons/ic_potato.png',
            'order' => '3',
        ]);
        DB::table('__vegetables')->insertGetId([
            'object_id' => 3,
            'language_id' => 1,
            'name' => 'Potato',
            'image' => 'images/nutrition-icons/ic_potato.png',
            'order' => '3',
        ]);
        DB::table('__vegetables')->insertGetId([
            'object_id' => 3,
            'language_id' => 2,
            'name' => 'Krompir',
            'image' => 'images/nutrition-icons/ic_potato.png',
            'order' => '3',
        ]);

        DB::table('vegetables')->insertGetId([
            'name' => 'Pepper',
            'image' => 'images/nutrition-icons/ic_pepper.png',
            'order' => '4',
        ]);
        DB::table('__vegetables')->insertGetId([
            'object_id' => 4,
            'language_id' => 1,
            'name' => 'Pepper',
            'image' => 'images/nutrition-icons/ic_pepper.png',
            'order' => '4',
        ]);
        DB::table('__vegetables')->insertGetId([
            'object_id' => 4,
            'language_id' => 2,
            'name' => 'Paprika',
            'image' => 'images/nutrition-icons/ic_pepper.png',
            'order' => '4',
        ]);
        DB::table('vegetables')->insertGetId([
            'name' => 'Eggplant',
            'image' => 'images/nutrition-icons/ic_eggplant.png',
            'order' => '5',
        ]);
        DB::table('__vegetables')->insertGetId([
            'object_id' => 5,
            'language_id' => 1,
            'name' => 'Eggplant',
            'image' => 'images/nutrition-icons/ic_eggplant.png',
            'order' => '5',
        ]);
        DB::table('__vegetables')->insertGetId([
            'object_id' => 5,
            'language_id' => 2,
            'name' => 'Plavi patlidzan',
            'image' => 'images/nutrition-icons/ic_eggplant.png',
            'order' => '5',
        ]);
        DB::table('vegetables')->insertGetId([
            'name' => 'No Vegetables',
            'image' => 'images/nutrition-icons/ic_no_veggies.png',
            'order' => '6',
        ]);
        DB::table('__vegetables')->insertGetId([
            'object_id' => 6,
            'language_id' => 1,
            'name' => 'No Vegetables',
            'image' => 'images/nutrition-icons/ic_no_veggies.png',
            'order' => '6',
        ]);
        DB::table('__vegetables')->insertGetId([
            'object_id' => 6,
            'language_id' => 2,
            'name' => 'Ne jedem povrće',
            'image' => 'images/nutrition-icons/ic_no_veggies.png',
            'order' => '6',
        ]);


    }
}
