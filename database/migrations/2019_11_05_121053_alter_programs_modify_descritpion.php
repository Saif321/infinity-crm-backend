<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProgramsModifyDescritpion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement( 'ALTER TABLE `programs` MODIFY `description` text DEFAULT NULL;' );
        DB::statement( 'ALTER TABLE `__programs` MODIFY `description` text DEFAULT NULL;' );
        DB::statement( 'ALTER TABLE `user_programs` MODIFY `description` text DEFAULT NULL;' );
        DB::statement( 'ALTER TABLE `__user_programs` MODIFY `description` text DEFAULT NULL;' );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
