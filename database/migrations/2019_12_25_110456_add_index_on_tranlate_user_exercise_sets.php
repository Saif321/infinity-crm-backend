<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexOnTranlateUserExerciseSets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('__user_exercise_sets', function (Blueprint $table) {
            $table->index(['language_id', 'user_training_id'], 'idx_user_training_id_language_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('__user_exercise_sets', function (Blueprint $table) {
            //
        });
    }
}
