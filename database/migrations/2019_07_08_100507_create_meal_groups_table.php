<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealGroupsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'meal_groups', function ( Blueprint $table ) {
			$table->bigIncrements( 'id' );
			$table->string( 'name' );
			$table->string( 'eating_time' );
			$table->timestamps();
		} );

		Schema::create( '__meal_groups', function ( Blueprint $table ) {
			$table->bigIncrements( 'id' );
			$table->integer( 'object_id' );
			$table->integer( 'language_id' );
			$table->string( 'name' );
			$table->string( 'eating_time' );
			$table->timestamps();
			$table->index( [ 'object_id', 'language_id' ] );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'meal_groups' );
		Schema::dropIfExists( '__meal_groups' );
	}
}
