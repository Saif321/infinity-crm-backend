<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNutritionProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nutrition_programs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description');
            $table->integer('video_id')->default(0);
            $table->integer('image_id')->default(0);
            $table->integer('program_length')->default(0); // in weeks
            $table->integer('meals_per_day')->default(0);
            $table->bigInteger('comment_count')->default(0);
            $table->bigInteger('like_count')->default(0);
            $table->bigInteger('review_count')->default(0);
            $table->bigInteger('avg_rating')->nullable();
	        $table->boolean('is_published')->default(false);
	        $table->boolean('is_free')->default(false);
            $table->timestamps();
        });
        Schema::create('__nutrition_programs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('object_id');
            $table->integer('language_id');
            $table->string('name');
            $table->text('description');
            $table->integer('video_id')->default(0);
            $table->integer('image_id')->default(0);
            $table->integer('program_length')->default(0); // in weeks
            $table->integer('meals_per_day')->default(0);
            $table->bigInteger('comment_count')->default(0);
            $table->bigInteger('like_count')->default(0);
            $table->bigInteger('review_count')->default(0);
            $table->bigInteger('avg_rating')->nullable();
            $table->boolean('is_published')->default(false);
	        $table->boolean('is_free')->default(false);
            $table->timestamps();
            $table->index(['object_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nutrition_programs');
        Schema::dropIfExists('__nutrition_programs');
    }
}
