<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('video_id')->default(0);
            $table->integer('image_id')->default(0);
            $table->string('name');
            $table->text('description');
            $table->integer('preparation_time_in_minutes');
            $table->integer('number_of_calories');
            $table->integer('fat');
            $table->integer('proteins');
            $table->integer('carbohydrates');
            $table->boolean('is_published')->default(false);
            $table->timestamps();
        });

        Schema::create('__meals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('object_id');
            $table->integer('language_id');
            $table->integer('video_id')->default(0);
            $table->integer('image_id')->default(0);
            $table->string('name');
            $table->text('description');
            $table->integer('preparation_time_in_minutes');
            $table->integer('number_of_calories');
            $table->integer('fat');
            $table->integer('proteins');
            $table->integer('carbohydrates');
            $table->boolean('is_published')->default(false);
            $table->timestamps();
            $table->index(['object_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meals');
        Schema::dropIfExists('__meals');
    }
}
