<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleriesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'galleries', function ( Blueprint $table ) {
			$table->bigIncrements( 'id' );
			$table->string( 'name' );
			$table->string( 'mime_type' )->nullable();
			$table->string( 'extension' )->nullable();
			$table->string( 'size' )->nullable();
			$table->string( 'url' );
			$table->boolean( 'is_user_avatar' )->default( false );
			$table->enum( 'type', [ 'image', 'video' ] );
			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'galleries' );
	}
}
