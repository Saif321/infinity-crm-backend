<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTablePaymentsAddGooglePurchaceTokenColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->string('google_purchase_token')->default('');
            $table->string('google_product_id')->default('');
        });

        Schema::table('user_packages', function (Blueprint $table) {
            $table->decimal('charged_value')->default(0);
            $table->string('currency_code')->default('');
            $table->boolean('renew_stopped')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('google_purchase_token');
            $table->dropColumn('google_product_id');
        });
        Schema::table('user_packages', function (Blueprint $table) {
            $table->dropColumn('charged_value');
            $table->dropColumn('currency_code');
            $table->dropColumn('renew_stopped');
        });
    }
}
