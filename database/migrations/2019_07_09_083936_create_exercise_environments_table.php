<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExerciseEnvironmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercise_environments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('order');
            $table->timestamps();
        });

        Schema::create('__exercise_environments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('object_id');
            $table->integer('language_id');
            $table->string('name');
            $table->integer('order');
            $table->timestamps();
            $table->index(['object_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercise_environments');
        Schema::dropIfExists('__exercise_environments');
    }
}
