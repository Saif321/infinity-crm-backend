<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserExerciseSetItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // visak
        /*
        Schema::create('user_exercise_set_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('round_id');
            $table->enum('type',['pause','exercise']);
            $table->tinyInteger('duration_in_seconds');
            $table->tinyInteger('strength_percentage');
            $table->tinyInteger('number_of_reps');
            $table->integer('exercise_id');
            $table->tinyInteger('weight_in_kilograms');
            $table->boolean('is_finished');
            $table->date('finished_at');
            $table->integer('order');
            $table->timestamps();
        });

        Schema::create('__user_exercise_set_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('object_id');
            $table->integer('language_id');
            $table->integer('round_id');
            $table->enum('type',['pause','exercise']);
            $table->tinyInteger('duration_in_seconds');
            $table->tinyInteger('strength_percentage');
            $table->tinyInteger('number_of_reps');
            $table->integer('exercise_id');
            $table->tinyInteger('weight_in_kilograms');
            $table->boolean('is_finished');
            $table->date('finished_at');
            $table->integer('order');
            $table->timestamps();
            $table->index(['object_id', 'language_id']);
        });
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_exercise_set_items');
        Schema::dropIfExists('__user_exercise_set_items');
    }
}
