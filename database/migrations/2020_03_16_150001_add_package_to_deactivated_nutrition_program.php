<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPackageToDeactivatedNutritionProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deactivated_nutrition_programs', function (Blueprint $table) {
            $table->unsignedBigInteger('user_package_id')->nullable();
            $table->foreign('user_package_id', 'fk_deactivated_nutrition_programs_package_id')->references('id')->on('user_packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deactivated_nutrition_programs', function (Blueprint $table) {
            $table->dropForeign('fk_deactivated_nutrition_programs_package_id');
            $table->dropColumn('user_package_id');
        });
    }
}
