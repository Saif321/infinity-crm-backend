<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNutritionQuestionnaireVegetableItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nutrition_questionnaire_vegetable_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('nutrition_questionnaire_id');
            $table->integer('vegetable_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nutrition_questionnaire_vegetable_items');
    }
}
