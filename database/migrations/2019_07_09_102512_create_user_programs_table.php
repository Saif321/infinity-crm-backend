<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_programs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('level_id');
            $table->integer('exercise_environment_id');
            $table->string('name');
            $table->string('description');
            $table->integer('workout_days_per_week')->default(0);
            $table->integer('program_length_in_weeks')->default(0);
            $table->integer('video_id');
            $table->integer('image_id');
            $table->bigInteger('comment_count')->default(0);
            $table->bigInteger('like_count')->default(0);
            $table->bigInteger('review_count')->default(0);
            $table->bigInteger('avg_rating')->nullable();
            $table->boolean('is_published')->default(false);
            $table->timestamps();
        });
        Schema::create('__user_programs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('object_id');
            $table->integer('language_id');
            $table->integer('level_id');
            $table->integer('exercise_environment_id');
            $table->string('name');
            $table->string('description');
            $table->integer('workout_days_per_week');
            $table->integer('program_length_in_weeks')->default(0);
            $table->integer('video_id');
            $table->integer('image_id');
            $table->bigInteger('comment_count')->default(0);
            $table->bigInteger('like_count')->default(0);
            $table->bigInteger('review_count')->default(0);
            $table->bigInteger('avg_rating')->nullable();
            $table->boolean('is_published')->default(false);
            $table->timestamps();
            $table->index(['object_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_programs');
        Schema::dropIfExists('__user_programs');
    }
}
