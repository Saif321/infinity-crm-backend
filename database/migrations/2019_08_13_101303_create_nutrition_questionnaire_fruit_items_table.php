<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNutritionQuestionnaireFruitItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nutrition_questionnaire_fruit_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('nutrition_questionnaire_id');
            $table->integer('fruit_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nutrition_questionnaire_fruit_items');
    }
}
