<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexExerciseEnvironmentItems extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table( 'exercise_environment_items', function ( Blueprint $table ) {
			$table->index( 'exercise_id', 'idx_exercise_id' );
			$table->index( 'exercise_environment_id', 'idx_exercise_environment_id' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table( 'exercise_environment_items', function ( Blueprint $table ) {
			//
		} );
	}
}
