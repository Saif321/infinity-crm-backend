<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVegetablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vegetables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('image');
            $table->integer('order')->default(0);
            $table->timestamps();
        });
        Schema::create('__vegetables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('object_id');
            $table->integer('language_id');
            $table->string('name');
            $table->string('image');
            $table->integer('order')->default(0);
            $table->timestamps();
            $table->index(['object_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vegetables');
        Schema::dropIfExists('__vegetables');
    }
}
