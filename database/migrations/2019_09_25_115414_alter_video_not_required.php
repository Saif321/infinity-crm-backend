<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVideoNotRequired extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement('ALTER TABLE `exercises` MODIFY `video_id` int(11) NULL;');
        DB::statement('ALTER TABLE `__exercises` MODIFY `video_id` int(11) NULL;');

        DB::statement('ALTER TABLE `user_exercises` MODIFY `video_id` int(11) NULL;');
        DB::statement('ALTER TABLE `__user_exercises` MODIFY `video_id` int(11) NULL;');

        DB::statement('ALTER TABLE `user_programs` MODIFY `video_id` int(11) NULL;');
        DB::statement('ALTER TABLE `__user_programs` MODIFY `video_id` int(11) NULL;');

        DB::statement('ALTER TABLE `nutrition_programs` MODIFY `video_id` int(11) NULL;');
        DB::statement('ALTER TABLE `__nutrition_programs` MODIFY `video_id` int(11) NULL;');

        DB::statement('ALTER TABLE `meals` MODIFY `video_id` int(11) NULL;');
        DB::statement('ALTER TABLE `__meals` MODIFY `video_id` int(11) NULL;');

        DB::statement('ALTER TABLE `programs` MODIFY `video_id` int(11) NULL;');
        DB::statement('ALTER TABLE `__programs` MODIFY `video_id` int(11) NULL;');

        DB::statement('ALTER TABLE `programs` MODIFY `video_id` int(11) NULL;');
        DB::statement('ALTER TABLE `__programs` MODIFY `video_id` int(11) NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
