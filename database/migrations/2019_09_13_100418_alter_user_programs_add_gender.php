<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserProgramsAddGender extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('user_programs', function (Blueprint $table) {
            $table->enum('gender', ['male','female'])->nullable();
        });

        Schema::table('__user_programs', function (Blueprint $table) {
            $table->enum('gender', ['male','female'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
