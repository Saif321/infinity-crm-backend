<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTrainingsAddParentId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('trainings', function (Blueprint $table) {
            $table->integer('parent_id')->nullable();
        });

        Schema::table('__trainings', function (Blueprint $table) {
            $table->integer('parent_id')->nullable();
        });

        Schema::table('user_trainings', function (Blueprint $table) {
            $table->integer('parent_id')->nullable();
        });

        Schema::table('__user_trainings', function (Blueprint $table) {
            $table->integer('parent_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
