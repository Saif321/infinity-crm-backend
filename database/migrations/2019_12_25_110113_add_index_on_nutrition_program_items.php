<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexOnNutritionProgramItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nutrition_program_items', function (Blueprint $table) {
            $table->index(['nutrition_program_meal_group_id', 'order'], 'idx_nutrition_program_meal_group_id_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nutrition_program_items', function (Blueprint $table) {
            //
        });
    }
}
