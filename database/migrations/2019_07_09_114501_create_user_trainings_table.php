<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTrainingsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'user_trainings', function ( Blueprint $table ) {
			$table->bigIncrements( 'id' );
			$table->string( 'name' );
			$table->text( 'description' );
			$table->integer( 'number_of_calories' );
			$table->timestamps();
		} );

		Schema::create( '__user_trainings', function ( Blueprint $table ) {
			$table->bigIncrements( 'id' );
			$table->integer( 'object_id' );
			$table->integer( 'language_id' );
			$table->string( 'name' );
			$table->text( 'description' );
			$table->integer( 'number_of_calories' );
			$table->timestamps();
			$table->index( [ 'object_id', 'language_id' ] );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'user_trainings' );
		Schema::dropIfExists( '__user_trainings' );
	}
}
