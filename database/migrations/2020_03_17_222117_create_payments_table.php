<?php

use App\Models\Payment;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->enum('method', [
                Payment::METHOD_SMS,
                Payment::METHOD_APPLE_PAY,
                Payment::METHOD_GOOGLE_PAY,
                Payment::METHOD_CREDIT_CARD,
            ]);
            $table->string('transaction_id');
            $table->enum('status', [
                Payment::STATUS_FAILED,
                Payment::STATUS_PROCESSING,
                Payment::STATUS_SUCCESS,
                Payment::STATUS_VOID,
            ]);
            $table->json('details')->nullable();
            $table->unsignedBigInteger('user_package_id')->nullable();
            $table->timestamps();

            $table->foreign('user_package_id')->references('id')->on('user_packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
