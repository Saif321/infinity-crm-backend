<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExerciseRoundItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercise_round_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('round_id');
            $table->enum('type',['pause','exercise']);
            $table->tinyInteger('duration_in_seconds');
            $table->tinyInteger('strength_percentage');
            $table->tinyInteger('number_of_reps');
            $table->integer('exercise_id');
            $table->integer('order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercise_round_items');
    }
}
