<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserExerciseRoundItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('user_exercise_round_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_round_id');
            $table->integer('exercise_id');
            $table->enum('type',['pause','exercise']);
            $table->tinyInteger('duration_in_seconds');
            $table->tinyInteger('strength_percentage');
            $table->tinyInteger('number_of_reps');
            $table->boolean('is_finished');
            $table->dateTime('started_at');
            $table->dateTime('finished_at');
            $table->integer('order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('user_exercise_round_items');
    }
}
