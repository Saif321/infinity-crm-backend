<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTrainingsDescriptionNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement('ALTER TABLE `trainings` MODIFY `description` text NULL;');
        DB::statement('ALTER TABLE `__trainings` MODIFY `description` text NULL;');

        DB::statement('ALTER TABLE `user_trainings` MODIFY `description` text NULL;');
        DB::statement('ALTER TABLE `__user_trainings` MODIFY `description` text NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
