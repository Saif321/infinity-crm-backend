<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProgramWeekItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_program_week_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_program_week_id');
            $table->integer('user_training_id')->nullable();
            $table->boolean('is_locked')->default(true);
            $table->boolean('is_rest_day')->default(false);
            $table->integer('order')->default(0);
            $table->dateTime('started_at')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_program_week_items');
    }
}
