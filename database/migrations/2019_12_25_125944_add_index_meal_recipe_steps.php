<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexMealRecipeSteps extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table( '__meal_recipe_steps', function ( Blueprint $table ) {
			$table->index( [ 'language_id', 'meal_id' ], 'idx_meal_id_language_id' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table( '__meal_recipe_steps', function ( Blueprint $table ) {
			//
		} );
	}
}
