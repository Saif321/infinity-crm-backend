<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexOnGoalProgramItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('goal_program_items', function (Blueprint $table) {
            $table->index('goal_id', 'idx_goal_id');
            $table->index('program_id', 'idx_program_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goal_program_items', function (Blueprint $table) {
            //
        });
    }
}
