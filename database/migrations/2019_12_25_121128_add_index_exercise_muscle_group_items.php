<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexExerciseMuscleGroupItems extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table( 'exercise_muscle_group_items', function ( Blueprint $table ) {
			$table->index( 'exercise_id', 'idx_exercise_id' );
			$table->index( 'muscle_group_id', 'idx_muscle_group_id' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table( 'exercise_muscle_group_items', function ( Blueprint $table ) {
			//
		} );
	}
}
