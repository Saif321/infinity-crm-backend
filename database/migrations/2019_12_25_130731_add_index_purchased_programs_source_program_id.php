<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexPurchasedProgramsSourceProgramId extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table( 'purchased_programs', function ( Blueprint $table ) {
			$table->index( 'source_program_id', 'idx_source_program_id' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table( 'purchased_programs', function ( Blueprint $table ) {
			//
		} );
	}
}
