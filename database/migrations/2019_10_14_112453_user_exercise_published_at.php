<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserExercisePublishedAt extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		DB::statement( 'ALTER TABLE `user_exercises` MODIFY `published_at` timestamp NULL DEFAULT NULL;' );
		DB::statement( 'ALTER TABLE `__user_exercises` MODIFY `published_at` timestamp NULL DEFAULT NULL;' );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		//
	}
}
