<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPriceFieldsToPackagePaymentMethods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_payment_methods', function (Blueprint $table) {
            $table->decimal('price')->default(0);
            $table->string('currency_code')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_payment_methods', function (Blueprint $table) {
            $table->dropColumn('currency_code');
            $table->dropColumn('price');
        });
    }
}
