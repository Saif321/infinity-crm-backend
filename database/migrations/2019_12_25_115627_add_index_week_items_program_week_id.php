<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexWeekItemsProgramWeekId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('week_items', function (Blueprint $table) {
            $table->index(['program_week_id', 'training_id'], 'idx_program_week_id_training_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('week_items', function (Blueprint $table) {
            //
        });
    }
}
