<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_exercises', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('video_id')->default(0);
            $table->integer('image_id')->default(0);
            $table->integer('level_id')->default(0);

            $table->string('name');
            $table->text('description');

            $table->boolean('is_published')->default(false);
            $table->boolean('is_free')->default(false);

            $table->dateTime('published_at')->nullable();

            $table->timestamps();
        });

        Schema::create('__user_exercises', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('object_id');
            $table->integer('language_id');

            $table->integer('video_id')->default(0);
            $table->integer('image_id')->default(0);
            $table->integer('level_id')->default(0);

            $table->string('name');
            $table->text('description');

            $table->boolean('is_published')->default(false);
            $table->boolean('is_free')->default(false);

            $table->dateTime('published_at')->nullable();

            $table->timestamps();
            $table->index(['object_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_exercises');
        Schema::dropIfExists('__user_exercises');
    }
}
