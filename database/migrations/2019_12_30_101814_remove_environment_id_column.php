<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveEnvironmentIdColumn extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table( 'programs', function ( Blueprint $table ) {
			$table->dropColumn( 'exercise_environment_id' );
		} );

		Schema::table( '__programs', function ( Blueprint $table ) {
			$table->dropColumn( 'exercise_environment_id' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table( 'programs', function ( Blueprint $table ) {
			$table->integer( 'exercise_environment_id' );
		} );

		Schema::table( '__programs', function ( Blueprint $table ) {
			$table->integer( 'exercise_environment_id' );
		} );
	}
}
