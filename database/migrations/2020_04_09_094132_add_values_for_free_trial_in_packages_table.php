<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddValuesForFreeTrialInPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('packages')->update([
            'number_of_free_trial_programs' => 2,
            'number_of_free_trial_nutrition_programs' => 2
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages', function (Blueprint $table) {
            DB::table('packages')->update([
                'number_of_free_trial_programs' => 0,
                'number_of_free_trial_nutrition_programs' => 0
            ]);
        });
    }
}
