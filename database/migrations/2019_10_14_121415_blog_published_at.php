<?php

use Illuminate\Database\Migrations\Migration;

class BlogPublishedAt extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		DB::statement( 'ALTER TABLE `blogs` MODIFY `published_at` timestamp NULL DEFAULT NULL;' );
		DB::statement( 'ALTER TABLE `__blogs` MODIFY `published_at` timestamp NULL DEFAULT NULL;' );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		//
	}
}
