<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('image_id')->nullable();
            $table->string('title');
            $table->longText('description');
            $table->bigInteger('comment_count')->default(0);
            $table->bigInteger('like_count')->default(0);
            $table->bigInteger('review_count')->default(0);
            $table->boolean('is_published')->default(false);
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
        });

        Schema::create('__blogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('object_id');
            $table->integer('language_id');
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('image_id')->nullable();
            $table->string('title');
            $table->longText('description');
            $table->bigInteger('comment_count')->default(0);
            $table->bigInteger('like_count')->default(0);
            $table->bigInteger('review_count')->default(0);
            $table->boolean('is_published')->default(false);
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
            $table->index(['object_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
        Schema::dropIfExists('__blogs');
    }
}
