<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;

class AlterAvgRatingDefaults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `nutrition_programs` CHANGE COLUMN `avg_rating` `avg_rating` bigint(20) NOT NULL DEFAULT 0;');
        DB::statement('ALTER TABLE `__nutrition_programs` CHANGE COLUMN `avg_rating` `avg_rating` bigint(20) NOT NULL DEFAULT 0;');
        DB::statement('ALTER TABLE `programs` CHANGE COLUMN `avg_rating` `avg_rating` bigint(20) NOT NULL DEFAULT 0;');
        DB::statement('ALTER TABLE `__programs` CHANGE COLUMN `avg_rating` `avg_rating` bigint(20) NOT NULL DEFAULT 0;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
