<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealIngredientsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'meal_ingredients', function ( Blueprint $table ) {
			$table->bigIncrements( 'id' );
			$table->integer( 'meal_id' );
			$table->string( 'name' );
			$table->timestamps();
		} );

		Schema::create( '__meal_ingredients', function ( Blueprint $table ) {
			$table->bigIncrements( 'id' );
			$table->integer( 'object_id' )->default( 0 );
			$table->integer( 'language_id' )->default( 0 );
			$table->integer( 'meal_id' );
			$table->string( 'name' );
			$table->timestamps();
			$table->index( [ 'object_id', 'language_id' ] );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'meal_ingredients' );
		Schema::dropIfExists( '__meal_ingredients' );
	}
}
