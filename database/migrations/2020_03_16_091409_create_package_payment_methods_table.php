<?php

use App\Models\PackagePaymentMethod;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagePaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_payment_methods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('package_id');
            $table->enum('payment_method', [
                PackagePaymentMethod::PAYMENT_METHOD_SMS,
                PackagePaymentMethod::PAYMENT_METHOD_APPLE_PAY,
                PackagePaymentMethod::PAYMENT_METHOD_GOOGLE_PAY,
                PackagePaymentMethod::PAYMENT_METHOD_CREDIT_CARD,
            ]);
            $table->timestamps();
            $table->foreign('package_id')->references('id')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_payment_methods');
    }
}
