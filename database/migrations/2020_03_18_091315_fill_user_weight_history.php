<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class FillUserWeightHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement( 'INSERT INTO user_weight_history (user_id, weight_in_kilograms, created_at, updated_at) SELECT user_id, weight_in_kilograms, created_at, created_at FROM basic_questionnaires WHERE weight_in_kilograms IS NOT NULL AND (user_id, weight_in_kilograms, created_at) NOT IN (SELECT user_id, weight_in_kilograms, created_at FROM user_weight_history);' );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
