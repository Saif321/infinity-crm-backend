<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExerciseRoundIdemAlterDuration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement('ALTER TABLE `exercise_round_items` MODIFY `duration_in_seconds` int(11) NOT NULL;');
        DB::statement('ALTER TABLE `user_exercise_round_items` MODIFY `duration_in_seconds` int(11) NOT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
