<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExerciseSetsDescriptionNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement('ALTER TABLE `exercise_sets` MODIFY `description` TEXT NULL;');
        DB::statement('ALTER TABLE `__exercise_sets` MODIFY `description` TEXT NULL;');
        DB::statement('ALTER TABLE `user_exercise_sets` MODIFY `description` TEXT NULL;');
        DB::statement('ALTER TABLE `__user_exercise_sets` MODIFY `description` TEXT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
