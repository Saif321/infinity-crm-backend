<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserExerciseSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_exercise_sets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_training_id');
            $table->string('name');
            $table->text('description');
            $table->enum('type',['round','time'])->default('round');
            $table->integer('duration_in_seconds');
            $table->integer('order');
            $table->timestamps();
        });

        Schema::create('__user_exercise_sets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('object_id');
            $table->integer('language_id');
            $table->integer('user_training_id');
            $table->string('name');
            $table->text('description');
            $table->enum('type',['round','time'])->default('round');
            $table->integer('duration_in_seconds');
            $table->integer('order');
            $table->timestamps();
            $table->index(['object_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_exercise_sets');
        Schema::dropIfExists('__user_exercise_sets');
    }
}
