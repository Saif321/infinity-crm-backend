<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeekItemsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'week_items', function ( Blueprint $table ) {
			$table->bigIncrements( 'id' );
			$table->integer( 'program_week_id' );
			$table->integer( 'training_id' )->nullable();
			$table->boolean( 'is_locked' )->default( true );
			$table->boolean( 'is_rest_day' )->default( false );
			$table->integer( 'order' )->default( 0 );
			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'week_items' );
	}
}
