<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UserExerciseRoundItemsDatetimeToTimestamp extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		DB::statement( 'ALTER TABLE `user_exercise_round_items` 
							MODIFY `started_at` timestamp NULL DEFAULT NULL,
							MODIFY `finished_at` timestamp NULL DEFAULT NULL;' );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
	}
}
