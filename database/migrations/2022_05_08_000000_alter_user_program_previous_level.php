<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUserProgramPreviousLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('user_programs', function (Blueprint $table) {
            $table->integer('previous_level_program_id')->default(0);
        });
        Schema::table('__user_programs', function (Blueprint $table) {
            $table->integer('previous_level_program_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_programs', function (Blueprint $table) {
            $table->dropColumn('previous_level_program_id');
        });
        Schema::table('__user_programs', function (Blueprint $table) {
            $table->dropColumn('previous_level_program_id');
        });
    }
}
