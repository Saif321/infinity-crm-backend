<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class ExercisesPublishedAt extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		DB::statement( 'ALTER TABLE `exercises` MODIFY `published_at` timestamp NULL DEFAULT NULL;' );
		DB::statement( 'ALTER TABLE `__exercises` MODIFY `published_at` timestamp NULL DEFAULT NULL;' );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		//
	}
}
