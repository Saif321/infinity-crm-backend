<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoreIndexOnUserExerciseRoundItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up() {
 		Schema::table( 'user_exercise_round_items', function ( Blueprint $table ) {
 			$table->index( 'is_finished', 'idx_is_finished' );
 			$table->index( 'finished_at', 'idx_finished_at' );
 		} );
 	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'user_exercise_round_items', function ( Blueprint $table ) {
			//
		} );
    }
}
