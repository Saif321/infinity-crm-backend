<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnvironmentUserProgramItems extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'environment_user_program_items', function ( Blueprint $table ) {
			$table->bigIncrements( 'id' );
			$table->integer( 'environment_id' );
			$table->integer( 'user_program_id' );

			$table->index( [ 'user_program_id' ], 'idx_user_program_id' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'environment_user_program_items' );
	}
}
