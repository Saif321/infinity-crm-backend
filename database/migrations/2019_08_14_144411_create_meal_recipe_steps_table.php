<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealRecipeStepsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'meal_recipe_steps', function ( Blueprint $table ) {
			$table->bigIncrements( 'id' );
			$table->integer( 'meal_id' );
			$table->text( 'description' );
			$table->integer( 'order' );
			$table->timestamps();
		} );

		Schema::create( '__meal_recipe_steps', function ( Blueprint $table ) {
			$table->bigIncrements( 'id' );
			$table->integer( 'object_id' )->default( 0 );
			$table->integer( 'language_id' )->default( 0 );
			$table->integer( 'meal_id' );
			$table->text( 'description' );
			$table->integer( 'order' );
			$table->timestamps();
			$table->index( [ 'object_id', 'language_id' ] );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'meal_recipe_steps' );
		Schema::dropIfExists( '__meal_recipe_steps' );
	}
}
