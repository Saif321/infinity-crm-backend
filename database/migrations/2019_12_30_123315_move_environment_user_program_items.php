<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class MoveEnvironmentUserProgramItems extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		$data = DB::table( 'user_programs' )->select( [ 'id', 'exercise_environment_id' ] )->orderBy( 'id' );

		$data->each( function ( $data ) {
			DB::table( 'environment_user_program_items' )->insert( [
				'environment_id'  => $data->exercise_environment_id,
				'user_program_id' => $data->id
			] );
		}, 10 );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		DB::table( 'environment_user_program_items' )->truncate();
	}
}
