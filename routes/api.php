<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});



Route::namespace('Api')->group(function() {
    Route::group(['middleware' => ['localize']], function () {
        Route::prefix('v1.0')->group(function () {

            Route::post('/give_customer_role','AuthController@give_customer_role');

            Route::post('/oauth/token','Vendor\CustomAccessTokenController@issueUserToken');
            Route::post('resend-activation-email', 'AuthController@resendActivationEmail');

            Route::post('forgotten-password/send-otp', 'ForgottenPasswordController@sendOtp');
            Route::post('forgotten-password/check-otp', 'ForgottenPasswordController@checkOtp');
            Route::post('forgotten-password/change-password', 'ForgottenPasswordController@changePassword');
            Route::get('translate', 'TranslateController@translate');
            Route::get('language', 'LanguageController@index');
            Route::get('terms', 'TermsController@index');
            Route::get('sources', 'SourcesController@index');
            Route::get('privacy', 'PrivacyController@index');
            Route::post('user/register', 'UserController@register');
            Route::get('/basic-questionnaire-questions', 'BasicQuestionnaireQuestionsController@index');

            Route::group(['middleware' => ['throttle:300,1']], function () {
                Route::post('/apple/log', 'ApplePhoneLogController@log');
            });
            Route::group(['middleware' => ['role:customer|administrator']], function () {
                Route::post('user/photo', 'UserController@uploadPhoto');
                Route::post('user/photo_base64', 'UserController@uploadPhotoBase64');
                Route::post('upload_image_base64', 'UserController@uploadImageBase64');
                Route::post('logout', 'AuthController@log_out');
                Route::get('user', 'UserController@index');

                // Homee_Feed
                Route::post('create_home_feed', 'HomeFeedController@createHomeFeed');
                Route::delete('delete_home_feed/{id}', 'HomeFeedController@deleteHomeFeed');
                Route::get('explore_feed', 'HomeFeedController@getExploreHomeFeed');
                Route::get('home_feed', 'HomeFeedController@getHomeFeed');
                Route::get('home_feed/{id}', 'HomeFeedController@getHomeFeedById');
                Route::get('my_home_feed', 'HomeFeedController@getMyHomeFeed');

                Route::post('blockuser/{user_id}', 'UserController@blockuser');
                Route::post('unblockuser/{user_id}', 'UserController@unblockuser');
                Route::get('blockedusers', 'UserController@blockedusers');
            });
            Route::group(['middleware' => ['role:customer']], function () {

                Route::post('follow/{id}', 'FollowerController@follow');
                Route::post('unfollow/{id}', 'FollowerController@unfollow');

                Route::get('home', 'HomeController@index');
                Route::post('google/verify-payment', 'GooglePayController@token');
                Route::post('apple/verify-payment', 'ApplePayController@validateReceipt');

                Route::post('user/change-password', 'UserController@changePassword');
                Route::post('user/questionnaire', 'UserController@questionnaire');
                Route::post('user/nutrition', 'UserController@nutrition');
                Route::post('user/update-weight', 'UserController@updateWeight');
                Route::post('user', 'UserController@store');
                Route::resource('level', 'LevelController', ['only' => ['index']]);
                Route::put('program/purchased/{purchased}/reset', 'PurchasedProgramController@reset');
                Route::put('program/purchased/{purchased}/deactivate', 'PurchasedProgramController@destroy');
                Route::resource('program/purchased', 'PurchasedProgramController', ['only' => ['index', 'show']]);
                Route::post('program/{program}/buy', 'ProgramController@buy');
                Route::resource('program', 'ProgramController', ['only' => ['index', 'show']]);
                Route::resource('exercise-environment', 'ExerciseEnvironmentController', ['only' => ['index']]);
                Route::resource('blog', 'BlogController', ['only' => ['index', 'show']]);
                Route::resource('like', 'LikeController', ['only' => ['store']]);
                Route::resource('comment', 'CommentController', ['only' => ['index', 'store']]);
                Route::resource('review', 'ReviewController', ['only' => ['index', 'store']]);
                Route::resource('goal', 'GoalController', ['only' => ['index']]);
                Route::resource('muscle-group', 'MuscleGroupController', ['only' => ['index']]);
                Route::resource('exercise', 'ExerciseController', ['only' => ['index']]);
	            Route::get('exercise/{exercise_id}', 'ExerciseController@show');
	            Route::get('exercise/{exercise_id}/program/{program_id}', 'ExerciseController@showExerciseInProgram');
                Route::post('nutrition-program/{nutrition_program}/buy', 'NutritionProgramController@buy');
                Route::resource('nutrition-program/purchased', 'PurchasedNutritionProgramController', ['only' => ['index']]);
                Route::put('nutrition-program/purchased/{nutrition_program}/deactivate', 'PurchasedNutritionProgramController@deactivate');
                Route::resource('nutrition-program', 'NutritionProgramController', ['only' => ['index', 'show']]);
                Route::resource('meal', 'MealController', ['only' => ['index', 'show']]);
                Route::resource('meal-group', 'MealGroupController', ['only' => ['index']]);
                Route::resource('nutrition-questionnaire-questions', 'NutritionQuestionnaireQuestionController', ['only' => ['index']]);

	            Route::post('user-training/{user_training}/finish', 'UserTrainingController@finishCompleteTraining');
	            Route::post('user-training/{user_training}/reset', 'UserTrainingController@reset');
	            Route::post('user-training/{user_training}/finish/{user_exercise_round_item}', 'UserTrainingController@finish');
	            Route::post('user-training/{user_training}/set-user-data/{user_exercise_round_item}', 'UserTrainingController@setRoundItemUserData');
                Route::resource('user-training', 'UserTrainingController', ['only' => ['show']]);

                Route::resource('training', 'TrainingController', ['only' => ['show']]);

                Route::get('package/active', 'PackageController@showActivePackage');
                Route::put('package/{package_id}/unsubscribe', 'PackageController@unsubscribe');
                Route::resource('package', 'PackageController', ['only' => ['index']]);

                Route::get('activity/dates/{year}/{month}', 'ActivityController@dates');
                Route::get('activity/{date}', 'ActivityController@date');

            });

            /*
             * Admin routes
             */
            Route::namespace('Admin')->group(function () {
                Route::prefix('admin')->group(function () {

                    Route::group(['middleware' => ['role:administrator']], function () {
                        Route::get('reports/customers', 'ReportController@getCustomerReport');
                        Route::get('reports/user-packages', 'ReportController@getUserPackageReport');
                        Route::get('reports/payments', 'ReportController@getPaymentReport');
                        Route::get('reports/payment-history/{transaction_id}', 'ReportController@getPaymentHistory');

                        Route::put('blog/{blog}/publish', 'BlogController@publish');
                        Route::resource('blog', 'BlogController', ['only' => ['index', 'show', 'update', 'destroy']]);

                        Route::put('comment/{comment}/publish', 'CommentController@publish');
                        Route::resource('comment', 'CommentController', ['only' => ['index', 'update', 'destroy']]);

                        Route::get('exercise-environment/list', 'ExerciseEnvironmentController@fullList');
                        Route::resource('exercise-environment', 'ExerciseEnvironmentController', ['only' => ['index', 'show', 'update', 'destroy']]);

                        Route::get('muscle-group/list', 'MuscleGroupController@fullList');
                        Route::resource('muscle-group', 'MuscleGroupController', ['only' => ['index', 'show', 'update', 'destroy']]);

                        Route::put('review/{review}/publish', 'ReviewController@publish');
                        Route::resource('review', 'ReviewController', ['only' => ['index', 'show', 'update', 'destroy']]);

	                    Route::get('gallery/avatar', 'GalleryController@getAvatars');
	                    Route::get('gallery/upload-limits', 'GalleryController@uploadLimits');
                        Route::resource('gallery', 'GalleryController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);

                        Route::get('level/list', 'LevelController@fullList');
                        Route::resource('level', 'LevelController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);

                        Route::resource('goal', 'GoalController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);

	                    Route::put('exercise/{exercise}/publish', 'ExerciseController@publish');
                        Route::resource('exercise', 'ExerciseController', ['only' => ['index', 'show', 'update', 'destroy']]);

                        Route::resource('administrator', 'AdministratorController', ['only' => ['index', 'show', 'update', 'destroy']]);

                        Route::get('customer/{customer}/programs', 'CustomerController@programs');
                        Route::get('customer/{customer}/nutrition-programs', 'CustomerController@nutritionPrograms');

                        Route::resource('customer', 'CustomerController', ['only' => ['index', 'show', 'update', 'destroy']]);

                        Route::resource('meal', 'MealController', ['only' => ['index', 'show', 'update', 'destroy']]);

                        Route::resource('ingredient', 'IngredientController', ['only' => ['index', 'show', 'update', 'destroy']]);

                        Route::resource('recipe_step', 'RecipeStepController', ['only' => ['index', 'show', 'update', 'destroy']]);

                        Route::resource('meal-group', 'MealGroupController', ['only' => ['index', 'show', 'update', 'destroy']]);

                        Route::put('nutrition-program/{nutrition_program}/publish', 'NutritionProgramController@publish');
                        Route::get('nutrition-program/{nutrition_program}/users', 'NutritionProgramController@users');
                        Route::put('nutrition-program/{nutrition_program}/user/{user}', 'NutritionProgramController@buyForUser');
                        Route::resource('nutrition-program', 'NutritionProgramController', ['only' => ['index', 'show', 'update', 'destroy']]);

                        Route::resource('workout', 'WorkoutController', ['only' => ['index', 'show', 'update', 'destroy']]);

                        Route::put('program/{program}/publish', 'ProgramController@publish');
                        Route::get('program/{program}/user', 'ProgramController@user');
                        Route::put('program/{program}/user/{user}', 'ProgramController@buyForUser');
                        Route::resource('program', 'ProgramController', ['only' => ['index', 'show', 'update']]);
                        Route::get('allprograms', 'ProgramController@all_programs');

                        Route::post('package/customer', 'PackageController@assignToCustomer');

                        Route::post('notification', 'NotificationController@notifyUsers');
                    });
                });
            });
        });
    });
});
