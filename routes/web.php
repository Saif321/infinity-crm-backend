<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Responses\JsonResponse;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return response()->json(new JsonResponse(null, false, 401, 'Route is required'), 401);
    //return view('welcome');
});

Route::any('/payment/apple-receipt', 'AppleController@getData')->name('appleData');

Route::get('/confirm/{id}/{code}', 'HomeController@confirmation')->name('confirmation');

Route::get('/payment/first-data/test/{test_case}', 'FirstDataController@redirectTest')->name('firstDataRedirectTest');
Route::post('/payment/first-data/test/success', 'FirstDataController@successfulPaymentTest')->name('firstDataSuccessfulPaymentTest');
Route::post('/payment/first-data/test/failure', 'FirstDataController@failedPaymentTest')->name('firstDataFailedPaymentTest');

Route::get('/payment/first-data/{user_id}/{package_id}', 'FirstDataController@redirect')->name('firstDataRedirect');
Route::post('/payment/first-data/success', 'FirstDataController@successfulPayment')->name('firstDataSuccessfulPayment');
Route::post('/payment/first-data/failure', 'FirstDataController@failedPayment')->name('firstDataFailedPayment');

Route::get('/payment/sms/check', 'SmsPaymentController@checkAvailability');
Route::post('/payment/sms/success', 'SmsPaymentController@successfulPayment');
Route::post('/payment/sms/unsubscribe', 'SmsPaymentController@unsubscribe');

Route::get('/terms', 'HomeController@terms')->name('terms');
Route::get('/privacy', 'HomeController@privacy')->name('privacy');
