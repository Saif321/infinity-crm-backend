<?php

use ReceiptValidator\iTunes\Validator;

return [
    'use_packages' => env('INFINITY_FIT_USE_PACKAGES', true),
    'access_token_duration' => env('ACCESS_TOKEN_DURATION_IN_MINUTES', 10),
    'refresh_token_duration' => env('REFRESH_TOKEN_DURATION_IN_MINUTES', 20),

    'first_data' => [
        'store_name' => env('FIRST_DATA_STORE_NAME', 5521150711105),
        'shared_secret' => env('FIRST_DATA_SHARED_SECRET', 'j!9\Pi8mEu'),
        'gateway_url' => env('FIRST_DATA_GATEWAY_URL', 'https://test.ipg-online.com/connect/gateway/processing'),
        'test_mode' => env('FIRST_DATA_TEST_MODE', true),
        'currency' => env('FIRST_DATA_CURRENCY', 941),
        'credit_card_authorization_price' => env('FIRST_DATA_CREDIT_CARD_AUTHORIZATION_PRICE', 16)
    ],

    'ipay_cof' => [
        'gateway_url' => env('IPAY_COF_GATEWAY_URL', 'https://test-cof.ipay.rs/executeHostedDataTransaction'),
        'merchant_username' => env('IPAY_COF_MERCHANT_USERNAME', 'infinityfit_test'),
        'hosted_data_store_id' => env('IPAY_COF_HOSTED_DATA_STORE_ID', 5521150711105),
        'currency' => env('IPAY_COF_CURRENCY', 941),
        'test_mode' => env('IPAY_COF_TEST_MODE', true),
        'secret' => env('IPAY_COF_SECRET', 'bE*O#OZP*sqPA7n^AzDY')
    ],

    'google_pay' => [
        'test_mode' => env('GOOGLE_PAY_TEST_MODE', true),
        'pub-sub_client' => [
            'projectId' => env('GOOGLE_PAY_PROJECT_ID'),
            'keyFilePath' => storage_path(env('PUB_SUB_CREDENTIALS'))
        ],
        'subscription_name' => 'server-pull',
        'google_pay_client' => storage_path(env('GOOGLE_PAY_CREDENTIALS'))
    ],


    'apple_pay' => [
        'payment_after_build_number' => env('APPLE_PAYMENT_AFTER_BUILD_NUMBER', 67),
        'shared_secret' => env('APPLE_PAY_SHARED_SECRET', '1f28e507dc8c45d3a41d26c9b38977ef'),
    ],

    'sms_payment' => [
        //free of charge for telenor, vip and globaltel, 36 rsd for mts
        'short_number' => env('SMS_PAYMENT_SHORT_NUMBER', '4455'),
        'body_pattern' => 'FIT {user_id}-{package_id}',
        'deactivation_short_number' => env('SMS_PAYMENT_SHORT_NUMBER', '4455'),
        'deactivation_sms' => 'stopfit'
    ],

    'notifications' => [
        'firebase' => [
            'api_key' => env('NOTIFICATION_FIREBASE_API_KEY', 'AIzaSyA18El9PkOB3zId-WTJoJLsqSpWBuCFD_c'),
            'url' => 'https://fcm.googleapis.com/fcm/send'
        ]
    ],

    'wordpress' => [
        'wordpress_sync' => env('WORDPRESS_SYNC', false),
        'upload_image_width' => env('WORDPRESS_UPLOAD_IMAGE_WIDTH', 900),
        'url' => env('WORDPRESS_URL', 'http://wordpress:80'),
        'username' => env('WORDPRESS_USERNAME', 'telego'),
        'password' => env('WORDPRESS_PASSWORD', 'sKbhex^*bCJowP3ag&'),
        'blog_category_id' => env('WORDPRESS_BLOG_CATEGORY_ID', 105),
        'nutrition_category_id' => env('WORDPRESS_NUTRITION_CATEGORY_ID', 103),
        'workout_program_category_id' => env('WORDPRESS_WORKOUT_CATEGORY_ID', 104)
    ]
];
