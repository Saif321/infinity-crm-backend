<?php
return [
    0 => [
        'name' => 'Koliko ste aktivni?',
        'description' => 'Možete odabrati samo jedan opis.',
        'question_type' => 'text',
        'answers' => [
            'Uopšte ne vežbam.',
            'Svakodnevno šetam.',
            'Vežbam 1-2 puta nedeljno.',
            'Vežbam 3-4 puta nedeljno.',
        ]
    ],

    1 => [
        'name' => 'Šta vas najbolje opisuje?',
        'description' => 'Možete odabrati samo jedan opis.',
        'question_type' => 'text',
        'answers' => [
            'Nedovoljno spavam.',
            'Jedem kasno uveče.',
            'Često pijem sokove.',
            'Unosim veliku količinu šećera.',
        ]
    ],

    2 => [
        'name' => 'Kako obično provodite dan?',
        'description' => 'Možete odabrati samo jedan opis.',
        'question_type' => 'text',
        'answers' => [
            'U kancelariji.',
            'U kancelariji, ali često izlazim napolje.',
            'Veći deo dana provodim na nogama.',
            'Baveći se manuelnim radom.',
        ]
    ],

    3 => [
        'name' => 'Koje povrće volite?',
        'description' => 'Možete odabrati više opcija.',
        'question_type' => 'image',
        'answers' => [
        ]
    ],

    4 => [
        'name' => 'Koje meso volite?',
        'description' => 'Možete odabrati više opcija.',
        'question_type' => 'image',
        'answers' => [
        ]
    ],

    5 => [
        'name' => 'Koje voće volite?',
        'description' => 'Možete odabrati više opcija.',
        'question_type' => 'image',
        'answers' => [
        ]
    ],


];
