<?php

return "{
  \"question\": [
    {
      \"id\": 4,
      \"title\": \"Odaberite svoj pol?\",
      \"description\": \"\",
      \"type\": \"imageView\",
      \"answers\": [
        {
          \"id\": 1,
          \"title\": \"male\",
          \"description\": \"male\"
        },
        {
          \"id\": 2,
          \"title\": \"female\",
          \"description\": \"female\"
        }
      ]
    },
    {
      \"id\": 5,
      \"title\": \"Koliko imate godina?\",
      \"description\": \"Fitnes programi se razlikuju u zavisnosti od vaših godina.\",
      \"type\": \"datePicker\",
      \"answers\": [
        {
          \"id\": 1,
          \"title\": \"\",
          \"description\": \"\"
        }
      ]
    },
    {
      \"id\": 6,
      \"title\": \"Koliko ste visoki?\",
      \"description\": \"Unesite vašu trenutnu visinu.\",
      \"type\": \"numberPicker\",
      \"answers\": [
        {
          \"id\": 1,
          \"title\": \"\",
          \"description\": \"\",
          \"minValue\": 120,
          \"defaultValue\": 150,
          \"maxValue\": 220
        }
      ]
    },
    {
      \"id\": 7,
      \"title\": \"Kolika je vaša težina?\",
      \"description\": \"Unesite vašu trenutnu težinu.\",
      \"type\": \"numberPicker\",
      \"answers\": [
        {
          \"id\": 1,
          \"title\": \"\",
          \"description\": \"\",
          \"minValue\": 30,
          \"defaultValue\": 50,
          \"maxValue\": 200
        }
      ]
    }
  ]
}";
