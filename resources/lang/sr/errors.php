<?php

return [

    1 => 'User not found.',
    2 => 'Validation error.',
    3 => 'Code is not valid.',
    4 => 'Token is not valid,',
    5 => 'User is not valid.',
    6 => 'Unknown object.',
    7 => 'No image provided',
    8 => 'You don\'t have permissions for this action',
    9 => 'This exercise is not free',
    10 => 'This item is not published.',
    11 => 'You can\'t delete this item.',
    12 => 'Destination email is required.',




    200 => 'Ok.',

    403 => 'Invalid login credentials',
    406 => 'User is not activated',

];
