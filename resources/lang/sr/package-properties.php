<?php

return [
    1 => [
        'plan_name' => 'MESEČNA PRETPLATA',
        'free_trial_badge' => '14 dana probni period',
        'discount_badge' => '',
        'from_label' => 'Od',
        'benefits' => [
            'Pristup svim blog postovima',
            '2 programa za vežbanje',
            '2 programa za ishranu'
        ]
    ],
    2 => [
        'plan_name' => 'GODIŠNJA PRETPLATA',
        'free_trial_badge' => '14 dana probni period',
        'discount_badge' => '7% popusta',
        'from_label' => 'Od',
        'benefits' => [
            'Pristup svim blog postovima',
            'Neograničen pristup programima za vežbanje',
            'Neograničen pristup programima za ishranu'
        ]
    ],
];
