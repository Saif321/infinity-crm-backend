<?php

use Illuminate\Support\Facades\Config;

return [
    'language_iso_code' => Config::get( 'language' )['iso'],
    'language_name' => Config::get( 'language' )['language'],

    // -------------------------------------------------------
    // iOS ---------------------------------------------------
    // -------------------------------------------------------

    'iOS_apiLink' => 'https://api.infinityfit.io',
    'iOS_loggingEnabled' => true,

    // GENERAL
    'iOS_buildVersion'=>101,
    'iOS_transVersion'=>'3',
    'iOS_noInternetConnection'=>'Nema internet konekcije!',
    'iOS_userUnauthorized'=>'Neovlašćeni korisnik',
    'iOS_requestErrorMessage'=>'Ažurirajte aplikaciju, došlo je do greške',
    'iOS_internalServerError'=>'Interna greška na serveru',
    'iOS_success'=>'Uspešno!',
    // -------------------------------------------------------

    // ALERTS
    'iOS_ALERT_positive'=>'Da',
    'iOS_ALERT_confirm'=>'OK',
    'iOS_ALERT_negative'=>'Ne',
    'iOS_ALERT_cancel'=>'Otkaži',
    'iOS_ALERT_dismiss'=>'Zatvori',

    'iOS_ALERT_resendSuccessTitle'=>'Poslat link',
    'iOS_ALERT_resendSuccessMessage'=>'Aktivacioni link je uspešno poslat na vaš email',

    'iOS_ALERT_questionnaireCompletedTitle'=>'Popunjen upitnik',
    'iOS_ALERT_questionnaireCompletedMessage'=>'Hvala što ste popunili upitnik. Tako nam pomažete da vam preporučimo odgovarajuće programe vežbi.',
    'iOS_ALERT_questionnaireINcompletedMessage'=>'Upitnik je nepotpun, molimo vas da proverite svoje odgovore.',
    'iOS_ALERT_passwordChangeSuccess'=>'Upitnik je nepotpun, molimo vas da proverite svoje odgovore.',
    'iOS_ALERT_notAuthorizedTitle'=>'Vaš nalog još uvek nije aktiviran',
    'iOS_ALERT_notAuthorizedDescription'=>'Ukoliko još uvek niste primili aktivacioni email, kliknite dugme "Pošalji ponovo aktivacioni email" kako biste potvrdili vašu email adresu i aktivirali nalog.',
    // -------------------------------------------------------

    // ERRORS -------------------------------------------------------
    'iOS_ERROR_default'=>'Greška',
    'iOS_ERROR_emptyFieldError'=>'Obavezno polje',
    'iOS_ERROR_numberFieldError'=>'Morate uneti broj',
    'iOS_ERROR_emptyCommentFieldError'=>'Polje "Komentar" je obavezno',
    'iOS_ERROR_unableToPostComment'=>'Trenutno nije moguće objaviti komentar.',
    'iOS_ERROR_invalidEmailFormat'=>'Format email adrese je neispravan',
    'iOS_ERROR_emptyEmailField'=>'Polje "Email" je obavezno',
    'iOS_ERROR_codeIncomplete'=>'Nisu uneti svi brojevi',
    'iOS_ERROR_emptyPasswordField'=>'Polje "Lozinka" je obavezno',
    'iOS_ERROR_passwordMissmatch'=>'Lozinke se ne poklapaju',
    'iOS_ERROR_emptyUsernameField'=>'Polje "Korisničko ime" je obavezno',
    'iOS_ERROR_userInfoReadingError'=>'Došlo je do greške prilikom prikupljanja korisničkih podataka.',
    'iOS_ERROR_503errorTitle'=>'Tražena strana ne postoji.',
    'iOS_ERROR_503errorMessage'=>'Pokušajte ponovo kasnije.',
    // -------------------------------------------------------

    // FIELD
    'iOS_FIELD_next'=>'Dalje',
    'iOS_FIELD_emailFieldTitle'=>'Email',
    'iOS_FIELD_passwordFieldTitle'=>'Lozinka',
    'iOS_FIELD_passwordConfirmFieldTitle'=>'Potvrdite lozinku',
    'iOS_FIELD_firstNameTitle'=>'Ime',
    'iOS_FIELD_lastNameTitle'=>'Prezime',
    'iOS_FIELD_usernameTitle'=>'Korisničko ime',
    'iOS_FIELD_verificationCodeTitle'=>'Verifikacioni kod',
    'iOS_FIELD_newPasswordTitle'=>'Nova lozinka',
    'iOS_FIELD_confirmNewPasswordTitle'=>'Potvrdite novu lozinku',
    'iOS_FIELD_nutritionInfo'=>'Nutritivni podaci',
    'iOS_FIELD_ingredients'=>'Sastojci',
    'iOS_FIELD_changePass'=>'Promenite lozinku',
    'iOS_FIELD_oldPass'=>'Stara lozinka',
    'iOS_FIELD_newPass'=>'Nova lozinka',
    'iOS_FIELD_confirmPass'=>'Potvrdite lozinku',
    'iOS_FIELD_comments'=>'Komentari: (%d) ',
    'iOS_FIELD_addComment'=>'Dodajte komentar...',
    'iOS_FIELD_browseAvailable'=>'%@ vežbi',
    'iOS_FIELD_browseMeals'=>'%@',
    'iOS_FIELD_review'=>'ocena',
    'iOS_FIELD_likeThis'=>'Kliknite "Sviđa mi se"',
    'iOS_FIELD_reset'=>'Resetujte svoj napredak',
    'iOS_FIELD_round'=>'runda',
    'iOS_FIELD_roundsSRB'=>'rundi',
    'iOS_FIELD_surveyAlertTitle'=>'Personalizovana ishrana',
    'iOS_FIELD_surveyAlertBody'=>'Popunite upitnik o ishrani kako bismo vam preporučili odgovarajući plan ishrane.',
    'iOS_FIELD_finishWorkoutDay'=>'Završi dan za trening',
    'iOS_FIELD_resendActivationLink'=>'Pošalji ponovo aktivacioni link',
    'iOS_FIELD_areYouSureFinish'=>'Da li ste sigurni da želite da završite ovaj dan za trening?',
    'iOS_FIELD_areYouSureProgress'=>'Da li ste sigurni da želite da resetujete svoj napredak na ovom programu?',
    'iOS_FIELD_areYouSureProgressDay'=>'Da li ste sigurni da želite da resetujete svoj napredak na ovaj dan za trening?',
	'iOS_FIELD_deactivateThisProgram'=>'Deaktivacija programa',
    'iOS_FIELD_deactivate'=>'DEAKTIVIRAJ PROGRAM',
    'iOS_FIELD_areYouSureDeactivate'=>'Da li ste sigurni da želite da deaktivirate ovaj program?',
    // -------------------------------------------------------

    // MISC
    'iOS_FIELD_day'=>'dan',
    'iOS_FIELD_days'=>'dana',
    'iOS_FIELD_meal'=>'obrok',
    'iOS_FIELD_meals'=>'obroka',
    'iOS_FIELD_rep'=>'ponavljanje',
    'iOS_FIELD_reps'=>'ponv.',
    'iOS_FIELD_reviews'=>'ocene',
    'iOS_FIELD_workoutDays'=>'Dani za vežbanje',
    'iOS_FIELD_mealsPerDay'=>'Obroci po danu',
    'iOS_FIELD_description'=>'Opis',
    'iOS_FIELD_basicInfo'=>'Osnovne informacije',
    'iOS_FIELD_programLength'=>'Dužina programa',
    'iOS_FIELD_viewAllComments'=>'Pogledajte sve kometare',
    'iOS_FIELD_rateThisProgram'=>'Ocenite ovaj program',
    'iOS_FIELD_close'=>'Zatvori',
    'iOS_FIELD_rate'=>'Ocenite',
    'iOS_FIELD_workoutDescription'=>'Opis vežbe',
    'iOS_FIELD_proteins'=>'Proteini',
    'iOS_FIELD_carbs'=>'Ugljeni hidrati',
    'iOS_FIELD_fats'=>'Masti',
    'iOS_FIELD_step'=>'Korak',
    'iOS_FIELD_change'=>'Izmeni',
    'iOS_FIELD_freeWorkoutTitle'=>'Besplatni trening',
    'iOS_FIELD_of'=>'od',
    'iOS_FIELD_searchPlaceholder'=>'Pretraži...',
    'iOS_FIELD_week'=>'nedelja',
    'iOS_FIELD_weeks'=>'nedelje',
    'iOS_FIELD_completed'=>'završen',
    'iOS_FIELD_set'=>'Set',
    'iOS_FIELD_roundsOrder'=>'RUNDA %@ OD %@',
    // -------------------------------------------------------

    // BUTTON
    'iOS_BUTTON_signInApple'=>'Prijavite se putem Apple naloga',
    'iOS_BUTTON_signInGoogle'=>'Prijavite se putem Google naloga',
    'iOS_BUTTON_signIn'=>'Prijavite se',
    'iOS_BUTTON_createAccountBtn'=>'Napravite nalog',
    'iOS_BUTTON_sendEmail'=>'Pošalji email',
    'iOS_BUTTON_confirm'=>'Potvrdi',
    'iOS_BUTTON_resetPassword'=>'Resetuj lozinku',
    'iOS_BUTTON_finish'=>'Završi',
    'iOS_BUTTON_seeAll'=>'Pogledajte sve',
    'iOS_BUTTON_signOut'=>'Odjavite se',
    'iOS_BUTTON_continue'=>'Nastavi',
    'iOS_BUTTON_submit'=>'Primeni',
    'iOS_BUTTON_activateProgram'=>'AKTIVIRAJ PROGRAM',
    'iOS_BUTTON_mealsList'=>'IDI NA LISTU OBROKA',
    'iOS_BUTTON_continueProgress'=>'NASTAVITE SA NAPRETKOM',
    'iOS_BUTTON_startSurvey'=>'POPUNITE UPITNIK',
    'iOS_BUTTON_resendActivationEmail'=>'Pošalji ponovo aktivacioni email',
    // -------------------------------------------------------

    // LOGIN
    'iOS_LOGIN_signInTitle'=>'Prijavite se',
    'iOS_LOGIN_signInOption'=>'ili se prijavite putem email-a',
    'iOS_LOGIN_forgotPasswordLabel'=>'Zaboravili ste lozinku?',
    'iOS_LOGIN_noAccountLabel'=>'Nemam nalog.',
    // -------------------------------------------------------

    // CREATE ACCOUNT
    'iOS_CREATE_createAccountTitle'=>'Napravi nalog',
    'iOS_CREATE_alreadyHaveAccountLabel'=>'Već imam nalog.',
    'iOS_CREATE_successMessage'=>'Još malo pa ste završili! Proverite svoj email na koji smo vam poslali verifikacioni kod.',
    // -------------------------------------------------------

    // FORGOT PASS
    'iOS_FORGOT_forgotPasswordTitle'=>'Zaboravili ste lozinku?',
    'iOS_FORGOT_forgotPasswordSubTitle'=>'Poslaćemo vam verifikacioni kod na email.',
    // -------------------------------------------------------

    // VERIFICATION CODE
    'iOS_VERIFICATION_verificationCodeTitle'=>'Verifikacioni kod',
    'iOS_VERIFICATION_verificationCodeSubTitle'=>'Unesite verifikacioni kod koji smo vam prethodno poslali.',
    // -------------------------------------------------------

    // RESET PASSWORD
    'iOS_RESET_resetPasswordTitle'=>'Resetuj lozinku',
    'iOS_RESET_resetPasswordSubTitle'=>'Podesite i potvrdite svoju novu lozinku.',
    'iOS_RESET_resetPasswordSuccessTitle'=>'Uspešno ste resetovali lozinku!',
    // -------------------------------------------------------

    // HOME SCREEN
    'iOS_HOME_welcomeMessagePrefix'=>'Zdravo,',
    'iOS_HOME_recommendedWorkoutsTitle'=>'Preporučeni programi vežbi',
    'iOS_HOME_continueProgressTitle'=>'Nastavite sa napredovanjem',
    'iOS_HOME_recommendedNutritionTitle'=>'Preporučeni programi ishrane',
    'iOS_HOME_blogSectionTitle'=>'Pogledajte naše najnovije tekstove na blogu',
    // -------------------------------------------------------

    // PROGRAMS
    'iOS_PROGRAMS_myProgramsTabTitle'=>'Moji programi',
    'iOS_PROGRAMS_programsTabTitle'=>'Programi',
    'iOS_PROGRAMS_headerTitleLabel'=>'Pretražite dostupne programe vežbi',
    'iOS_PROGRAMS_noBoughtPrograms'=>'Nijedan program još uvek nije aktiviran.',
    'iOS_PROGRAMS_exercise'=>'Vežba',
    'iOS_PROGRAMS_round'=>'Runda',
    'iOS_PROGRAM_isFree'=>'Besplatan plan',
    // -------------------------------------------------------

    // NUTRITION
    'iOS_NUTRITION_myNutritionTabTitle'=>'Moja ishrana',
    'iOS_NUTRITION_nutritionTabTitle'=>'Ishrana',
    'iOS_NUTRITION_headerTitleLabel'=>'Pretražite dostupne programe ishrane',
    // -------------------------------------------------------

    // PROFILE
    'iOS_PROFILE_profileInfoTab'=>'Profilni podaci',
    'iOS_PROFILE_profileInfoSectionHeader'=>'Profilni podaci',
    'iOS_PROFILE_firstNameCellTitle'=>'Ime',
    'iOS_PROFILE_lastNameCellTitle'=>'Prezime',
    'iOS_PROFILE_dateOfBirthCellTitle'=>'Datum rođenja',
    'iOS_PROFILE_genderCellTitle'=>'Pol',
    // -------------------------------------------------------
    'iOS_PROFILE_acountInfoSectionHeader'=>'Podaci na nalogu',
    'iOS_PROFILE_usernameCellTitle'=>'Korisničko ime',
    'iOS_PROFILE_emailCellTitle'=>'Email',
    'iOS_PROFILE_passwrodCellTitle'=>'Lozinka',
    'iOS_PROFILE_languageCellTitle'=>'Jezik',
    'iOS_PROFILE_languageModalTitle'=>'Promeni jezik',
    // -------------------------------------------------------
    'iOS_PROFILE_aboutSectionHeader'=>'O Aplikaciji',
    'iOS_PROFILE_privacyCellTitle'=>'Privatnost',
    'iOS_PROFILE_termsOfUseCellTitle'=>'Uslovi korišćenja',
    'iOS_PROFILE_sourcesCellTitle'=>'Preporuka',
    'iOS_PROFILE_cookiePolicyCellTitle'=>'Politika kolačića',
    // -------------------------------------------------------
    'iOS_PROFILE_statisticsTab'=>'Statistika',
    'iOS_PROFILE_profileSummaryTitle'=>'Pregled profila',
    'iOS_PROFILE_heightLabel'=>'centimetara',
    'iOS_PROFILE_weightLabel'=>'kilograma',
    'iOS_PROFILE_yearsLabel'=>'godina',
    'iOS_PROFILESTATS_programsTitle'=>'Programi',
    'iOS_PROFILESTATS_weeksTitle'=>'Nedelje',
    'iOS_PROFILESTATS_workoutsTitle'=>'Vežbe',
    // -------------------------------------------------------
    'iOS_PROFILE_weightStatsTitle'=>'Statistika težine',
    'iOS_PROFILE_initial'=>'Početna',
    'iOS_PROFILE_current'=>'Trenutna',
    'iOS_PROFILE_differece'=>'Razlika',
    'iOS_PROFILE_addWeight'=>'Unesite težinu',
    'iOS_PROFILE_workoutActivity'=>'Aktivnost na vežbama',
    // -------------------------------------------------------
    'iOS_PROFILE_subscriptionInfo'=>'Informacije o pretplati',
    'iOS_PROFILE_activePlanLabel'=>'Aktivni plan',
    'iOS_PROFILE_changePlanLabel'=>'Promena plana',
    'iOS_PROFILE_noPlanLabel'=>'Besplatan plan',

    // SURVEY
    'iOS_SURVEY_q1Title'=>'Koji je vaš nivo vežbanja?',
    'iOS_SURVEY_q1SubTitle'=>'Opišite svoj trenutni nivo vežbanja.',
    'iOS_SURVEY_q2Title'=>'Koji je vaš cilj?',
    'iOS_SURVEY_q2SubTitle'=>'Recite nam šta želite da postignete.',
    'iOS_SURVEY_q3Title'=>'Gde vežbate?',
    'iOS_SURVEY_q3SubTitle'=>'Odaberite gde biste želeli da vežbate.',
    'iOS_SURVEY_q3answer1Title'=>'Teretana',
    'iOS_SURVEY_q3answer1SubTitle'=>'Vežbam u teretani',
    'iOS_SURVEY_q3answer2Title'=>'Kod kuće',
    'iOS_SURVEY_q3answer2SubTitle'=>'Vežbam kod kuće',
    'iOS_SURVEY_q3answer3Title'=>'Napolju',
    'iOS_SURVEY_q3answer3SubTitle'=>'Vežbam na otvorenom',
    'iOS_SURVEY_q4Title'=>'Odaberite svoj pol?',
    'iOS_SURVEY_q4SubTitle'=>'',
    'iOS_SURVEY_q5Title'=>'Koliko imate godina?',
    'iOS_SURVEY_q5SubTitle'=>'Fitnes programi se razlikuju u zavisnosti od vaših godina.',
    'iOS_SURVEY_q6Title'=>'Koliko ste visoki?',
    'iOS_SURVEY_q6SubTitle'=>'Unesite vašu trenutnu visinu.',
    'iOS_SURVEY_q7Title'=>'Kolika je vaša težina?',
    'iOS_SURVEY_q7SubTitle'=>'Unesite vašu trenutnu težinu.',
    // -------------------------------------------------------

    // PAYMENT
    'iOS_PAYMENT_activePlanLabel'=>'AKTIVAN PLAN',
    'iOS_PAYMENT_deactivatePlan'=>'DEAKTIVIRAJ PLAN',
    'iOS_PAYMENT_nextPaymentLabel'=>'Vaš tekući plan važi do %@',
    'iOS_PAYMENT_headerTitleLabel'=>'Započnite već danas vašu fitnes avanturu uz Infinity Fit Pro!',
    'iOS_PAYMENT_selectMethodLabel'=>'Izaberite metod plaćanja',
    'iOS_PAYMENT_appleMethodLabel'=>'In-App plaćanje',
    'iOS_PAYMENT_creditCardMethodLabel'=>'Kreditna ili debitna kartica',
    'iOS_PAYMENT_smsMethodLabel'=>'SMS',
    'iOS_PAYMENT_FromPaymentLabel'=>'Od',
    'iOS_PAYMENT_successTitleAlert'=>'Vaš zahtev je uspešno primljen!',
    'iOS_PAYMENT_successSubTitleAlert'=>'Obavestićemo vas mejlom kada vaš besplatan pristup bude bio aktiviran.',
    'iOS_PAYMENT_backToPlansLabel'=>'POVRATAK NA PLANOVE',
    'iOS_PAYMENT_deactivatePlanTitle'=>'Deaktiviraj plan',
    'iOS_PAYMENT_deactivatePlanSubTitle'=>'Da li ste sigurni da želite da deaktivirate vaš plan?',
    'iOS_PAYMENT_deactivatePlanConfirm'=>'Da, deaktiviraj moj plan',
    'iOS_PAYMENT_planDeactivateSuccessTitle'=>'Plan je deaktiviran',
    'iOS_PAYMENT_planDeactivateSuccessSubTitle'=>'Uspešno ste deaktivirali vaš plan.',
    'iOS_PAYMENT_planActiveUntilLabel'=>'Vaš plan je deaktiviran i važiće do  %@',
    'iOS_PAYMENT_changePlan'=>'PROMENITE VAŠ PLAN',
    'iOS_PAYMENT_buyPlan'=>'KUPI PLAN',
    'iOS_PAYMENT_noReceiptError'=>'Došlo je do greške prilikom obrade vašeg računa za paket, molimo vas da pokušate ponovo malo kasnije.',
    'iOS_PAYMENT_subscriptionInfo'=>'Pretplatom na Inifntiy Fit aplikaciju saglasni ste sa pravilima i uslovima korišćenja proizvoda. Pretplata se obnavlja u roku od 24 sata pred istek perioda korišćenja. Naplata će se izvrštiti putem već izabranog načina plaćanja. Vašom pretplatom možete upravljati sa profilne stranice.%@%@Ukoliko uslugu plaćate preko in-app pretplate, pristajete na App Store uslove i pravila korišćenja.',
    'iOS_PAYMENT_monthlySmallLabel'=>' (%@%@ mesečno)',
    'iOS_PAYMENT_nextPaymentLabelTrial'=>'Vaših 14 dana besplatnog pristupa ističe %@',
    'iOS_PAYMENT_activeTrialPlanLabel'=>'AKTIVAN PROBNI PERIOD',
    'iOS_PAYMENT_cannotActivateLabel'=>'Ako želite da aktivirate ovaj plan naplate, deaktivirajte vaš trenutni plan ili sačekajte da istekne.',
    'iOS_PAYMENT_activateAPlan'=>'Aktiviraj Plan',
    'iOS_PAYMENT_Buy'=>'Kupi',
    'iOS_PAYMENT_restorePurchase' => 'Povrati Kupovine',
    'iOS_PAYMENT_restoringPurchasesTitle' => 'Kupovine se vraćaju',
    'iOS_PAYMENT_restoringPurchasesMessage' => 'Pokušaćemo da povratimo vaše ranije kupljenje sadržaje. Ako imate aktivne pretplate, paketi će biti ažurirani',

    // UNITS
    'iOS_UNITS_ago'=>'Pre ',
    'iOS_UNITS_minute'=>'minut',
    'iOS_UNITS_minutes'=>'minuta',
    'iOS_UNITS_hour'=>'sat',
    'iOS_UNITS_hours'=>'sati',
    'iOS_UNITS_moment'=>'Malopre',

    // ONBOARD
    'iOS_ONBOARD_q1Title'=>'Aktivirajte personalizovane programe vežbi',
    'iOS_ONBOARD_q1subTitle'=>'Na osnovu podataka o vašoj fizičkoj spremnosti, tipu tela i ciljevima preporučićemo vam odgovarajuće programe vežbi',
    'iOS_ONBOARD_q2Title'=>'Pratite svoj napredak',
    'iOS_ONBOARD_q2subTitle'=>'Pogledajte statistiku vežbanja i proverite svoj dosadašnji napredak.',
    'iOS_ONBOARD_q3Title'=>'Mnoštvo programa i vežbi',
    'iOS_ONBOARD_q3subTitle'=>'InfinityFit aplikacija nudi mnoštvo programa za treniranje, planova obroka i vežbi.',

    // -------------------------------------------------------
    'iOS_FIELD_weight'=>'težinu',
    'iOS_FIELD_height'=>'visinu',
    'iOS_WORKOUT_edit'=>'IZMENA',
    'iOS_WORKOUT_start' => 'POČNI TRENING',
    'iOS_genderMale' => 'Muški',
    'iOS_genderFemale' => 'Ženski',

    'WEB_CONFIRMATION_EMAIL_SUBJECT' => 'InfinityFit email za potvrdu registracije naloga',

    // -------------------------------------------------------
    // Android -----------------------------------------------
    // -------------------------------------------------------
    'AND_VERSION_CODE' => "50",
    'AND_NO_INTERNET_CONNECTION' => "Nema internet konekcije!",
    'AND_USER_IS_NOTU_AUTHORIZED_TOAST_MESSAGE' =>  "Neovlašćeni korisnik",
    'AND_TOO_MANY_REQUESTS_TOAST_MESSAGE' => "Previše zahteva",
    'AND_REQUEST_RESPONSE_ERROR_MSG' =>  "Došlo je do greške: ",
    'AND_INTERNAL_SERVER_ERROR_MSG' =>  "Interna greška na serveru",
    'AND_LOGIN_REQUIRED_MSG' => "Morate biti ulogovani da biste izvršili ovu radnju.",
    'AND_REVIEWS_TEXT' => " ocene",
    'AND_WEEKS_TEXT' =>  " nedelja",
    'AND_DAYS_TEXT' => " dana",
    'AND_MEALS_TEXT' => " obroka",
    'AND_MIN_TEXT' =>  " min.",
    'AND_MINUS_TEXT' =>  " - ",
    'AND_KCAL_TEXT' => " kcal",
    'AND_WEEK_TEXT' => "Nedelja ",
    'AND_REPS_TEXT' => " ponv. ", // TODO SETOVATI OVO NEKAKO U SAM MODEL
    'AND_KILOGRAM_TEXT' => "kg",
    'AND_STRENGTH_PERCENTAGE_TEXT' =>"%",  // TODO SETOVATI OVO NEKAKO U SAM MODEL
    'AND_ROUNDS_TEXT' =>" rundi",
    'AND_SHARE_LINK_TEXT' =>"https://play.google.com/store/apps/details?id=rs.telego.fitness", // TODO URL for play store app
    'AND_REST_DAY_TITLE_TEXT' =>"Dan za odmor",
    'AND_REST_DAY_SUBTITLE_TEXT' => "Dan za odmor",
    'AND_IN_PROGRESS_TEXT' =>  " (u toku)",
    'AND_IN_CLOSE_TEXT' => "Zatvori",
    'AND_DAY_TEXT' => "Dan ",
    'AND_SECONDS_TEXT' => "s",
    'AND_COMPLETED_TIMES_TEXT' => "x",

    // Instruction Activity
    'AND_INSTRUCTION_ACTIVITY_SIGN_IN_BTN_TEXT' => 'Dalje',
    'AND_INSTRUCTION_ACTIVITY_SIGN_UP_BTN_TEXT' =>"Napravi nalog",
    'AND_INSTRUCTION_ACTIVITY_LOGIN_TEXT' =>"Već imam nalog.",
    'AND_INSTRUCTION_ACTIVITY_PAGE_ONE_TITLE' =>"Aktivirajte personalizovane programe vežbi",
    'AND_INSTRUCTION_ACTIVITY_PAGE_ONE_MESSAGE' =>"Na osnovu podataka o vašoj fizičkoj spremnosti, tipu tela i ciljevima \n preporučićemo vam odgovarajuće programe vežbi.",
    'AND_INSTRUCTION_ACTIVITY_PAGE_TWO_TITLE' =>"Pratite svoj napredak",
    'AND_INSTRUCTION_ACTIVITY_PAGE_TWO_MESSAGE' =>"Pogledajte statistiku vežbanja i proverite svoj dosadašnji napredak.",
    'AND_INSTRUCTION_ACTIVITY_PAGE_THREE_TITLE' =>"Mnoštvo programa i vežbi",
    'AND_INSTRUCTION_ACTIVITY_PAGE_THREE_MESSAGE' =>"InfinityFit aplikacija nudi mnoštvo programa za treniranje, planova obroka i vežbi.",

    // Login Activity
    'AND_LOGIN_ACTIVITY_TITLE_TEXT' =>"Prijavite se",
    'AND_LOGIN_ACTIVITY_GOOGLE_TITLE_TEXT' =>"Prijavite se putem Google naloga",
    'AND_LOGIN_ACTIVITY_SING_IN_OR_EMAIL_TEXT' =>"ili se prijavite putem email-a",
    'AND_LOGIN_ACTIVITY_EMAIL_TEXT' =>"Email",
    'AND_LOGIN_ACTIVITY_PASSWORD_TEXT' =>"Lozinka",
    'AND_LOGIN_ACTIVITY_FORGOT_PASSWORD_TEXT' => "Zaboravili ste lozinku",
    'AND_LOGIN_ACTIVITY_LOGIN_BTN_TEXT' =>"Ulogujte se",
    'AND_LOGIN_ACTIVITY_SIGN_UP_TEXT' =>"Trenutno nemam nalog. Kreiraj nalog.",
    'AND_LOGIN_ACTIVITY_SUCCESS_LOGIN' =>"Uspešno ste se prijavili",
    'AND_LOGIN_REQUEST_EMAIL_REQUIRED' =>"Email polje je obavezno.",
    'AND_LOGIN_REQUEST_EMAIL_WRONG_FORMAT' =>"Format email adrese je neispravan",
    'AND_LOGIN_REQUEST_PASSWORD_REQUIRED' =>"Polje 'Lozinka' je obavezno.",
    'AND_LOGIN_REQUEST_PASSWORD_WRONG_LENGTH' =>"Lozinka mora biti dužine između 8 i 30 kataktera.",
    'AND_LOGIN_REQUEST_PASSWORD_WRONG_FORMAT' =>"Format lozinke je neispravan",
    'AND_LOGIN_REQUEST_INVALID_LOGIN' => "Pogrešni podaci",
    'AND_LOGIN_ACTIVITY_GOOGLE_LOGIN_ERROR_TEXT' =>"Greška pri logovanju putem Google naloga",
    'AND_LOGIN_ACTIVITY_GOOGLE_TOKEN_INVALID_ERROR_TEXT' =>"Neispravan Google Token",

    // Forgot Activity
    'AND_FORGOT_PASSWORD_ACTIVITY_TITLE_TEXT' =>"Zaboravili ste lozinku",
    'AND_FORGOT_PASSWORD_ACTIVITY_SUBTITLE_TEXT' =>"Poslaćemo vam verifikacioni kod na email adresu.",
    'AND_FORGOT_PASSWORD_ACTIVITY_EMAIL_TEXT' =>"Email",
    'AND_FORGOT_PASSWORD_ACTIVITY_SUBMIT_BTN_TEXT' => "Pošalji email",
    'AND_FORGOT_PASSWORD_REQUEST_EMAIL_REQUIRED' =>"Email polje je obavezno",
    'AND_FORGOT_PASSWORD_REQUEST_EMAIL_WRONG_FORMAT' =>"Format email adrese je neispravan",
    'AND_FORGOT_PASSWORD_REQUEST_REQUSET_ON_SUCCESS_SEND_CODE' =>"Verifikacioni kod vam je uspešno poslat na email adresu.",

    // OTP Code Fragment
    'AND_OTP_CODE_FRAGMENT_TITLE_TEXT' =>"Verifikacioni kod",
    'AND_OTP_CODE_FRAGMENT_OTP_TITLE_TEXT' =>"Verifikacioni kod",
    'AND_OTP_CODE_FRAGMENT_SUBTITLE_TEXT' =>"Unesite verifikacioni kod koji smo vam prethodno poslali",
    'AND_OTP_CODE_FRAGMENT_RESEND_CODE_MESSAGE_TEXT' =>"Da li ste sigurni da želite da vam ponovo pošaljemo OTP kod?",
    'AND_OTP_CODE_FRAGMENT_RESEND_CODE_POSITIVE_BTN_TEXT' =>"Pošalji ponovo",
    'AND_OTP_CODE_FRAGMENT_RESEND_CODE_NEGATIVE_BTN_TEXT' =>"Otkaži",
    'AND_OTP_CODE_FRAGMENT_REQUSET_ON_SUCCESS_RESEND_CODE' => "OTP kod vam je uspešno poslat na email adresu.",
    'AND_OTP_CODE_FRAGMENT_CONFIRM_SUBMIT_BTN' =>"Potvrdi",

    // Reset Password Fragment
    'AND_RESET_PASSWORD_FRAGMENT_TITLE_TEXT' =>"Resetuj lozinku",
    'AND_RESET_PASSWORD_FRAGMENT_SUBTITLE_TEXT' =>"Podesite i potvrdite svoju novu lozinku.",
    'AND_RESET_PASSWORD_FRAGMENT_NEW_PASSWORD_TEXT' =>"Nova lozinka",
    'AND_RESET_PASSWORD_FRAGMENT_CONFIRM_NEW_PASSWORD_TEXT' =>"Potvrdite novu lozinku",
    'AND_RESET_PASSWORD_FRAGMENT_SUBMIT_BTN_TEXT' =>"Resetuj lozinku",
    'AND_RESET_PASSWORD_FRAGMENT_REQUEST_NEW_PASSWOR_REQUIRED' =>"Polje je prazno",
    'AND_RESET_PASSWORD_FRAGMENT_REQUEST_NEW_PASSWORD_WRONG_LENGTH' => "Lozinka mora biti dužine između 8 i 30 kataktera.",
    'AND_RESET_PASSWORD_FRAGMENT_REQUEST_CONFIRM_PASSWOR_REQUIRED' =>"Polje je prazno",
    'AND_RESET_PASSWORD_FRAGMENT_REQUEST_CONFIRM_PASSWORD_WRONG_LENGTH' =>"Lozinka mora biti dužine između 8 i 30 kataktera.",
    'AND_RESET_PASSWORD_FRAGMENT_REQUEST_PASSWORD_NOT_MATCH' => "Lozinke se ne poklapaju.",
    'AND_RESET_PASSWORD_FRAGMENT_REQUEST_EMAIL_REQUIRED' =>"Email polje je obavezno",
    'AND_RESET_PASSWORD_FRAGMENT_REQUEST_EMAIL_WRONG_FORMAT' => "Format email adrese je neispravan",
    'AND_RESET_PASSWORD_FRAGMENT_REQUEST_OTP_CODE_REQUIRED' =>"OTP kod je obavezan",

    // Success Reset Password Fragment
    'AND_SUCCESS_RESET_PASSWORD_FRAGMENT_DESCRIPTION_TEXT' =>"Uspešno ste \n resetovani svoju lozinku!",

    // Create Account Fragment
    'AND_CREATE_ACCOUNT_FRAGMENT_TITLE_TV' =>"Napravi nalog",
    'AND_CREATE_ACCOUNT_FRAGMENT_FIRST_NAME_TEXT_TV' =>"Ime",
    'AND_CREATE_ACCOUNT_FRAGMENT_LAST_NAME_TEXT_TV' =>"Prezime",
    'AND_CREATE_ACCOUNT_FRAGMENT_EMAIL_TEXT_TV' =>"Email",
    'AND_CREATE_ACCOUNT_FRAGMENT_USERNAME_TEXT_TV' => "Korisničko ime",
    'AND_CREATE_ACCOUNT_FRAGMENT_PASSWORD_TEXT_TV' =>"Lozinka",
    'AND_CREATE_ACCOUNT_FRAGMENT_SUBMIT_TEXT_TV' =>"Napravi nalog",
    'AND_CREATE_ACCOUNT_FRAGMENT_LOGIN_TEXT_TV' =>"Već imam nalog.",
    'AND_CREATE_ACCOUNT_REQUEST_FIRST_NAME_REQUIRED' =>"Obavezno polje",
    'AND_CREATE_ACCOUNT_REQUEST_LAST_NAME_REQUIRED' =>"Obavezno polje",
    'AND_CREATE_ACCOUNT_REQUEST_EMAIL_REQUIRED' =>"Email polje je obavezno",
    'AND_CREATE_ACCOUNT_REQUEST_EMAIL_WRONG_FORMAT' =>"Format email adrese je neispravan",
    'AND_CREATE_ACCOUNT_REQUEST_USERNAME_REQUIRED' =>"Obavezno polje",
    'AND_CREATE_ACCOUNT_REQUEST_PASSWORD_REQUIRED' =>"Polje 'Lozinka' je obavezno",
    'AND_CREATE_ACCOUNT_REQUEST_PASSWORD_WRONG_LENGTH' =>"Lozinka mora biti dužine između 8 i 30 kataktera.",
    'AND_CREATE_ACCOUNT_REQUEST_PASSWORD_WRONG_FORMAT' =>"Format lozinke je neispravan",

    // Success Reset Password Fragment
    'AND_SUCCESS_ACCOUNT_FRAGMENT_DESCRIPTION_TEXT' =>"Još malo pa ste završili! Proverite svoj email na koji smo vam poslali verifikacioni kod.",

    // Change password Activity
    'AND_CHANGE_PASSWORD_ACTIVITY_TITLE_TEXT' =>"Promenite lozinku",
    'AND_CHANGE_PASSWORD_ACTIVITY_OLD_PASSWORD_TEXT' =>"Stara lozinka",
    'AND_CHANGE_PASSWORD_ACTIVITY_NEW_PASSWORD_TEXT' =>"Nova lozinka",
    'AND_CHANGE_PASSWORD_ACTIVITY_CONFIRM_PASSWORD_TEXT' =>"Potvrdite lozinku",
    'AND_CHANGE_PASSWORD_ACTIVITY_SUBMIT_TEXT' =>"Potvrdi",
    'AND_CHANGE_PASSWORD_ACTIVITY_DESCRIPTION_TITLE_TEXT' =>"Lozinka mora da sadrži minimum",
    'AND_CHANGE_PASSWORD_ACTIVITY_DESCRIPTION_CONTENT_TEXT' =>"8 karaktera\n- 1 veliko slovo\n- 1 broj",
    'AND_CHANGE_PASSWORD_REQUEST_PASSWORD_REQUIRED' =>"Polje 'Lozinka' je obavezno",
    'AND_CHANGE_PASSWORD_REQUEST_PASSWORD_WRONG_LENGTH' =>"Lozinka mora biti dužine između 8 i 30 kataktera.",
    'AND_CHANGE_PASSWORD_REQUEST_PASSWORD_WRONG_FORMAT' =>"Format lozinke je neispravan",
    'AND_CHANGE_PASSWORD_REQUEST_PASSWORD_NOT_MATCH' =>"Lozinke se ne poklapaju",
    'AND_CHANGE_PASSWORD_ACTIVITY_SUCCESS_CHANGE_PASSWORD' =>"Uspešno ste promenili lozinku!",

    // Profile Activity
    'AND_PROFILE_ACTIVITY_TAB_PROFILE_INFO' =>"Profil info",
    'AND_PROFILE_ACTIVITY_TAB_STATISTICS' =>"Statistika",
    'AND_PROFILE_ACTIVITY_REQUEST_PHOTO_WRONG_EXTENSION' =>"Podržani formati slika su jpg, jpeg, png",
    'AND_PROFILE_ACTIVITY_REQUEST_PHOTO_WRONG_SIZE' =>"Slika je prevelika. Maksimalna veličina za upload je 8MB",
    'AND_PROFILE_ACTIVITY_PERMISSION_NEEDED_TEXT' =>"Aplikacija nema pristup slikama",
    'AND_PROFILE_ACTIVITY_PERMISSION_NEEDED_MESSAGE' =>"Da li dozvoljavate aplikaciji pristup slikama?",
    'AND_PROFILE_ACTIVITY_PERMISSION_NEEDED_POSITIVE_BTN' =>"Da",
    'AND_PROFILE_ACTIVITY_PERMISSION_NEEDED_NEGATIVE_BUTTON' =>"Otkaži",
    'AND_PROFILE_ACTIVITY_PERMISSION_ERROR' =>"Došlo je do greške. Izaberite drugu aplikaciju za odabir slika.",
    'AND_PROFILE_ACTIVITY_PERMISSION_DENIED_TEXT' =>"Nedozvoljen pristup",
    'AND_PROFILE_ACTIVITY_PERMISSION_SHOW_NEVER_ASK_TEXT' =>"Pristup trajno onemogućen. Izmenite podešavanja.",
    'AND_PROFILE_ACTIVITY_SUCCESS_UPDATE' =>"Uspešno ste izmenili svoju sliku!",
    'AND_PROFILE_ACTIVITY_PROFILE_PROGRAM_TITLE_TEXT' => "Program",
    'AND_PROFILE_ACTIVITY_PROFILE_WEEKS_TITLE_TEXT' => "Nedelje",
    'AND_PROFILE_ACTIVITY_PROFILE_WORKOUTS_TITLE_TEXT' => "Vežbe",

    // Profile Info Fragment
    'AND_PROFILE_INFO_FRAGMENT_PERSONAL_INFO_TITLE_TEXT' =>"Informacije o Vama",
    'AND_PROFILE_INFO_FRAGMENT_FIRST_NAME_TITLE_TEXT' =>"Ime",
    'AND_PROFILE_INFO_FRAGMENT_LAST_NAME_TITLE_TEXT' =>"Prezime",
    'AND_PROFILE_INFO_FRAGMENT_DATE_OF_BIRTH_TITLE_TEXT' =>"Datum rođenja",
    'AND_PROFILE_INFO_FRAGMENT_GENDER_TITLE_TEXT' =>"Pol",
    'AND_PROFILE_INFO_FRAGMENT_ACCOUNT_INFO_TITLE_TEXT' =>"Podaci na nalogu",
    'AND_PROFILE_INFO_FRAGMENT_EMAIL_TITLE_TEXT' =>"Email",
    'AND_PROFILE_INFO_FRAGMENT_PASSWORD_TITLE_TEXT' =>"Lozinka",
    'AND_PROFILE_INFO_FRAGMENT_LANGUAGE_TITLE_TEXT' => "Jezik",
    'AND_PROFILE_INFO_FRAGMENT_ABOUT_TITLE_TEXT' =>"O Aplikaciji",
    'AND_PROFILE_INFO_FRAGMENT_PRIVACY_TITLE_TEXT' =>"Privatnost",
    'AND_PROFILE_INFO_FRAGMENT_TERMS_OF_USE_TITLE_TEXT' =>"Uslovi korišćenja",
    'AND_PROFILE_INFO_FRAGMENT_SIGN_OUT_TEXT' =>"Odjavite se",
    'AND_PROFILE_INFO_FRAGMENT_SUCCESS_UPDATE' =>"Uspešno ste ažurirali podatke na profilu.",
    'AND_PROFILE_INFO_FRAGMENT_CM_SUBTITLE_TEXT' => " cm",
    'AND_PROFILE_INFO_FRAGMENT_SUBSCRIPTION_INFO_TITLE_TEXT' => "Informacije o pretplati",
    'AND_PROFILE_INFO_FRAGMENT_ACTIVE_PLAN_TITLE_TEXT' => "Trenutni plan",
    'AND_PROFILE_INFO_FRAGMENT_CHANGE_PLAN_TITLE_TEXT' => "Promena plana",
    'AND_PROFILE_INFO_FRAGMENT_FREE' => "Besplatan",

    // Profile info Fragment
    'AND_PROFILE_STATISTICS_FRAGMENT_PROFILE_SUMMARY_TITLE_TEXT' =>"Pregled profila",
    'AND_PROFILE_STATISTICS_FRAGMENT_SUCCESS_UPDATE' =>"Uspešno ste ažurirali podatke na profilu.",
    'AND_PROFILE_STATISTICS_FRAGMENT_SURVEY' =>"Upitnik",
    'AND_PROFILE_STATISTICS_FRAGMENT_WORKOUT_ACTIVITY_TITLE_TEXT' => "Aktivnost na vežbama",
    'AND_PROFILE_STATISTICS_FRAGMENT_HEIGHT_TITLE_TEXT' => "Visina",
    'AND_PROFILE_STATISTICS_FRAGMENT_WEIGHT_TITLE_TEXT' => "Težina",
    'AND_PROFILE_STATISTICS_FRAGMENT_PROFILE_SUMMARY_INITIAL_TITLE_TEXT' => "Početna",
    'AND_PROFILE_STATISTICS_FRAGMENT_PROFILE_SUMMARY_CURRENT_TITLE_TEXT' => "Trenutna",
    'AND_PROFILE_STATISTICS_FRAGMENT_PROFILE_SUMMARY_DIFFERENCE_TITLE_TEXT' => "Razlika",
    'AND_PROFILE_STATISTICS_FRAGMENT_KG_SUBTITLE_TEXT' => "kg",
    'AND_PROFILE_STATISTICS_FRAGMENT_ADD_WEIGHT' => "Unesite težinu",

    // Payment
    'AND_PAYMENT_FRAGMENT_DESCRIPTION_START_TEXT' => "Započnite već danas vašu fitnes avanturu uz Infinity Fit Pro!",
    'AND_PAYMENT_FRAGMENT_DESCRIPTION_END_UP_TEXT' => "Pretplatom na Inifntiy Fit aplikaciju saglasni ste sa pravilima i uslovima korišćenja proizvoda. Pretplata se obnavlja u roku od 24 sata pred istek perioda korišćenja. Naplata će se izvrštiti putem već izabranog načina plaćanja. Vašom pretplatom možete upravljati sa profilne stranice.\n\nUkoliko uslugu plaćate preko in-app pretplate, pristajete na Google Play Store uslove i pravila korišćenja.",
    'AND_PAYMENT_FRAGMENT_ACTIVE_PLAN_TEXT' => "Trenutni plan",
    'AND_PAYMENT_FRAGMENT_DEACTIVATE' => "Deaktivacija plana",
    'AND_PAYMENT_FRAGMENT_RECOMMENDED_WORKOUT' => "Dva preporučena programa vežbi",
    'AND_PAYMENT_FRAGMENT_RECOMMENDED_NUTRITION' => "Dva preporučena programa ishrane",
    'AND_PAYMENT_FRAGMENT_DESCRIPTION_END_FREE_TEXT' => "Izaberite plan kako biste dobili besplatan pristup. Nakon 14 dana besplatnog pristupa bićete automatski naplaćeni sem ukoliko se u međuvremenu ne odjavite sa pretplate.",
    'AND_PAYMENT_FRAGMENT_NEXT_PAYMENT' => "Sledeća naplata od %s1 će se desiti %s2",
    'AND_PAYMENT_FRAGMENT_PLAN_ENDS' => "Vaš trenutni plan važi do %s",
    'AND_PAYMENT_FRAGMENT_PLAN_DEACTIVATED' => "Vaš plan je deaktiviran i važiće do %s",
    'AND_PAYMENT_FRAGMENT_ACTIVE_FREE_TRIAL_TEXT' => "Aktivan besplatan pristup",
    'AND_PAYMENT_FRAGMENT_YOUR_TRIAL_ENDS' => "Vaš besplatan pristup ističe %s",
    'AND_PAYMENT_FRAGMENT_FREE_TRAIL' => " besplatan pristup.",
    'AND_PAYMENT_FRAGMENT_PER_MONTH' => " mesečno",
    'AND_PAYMENT_CARD_FRAGMENT_PAYMENT_FAILED' => "Plaćanje karticom nije uspelo",

    'AND_PAYMENT_PLAN_ACTIVATED_FRAGMENT_BACK' => "Nazad",
    'AND_PAYMENT_PLAN_ACTIVATED_FRAGMENT_TITLE' => "Vaš zahtev je uspešno primljen!",
    'AND_PAYMENT_PLAN_ACTIVATED_FRAGMENT_DESCRIPTION_FREE' => "Obavestićemo vas mejlom kada vam besplatan pristup bude bio aktiviran.",
    'AND_PAYMENT_PLAN_ACTIVATED_FRAGMENT_DESCRIPTION' => "Obavestićemo vas mejlom kada vaš plan bude bio aktiviran.",

    'AND_PAYMENT_ACTIVITY_BOTTOM_DIALOG_FRAGMENT_TITLE' => "Izaberite metod plaćanja",
    'AND_PAYMENT_ACTIVITY_BOTTOM_DIALOG_FRAGMENT_GPAY' => "Google Pay",
    'AND_PAYMENT_ACTIVITY_BOTTOM_DIALOG_FRAGMENT_CARD' => "Kreditna ili debitna kartica",
    'AND_PAYMENT_ACTIVITY_BOTTOM_DIALOG_FRAGMENT_SMS' => "SMS",
    'AND_PAYMENT_ACTIVITY_BOTTOM_DIALOG_FRAGMENT_DESC_FREE' => "Izaberite metod plaćanja kako biste dobili %s2. Nećete biti naplaćeni ako se odjavite pre %s1",
    'AND_PAYMENT_ACTIVITY_BOTTOM_DIALOG_FRAGMENT_DESCRIPTION' => "Izaberite metod plaćanja za vaš %s.",

    'AND_PAYMENT_PLAN_DEACTIVATED_FRAGMENT_BACK' => "Nazad",
    'AND_PAYMENT_PLAN_DEACTIVATED_FRAGMENT_TITLE' => "Plan je deaktiviran",
    'AND_PAYMENT_PLAN_DEACTIVATED_FRAGMENT_DESCRIPTION' => "Uspešno ste deaktivirali vaš plan.",

    'AND_PAYMENT_ACTIVITY_CANNOT_ACTIVATE' => 'Ako želite da aktivirate ovaj plan naplate, deaktivirajte vaš trenutni plan ili sačekajte da istekne.',

    'AND_DIALOG_DEACTIVATE_PLAN_CONFIRMATION_TITLE' => "Deaktiviraj plan",
    'AND_DIALOG_DEACTIVATE_PLAN_CONFIRMATION_DESCRIPTION' => "Da li ste sigurni da želite da deaktivirate vaš plan %s?",
    'AND_DIALOG_DEACTIVATE_PLAN_CONFIRMATION_DECLINE' => "Ne",
    'AND_DIALOG_DEACTIVATE_PLAN_CONFIRMATION_ACCEPT' => "Da, deaktiviraj moj plan %s",

    // Billing
    'AND_BILLING_MANAGER_SERVICE_TIMEOUT' => "Greška prilikom naplate: service timeout.",
    'AND_BILLING_MANAGER_FEATURE_NOT_SUPPORTED' => "Greška prilikom naplate: feature not supported.",
    'AND_BILLING_MANAGER_SERVICE_DISCONNECTED' => "Greška prilikom naplate: service disconnected.",
    'AND_BILLING_MANAGER_USER_CANCELLED' => "Greška prilikom naplate: user cancelled.",
    'AND_BILLING_MANAGER_SERVICE_UNAVAILABLE' => "Greška prilikom naplate: service unavailable.",
    'AND_BILLING_MANAGER_BILLING_UNAVAILABLE' => "Greška prilikom naplate: billing unavailable.",
    'AND_BILLING_MANAGER_ITEM_UNAVAILABLE' => "Greška prilikom naplate: item unavailable.",
    'AND_BILLING_MANAGER_DEVELOPER_ERROR' => "Greška prilikom naplate: developer error.",
    'AND_BILLING_MANAGER_ERROR' => "Greška prilikom naplate: general error.",
    'AND_BILLING_MANAGER_ITEM_ALREADY_OWNED' => "Greška prilikom naplate: item already owned.",
    'AND_BILLING_MANAGER_ITEM_NOT_OWNED' => "Greška prilikom naplate: item not owned.",

    // Privacy Activity
    'AND_PRIVACY_ACTIVITY_TITLE_TEXT' =>"Privatnost",
    'AND_PRIVACY_ACTIVITY_SUBTITLE_TEXT' =>"Koje podatke ćemo prikupljati?",
    'AND_PRIVACY_ACTIVITY_DESCRIPTION_TEXT' =>"Opis....",

    // Terms of use Activity
    'AND_TERMS_OF_USE_ACTIVITY_TITLE_TEXT' =>"Uslovi korišćenja",
    'AND_TERMS_OF_USE_ACTIVITY_SUBTITLE_TEXT' =>"Koje podatke ćemo prikupljati?",
    'AND_TERMS_OF_USE_ACTIVITY_DESCRIPTION_TEXT' =>"Opis....",

    // Edit First Name Dialog
    'AND_EDIT_FIRST_NAME_TITLE' =>"Unesite Ime",
    'AND_EDIT_FIRST_NAME_FIRST_NAME_TV' =>"Ime",
    'AND_EDIT_FIRST_NAME_SUBMIT_BTN' =>"Sačuvaj",
    'AND_EDIT_FIRST_NAME_REQUEST_FIRST_NAME_REQUIRED' =>"Obavezno polje",

    // Edit Last Name Dialog
    'AND_EDIT_LAST_NAME_TITLE' =>"Unesite Prezime",
    'AND_EDIT_LAST_NAME_LAST_NAME_TV' =>"Prezime",
    'AND_EDIT_LAST_NAME_SUBMIT_BTN' =>"Sačuvaj",
    'AND_EDIT_LAST_NAME_REQUEST_LAST_NAME_REQUIRED' =>"Obavezno polje",

    // Edit User Name Dialog
    'AND_EDIT_USER_NAME_TITLE' =>"Korisničko ime",
    'AND_EDIT_USER_NAME_USER_NAME_TV' =>"Korisničko ime:",
    'AND_EDIT_USER_NAME_SUBMIT_BTN' =>"Sačuvaj",
    'AND_EDIT_USER_NAME_REQUEST_USER_NAME_REQUIRED' =>"Obavezno polje",

    // Gender Dialog
    'AND_EDIT_GENDER_GENDER_TITLE' =>"Unesite pol",
    'AND_EDIT_GENDER_GENDER_REQUIRED' =>"Polje 'Pol' je obavezno",

    // Language
    'AND_LANGUAGE_TITLE' => "Jezik",
    'AND_LANGUAGE_NAME_SUBMIT_BTN' => "Primeni",

    // Blog Activity
    'AND_BLOG_ACTIVITY_SUCCESS_ADD_COMMENT' =>"Vaš komentar će biti prikazan nakon odobrenja.",
    'AND_BLOG_ACTIVITY_SUCCESS_LIKE_POST' =>"Uspešno ste kliknuli 'sviđa mi se'.",
    'AND_BLOG_ACTIVITY_SUCCESS_DISLIKE_POST' =>"Uspešno ste kliknuli 'ne sviđa mi se'.",
    'AND_BLOG_ACTIVITY_ADD_COMMENT_TITLE' =>"Dodajte komentar ...",
    'AND_BLOG_ACTIVITY_COMMENTS_TITLE' =>"Komentari (%s)",

    // Add comment Dialog
    'AND_ADD_COMMENT_DIALOG_ADD_COMMENT_NAME_TV' =>"Dodajte komentar",
    'AND_ADD_COMMENT_DIALOG_SUBMIT_BTN' =>"Sačuvaj",
    'AND_ADD_COMMENT_DIALOG_CLOSE_TEXT' =>"Zatvori",
    'AND_ADD_COMMENT_DIALOG_REQUEST_COMMENT_REQUIRED' =>"Obavezno polje",

    // Edit Height Dialog
    'AND_EDIT_HEIGHT_TITLE' =>"Unesite visinu",
    'AND_EDIT_HEIGHT_SUBMIT_BTN' =>"Sačuvaj",
    'AND_EDIT_HEIGHT_REQUEST_HEIGHT_REQUIRED' =>"Obavezno polje",

    // Edit Height Dialog
    'AND_EDIT_WEIGHT_TITLE' =>"Unesite težinu",
    'AND_EDIT_WEIGHT_SUBMIT_BTN' =>"Sačuvaj",
    'AND_EDIT_WEIGHT_REQUEST_WEIGHT_REQUIRED' =>"Obavezno polje",

    // Survey Activity
    'AND_SURVEY_ACTIVITY_DIALOG_STOP_TITLE' =>"Pažnja",
    'AND_SURVEY_ACTIVITY_DIALOG_STOP_MESSAGE' =>"Ukoliko sada izađete iz upitnika, uneti podaci neće biti sačuvani",
    'AND_SURVEY_ACTIVITY_DIALOG_STOP_POSITIVE_BTN' =>"Slažem se",
    'AND_SURVEY_ACTIVITY_DIALOG_STOP_NEGATIVE_BTN' =>"Nastavi",
    'AND_SURVEY_ACTIVITY_STEP_TEXT' =>"Korak ",
    'AND_SURVEY_ACTIVITY_OF_TEXT' =>" od ",
    'AND_SURVEY_ACTIVITY_STOPPED_WITHOUT_CONNECTION' =>"Prekid usled nedostatka konekcije.",
    'AND_SURVEY_ACTIVITY_SUCCESS' =>"Promene su uspešno sačuvane.",
    'AND_GENDER_MALE_TEXT' => "Muški",
    'AND_GENDER_FEMALE_TEXT' => "Ženski",

    // Question Fragment
    'AND_QUESTION_FRAGMENT_DATE_PICKER_TEXT' => "Unesite datum rođenja",
    'AND_QUESTION_FRAGMENT_NUMBER_PICKER_TEXT' =>"Podesi",

    // Home fragment
    'AND_HOME_FRAGMENT_WELCOME_TITLE' =>"Dobrodošli",
    'AND_HOME_FRAGMENT_BLOGS_TITLE' =>"Pogledajte naše najnovije tekstove na blogu",

    // Home Fragment Recommended
    'AND_HOME_FRAGMENT_RECOMMENDED_PROGRAMS_TITLE' =>"Preporučeni programi vežbi",
    'AND_HOME_FRAGMENT_RECOMMENDED_PROGRAMS_SEE_ALL_TITLE' =>"Pogledaj sve",
    'AND_HOME_FRAGMENT_CONTINUE_PROGRAMS_TITLE' =>"Nastavite sa napretkom",
    'AND_HOME_FRAGMENT_RECOMMENDED_NUTRITIONS_TITLE' =>"Preporučeni planovi ishrane",
    'AND_HOME_FRAGMENT_NUTRITIONS_SEE_ALL_TITLE' =>"Pogledaj sve",
    'AND_HOME_FRAGMENT_CONTINUE_PROGRAMS_DAY_TEXT' => " ( Dan ",
    'AND_HOME_FRAGMENT_CONTINUE_PROGRAMS_OF_TEXT' =>        " od ",
    'AND_HOME_FRAGMENT_CONTINUE_PROGRAMS_COMPLETED_TEXT' =>" završen )",

    // Main Activity
    'AND_MAIN_ACTIVITY_HOME_TITLE' =>"Početna",
    'AND_MAIN_ACTIVITY_EXERCISE_TITLE' =>"Vežbe",
    'AND_MAIN_ACTIVITY_PROGRAMS_TITLE' =>"Programi",
    'AND_MAIN_ACTIVITY_NUTRITION_TITLE' =>"Ishrana",
    'AND_MAIN_ACTIVITY_PROFILE_TITLE' =>"Profil",

    // Nutrition Program Activity
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_SUCCESS_ADD_COMMENT' =>"Vaš komentar će biti objavljen nakon odobrenja.",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_ADD_COMMENT_TITLE' =>"Dodajte komentar ...",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_COMMENTS_TITLE' =>"Komentari (%s)",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_SUCCESS_LIKE_PROGRAM' =>"Uspešno ste kliknuli 'sviđa mi se'.",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_SUCCESS_DISLIKE_PROGRAM' =>"Upešno ste kliknuli 'ne sviđa mi se'.",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_SUCCESS_RESET_PROGRAM' =>"Uspešno ste resetovali ovaj program.",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_SUCCESS_RATE_PROGRAM' =>"Uspešno ste ocenili ovaj program.",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_ADD_COMMENT_NOT_PERMISSION' =>"Da biste ostavili komentar, morate prvo aktivirati program.",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_DESCRIPTION_TITLE' =>"Opis",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_BASIC_INFO_TITLE' =>"Osnovne informacije",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_PROGRAM_LENGTH_TITLE' =>"Dužina programa",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_MEALS_PER_DAY_TITLE' =>"Obroci po danu",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_RATE_WRONG' =>"Ocene moraju biti od 1 do 5!",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_BUY_PROGRAM_BTN' =>"Aktivirajete ovaj program",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_CONTINUE_PROGRAM_BTN' =>"Idi na listu obroka",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_SUCCESS_BUY_PROGRAM' =>"Uspešno ste aktivirali program.",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_NEXT_TRAINING_DOES_NOT_EXIST' =>"Naredni program ne postoji.",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_SUCCESS_DEACTIVATE_PROGRAM' => "Program je uspešno deaktiviran",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_CHANGE_PLAN' => "PROMENA PAKETA",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_BUY_PLAN' => "KUPI PAKET",

    // Nutrition Fragment
    'AND_NUTRITION_FRAGMENT_TAB_MY_NUTRITION' =>"Moj plan ishrane",
    'AND_NUTRITION_FRAGMENT_TAB_NUTRITION' =>"Ishrana",
    'AND_NUTRITION_FRAGMENT_EMPTY_DATA' =>"Nijedan program ishrane još uvek nije aktiviran.",
    'AND_NUTRITION_FRAGMENT_FREE_PROGRAM' => "Besplatan program",

    // Program info dialog
    'AND_PROGRAM_INFO_DIALOG_LIKE' =>"Kliknite 'Sviđa mi se'",
    'AND_PROGRAM_INFO_DIALOG_DISLIKE' =>"Kliknite 'Ne sviđa mi se'",
    'AND_PROGRAM_INFO_DIALOG_RATE' =>"Ocenite ovaj program",
    'AND_PROGRAM_INFO_DIALOG_RESET' =>"Resetujte svoj napredak",

    // Rate Program Dialog
    'AND_RATE_PROGRAM_DIALOG_TITLE' =>"Ocenite ovaj program",
    'AND_RATE_PROGRAM_DIALOG_CLOSE' =>"Zatvori",
    'AND_RATE_PROGRAM_DIALOG_SUBMIT' =>"Oceni",

    // Reset Program Dialog
    'AND_RESET_PROGRAM_DIALOG_TITLE' =>"Resetujte svoj napredak",
    'AND_RESET_PROGRAM_DIALOG_DESCRIPTION' =>"Da li ste sigurni da želite da resetujete\nsvoj napredak na ovom programu?",
    'AND_RESET_PROGRAM_DIALOG_SUBMIT' =>"Resetuj napredak",
    'AND_RESET_PROGRAM_DIALOG_CLOSE' =>"Zatvori",

    // Exercises Fragment
    'AND_EXERCISES_FRAGMENT_TITLE' =>"Vežbe",
    'AND_EXERCISES_FRAGMENT_SUBTITLE' =>"Izaberite željenu grupu mišića",

    // Exercises Info Activity
    'AND_EXERCISES_INFO_ACTIVITY_DESCRIPTION_TITLE' =>"Opis vežbe",

    // Exercises List Activity
    'AND_EXERCISES_LIST_ACTIVITY_TITLE' =>"%s vežbi",
    'AND_EXERCISES_LIST_ACTIVITY_SEARCH_HINT_TEXT' =>"Pretražite vežbe...",

    // Nutrition Programs Fragment
    'AND_NUTRITION_PROGRAMS_FRAGMENT_TITLE' =>"Programi ishrane",
    'AND_NUTRITION_PROGRAMS_SEARCH_HINT_TEXT' =>"Pretražite programe...",

    // Workout Programs Fragment
    'AND_WORKOUT_PROGRAMS_FRAGMENT_TITLE' =>"Programi vežbi",
    'AND_WORKOUT_PROGRAMS_SEARCH_HINT_TEXT' =>"Pretražite programe...",

    // Workout  Fragment
    'AND_WORKOUT_FRAGMENT_TAB_MY_PROGRAMS' =>"Moji programi",
    'AND_WORKOUT_FRAGMENT_TAB_PROGRAMS' =>"Programi",
    'AND_WORKOUT_FRAGMENT_FREE_PROGRAM' => "Besplatan program",

    // Exercises Fragment
    'AND_EXERCISES_FRAGMENT_EMPTY_DATA' =>"Nijedna vežba još uvek nije aktivirana.",

    // Workout Fragment
    'AND_WORKOUT_FRAGMENT_EMPTY_DATA' =>"Nijedan trening još uvek nije aktiviran.",

    // Meal Fragment
    'AND_MEAL_FRAGMENT_EMPTY_DATA' =>"Nijedan program još uvek nije aktiviran.",

    // Meal Info Activity
    'AND_MEAL_INFO_ACTIVITY_DESCRIPTION_TITLE' =>"Opis",
    'AND_MEAL_INFO_ACTIVITY_NUTRITION_INFO' =>"Nutritivni podaci",

    'AND_MEAL_INFO_ACTIVITY_PROTEINS' =>"Proteini",
    'AND_MEAL_INFO_ACTIVITY_CARBOHYDRATES' => "Ugljeni hidrati",
    'AND_MEAL_INFO_ACTIVITY_FAT' => "Masti",
    'AND_MEAL_INFO_ACTIVITY_GRAM' => "g",
    'AND_MEAL_INFO_ACTIVITY_PREPARE_TITLE' => "Kako da pripremite ovaj obrok?",
    'AND_MEAL_INFO_ACTIVITY_INGREDIENTS_TITLE' => "Sastojci",
    'AND_MEAL_INFO_ACTIVITY_MEALS_PER_DAYS_NOT_PERMISSION' => "Prvo morate aktivirati program.",

// TODO Refaktorisati imena kljuceva na APIU (Obrisati prefix Workout) > ProgramInfoActivity
// Program Info Activity
    'AND_PROGRAM_INFO_ACTIVITY_ADD_COMMENT_NOT_PERMISSION' => "Da biste ostavili komentar, morate prvo aktivirati program.",
    'AND_PROGRAM_INFO_ACTIVITY_SUCCESS_ADD_COMMENT' => "Vaš komentar će biti objavljen nakon odobrenja.",
    'AND_PROGRAM_INFO_ACTIVITY_ADD_COMMENT_TITLE' => "Dodajte komentar ...",
    'AND_PROGRAM_INFO_ACTIVITY_COMMENTS_TITLE' => "Komentari (%s)",
    'AND_PROGRAM_INFO_ACTIVITY_SUCCESS_LIKE_PROGRAM' => "Uspešno ste kliknuli 'sviđa mi se'.",
    'AND_PROGRAM_INFO_ACTIVITY_SUCCESS_DISLIKE_PROGRAM' => "Upešno ste kliknuli 'ne sviđa mi se'.",
    'AND_PROGRAM_INFO_ACTIVITY_SUCCESS_RESET_PROGRAM' => "Uspešno ste resetovali ovaj program.",
    'AND_PROGRAM_INFO_ACTIVITY_SUCCESS_RATE_PROGRAM' => "Uspešno ste ocenili ovaj program.",
    'AND_PROGRAM_INFO_ACTIVITY_DESCRIPTION_TITLE' => "Opis",
    'AND_PROGRAM_INFO_ACTIVITY_BASIC_INFO_TITLE' => "Osnovne informacije",
    'AND_PROGRAM_INFO_ACTIVITY_PROGRAM_LENGTH_TITLE' => "Dužina programa",
    'AND_PROGRAM_INFO_ACTIVITY_WORKOUT_DAYS_TITLE' => "Dani za vežbanje",
    'AND_PROGRAM_INFO_ACTIVITY_BUY_PROGRAM_BTN' => "Ativirajte ovaj program",
    'AND_PROGRAM_INFO_ACTIVITY_CONTINUE_PROGRAM_BTN' => "Nastavite sa napretkom",
    'AND_PROGRAM_INFO_ACTIVITY_SUCCESS_BUY_PROGRAM' => "Uspešno ste aktivirali program.",
    'AND_PROGRAM_INFO_ACTIVITY_IS_LOCK' => "Trening je zaključan!",
    'AND_PROGRAM_INFO_ACTIVITY_IS_FINISHED' => "Završili ste trening!",
    'AND_PROGRAM_INFO_ACTIVITY_IS_REST_DAY' => "Dan za odmor!",
    'AND_PROGRAM_INFO_ACTIVITY_SUCCESS_DEACTIVATE_PROGRAM' => "Program je uspešno deaktiviran",
    'AND_PROGRAM_INFO_ACTIVITY_DEACTIVATE_PROGRAM_FAILED' => "Deaktivacija nije uspela",
    'AND_PROGRAM_INFO_ACTIVITY_CHANGE_PLAN' => "PROMENA PAKETA",
    'AND_DEACTIVATE_PROGRAM_DIALOG_TITLE' => "Deaktivacija programa",
    'AND_DEACTIVATE_PROGRAM_DIALOG_DESCRIPTION' => "Da li ste sigurni da želite\n da deaktivirate ovaj program?",
    'AND_DEACTIVATE_PROGRAM_DIALOG_SUBMIT' => "Deaktiviraj program",
    'AND_PROGRAM_INFO_DIALOG_DEACTIVATE' => "Deaktivacija programa",
    'AND_PROGRAM_INFO_ACTIVITY_BUY_PLAN' => "KUPI PAKET",

// Dialog Update App
    'AND_DIALOG_UPDATE_APP_TITLE' => "Najnovija verzija je dostupna",
    'AND_DIALOG_UPDATE_APP_MESSAGE' => "Najnovija verzija aplikacije je \n dostupna na Google Play.",
    'AND_DIALOG_UPDATE_APP_DONT_SHOW_TEXT' => "Ne prikazuj ovu poruku ponovo",
    'AND_DIALOG_UPDATE_APP_POSITIVE_BUTTON' => "Najnovija verzija",
    'AND_DIALOG_UPDATE_APP_NEGATIVE_BUTTON' => "Zatvori",

// Nutrition Survey Dialog
    'AND_DIALOG_NUTRITION_SURVEY_DIALOG_TITLE' => "Personalizovana ishrana",
    'AND_DIALOG_NUTRITION_SURVEY_DIALOG_DESCRIPTION' => "Popunite upitnik o ishrani kako \nbismo vam preporučili odgovarajući plan ishrane.",
    'AND_DIALOG_NUTRITION_SURVEY_DIALOG_SUBMIT_BTN_TEXT' => "Počnite",
    'AND_DIALOG_NUTRITION_SURVEY_DIALOG_CLOSE' => "Možda kasnije",

// Nutrition Question Fragment
    'AND_NUTRITION_QUESTION_FRAGMENT_NEXT_BTN' => "Dalje",
    'AND_NUTRITION_QUESTION_FRAGMENT_REQUIRED' => "Neophodno je obeležiti barem jedno polje!",

// Nutrition Survey Activity
    'AND_NUTRITION SURVEY ACTIVITY' => "Promene su uspešno sačuvane.",

// Training Activity
    'AND_TRAINING_ACTIVITY_SUBMIT_BTN_TEXT' => "Počni trening",
    'AND_TRAINING_ACTIVITY_SLASH_TEXT' => " / ",
    'AND_TRAINING_ACTIVITY_CURRENT_WEEK_TEXT' => "Nedelja ",
    'AND_TRAINING_ACTIVITY_CURRENT_DAY_TEXT' =>  " / Dan ",
    'AND_TRAINING_ACTIVITY_ROUND_TEXT' => "Runda ",
    'AND_TRAINING_ACTIVITY_START_WORKOUT_TEXT' => "Počni trening",
    'AND_TRAINING_ACTIVITY_SET_TAB_TEXT' => "Set ",
    'AND_TRAINING_ACTIVITY_ROUND_OF_TEXT' => " od ",
    'AND_TRAINING_ACTIVITY_BUY_PLAN_TEXT' => "Kupi paket",

// TODO NEW NEW NEW
// Reset Training Dialog
    'AND_RESET_TRAINING_DIALOG_RESET_TITLE_TEXT' => "Resetuj dan za trening",
    'AND_RESET_TRAINING_DIALOG_CHECK_TITLE_TEXT' => "Završi dan za trening",

    'AND_RESET_TRAINING_DIALOG_RESET_DESCRIPTION_TEXT' => "Da li ste sigurni da želite da resetujete \nnapredak na ovaj dan za trening?",
    'AND_RESET_TRAINING_DIALOG_CHECK_DESCRIPTION_TEXT' => "Da li ste sigurni da želite \nda završite ovaj dan za trening?",
    'AND_RESET_TRAINING_DIALOG_RESET_SUBMIT_BTN_TEXT' => "Resetuj dan za trening",
    'AND_RESET_TRAINING_DIALOG_CHECK_SUBMIT_BTN_TEXT' => "Završi dan za trening",
    'AND_RESET_TRAINING_DIALOG_CHECK_CLOSE_TEXT' => "Zatvori",

// Edit Exercise Dialog
    'AND_EDIT_EXERCISE_DIALOG_SUBMIT_BTN' => "Sačuvaj",
    'AND_EDIT_EXERCISE_VALUE_REQUIRED' => "Polja su obavezna!",
    'AND_EDIT_EXERCISE_DIALOG_CLOSE_TEXT' => "Zatvori",

// Workout Activity
    'AND_WORKOUT_ACTIVITY_SUCCESS EDIT EXERCISE TEXT' => "Uspešno ste ažurirali podešavanja za vežbe.",
    'AND_WORKOUT_ACTIVITY_REST_TIME_TEXT' =>  "Vreme za odmor",
    'AND_WORKOUT_ACTIVITY_WORKOUT_TIME_TEXT' => "Vreme za trening",
    'AND_WORKOUT_ACTIVITY_EXERCISE_TIME_TEXT' => "Vreme za vežbu",
    'AND_WORKOUT_ACTIVITY_PAUSE_TEXT' =>  "Pauza",
    'AND_WORKOUT_ACTIVITY_FINISH_BTN_TEXT' =>  "Počnite",
    'AND_WORKOUT_ACTIVITY_DID_IT_BTN_TEXT' =>  "Uspeli ste",
    'AND_WORKOUT_ACTIVITY_SKIP_BTN_TEXT' => 'Preskoči',
    'AND_WORKOUT_ACTIVITY_CURRENT_ROUND' => '%s (Runda %s od %s)',
    'AND_WORKOUT_ACTIVITY_CURRENT_EXERCISE' => '%s (Vežba %s od %s)',
    'AND_WORKOUT_ACTIVITY_EDIT_BTN_TEXT' => 'Izmeni',

// Training Statistic Activity
    'AND_TRAINING_STATISTIC_ACTIVITY_TITLE_TEXT' => "Bravo!",
    'AND_TRAINING_STATISTIC_ACTIVITY_SUBTITLE_TEXT' => "Završili ste trening! \nProverite statistiku vežbanja.",
    'AND_TRAINING_STATISTIC_ACTIVITY_SETS_TEXT' => "Setovi",
    'AND_TRAINING_STATISTIC_ACTIVITY_EXERCISES_TEXT' => "Vežbe",
    'AND_TRAINING_STATISTIC_ACTIVITY_CALORIES_TEXT' => "Kalorije",
    'AND_TRAINING_STATISTIC_ACTIVITY_SUBMIT_BTN_TEXT' => "Završeno",

// Program Statistic Activity
    'AND_PROGRAM_STATISTIC_ACTIVITY_TITLE_TEXT' => "Čestitamo!",
    'AND_PROGRAM_STATISTIC_ACTIVITY_SUBTITLE_TEXT' => "Završili ste \n%s!\nPogledajte statistiku vežbanja",
    'AND_PROGRAM_STATISTIC_ACTIVITY_WEEKS_TEXT' => "Nedelje",
    'AND_PROGRAM_STATISTIC_ACTIVITY_WORKOUTS_TEXT' => "Vežbe",
    'AND_PROGRAM_STATISTIC_ACTIVITY_SUBMIT_BTN_TEXT' => "Završeno",

    'AND_RESEND_EMAIL_DIALOG_TITLE_TEXT' => "Vaš nalog još uvek nije aktiviran",
    'AND_RESEND_EMAIL_DIALOG_DESCRIPTION_TEXT' => "Ukoliko još uvek niste primili aktivacioni email, kliknite dugme 'Pošalji ponovo aktivacioni email' kako biste potvrdili vašu email adresu i aktivirali nalog.",
    'AND_RESEND_EMAIL_DIALOG_SUBMIT_BTN_TEXT' => "Pošalji",

// Date Format Ago
    'AND_DATE_FORMAT_SECONDS_TEXT' => "pre %s sekundi",
    'AND_DATE_FORMAT_MINUTES_TEXT' => "pre %s minuta",
    'AND_DATE_FORMAT_HOURS_TEXT' => "pre %s sati",
    'AND_DATE_FORMAT_DAYS_TEXT' => "pre %s dana",

// User Survey
    'AND_USER_SURVEY_LEVELS_TITLE_TEXT' => "Koji je vaš nivo?",
    'AND_USER_SURVEY_LEVELS_DESCRIPTION_TEXT' => "Opišite svoj trenutni nivo važbanja.",
    'AND_USER_SURVEY_GOALS_TITLE_TEXT' => "Koji je vaš cilj?",
    'AND_USER_SURVEY_GOALS_DESCRIPTION_TEXT' => "Recite nam koji cilj želite d apostignete.",
    'AND_USER_SURVEY_ENVIRONMENT_TITLE_TEXT' => "Koje je vaše okruženje za vežbanje?",
    'AND_USER_SURVEY_ENVIRONMENT_DESCRIPTION_TEXT' => "Odaberite gde biste želeli da vežbate.",

    // Finish Workout Dialog
    "AND_FINISH_WORKOUT_DIALOG_TITLE_TEXT" => "Finish your workout",
    "AND_FINISH_WORKOUT_DIALOG_SUBTITLE_TEXT" => "Are you sure you want to \nfinish your workout?",
    "AND_FINISH_WORKOUT_DIALOG_SUBMIT_BTN_TEXT" => "Yes",
    "AND_FINISH_WORKOUT_DIALOG_CLOSE_BTN_TEXT" => "No",

    "AND_ROUND_TEXT" => " runda",
    "AND_ROUND_TO_FIVE_TEXT" => " runde",

    "AND_BASIC_QUESTIONAIRE" => require( realpath(__DIR__ . '/basic-questionare.php') )
];
