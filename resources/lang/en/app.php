<?php

use Illuminate\Support\Facades\Config;

return [
    'language_iso_code' => Config::get( 'language' )['iso'],
    'language_name' => Config::get( 'language' )['language'],

    // -------------------------------------------------------
    // iOS ---------------------------------------------------
    // -------------------------------------------------------

    'iOS_apiLink' => 'https://api.infinityfit.io',
    'iOS_loggingEnabled' => true,

    // GENERAL
    'iOS_buildVersion'=>101,
    'iOS_transVersion'=>'3',
    'iOS_noInternetConnection'=>'No Internet connection!',
    'iOS_userUnauthorized'=>'Unauthorized user',
    'iOS_requestErrorMessage'=>'Please update application, an error has occurred',
    'iOS_internalServerError'=>'Internal Server Error',
    'iOS_success'=>'Success!',
    // -------------------------------------------------------

    // ALERTS
    'iOS_ALERT_positive'=>'Yes',
    'iOS_ALERT_confirm'=>'OK',
    'iOS_ALERT_negative'=>'No',
    'iOS_ALERT_cancel'=>'Cancel',
    'iOS_ALERT_dismiss'=>'Close',

    'iOS_ALERT_resendSuccessTitle'=>'Link Sent',
    'iOS_ALERT_resendSuccessMessage'=>'Activation link was successfully sent to your email',

    'iOS_ALERT_questionnaireCompletedTitle'=>'Questionnaire Completed',
    'iOS_ALERT_questionnaireCompletedMessage'=>'Thank you for completing the questionnaire, this will help us suggest the right programs to you.',
    'iOS_ALERT_questionnaireINcompletedMessage'=>'Questionnaire incomplete, please check your answers.',
    'iOS_ALERT_passwordChangeSuccess'=>'Questionnaire incomplete, please check your answers.',
    'iOS_ALERT_notAuthorizedTitle'=>'Your account hasn\'t been activated yet',
    'iOS_ALERT_notAuthorizedDescription'=>'If you haven\'t received the activation email click the Resend Activation Email button to confirm your email address and activate your account.',
    // -------------------------------------------------------

    // ERRORS -------------------------------------------------------
    'iOS_ERROR_default'=>'Error',
    'iOS_ERROR_emptyFieldError'=>'Required field',
    'iOS_ERROR_numberFieldError'=>'You must enter a number',
    'iOS_ERROR_emptyCommentFieldError'=>'Comment field is required',
    'iOS_ERROR_unableToPostComment'=>'We are currently unable to post your comment.',
    'iOS_ERROR_invalidEmailFormat'=>'Invalid email address format',
    'iOS_ERROR_emptyEmailField'=>'Email field is required',
    'iOS_ERROR_codeIncomplete'=>'Not all numbers are entered.',
    'iOS_ERROR_emptyPasswordField'=>'Password field is required',
    'iOS_ERROR_passwordMissmatch'=>'Password does not match',
    'iOS_ERROR_emptyUsernameField'=>'Username field is required',
    'iOS_ERROR_userInfoReadingError'=>'An error occurred while getting the user info.',
    'iOS_ERROR_503errorTitle'=>'Requested page does not exists.',
    'iOS_ERROR_503errorMessage'=>'Try again later.',
    // -------------------------------------------------------

    // FIELD
    'iOS_FIELD_next'=>'Next',
    'iOS_FIELD_emailFieldTitle'=>'Email',
    'iOS_FIELD_passwordFieldTitle'=>'Password',
    'iOS_FIELD_passwordConfirmFieldTitle'=>'Confirm Password',
    'iOS_FIELD_firstNameTitle'=>'First Name',
    'iOS_FIELD_lastNameTitle'=>'Last Name',
    'iOS_FIELD_usernameTitle'=>'Username',
    'iOS_FIELD_verificationCodeTitle'=>'Verification Code',
    'iOS_FIELD_newPasswordTitle'=>'New password',
    'iOS_FIELD_confirmNewPasswordTitle'=>'Confirm new password',
    'iOS_FIELD_nutritionInfo'=>'Nutrition Info',
    'iOS_FIELD_ingredients'=>'Ingredients',
    'iOS_FIELD_changePass'=>'Change your password',
    'iOS_FIELD_oldPass'=>'Old Password',
    'iOS_FIELD_newPass'=>'New Password',
    'iOS_FIELD_confirmPass'=>'Confirm Password',
    'iOS_FIELD_comments'=>'Comments: (%d) ',
    'iOS_FIELD_addComment'=>'Add a comment...',
    'iOS_FIELD_browseAvailable'=>'%@ exercises',
    'iOS_FIELD_browseMeals'=>'%@',
    'iOS_FIELD_review'=>'review',
    'iOS_FIELD_likeThis'=>'Like this program',
    'iOS_FIELD_reset'=>'Reset your progress',
    'iOS_FIELD_round'=>'round',
    'iOS_FIELD_rounds'=>'rounds',
    'iOS_FIELD_surveyAlertTitle'=>'Get personalized nutrition',
    'iOS_FIELD_surveyAlertBody'=>'Complete our nutrition survey so we can recommend you the best nutrition plans.',
    'iOS_FIELD_finishWorkoutDay'=>'Finish workout day',
    'iOS_FIELD_resendActivationLink'=>'RESEND ACTIVATION LINK',
    'iOS_FIELD_areYouSureFinish'=>'Are you sure you want to finish this workout day?',
    'iOS_FIELD_areYouSureProgress'=>'Are you sure you want to reset progress on this workout program?',
    'iOS_FIELD_areYouSureProgressDay'=>'Are you sure you want to reset progress on this workout day?',
    'iOS_FIELD_deactivateThisProgram'=>'Deactivate this program',
    'iOS_FIELD_deactivate'=>'DEACTIVATE PROGRAM',
    'iOS_FIELD_areYouSureDeactivate'=>'Are you sure you want to deactivate this program?',
    // -------------------------------------------------------

    // MISC
    'iOS_FIELD_day'=>'day',
    'iOS_FIELD_days'=>'days',
    'iOS_FIELD_meal'=>'meal',
    'iOS_FIELD_meals'=>'meals',
    'iOS_FIELD_rep'=>'rep',
    'iOS_FIELD_reps'=>'reps',
    'iOS_FIELD_reviews'=>'reviews',
    'iOS_FIELD_workoutDays'=>'Workout Days',
    'iOS_FIELD_mealsPerDay'=>'Meals Per Day',
    'iOS_FIELD_description'=>'Description',
    'iOS_FIELD_basicInfo'=>'Basic Info',
    'iOS_FIELD_programLength'=>'Program Length',
    'iOS_FIELD_viewAllComments'=>'View all comments',
    'iOS_FIELD_rateThisProgram'=>'Rate this program',
    'iOS_FIELD_close'=>'Close',
    'iOS_FIELD_rate'=>'RATE',
    'iOS_FIELD_workoutDescription'=>'Exercise description',
    'iOS_FIELD_proteins'=>'Proteins',
    'iOS_FIELD_carbs'=>'Carbs',
    'iOS_FIELD_fats'=>'Fats',
    'iOS_FIELD_step'=>'Step',
    'iOS_FIELD_change'=>'Change',
    'iOS_FIELD_freeWorkoutTitle'=>'Free Workout',
    'iOS_FIELD_of'=>'of',
    'iOS_FIELD_searchPlaceholder'=>'Search...',
    'iOS_FIELD_week'=>'week',
    'iOS_FIELD_weeks'=>'weeks',
    'iOS_FIELD_completed'=>'completed',
    'iOS_FIELD_set'=>'Set',
    'iOS_FIELD_roundsOrder'=>'ROUND %@ OF %@',
    // -------------------------------------------------------

    // BUTTON
    'iOS_BUTTON_signInApple'=>'Sign in with Apple',
    'iOS_BUTTON_signInGoogle'=>'Sign in with Google',
    'iOS_BUTTON_signIn'=>'SIGN IN',
    'iOS_BUTTON_createAccountBtn'=>'CREATE ACCOUNT',
    'iOS_BUTTON_sendEmail'=>'SEND EMAIL',
    'iOS_BUTTON_confirm'=>'CONFIRM',
    'iOS_BUTTON_resetPassword'=>'RESET PASSWORD',
    'iOS_BUTTON_finish'=>'FINISH',
    'iOS_BUTTON_seeAll'=>'See all',
    'iOS_BUTTON_signOut'=>'SIGN OUT',
    'iOS_BUTTON_continue'=>'CONTINUE',
    'iOS_BUTTON_submit'=>'SUBMIT',
    'iOS_BUTTON_activateProgram'=>'ACTIVATE THIS PROGRAM',
    'iOS_BUTTON_mealsList'=>'GO TO MEALS LIST',
    'iOS_BUTTON_continueProgress'=>'CONTINUE YOUR PROGRESS',
    'iOS_BUTTON_startSurvey'=>'START SURVEY',
    'iOS_BUTTON_resendActivationEmail'=>'Resend Activation Email',
    // -------------------------------------------------------

    // LOGIN
    'iOS_LOGIN_signInTitle'=>'Sign In',
    'iOS_LOGIN_signInOption'=>'or sign in with email',
    'iOS_LOGIN_forgotPasswordLabel'=>'Forgot Password?',
    'iOS_LOGIN_noAccountLabel'=>'I don’t have an account.',
    // -------------------------------------------------------

    // CREATE ACCOUNT
    'iOS_CREATE_createAccountTitle'=>'Create Account',
    'iOS_CREATE_alreadyHaveAccountLabel'=>'I already have an account.',
    'iOS_CREATE_successMessage'=>'You are almost done! Check your email for verification code.',
    // -------------------------------------------------------

    // FORGOT PASS
    'iOS_FORGOT_forgotPasswordTitle'=>'Forgot Password',
    'iOS_FORGOT_forgotPasswordSubTitle'=>'We will send you a verification code via email.',
    // -------------------------------------------------------

    // VERIFICATION CODE
    'iOS_VERIFICATION_verificationCodeTitle'=>'Verification Code',
    'iOS_VERIFICATION_verificationCodeSubTitle'=>' Enter the verification code we have sent you.',
    // -------------------------------------------------------

    // RESET PASSWORD
    'iOS_RESET_resetPasswordTitle'=>'Reset Password',
    'iOS_RESET_resetPasswordSubTitle'=>'Set and confirm your new password.',
    'iOS_RESET_resetPasswordSuccessTitle'=>'You have successfully reset your password!',
    // -------------------------------------------------------

    // HOME SCREEN
    'iOS_HOME_welcomeMessagePrefix'=>'Welcome back,',
    'iOS_HOME_recommendedWorkoutsTitle'=>'Recommended workout programs',
    'iOS_HOME_continueProgressTitle'=>'Continue with your progress',
    'iOS_HOME_recommendedNutritionTitle'=>'Recommended nutrition programs',
    'iOS_HOME_blogSectionTitle'=>'See our latest blog posts',
    // -------------------------------------------------------

    // PROGRAMS
    'iOS_PROGRAMS_myProgramsTabTitle'=>'My Programs',
    'iOS_PROGRAMS_programsTabTitle'=>'Programs',
    'iOS_PROGRAMS_headerTitleLabel'=>'Search available workout programs',
    'iOS_PROGRAMS_noBoughtPrograms'=>'You have not activated any program.',
    'iOS_PROGRAMS_exercise'=>'Exercise',
    'iOS_PROGRAMS_round'=>'Round',
    'iOS_PROGRAM_isFree'=>'Free plan',
    // -------------------------------------------------------

    // NUTRITION
    'iOS_NUTRITION_myNutritionTabTitle'=>'My Nutrition',
    'iOS_NUTRITION_nutritionTabTitle'=>'Nutrition',
    'iOS_NUTRITION_headerTitleLabel'=>'Search available nutrition programs',
    // -------------------------------------------------------

    // PROFILE
    'iOS_PROFILE_profileInfoTab'=>'Profile Info',
    'iOS_PROFILE_profileInfoSectionHeader'=>'Profile Info',
    'iOS_PROFILE_firstNameCellTitle'=>'First Name',
    'iOS_PROFILE_lastNameCellTitle'=>'Last Name',
    'iOS_PROFILE_dateOfBirthCellTitle'=>'Date of Birth',
    'iOS_PROFILE_genderCellTitle'=>'Gender',
    // -------------------------------------------------------
    'iOS_PROFILE_acountInfoSectionHeader'=>'Account Info',
    'iOS_PROFILE_usernameCellTitle'=>'Username',
    'iOS_PROFILE_emailCellTitle'=>'Email',
    'iOS_PROFILE_passwrodCellTitle'=>'Password',
    'iOS_PROFILE_languageCellTitle'=>'Language',
    'iOS_PROFILE_languageModalTitle'=>'Change language',
    // -------------------------------------------------------
    'iOS_PROFILE_aboutSectionHeader'=>'About',
    'iOS_PROFILE_privacyCellTitle'=>'Privacy',
    'iOS_PROFILE_termsOfUseCellTitle'=>'Terms Of Use',
    'iOS_PROFILE_sourcesCellTitle'=>'Recommendation',
    'iOS_PROFILE_cookiePolicyCellTitle'=>'Cookie Policy',
    // -------------------------------------------------------
    'iOS_PROFILE_statisticsTab'=>'Statistics',
    'iOS_PROFILE_profileSummaryTitle'=>'Profile summary',
    'iOS_PROFILE_heightLabel'=>'centimeters',
    'iOS_PROFILE_weightLabel'=>'kilograms',
    'iOS_PROFILE_yearsLabel'=>'years',
    'iOS_PROFILESTATS_programsTitle'=>'Programs',
    'iOS_PROFILESTATS_weeksTitle'=>'Weeks',
    'iOS_PROFILESTATS_workoutsTitle'=>'Workouts',
    // -------------------------------------------------------
    'iOS_PROFILE_weightStatsTitle'=>'Weight statistics',
    'iOS_PROFILE_initial'=>'Initial',
    'iOS_PROFILE_current'=>'Current',
    'iOS_PROFILE_differece'=>'Difference',
    'iOS_PROFILE_addWeight'=>'Add Weight',
    'iOS_PROFILE_workoutActivity'=>'Workout activity',
    // -------------------------------------------------------
    'iOS_PROFILE_subscriptionInfo'=>'Subscription info',
    'iOS_PROFILE_activePlanLabel'=>'Active Plan',
    'iOS_PROFILE_changePlanLabel'=>'Change plan',
    'iOS_PROFILE_noPlanLabel'=>'Free plan',

    // SURVEY
    'iOS_SURVEY_q1Title'=>'What is your level?',
    'iOS_SURVEY_q1SubTitle'=>'Describe your current workout level.',
    'iOS_SURVEY_q2Title'=>'What is your goal?',
    'iOS_SURVEY_q2SubTitle'=>'Tell us what goal you want to achieve.',
    'iOS_SURVEY_q3Title'=>'What is your workout enviroment?',
    'iOS_SURVEY_q3SubTitle'=>'Choose where you prefer to workout.',
    'iOS_SURVEY_q3answer1Title'=>'Gym',
    'iOS_SURVEY_q3answer1SubTitle'=>'Practice at a gym',
    'iOS_SURVEY_q3answer2Title'=>'Home',
    'iOS_SURVEY_q3answer2SubTitle'=>'Practice at home',
    'iOS_SURVEY_q3answer3Title'=>'Outdoors',
    'iOS_SURVEY_q3answer3SubTitle'=>'Practice outdoors',
    'iOS_SURVEY_q4Title'=>'What is your gender?',
    'iOS_SURVEY_q4SubTitle'=>'',
    'iOS_SURVEY_q5Title'=>'How old are you?',
    'iOS_SURVEY_q5SubTitle'=>'Fitness approaches differ by age.',
    'iOS_SURVEY_q6Title'=>'How tall are you?',
    'iOS_SURVEY_q6SubTitle'=>'Enter your current height.',
    'iOS_SURVEY_q7Title'=>'What is your weight?',
    'iOS_SURVEY_q7SubTitle'=>'Enter your current weight.',
    // -------------------------------------------------------

    // PAYMENT
    'iOS_PAYMENT_activePlanLabel'=>'ACTIVE PLAN',
    'iOS_PAYMENT_deactivatePlan'=>'DEACTIVATE PLAN',
    'iOS_PAYMENT_nextPaymentLabel'=>'Your current plan is valid until %@.',
    'iOS_PAYMENT_headerTitleLabel'=>'Start your fitness journey today with Infinity Fit Pro!',
    'iOS_PAYMENT_selectMethodLabel'=>'Select payment method',
    'iOS_PAYMENT_appleMethodLabel'=>'In-App purchase',
    'iOS_PAYMENT_creditCardMethodLabel'=>'Credit or debit card',
    'iOS_PAYMENT_smsMethodLabel'=>'SMS',
    'iOS_PAYMENT_FromPaymentLabel'=>'From',
    'iOS_PAYMENT_successTitleAlert'=>'Your request received successfully!',
    'iOS_PAYMENT_successSubTitleAlert'=>'We will send you an e-mail when your plan is activated.',
    'iOS_PAYMENT_backToPlansLabel'=>'BACK TO PLANS',
    'iOS_PAYMENT_deactivatePlanTitle'=>'Deactivate plan',
    'iOS_PAYMENT_deactivatePlanSubTitle'=>'Are you sure you want to deactivate your plan?',
    'iOS_PAYMENT_deactivatePlanConfirm'=>'Yes, deactivate my plan',
    'iOS_PAYMENT_planDeactivateSuccessTitle'=>'Plan deactivated',
    'iOS_PAYMENT_planDeactivateSuccessSubTitle'=>'You have successfully deactivated your plan.',
    'iOS_PAYMENT_planActiveUntilLabel'=>'Your plan has been deactivated and will be vaild until %@',
    'iOS_PAYMENT_changePlan'=>'CHANGE YOUR PLAN',
    'iOS_PAYMENT_buyPlan'=>'BUY A PLAN',
    'iOS_PAYMENT_noReceiptError'=>'There was an error while verifying your package receipt, please try again later.',
    'iOS_PAYMENT_subscriptionInfo'=>'Subscribing to this app you agree to the Infinity Fit Term and Conditions. Subscriptions renew within 24 hours before the ending of subscription period. You will be charged through your selected payment method. You can manage and edit your subscription in the Profile section.%@%@By purchasing a subscription via in-app subscription, you agree to the App Store terms and conditions.',
    'iOS_PAYMENT_monthlySmallLabel'=>' (%@%@/month)',
    'iOS_PAYMENT_nextPaymentLabelTrial'=>'Your 14-day free trial ends on %@.',
    'iOS_PAYMENT_activeTrialPlanLabel'=>'ACTIVE FREE TRIAL',
    'iOS_PAYMENT_cannotActivateLabel'=>'If you want to activate this subscription plan, please wait until your current plan expires or deactivate it first.',
    'iOS_PAYMENT_activateAPlan'=>'Activate a Plan',
    'iOS_PAYMENT_Buy'=>'Buy Now',
    'iOS_PAYMENT_restorePurchase' => 'Restore Purchases',
    'iOS_PAYMENT_restoringPurchasesTitle' => 'Restoring Purchases',
    'iOS_PAYMENT_restoringPurchasesMessage' => 'We will attempt to restore previously purchased products. If you have an active subscription, your active plan will update.',

    // UNITS
    'iOS_UNITS_ago'=>'ago',
    'iOS_UNITS_minute'=>'minute',
    'iOS_UNITS_minutes'=>'minutes',
    'iOS_UNITS_hour'=>'hour',
    'iOS_UNITS_hours'=>'hours',
    'iOS_UNITS_moment'=>'a moment ago',

    // ONBOARD
    'iOS_ONBOARD_q1Title'=>'Get tailor-made programs',
    'iOS_ONBOARD_q1subTitle'=>'Based on your personal info and goals we will recommend you programs that best suit your needs.',
    'iOS_ONBOARD_q2Title'=>'Track your progress',
    'iOS_ONBOARD_q2subTitle'=>'Check out workout statistics and see how much progress you have made so far.',
    'iOS_ONBOARD_q3Title'=>'Tons of programs and workouts',
    'iOS_ONBOARD_q3subTitle'=>'InfinityFit offers an array of workout and nutrition programs as well as exercises.',

    // -------------------------------------------------------
    'iOS_FIELD_weight'=>'weight',
    'iOS_FIELD_height'=>'height',
    'iOS_WORKOUT_edit'=>'EDIT',
    'iOS_WORKOUT_start' => 'START WORKOUT',
    'iOS_genderMale' => 'Male',
    'iOS_genderFemale' => 'Female',

    'WEB_CONFIRMATION_EMAIL_SUBJECT' => 'InfinityFit Signup verification email',
    // -------------------------------------------------------
    // Android -----------------------------------------------
    // -------------------------------------------------------
    'AND_VERSION_CODE' => "50",
    'AND_NO_INTERNET_CONNECTION' => "No Internet connection!",
    'AND_USER_IS_NOTU_AUTHORIZED_TOAST_MESSAGE' =>  "Unauthorized user.",
    'AND_TOO_MANY_REQUESTS_TOAST_MESSAGE' => "Too Many Requests",
    'AND_REQUEST_RESPONSE_ERROR_MSG' =>  "An error has occurred: ",
    'AND_INTERNAL_SERVER_ERROR_MSG' =>  "Internal Server Error",
    'AND_LOGIN_REQUIRED_MSG' => "You need to be logged in to perform this action.",
    'AND_REVIEWS_TEXT' => " reviews",
    'AND_WEEKS_TEXT' =>  " weeks",
    'AND_DAYS_TEXT' => " days",
    'AND_MEALS_TEXT' => " meals",
    'AND_MIN_TEXT' =>  " min",
    'AND_MINUS_TEXT' =>  " - ",
    'AND_KCAL_TEXT' => " kcal",
    'AND_WEEK_TEXT' => "Week ",
    'AND_REPS_TEXT' => " reps ", // TODO SETOVATI OVO NEKAKO U SAM MODEL
    'AND_KILOGRAM_TEXT' => "kg",
    'AND_STRENGTH_PERCENTAGE_TEXT' =>"%",  // TODO SETOVATI OVO NEKAKO U SAM MODEL
    'AND_ROUNDS_TEXT' =>" rounds",
    'AND_SHARE_LINK_TEXT' =>"https://play.google.com/store/apps/details?id=rs.telego.fitness", // TODO URL for play store app
    'AND_REST_DAY_TITLE_TEXT' =>"Rest day",
    'AND_REST_DAY_SUBTITLE_TEXT' => "Rest day",
    'AND_IN_PROGRESS_TEXT' =>  " (in progress)",
    'AND_IN_CLOSE_TEXT' => "Close",
    'AND_DAY_TEXT' => "Day ",
    'AND_SECONDS_TEXT' => "s",
    'AND_COMPLETED_TIMES_TEXT' => "x",

    // Instruction Activity
    'AND_INSTRUCTION_ACTIVITY_SIGN_IN_BTN_TEXT' => 'Next',
    'AND_INSTRUCTION_ACTIVITY_SIGN_UP_BTN_TEXT' =>"CREATE ACCOUNT",
    'AND_INSTRUCTION_ACTIVITY_LOGIN_TEXT' =>"I already have an account.",
    'AND_INSTRUCTION_ACTIVITY_PAGE_ONE_TITLE' =>"Get tailor-made programs",
    'AND_INSTRUCTION_ACTIVITY_PAGE_ONE_MESSAGE' =>"Based on your personal info and goals we will \n recommend you programs that best suit your needs.",
    'AND_INSTRUCTION_ACTIVITY_PAGE_TWO_TITLE' =>"Track your progress",
    'AND_INSTRUCTION_ACTIVITY_PAGE_TWO_MESSAGE' =>"Check out workout statistics and see how much progress you have made so far.",
    'AND_INSTRUCTION_ACTIVITY_PAGE_THREE_TITLE' =>"Tons of programs and workouts",
    'AND_INSTRUCTION_ACTIVITY_PAGE_THREE_MESSAGE' =>"InfinityFit offers an array of workout and nutrition programs as well as exercises.",

    // Login Activity
    'AND_LOGIN_ACTIVITY_TITLE_TEXT' =>"Sign in",
    'AND_LOGIN_ACTIVITY_GOOGLE_TITLE_TEXT' =>"Sign in with Google",
    'AND_LOGIN_ACTIVITY_SING_IN_OR_EMAIL_TEXT' =>"or sign in with email",
    'AND_LOGIN_ACTIVITY_EMAIL_TEXT' =>"Email",
    'AND_LOGIN_ACTIVITY_PASSWORD_TEXT' =>"Password",
    'AND_LOGIN_ACTIVITY_FORGOT_PASSWORD_TEXT' => "Forgot password",
    'AND_LOGIN_ACTIVITY_LOGIN_BTN_TEXT' =>"LOGIN",
    'AND_LOGIN_ACTIVITY_SIGN_UP_TEXT' =>"I don`t have an account, create one now.",
    'AND_LOGIN_ACTIVITY_SUCCESS_LOGIN' =>"You have successfully signed in",
    'AND_LOGIN_REQUEST_EMAIL_REQUIRED' =>"Email field is required",
    'AND_LOGIN_REQUEST_EMAIL_WRONG_FORMAT' =>"Invalid email address format",
    'AND_LOGIN_REQUEST_PASSWORD_REQUIRED' =>"Password field is required",
    'AND_LOGIN_REQUEST_PASSWORD_WRONG_LENGTH' =>"Password must be between 8 and 30 characters long.",
    'AND_LOGIN_REQUEST_PASSWORD_WRONG_FORMAT' =>"Invalid password format",
    'AND_LOGIN_REQUEST_INVALID_LOGIN' => "Invalid login",
    'AND_LOGIN_ACTIVITY_GOOGLE_LOGIN_ERROR_TEXT' =>"Google Login Error Text",
    'AND_LOGIN_ACTIVITY_GOOGLE_TOKEN_INVALID_ERROR_TEXT' =>"Google Token Invalid Error Text",

    // Forgot Activity
    'AND_FORGOT_PASSWORD_ACTIVITY_TITLE_TEXT' =>"Forgot Password",
    'AND_FORGOT_PASSWORD_ACTIVITY_SUBTITLE_TEXT' =>"We will send a verification code to your email address.",
    'AND_FORGOT_PASSWORD_ACTIVITY_EMAIL_TEXT' =>"Email",
    'AND_FORGOT_PASSWORD_ACTIVITY_SUBMIT_BTN_TEXT' => "SEND EMAIL",
    'AND_FORGOT_PASSWORD_REQUEST_EMAIL_REQUIRED' =>"Email field is required",
    'AND_FORGOT_PASSWORD_REQUEST_EMAIL_WRONG_FORMAT' =>"Invalid email address format",
    'AND_FORGOT_PASSWORD_REQUEST_REQUSET_ON_SUCCESS_SEND_CODE' =>"Verification code was successfully sent to your email address.",

    // OTP Code Fragment
    'AND_OTP_CODE_FRAGMENT_TITLE_TEXT' =>"Verification Code",
    'AND_OTP_CODE_FRAGMENT_OTP_TITLE_TEXT' =>"Verification Code",
    'AND_OTP_CODE_FRAGMENT_SUBTITLE_TEXT' =>"Enter the verification code we have sent you.",
    'AND_OTP_CODE_FRAGMENT_RESEND_CODE_MESSAGE_TEXT' =>"Are you sure you want us to resend the OTP Code?",
    'AND_OTP_CODE_FRAGMENT_RESEND_CODE_POSITIVE_BTN_TEXT' =>"Resend",
    'AND_OTP_CODE_FRAGMENT_RESEND_CODE_NEGATIVE_BTN_TEXT' =>"Cancel",
    'AND_OTP_CODE_FRAGMENT_REQUSET_ON_SUCCESS_RESEND_CODE' => "OTP Code was successfully sent to your email address.",
    'AND_OTP_CODE_FRAGMENT_CONFIRM_SUBMIT_BTN' =>"CONFIRM",

    // Reset Password Fragment
    'AND_RESET_PASSWORD_FRAGMENT_TITLE_TEXT' =>"Reset Password",
    'AND_RESET_PASSWORD_FRAGMENT_SUBTITLE_TEXT' =>"Set and confirm your new password",
    'AND_RESET_PASSWORD_FRAGMENT_NEW_PASSWORD_TEXT' =>"New password",
    'AND_RESET_PASSWORD_FRAGMENT_CONFIRM_NEW_PASSWORD_TEXT' =>"Confirm new password",
    'AND_RESET_PASSWORD_FRAGMENT_SUBMIT_BTN_TEXT' =>"RESET PASSWORD",
    'AND_RESET_PASSWORD_FRAGMENT_REQUEST_NEW_PASSWOR_REQUIRED' =>"Field is empty",
    'AND_RESET_PASSWORD_FRAGMENT_REQUEST_NEW_PASSWORD_WRONG_LENGTH' => "Password must be between 8 and 30 characters long.'",
    'AND_RESET_PASSWORD_FRAGMENT_REQUEST_CONFIRM_PASSWOR_REQUIRED' =>"Field is empty",
    'AND_RESET_PASSWORD_FRAGMENT_REQUEST_CONFIRM_PASSWORD_WRONG_LENGTH' =>"Password must be between 8 and 30 characters long.",
    'AND_RESET_PASSWORD_FRAGMENT_REQUEST_PASSWORD_NOT_MATCH' => "Password does not match",
    'AND_RESET_PASSWORD_FRAGMENT_REQUEST_EMAIL_REQUIRED' =>"Email field is required",
    'AND_RESET_PASSWORD_FRAGMENT_REQUEST_EMAIL_WRONG_FORMAT' => "Invalid email address format",
    'AND_RESET_PASSWORD_FRAGMENT_REQUEST_OTP_CODE_REQUIRED' =>"OTP Code is required",

    // Success Reset Password Fragment
    'AND_SUCCESS_RESET_PASSWORD_FRAGMENT_DESCRIPTION_TEXT' =>"You have successfully\nreset your password!",

    // Create Account Fragment
    'AND_CREATE_ACCOUNT_FRAGMENT_TITLE_TV' =>"Create Account",
    'AND_CREATE_ACCOUNT_FRAGMENT_FIRST_NAME_TEXT_TV' =>"First Name",
    'AND_CREATE_ACCOUNT_FRAGMENT_LAST_NAME_TEXT_TV' =>"Last Name",
    'AND_CREATE_ACCOUNT_FRAGMENT_EMAIL_TEXT_TV' =>"Email",
    'AND_CREATE_ACCOUNT_FRAGMENT_USERNAME_TEXT_TV' => "Username",
    'AND_CREATE_ACCOUNT_FRAGMENT_PASSWORD_TEXT_TV' =>"Password",
    'AND_CREATE_ACCOUNT_FRAGMENT_SUBMIT_TEXT_TV' =>"CREATE ACCOUNT",
    'AND_CREATE_ACCOUNT_FRAGMENT_LOGIN_TEXT_TV' =>"I already have an account.",
    'AND_CREATE_ACCOUNT_REQUEST_FIRST_NAME_REQUIRED' =>"Required field",
    'AND_CREATE_ACCOUNT_REQUEST_LAST_NAME_REQUIRED' =>"Required field",
    'AND_CREATE_ACCOUNT_REQUEST_EMAIL_REQUIRED' =>"Email field is required",
    'AND_CREATE_ACCOUNT_REQUEST_EMAIL_WRONG_FORMAT' =>"Invalid email address format",
    'AND_CREATE_ACCOUNT_REQUEST_USERNAME_REQUIRED' =>"Required field",
    'AND_CREATE_ACCOUNT_REQUEST_PASSWORD_REQUIRED' =>"Password field is required",
    'AND_CREATE_ACCOUNT_REQUEST_PASSWORD_WRONG_LENGTH' =>"Password must be between 8 and 30 characters long.",
    'AND_CREATE_ACCOUNT_REQUEST_PASSWORD_WRONG_FORMAT' =>"Invalid password format",

    // Success Reset Password Fragment
    'AND_SUCCESS_ACCOUNT_FRAGMENT_DESCRIPTION_TEXT' =>"You are almost done! Check you email for verification code.",

    // Change password Activity
    'AND_CHANGE_PASSWORD_ACTIVITY_TITLE_TEXT' =>"Change your password",
    'AND_CHANGE_PASSWORD_ACTIVITY_OLD_PASSWORD_TEXT' =>"Old password",
    'AND_CHANGE_PASSWORD_ACTIVITY_NEW_PASSWORD_TEXT' =>"New password",
    'AND_CHANGE_PASSWORD_ACTIVITY_CONFIRM_PASSWORD_TEXT' =>"Confirm password",
    'AND_CHANGE_PASSWORD_ACTIVITY_SUBMIT_TEXT' =>"SUBMIT",
    'AND_CHANGE_PASSWORD_ACTIVITY_DESCRIPTION_TITLE_TEXT' =>"Password must contain at least",
    'AND_CHANGE_PASSWORD_ACTIVITY_DESCRIPTION_CONTENT_TEXT' =>" 8 characters\n- 1 capital letter\n- 1 number",
    'AND_CHANGE_PASSWORD_REQUEST_PASSWORD_REQUIRED' =>"Password field is required",
    'AND_CHANGE_PASSWORD_REQUEST_PASSWORD_WRONG_LENGTH' =>"Password must be between 8 and 30 characters long.",
    'AND_CHANGE_PASSWORD_REQUEST_PASSWORD_WRONG_FORMAT' =>"Invalid password format",
    'AND_CHANGE_PASSWORD_REQUEST_PASSWORD_NOT_MATCH' =>"Password does not match",
    'AND_CHANGE_PASSWORD_ACTIVITY_SUCCESS_CHANGE_PASSWORD' =>"You have successfully changed your password!",

    // Profile Activity
    'AND_PROFILE_ACTIVITY_TAB_PROFILE_INFO' =>"Profile info",
    'AND_PROFILE_ACTIVITY_TAB_STATISTICS' =>"Statistics",
    'AND_PROFILE_ACTIVITY_REQUEST_PHOTO_WRONG_EXTENSION' =>"Supported image formats are jpg, jpeg, png",
    'AND_PROFILE_ACTIVITY_REQUEST_PHOTO_WRONG_SIZE' =>"The image is too large. Max upload size is 8MB",
    'AND_PROFILE_ACTIVITY_PERMISSION_NEEDED_TEXT' =>"InfinityFit doesn't have access to your images",
    'AND_PROFILE_ACTIVITY_PERMISSION_NEEDED_MESSAGE' =>"Allow InfinityFit to access your images?",
    'AND_PROFILE_ACTIVITY_PERMISSION_NEEDED_POSITIVE_BTN' =>"Request",
    'AND_PROFILE_ACTIVITY_PERMISSION_NEEDED_NEGATIVE_BUTTON' =>"Cancel",
    'AND_PROFILE_ACTIVITY_PERMISSION_ERROR' =>"An error has occured. Choose another app to retrieve images.",
    'AND_PROFILE_ACTIVITY_PERMISSION_DENIED_TEXT' =>"Permission denied",
    'AND_PROFILE_ACTIVITY_PERMISSION_SHOW_NEVER_ASK_TEXT' =>"Permission permanently denied. Change settings.",
    'AND_PROFILE_ACTIVITY_SUCCESS_UPDATE' =>"You have successfully changed your picture!",
    'AND_PROFILE_ACTIVITY_PROFILE_PROGRAM_TITLE_TEXT' => "Program",
    'AND_PROFILE_ACTIVITY_PROFILE_WEEKS_TITLE_TEXT' => "Weeks",
    'AND_PROFILE_ACTIVITY_PROFILE_WORKOUTS_TITLE_TEXT' => "Workouts",

    // Profile Info Fragment
    'AND_PROFILE_INFO_FRAGMENT_PERSONAL_INFO_TITLE_TEXT' =>"Personal info",
    'AND_PROFILE_INFO_FRAGMENT_FIRST_NAME_TITLE_TEXT' =>"First Name",
    'AND_PROFILE_INFO_FRAGMENT_LAST_NAME_TITLE_TEXT' =>"Last Name",
    'AND_PROFILE_INFO_FRAGMENT_DATE_OF_BIRTH_TITLE_TEXT' =>"Date of Birth",
    'AND_PROFILE_INFO_FRAGMENT_GENDER_TITLE_TEXT' =>"Gender",
    'AND_PROFILE_INFO_FRAGMENT_ACCOUNT_INFO_TITLE_TEXT' =>"Account info",
    'AND_PROFILE_INFO_FRAGMENT_EMAIL_TITLE_TEXT' =>"Email",
    'AND_PROFILE_INFO_FRAGMENT_PASSWORD_TITLE_TEXT' =>"Password",
    'AND_PROFILE_INFO_FRAGMENT_LANGUAGE_TITLE_TEXT' => "Language",
    'AND_PROFILE_INFO_FRAGMENT_ABOUT_TITLE_TEXT' =>"About",
    'AND_PROFILE_INFO_FRAGMENT_PRIVACY_TITLE_TEXT' =>"Privacy",
    'AND_PROFILE_INFO_FRAGMENT_TERMS_OF_USE_TITLE_TEXT' =>"Terms of Use",
    'AND_PROFILE_INFO_FRAGMENT_SIGN_OUT_TEXT' =>"SIGN OUT",
    'AND_PROFILE_INFO_FRAGMENT_SUCCESS_UPDATE' =>"You have successfully updated your profile info.",
    'AND_PROFILE_INFO_FRAGMENT_CM_SUBTITLE_TEXT' => " cm",
    'AND_PROFILE_INFO_FRAGMENT_SUBSCRIPTION_INFO_TITLE_TEXT' => "Subscription info",
    'AND_PROFILE_INFO_FRAGMENT_ACTIVE_PLAN_TITLE_TEXT' => "Active plan",
    'AND_PROFILE_INFO_FRAGMENT_CHANGE_PLAN_TITLE_TEXT' => "Change plan",
    'AND_PROFILE_INFO_FRAGMENT_FREE' => "Free",

    // Profile info Fragment
    'AND_PROFILE_STATISTICS_FRAGMENT_PROFILE_SUMMARY_TITLE_TEXT' =>"Profile summary",
    'AND_PROFILE_STATISTICS_FRAGMENT_SUCCESS_UPDATE' =>"You have successfully updated your profile info.",
    'AND_PROFILE_STATISTICS_FRAGMENT_SURVEY' =>"Survey",
    'AND_PROFILE_STATISTICS_FRAGMENT_WORKOUT_ACTIVITY_TITLE_TEXT' => "Workout activity",
    'AND_PROFILE_STATISTICS_FRAGMENT_HEIGHT_TITLE_TEXT' => "Height",
    'AND_PROFILE_STATISTICS_FRAGMENT_WEIGHT_TITLE_TEXT' => "Weight",
    'AND_PROFILE_STATISTICS_FRAGMENT_PROFILE_SUMMARY_INITIAL_TITLE_TEXT' => "Initial",
    'AND_PROFILE_STATISTICS_FRAGMENT_PROFILE_SUMMARY_CURRENT_TITLE_TEXT' => "Current",
    'AND_PROFILE_STATISTICS_FRAGMENT_PROFILE_SUMMARY_DIFFERENCE_TITLE_TEXT' => "Difference",
    'AND_PROFILE_STATISTICS_FRAGMENT_KG_SUBTITLE_TEXT' => "kg",
    'AND_PROFILE_STATISTICS_FRAGMENT_ADD_WEIGHT' => "Add weight",

    // Payment
    'AND_PAYMENT_FRAGMENT_DESCRIPTION_START_TEXT' => "Start your fitness journey today with Infinity Fit Pro!",
    'AND_PAYMENT_FRAGMENT_DESCRIPTION_END_UP_TEXT' => "Subscribing to this app you agree to the Infinity Fit Term and Conditions. Subscriptions renew within 24 hours before the ending of subscription period. You will be charged through your selected payment method. You can manage and edit your subscription in the Profile section.\n\nBy purchasing a subscription via in-app subscription, you agree to the Google Plаy Store terms and conditions.\n\n",
    'AND_PAYMENT_FRAGMENT_ACTIVE_PLAN_TEXT' => "Active plan",
    'AND_PAYMENT_FRAGMENT_DEACTIVATE' => "Deactivate plan",
    'AND_PAYMENT_FRAGMENT_RECOMMENDED_WORKOUT' => "Two recommended workout programs",
    'AND_PAYMENT_FRAGMENT_RECOMMENDED_NUTRITION' => "Two recommended nutrition programs",
    'AND_PAYMENT_FRAGMENT_DESCRIPTION_END_FREE_TEXT' => "Select a plan to start your free trial. After the 14-days free trial you will be automatically charged unless you cancel the subscription.",
    'AND_PAYMENT_FRAGMENT_NEXT_PAYMENT' => "Next payment of %s1 will occur on following date: %s2.",
    'AND_PAYMENT_FRAGMENT_PLAN_ENDS' => "Your current plan is valid until %s.",
    'AND_PAYMENT_FRAGMENT_PLAN_DEACTIVATED' => "Your plan has been deactivated and will be vaild until: %s",
    'AND_PAYMENT_FRAGMENT_ACTIVE_FREE_TRIAL_TEXT' => "Active free trial",
    'AND_PAYMENT_FRAGMENT_YOUR_TRIAL_ENDS' => "Your free trial ends on: %s.",
    'AND_PAYMENT_FRAGMENT_FREE_TRAIL' => " free trial.",
    'AND_PAYMENT_FRAGMENT_PER_MONTH' => "/month",
    'AND_PAYMENT_CARD_FRAGMENT_PAYMENT_FAILED' => "Card payment failed",

    'AND_PAYMENT_PLAN_ACTIVATED_FRAGMENT_BACK' => "Back",
    'AND_PAYMENT_PLAN_ACTIVATED_FRAGMENT_TITLE' => "Your request received successfully!",
    'AND_PAYMENT_PLAN_ACTIVATED_FRAGMENT_DESCRIPTION_FREE' => "We will send you an e-mail when your free trial is activated.",
    'AND_PAYMENT_PLAN_ACTIVATED_FRAGMENT_DESCRIPTION' => "We will send you an e-mail when your plan is activated.",

    'AND_PAYMENT_ACTIVITY_BOTTOM_DIALOG_FRAGMENT_TITLE' => "Select payment method",
    'AND_PAYMENT_ACTIVITY_BOTTOM_DIALOG_FRAGMENT_GPAY' => "Google Pay",
    'AND_PAYMENT_ACTIVITY_BOTTOM_DIALOG_FRAGMENT_CARD' => "Credit or debit card",
    'AND_PAYMENT_ACTIVITY_BOTTOM_DIALOG_FRAGMENT_SMS' => "SMS",
    'AND_PAYMENT_ACTIVITY_BOTTOM_DIALOG_FRAGMENT_DESC_FREE' => "Add a payment method to start your %s2. You won't be charged if you cancel before %s1.",
    'AND_PAYMENT_ACTIVITY_BOTTOM_DIALOG_FRAGMENT_DESCRIPTION' => "Add a payment method to start your %s.",

    'AND_PAYMENT_PLAN_DEACTIVATED_FRAGMENT_BACK' => "Back",
    'AND_PAYMENT_PLAN_DEACTIVATED_FRAGMENT_TITLE' => "Plan deactivated",
    'AND_PAYMENT_PLAN_DEACTIVATED_FRAGMENT_DESCRIPTION' => "You have successfully deactivated your plan.",

    'AND_PAYMENT_ACTIVITY_CANNOT_ACTIVATE' => 'If you want to activate this subscription plan, please wait until your current plan expires or deactivate it first.',

    'AND_DIALOG_DEACTIVATE_PLAN_CONFIRMATION_TITLE' => "Deactivate plan",
    'AND_DIALOG_DEACTIVATE_PLAN_CONFIRMATION_DESCRIPTION' => "Are you sure you want to deactivate your %s?",
    'AND_DIALOG_DEACTIVATE_PLAN_CONFIRMATION_DECLINE' => "No",
    'AND_DIALOG_DEACTIVATE_PLAN_CONFIRMATION_ACCEPT' => "Yes, deactivate my %s.",

    // Billing
    'AND_BILLING_MANAGER_SERVICE_TIMEOUT' => "Billing error: service timeout.",
    'AND_BILLING_MANAGER_FEATURE_NOT_SUPPORTED' => "Billing error: feature not supported.",
    'AND_BILLING_MANAGER_SERVICE_DISCONNECTED' => "Billing error: service disconnected.",
    'AND_BILLING_MANAGER_USER_CANCELLED' => "Billing error: user cancelled.",
    'AND_BILLING_MANAGER_SERVICE_UNAVAILABLE' => "Billing error: service unavailable.",
    'AND_BILLING_MANAGER_BILLING_UNAVAILABLE' => "Billing error: billing unavailable.",
    'AND_BILLING_MANAGER_ITEM_UNAVAILABLE' => "Billing error: item unavailable.",
    'AND_BILLING_MANAGER_DEVELOPER_ERROR' => "Billing error: developer error.",
    'AND_BILLING_MANAGER_ERROR' => "Billing error: general error.",
    'AND_BILLING_MANAGER_ITEM_ALREADY_OWNED' => "Billing error: item already owned.",
    'AND_BILLING_MANAGER_ITEM_NOT_OWNED' => "Billing error: item not owned.",

    // Privacy Activity
    'AND_PRIVACY_ACTIVITY_TITLE_TEXT' =>"Privacy",
    'AND_PRIVACY_ACTIVITY_SUBTITLE_TEXT' =>"What data will we collect?",
    'AND_PRIVACY_ACTIVITY_DESCRIPTION_TEXT' =>"Description....",

    // Terms of use Activity
    'AND_TERMS_OF_USE_ACTIVITY_TITLE_TEXT' =>"Terms of use",
    'AND_TERMS_OF_USE_ACTIVITY_SUBTITLE_TEXT' =>"What data will we collect?",
    'AND_TERMS_OF_USE_ACTIVITY_DESCRIPTION_TEXT' =>"Description....",

    // Edit First Name Dialog
    'AND_EDIT_FIRST_NAME_TITLE' =>"Edit first name",
    'AND_EDIT_FIRST_NAME_FIRST_NAME_TV' =>"First name",
    'AND_EDIT_FIRST_NAME_SUBMIT_BTN' =>"Save",
    'AND_EDIT_FIRST_NAME_REQUEST_FIRST_NAME_REQUIRED' =>"Required field",

    // Edit Last Name Dialog
    'AND_EDIT_LAST_NAME_TITLE' =>"Edit last name",
    'AND_EDIT_LAST_NAME_LAST_NAME_TV' =>"Last name",
    'AND_EDIT_LAST_NAME_SUBMIT_BTN' =>"Save",
    'AND_EDIT_LAST_NAME_REQUEST_LAST_NAME_REQUIRED' =>"Required field",

    // Edit User Name Dialog
    'AND_EDIT_USER_NAME_TITLE' =>"User name",
    'AND_EDIT_USER_NAME_USER_NAME_TV' =>"User name:",
    'AND_EDIT_USER_NAME_SUBMIT_BTN' =>"Save",
    'AND_EDIT_USER_NAME_REQUEST_USER_NAME_REQUIRED' =>"Required field",

    // Gender Dialog
    'AND_EDIT_GENDER_GENDER_TITLE' =>"Edit gender",
    'AND_EDIT_GENDER_GENDER_REQUIRED' =>"Gender field is required",

    // Language
    'AND_LANGUAGE_TITLE' => "Language",
    'AND_LANGUAGE_NAME_SUBMIT_BTN' => "Submit",

    // Blog Activity
    'AND_BLOG_ACTIVITY_SUCCESS_ADD_COMMENT' =>"Your comment will appear after it has been approved.",
    'AND_BLOG_ACTIVITY_SUCCESS_LIKE_POST' =>"You have successfully liked this post.",
    'AND_BLOG_ACTIVITY_SUCCESS_DISLIKE_POST' =>"You have successfully disliked this post.",
    'AND_BLOG_ACTIVITY_ADD_COMMENT_TITLE' =>"Add comment ...",
    'AND_BLOG_ACTIVITY_COMMENTS_TITLE' =>"Comments (%s)",

    // Add comment Dialog
    'AND_ADD_COMMENT_DIALOG_ADD_COMMENT_NAME_TV' =>"Add comment",
    'AND_ADD_COMMENT_DIALOG_SUBMIT_BTN' =>"Save",
    'AND_ADD_COMMENT_DIALOG_CLOSE_TEXT' =>"Close",
    'AND_ADD_COMMENT_DIALOG_REQUEST_COMMENT_REQUIRED' =>"Required field",

    // Edit Height Dialog
    'AND_EDIT_HEIGHT_TITLE' =>"Edit height",
    'AND_EDIT_HEIGHT_SUBMIT_BTN' =>"Save",
    'AND_EDIT_HEIGHT_REQUEST_HEIGHT_REQUIRED' =>"Required field",

    // Edit Height Dialog
    'AND_EDIT_WEIGHT_TITLE' =>"Edit weight",
    'AND_EDIT_WEIGHT_SUBMIT_BTN' =>"Save",
    'AND_EDIT_WEIGHT_REQUEST_WEIGHT_REQUIRED' =>"Required field",

    // Survey Activity
    'AND_SURVEY_ACTIVITY_DIALOG_STOP_TITLE' =>"Are you sure you want to leave before completing the survey?",
    'AND_SURVEY_ACTIVITY_DIALOG_STOP_MESSAGE' =>"If you leave before completing the survey, the data you've entered will be lost.",
    'AND_SURVEY_ACTIVITY_DIALOG_STOP_POSITIVE_BTN' =>"I agree",
    'AND_SURVEY_ACTIVITY_DIALOG_STOP_NEGATIVE_BTN' =>"Continue",
    'AND_SURVEY_ACTIVITY_STEP_TEXT' =>"Step ",
    'AND_SURVEY_ACTIVITY_OF_TEXT' =>" of ",
    'AND_SURVEY_ACTIVITY_STOPPED_WITHOUT_CONNECTION' =>"Stopped Without Connection",
    'AND_SURVEY_ACTIVITY_SUCCESS' =>"Changes have been saved successfully.",
    'AND_GENDER_MALE_TEXT' => "Male",
    'AND_GENDER_FEMALE_TEXT' => "Female",

    // Question Fragment
    'AND_QUESTION_FRAGMENT_DATE_PICKER_TEXT' => "Insert your birthdate",
    'AND_QUESTION_FRAGMENT_NUMBER_PICKER_TEXT' => "Set",

    // Home fragment
    'AND_HOME_FRAGMENT_WELCOME_TITLE' =>"Welcome",
    'AND_HOME_FRAGMENT_BLOGS_TITLE' =>"See our latest blogs",

    // Home Fragment Recommended
    'AND_HOME_FRAGMENT_RECOMMENDED_PROGRAMS_TITLE' =>"Recommended workout programs",
    'AND_HOME_FRAGMENT_RECOMMENDED_PROGRAMS_SEE_ALL_TITLE' =>"See all",
    'AND_HOME_FRAGMENT_CONTINUE_PROGRAMS_TITLE' =>"Continue with your progress",
    'AND_HOME_FRAGMENT_RECOMMENDED_NUTRITIONS_TITLE' =>"Recommended nutrition programs",
    'AND_HOME_FRAGMENT_NUTRITIONS_SEE_ALL_TITLE' =>"See all",
    'AND_HOME_FRAGMENT_CONTINUE_PROGRAMS_DAY_TEXT' => " ( Day ",
    'AND_HOME_FRAGMENT_CONTINUE_PROGRAMS_OF_TEXT' =>        " of ",
    'AND_HOME_FRAGMENT_CONTINUE_PROGRAMS_COMPLETED_TEXT' =>" completed )",

    // Main Activity
    'AND_MAIN_ACTIVITY_HOME_TITLE' =>"Home",
    'AND_MAIN_ACTIVITY_EXERCISE_TITLE' =>"Exercise",
    'AND_MAIN_ACTIVITY_PROGRAMS_TITLE' =>"Programs",
    'AND_MAIN_ACTIVITY_NUTRITION_TITLE' =>"Nutrition",
    'AND_MAIN_ACTIVITY_PROFILE_TITLE' =>"Profile",

    // Nutrition Program Activity
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_SUCCESS_ADD_COMMENT' =>"Your comment will appear after it has been approved.",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_ADD_COMMENT_TITLE' =>"Add comment ...",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_COMMENTS_TITLE' =>"Comments (%s)",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_SUCCESS_LIKE_PROGRAM' =>"You have successfully liked this program.",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_SUCCESS_DISLIKE_PROGRAM' =>"You have successfully disliked this program.",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_SUCCESS_RESET_PROGRAM' =>"You have successfully reset this program.",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_SUCCESS_RATE_PROGRAM' =>"You have successfully rated this program.",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_ADD_COMMENT_NOT_PERMISSION' =>"To comment you need to activate the program.",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_DESCRIPTION_TITLE' =>"Description",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_BASIC_INFO_TITLE' =>"Basic info",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_PROGRAM_LENGTH_TITLE' =>"Program Length",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_MEALS_PER_DAY_TITLE' =>"Meals Per Day",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_RATE_WRONG' =>"Rating scale must be from 1 to 5!",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_BUY_PROGRAM_BTN' =>"ACTIVATE THIS PROGRAM",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_CONTINUE_PROGRAM_BTN' =>"Go to meals list",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_SUCCESS_BUY_PROGRAM' =>"You have successfully activated the program.",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_NEXT_TRAINING_DOES_NOT_EXIST' =>"Next workout doesn't exist.",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_SUCCESS_DEACTIVATE_PROGRAM' => "Program successfully deactivated",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_CHANGE_PLAN' => "CHANGE PLAN",
    'AND_NUTRITION_PROGRAM_INFO_ACTIVITY_BUY_PLAN' => "BUY A PLAN",

    // Nutrition Fragment
    'AND_NUTRITION_FRAGMENT_TAB_MY_NUTRITION' =>"My Nutrition",
    'AND_NUTRITION_FRAGMENT_TAB_NUTRITION' =>"Nutrition",
    'AND_NUTRITION_FRAGMENT_EMPTY_DATA' =>"You haven't activated any nutrition program.",
    'AND_NUTRITION_FRAGMENT_FREE_PROGRAM' => "Free program",

    // Program info dialog
    'AND_PROGRAM_INFO_DIALOG_LIKE' =>"Like this program",
    'AND_PROGRAM_INFO_DIALOG_DISLIKE' =>"Dislike this program",
    'AND_PROGRAM_INFO_DIALOG_RATE' =>"Rate this program",
    'AND_PROGRAM_INFO_DIALOG_RESET' =>"Reset your progress",

    // Rate Program Dialog
    'AND_RATE_PROGRAM_DIALOG_TITLE' =>"Rate this program",
    'AND_RATE_PROGRAM_DIALOG_CLOSE' =>"Close",
    'AND_RATE_PROGRAM_DIALOG_SUBMIT' =>"Set",

    // Reset Program Dialog
    'AND_RESET_PROGRAM_DIALOG_TITLE' =>"Reset your progress",
    'AND_RESET_PROGRAM_DIALOG_DESCRIPTION' =>"Are you sure you want to reset\nyour rogress in this program?",
    'AND_RESET_PROGRAM_DIALOG_SUBMIT' =>"RESET PROGRESS",
    'AND_RESET_PROGRAM_DIALOG_CLOSE' =>"Close",

    // Exercises Fragment
    'AND_EXERCISES_FRAGMENT_TITLE' =>"Exercise Page",
    'AND_EXERCISES_FRAGMENT_SUBTITLE' =>"Choose your preferred muscle group",

    // Exercises Info Activity
    'AND_EXERCISES_INFO_ACTIVITY_DESCRIPTION_TITLE' =>"Exercise description",

    // Exercises List Activity
    'AND_EXERCISES_LIST_ACTIVITY_TITLE' =>"%s exercises",
    'AND_EXERCISES_LIST_ACTIVITY_SEARCH_HINT_TEXT' =>"Search exercises...",

    // Nutrition Programs Fragment
    'AND_NUTRITION_PROGRAMS_FRAGMENT_TITLE' =>"Nutrition programs",
    'AND_NUTRITION_PROGRAMS_SEARCH_HINT_TEXT' =>"Search programs...",

    // Workout Programs Fragment
    'AND_WORKOUT_PROGRAMS_FRAGMENT_TITLE' =>"Workout programs",
    'AND_WORKOUT_PROGRAMS_SEARCH_HINT_TEXT' =>"Search programs...",

    // Workout  Fragment
    'AND_WORKOUT_FRAGMENT_TAB_MY_PROGRAMS' =>"My programs",
    'AND_WORKOUT_FRAGMENT_TAB_PROGRAMS' =>"Programs",
    'AND_WORKOUT_FRAGMENT_FREE_PROGRAM' => "Free program",

    // Exercises Fragment
    'AND_EXERCISES_FRAGMENT_EMPTY_DATA' =>"You haven't activated any exercises",

    // Workout Fragment
    'AND_WORKOUT_FRAGMENT_EMPTY_DATA' =>"You haven't activated any workouts",

    // Meal Fragment
    'AND_MEAL_FRAGMENT_EMPTY_DATA' =>"You haven't activated any programs.",

    // Meal Info Activity
    'AND_MEAL_INFO_ACTIVITY_DESCRIPTION_TITLE' =>"Description",
    'AND_MEAL_INFO_ACTIVITY_NUTRITION_INFO' =>"Nutrition info",

    'AND_MEAL_INFO_ACTIVITY_PROTEINS' =>"Proteins",
    'AND_MEAL_INFO_ACTIVITY_CARBOHYDRATES' => "Carbo",
    'AND_MEAL_INFO_ACTIVITY_FAT' => "Fat",
    'AND_MEAL_INFO_ACTIVITY_GRAM' => "g",
    'AND_MEAL_INFO_ACTIVITY_PREPARE_TITLE' => "How to prepare this meal?",
    'AND_MEAL_INFO_ACTIVITY_INGREDIENTS_TITLE' => "Ingredients",
    'AND_MEAL_INFO_ACTIVITY_MEALS_PER_DAYS_NOT_PERMISSION' => "You need to activate the program first.",

// TODO Refaktorisati imena kljuceva na APIU (Obrisati prefix Workout) > ProgramInfoActivity
// Program Info Activity
    'AND_PROGRAM_INFO_ACTIVITY_ADD_COMMENT_NOT_PERMISSION' => "To comment you need to activate the program.",
    'AND_PROGRAM_INFO_ACTIVITY_SUCCESS_ADD_COMMENT' => "Your comment will appear after it has been approved.",
    'AND_PROGRAM_INFO_ACTIVITY_ADD_COMMENT_TITLE' => "Add comment ...",
    'AND_PROGRAM_INFO_ACTIVITY_COMMENTS_TITLE' => "Comments (%s)",
    'AND_PROGRAM_INFO_ACTIVITY_SUCCESS_LIKE_PROGRAM' => "You have successfully liked this program.",
    'AND_PROGRAM_INFO_ACTIVITY_SUCCESS_DISLIKE_PROGRAM' => "You have successfully disliked this program.",
    'AND_PROGRAM_INFO_ACTIVITY_SUCCESS_RESET_PROGRAM' => "You have successfully reset this program.",
    'AND_PROGRAM_INFO_ACTIVITY_SUCCESS_RATE_PROGRAM' => "You have successfully rated this program.",
    'AND_PROGRAM_INFO_ACTIVITY_DESCRIPTION_TITLE' => "Description",
    'AND_PROGRAM_INFO_ACTIVITY_BASIC_INFO_TITLE' => "Basic info",
    'AND_PROGRAM_INFO_ACTIVITY_PROGRAM_LENGTH_TITLE' => "Program Length",
    'AND_PROGRAM_INFO_ACTIVITY_WORKOUT_DAYS_TITLE' => "Workout Days",
    'AND_PROGRAM_INFO_ACTIVITY_BUY_PROGRAM_BTN' => "ACTIVATE THIS PROGRAM",
    'AND_PROGRAM_INFO_ACTIVITY_CONTINUE_PROGRAM_BTN' => "CONTINUE YOUR PROGRESS",
    'AND_PROGRAM_INFO_ACTIVITY_SUCCESS_BUY_PROGRAM' => "You have activated the program successfully.",
    'AND_PROGRAM_INFO_ACTIVITY_IS_LOCK' => "Workout locked!",
    'AND_PROGRAM_INFO_ACTIVITY_IS_FINISHED' => "Workout done!",
    'AND_PROGRAM_INFO_ACTIVITY_IS_REST_DAY' => "Rest day!",
    'AND_PROGRAM_INFO_ACTIVITY_SUCCESS_DEACTIVATE_PROGRAM' => "Program successfully deactivated",
    'AND_PROGRAM_INFO_ACTIVITY_DEACTIVATE_PROGRAM_FAILED' => "Deactivation failed",
    'AND_PROGRAM_INFO_ACTIVITY_CHANGE_PLAN' => "CHANGE PLAN",
    'AND_DEACTIVATE_PROGRAM_DIALOG_TITLE' => "Deactivate this program",
    'AND_DEACTIVATE_PROGRAM_DIALOG_DESCRIPTION' => "Are you sure you want to\n deactivate this program?",
    'AND_DEACTIVATE_PROGRAM_DIALOG_SUBMIT' => "Deactivate program",
    'AND_PROGRAM_INFO_DIALOG_DEACTIVATE' => "Deactivate this program",
    'AND_PROGRAM_INFO_ACTIVITY_BUY_PLAN' => "BUY A PLAN",

// Dialog Update App
    'AND_DIALOG_UPDATE_APP_TITLE' => "Update available",
    'AND_DIALOG_UPDATE_APP_MESSAGE' => "New version of the application is \n1now available on Google Play.",
    'AND_DIALOG_UPDATE_APP_DONT_SHOW_TEXT' => "Don`t show this message again",
    'AND_DIALOG_UPDATE_APP_POSITIVE_BUTTON' => "Update",
    'AND_DIALOG_UPDATE_APP_NEGATIVE_BUTTON' => "Close",

// Nutrition Survey Dialog
    'AND_DIALOG_NUTRITION_SURVEY_DIALOG_TITLE' => "Get personalized nutrition",
    'AND_DIALOG_NUTRITION_SURVEY_DIALOG_DESCRIPTION' => "Complete our nutrition questionnaire so we can \nrecommend you the best nutrition plans.",
    'AND_DIALOG_NUTRITION_SURVEY_DIALOG_SUBMIT_BTN_TEXT' => "START",
    'AND_DIALOG_NUTRITION_SURVEY_DIALOG_CLOSE' => "Maybe later.",

// Nutrition Question Fragment
    'AND_NUTRITION_QUESTION_FRAGMENT_NEXT_BTN' => "Next",
    'AND_NUTRITION_QUESTION_FRAGMENT_REQUIRED' => "It is required to select at least one field!",

// Nutrition Survey Activity
    'AND_NUTRITION SURVEY ACTIVITY' => "Changes have been saved successfully.",

// Training Activity
    'AND_TRAINING_ACTIVITY_SUBMIT_BTN_TEXT' => "START WORKOUT",
    'AND_TRAINING_ACTIVITY_SLASH_TEXT' => " / ",
    'AND_TRAINING_ACTIVITY_CURRENT_WEEK_TEXT' => "Week ",
    'AND_TRAINING_ACTIVITY_CURRENT_DAY_TEXT' =>  " / Day ",
    'AND_TRAINING_ACTIVITY_ROUND_TEXT' => "Round ",
    'AND_TRAINING_ACTIVITY_START_WORKOUT_TEXT' => "Start Workout",
    'AND_TRAINING_ACTIVITY_SET_TAB_TEXT' => "Set ",
    'AND_TRAINING_ACTIVITY_ROUND_OF_TEXT' => " of ",
    'AND_TRAINING_ACTIVITY_BUY_PLAN_TEXT' => "Buy plan",

// TODO NEW NEW NEW
// Reset Training Dialog
    'AND_RESET_TRAINING_DIALOG_RESET_TITLE_TEXT' => "Reset workout day",
    'AND_RESET_TRAINING_DIALOG_CHECK_TITLE_TEXT' => "Finish workout day",

    'AND_RESET_TRAINING_DIALOG_RESET_DESCRIPTION_TEXT' => "Are you sure you want to reset \nprogress on this workout day?",
    'AND_RESET_TRAINING_DIALOG_CHECK_DESCRIPTION_TEXT' => "Are you sure you want to \nfinish this workout day?",
    'AND_RESET_TRAINING_DIALOG_RESET_SUBMIT_BTN_TEXT' => "RESET WORKOUT DAY",
    'AND_RESET_TRAINING_DIALOG_CHECK_SUBMIT_BTN_TEXT' => "FINISH WORKOUT DAY",
    'AND_RESET_TRAINING_DIALOG_CHECK_CLOSE_TEXT' => "Close",

// Edit Exercise Dialog
    'AND_EDIT_EXERCISE_DIALOG_SUBMIT_BTN' => "Save",
    'AND_EDIT_EXERCISE_DIALOG_CLOSE_TEXT' => "Close",
    'AND_EDIT_EXERCISE_VALUE_REQUIRED' => "Polja su obavezna!",

// Workout Activity
    'AND_WORKOUT_ACTIVITY_SUCCESS EDIT EXERCISE TEXT' => "You have successfully updated your exercise settings.",
    'AND_WORKOUT_ACTIVITY_REST_TIME_TEXT' =>  "Rest time",
    'AND_WORKOUT_ACTIVITY_WORKOUT_TIME_TEXT' => "Workout time",
    'AND_WORKOUT_ACTIVITY_EXERCISE_TIME_TEXT' => "Exercise time",
    'AND_WORKOUT_ACTIVITY_PAUSE_TEXT' =>  "Pause",
    'AND_WORKOUT_ACTIVITY_FINISH_BTN_TEXT' =>  "START",
    'AND_WORKOUT_ACTIVITY_DID_IT_BTN_TEXT' =>  "YOU DID IT",
    'AND_WORKOUT_ACTIVITY_SKIP_BTN_TEXT' => 'SKIP',
    'AND_WORKOUT_ACTIVITY_CURRENT_ROUND' => '%s (Round %s of %s)',
    'AND_WORKOUT_ACTIVITY_CURRENT_EXERCISE' => '%s (Exercise %s of %s)',

// Training Statistic Activity
    'AND_TRAINING_STATISTIC_ACTIVITY_TITLE_TEXT' => "Great job!",
    'AND_TRAINING_STATISTIC_ACTIVITY_SUBTITLE_TEXT' => "You have finished your workout!\nCheck out your workout statistics.",
    'AND_TRAINING_STATISTIC_ACTIVITY_SETS_TEXT' => "Sets",
    'AND_TRAINING_STATISTIC_ACTIVITY_EXERCISES_TEXT' => "Exercises",
    'AND_TRAINING_STATISTIC_ACTIVITY_CALORIES_TEXT' => "Calories",
    'AND_TRAINING_STATISTIC_ACTIVITY_SUBMIT_BTN_TEXT' => "Done",
    'AND_WORKOUT_ACTIVITY_EDIT_BTN_TEXT' => "Edit",

// Program Statistic Activity
    'AND_PROGRAM_STATISTIC_ACTIVITY_TITLE_TEXT' => "Congratulations!",
    'AND_PROGRAM_STATISTIC_ACTIVITY_SUBTITLE_TEXT' => "You have finished \n%s!\nCheck out your program statistics",
    'AND_PROGRAM_STATISTIC_ACTIVITY_WEEKS_TEXT' => "Weeks",
    'AND_PROGRAM_STATISTIC_ACTIVITY_WORKOUTS_TEXT' => "Workouts",
    'AND_PROGRAM_STATISTIC_ACTIVITY_SUBMIT_BTN_TEXT' => "Done",

    'AND_RESEND_EMAIL_DIALOG_TITLE_TEXT' =>"Your account hasn't been activated yet",
    'AND_RESEND_EMAIL_DIALOG_DESCRIPTION_TEXT' => "If you didn't receive the activation email click the Resend Activation Email button to confirm your email address and activate your account.",
    'AND_RESEND_EMAIL_DIALOG_SUBMIT_BTN_TEXT' => "SEND",

    // Date Format Ago
    'AND_DATE_FORMAT_SECONDS_TEXT' => "%s seconds ago",
    'AND_DATE_FORMAT_MINUTES_TEXT' => "%s minutes ago",
    'AND_DATE_FORMAT_HOURS_TEXT' => "%s hours ago",
    'AND_DATE_FORMAT_DAYS_TEXT' => "%s days ago",

    // User Survey
    'AND_USER_SURVEY_LEVELS_TITLE_TEXT' => "What is your level?",
    'AND_USER_SURVEY_LEVELS_DESCRIPTION_TEXT' => "Describe your current workout level.",
    'AND_USER_SURVEY_GOALS_TITLE_TEXT' => "What is your goal?",
    'AND_USER_SURVEY_GOALS_DESCRIPTION_TEXT' => "Tell us what goal you want to achieve?",
    'AND_USER_SURVEY_ENVIRONMENT_TITLE_TEXT' => "What is your workout environment?",
    'AND_USER_SURVEY_ENVIRONMENT_DESCRIPTION_TEXT' => "Choose where you prefer to workout.",

    // Finish Workout Dialog
    "AND_FINISH_WORKOUT_DIALOG_TITLE_TEXT" => "Finish your workout",
    "AND_FINISH_WORKOUT_DIALOG_SUBTITLE_TEXT" => "Are you sure you want to \nfinish your workout?",
    "AND_FINISH_WORKOUT_DIALOG_SUBMIT_BTN_TEXT" => "Yes",
    "AND_FINISH_WORKOUT_DIALOG_CLOSE_BTN_TEXT" => "No",

    "AND_ROUND_TEXT" => " rounds",
    "AND_ROUND_TO_FIVE_TEXT" => " rounds",

    "AND_BASIC_QUESTIONAIRE" => require( realpath(__DIR__ . '/basic-questionare.php') )
];
