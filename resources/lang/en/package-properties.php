<?php

return [
    1 => [
        'plan_name' => 'MONTHLY PLAN',
        'free_trial_badge' => '14-days free trial',
        'discount_badge' => '',
        'from_label' => 'From',
        'benefits' => [
            'Access to all blogs',
            '2 workout programs',
            '2 nutrition programs'
        ]
    ],
    2 => [
        'plan_name' => 'YEARLY PLAN',
        'free_trial_badge' => '14-days free trial',
        'discount_badge' => '7% off',
        'from_label' => 'From',
        'benefits' => [
            'Access to all blogs',
            'Unlimited access to workout programs',
            'Unlimited access to nutrition programs'
        ]
    ],

];
