<?php

return "{
  \"question\": [
    {
      \"id\": 4,
      \"title\": \"What is your gender?\",
      \"description\": \"\",
      \"type\": \"imageView\",
      \"answers\": [
        {
          \"id\": 1,
          \"title\": \"male\",
          \"description\": \"male\"
        },
        {
          \"id\": 2,
          \"title\": \"female\",
          \"description\": \"female\"
        }
      ]
    },
    {
      \"id\": 5,
      \"title\": \"How old are you?\",
      \"description\": \"Fitness approaches differ by age.\",
      \"type\": \"datePicker\",
      \"answers\": [
        {
          \"id\": 1,
          \"title\": \"\",
          \"description\": \"\"
        }
      ]
    },
    {
      \"id\": 6,
      \"title\": \"How tall are you?\",
      \"description\": \"Enter your current height.\",
      \"type\": \"numberPicker\",
      \"answers\": [
        {
          \"id\": 1,
          \"title\": \"\",
          \"description\": \"\",
          \"minValue\": 120,
          \"defaultValue\": 150,
          \"maxValue\": 220
        }
      ]
    },
    {
      \"id\": 7,
      \"title\": \"What is your weight?\",
      \"description\": \"Enter your current weight.\",
      \"type\": \"numberPicker\",
      \"answers\": [
        {
          \"id\": 1,
          \"title\": \"\",
          \"description\": \"\",
          \"minValue\": 30,
          \"defaultValue\": 50,
          \"maxValue\": 200
        }
      ]
    }
  ]
}";
