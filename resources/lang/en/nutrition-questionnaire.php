<?php
return [
    0 => [
        'name' => 'How active are you?',
        'description' => 'You can select only one statement.',
        'question_type' => 'text',
        'answers' => [
            'I do not practice at all.',
            'I walk on a daily basis.',
            'I exercise 1-2 times a week.',
            'I exercise 3-4 times a week.',
        ]
    ],

    1 => [
        'name' => 'What best describes your habits?',
        'description' => 'You can select only one statement.',
        'question_type' => 'text',
        'answers' => [
            'I don’t get enough sleep.',
            'I eat late at night.',
            'I drink a lot of sugary drinks.',
            'I consume a lot of sugar.',
        ]
    ],

    2 => [
        'name' => 'How do you spend your day?',
        'description' => 'You can select only one statement.',
        'question_type' => 'text',
        'answers' => [
            'At the office.',
            'At the office but I often go outside.',
            'I spend most of the day on foot.',
            'Manual labour.',
        ]
    ],

    3 => [
        'name' => 'What veggies do you like?',
        'description' => 'You can select multiple options.',
        'question_type' => 'image',
        'answers' => [
        ]
    ],

    4 => [
        'name' => 'What type of meat do you prefer?',
        'description' => 'You can select multiple options.',
        'question_type' => 'image',
        'answers' => [
        ]
    ],

    5 => [
        'name' => 'What fruit do you like?',
        'description' => 'You can select multiple options.',
        'question_type' => 'image',
        'answers' => [
        ]
    ],


];
