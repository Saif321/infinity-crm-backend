@extends('emails.layouts.fit')

@section('content')
<h2>Thank you.</h2>
<p>
    You've made a subscription purchase from InfinityFit.
    <br/>
    Your payment details:
</p>

<p>
    Item: <b>{{$packageName}}</b><br/>
    Order ID: <b>{{$orderId}}</b><br/>
    Price: <b>{{$currencyCode}}{{$value}}</b>
</p>

<p>
    Time: {{$paymentTime}}
</p>
@endsection
