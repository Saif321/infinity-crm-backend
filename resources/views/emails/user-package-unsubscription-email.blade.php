@extends('emails.layouts.fit')

@section('content')
<h2>Thank you.</h2>
<p>
    You've unsubscribed from InfinityFit package <b>{{$packageName}}</b>.
    <br />
    Your subscription is still valid until <b>{{$validUntil}}</b>
</p>

<p>
    Time: {{$currentTime}}
</p>
@endsection
