@extends('emails.layouts.fit')

@section('content')
<p>
    Hi,
    <br />
    We got a request to reset your InfinityFit password.
    Please use the following code to reset your password:
    <br />
    <br />
    <b>{{$code->code}}</b>
    <br />
    <br />
    This code will expire in 5 minutes.
</p>
@endsection
