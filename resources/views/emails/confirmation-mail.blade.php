@extends('emails.layouts.fit')

@section('content')
<p>
    Welcome to InfinityFit!
    <br />
    Please click the link below to activate your account.
</p>
<p><a href="<?=(route('confirmation', [$code->id, $code->code]))?>">Activate My Account</a></p>
@endsection
