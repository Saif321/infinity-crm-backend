<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title></title>

    <style>
    /* -------------------------------------
    GLOBAL
    ------------------------------------- */
    * {
        margin:0;
        padding:0;
    }
    * { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

    img {
        max-width: 100%;
    }
    .collapse {
        margin:0;
        padding:0;
    }
    body {
        -webkit-font-smoothing:antialiased;
        -webkit-text-size-adjust:none;
        width: 100%!important;
        height: 100%;
        color: #0b2441;
    }

    /* -------------------------------------
    ELEMENTS
    ------------------------------------- */
    a { color: #2BA6CB;}

    .btn {
        text-decoration:none;
        color: #FFF;
        background-color: #666;
        padding:10px 16px;
        font-weight:bold;
        margin-right:10px;
        text-align:center;
        cursor:pointer;
        display: inline-block;
    }

    p.callout {
        padding:15px;
        background-color:#ECF8FF;
        margin-bottom: 15px;
    }
    .callout a {
        font-weight:bold;
        color: #2BA6CB;
    }

    table.social {
        /* 	padding:15px; */
        background-color: #f7f7f7;

    }
    .social .soc-btn {
        padding: 3px 7px;
        font-size:12px;
        margin-bottom:10px;
        text-decoration:none;
        color: #FFF;font-weight:bold;
        display:block;
        text-align:center;
    }
    a.hp { background-color: #0b2441!important; }
    a.ps { background-color: #fff!important; color:#0b2441!important; }
    a.as { background-color: #00b04c!important; }
    a.fb { background-color: #3B5998!important; }
    a.tw { background-color: #1daced!important; }
    a.gp { background-color: #DB4A39!important; }
    a.ms { background-color: #000!important; }

    .sidebar .soc-btn {
        display:block;
        width:100%;
    }

    /* -------------------------------------
    HEADER
    ------------------------------------- */
    table.head-wrap { background-color: #f7f7f7; width: 100%;}

    .header.container table td.logo { padding: 15px; }
    .header.container table td.label { padding: 15px; padding-left:0px;}

    img.logo { width: 200px; max-width: 600px; height: auto; }

    /* -------------------------------------
    BODY
    ------------------------------------- */
    table.body-wrap { width: 100%;}

    /* -------------------------------------
    FOOTER
    ------------------------------------- */
    table.footer-wrap { width: 100%;	clear:both!important;
    }
    .footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
    .footer-wrap .container td.content p {
        font-size:10px;
        font-weight: bold;

    }

    .email {
        text-decoration:none;
        color: #00b04c;
    }

    /* -------------------------------------
    TYPOGRAPHY
    ------------------------------------- */
    h1,h2,h3,h4,h5,h6 {
        font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px;
    }
    h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

    h1 { font-weight:200; font-size: 44px;}
    h2 { font-weight:200; font-size: 37px;}
    h3 { font-weight:500; font-size: 27px;}
    h4 { font-weight:500; font-size: 23px;}
    h5 { font-weight:900; font-size: 17px;}
    h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

    .collapse { margin:0!important;}

    p, ul {
        margin-bottom: 10px;
        font-weight: normal;
        font-size:14px;
        line-height:1.6;
    }
    p.lead { font-size:17px; }
    p.last { margin-bottom:0px;}
    p.small { font-size:9px; }

    ul li {
        margin-left:5px;
        list-style-position: inside;
    }

    /* -------------------------------------
    SIDEBAR
    ------------------------------------- */
    ul.sidebar {
        background:#ebebeb;
        display:block;
        list-style-type: none;
    }
    ul.sidebar li { display: block; margin:0;}
    ul.sidebar li a {
        text-decoration:none;
        color: #666;
        padding:10px 16px;
        /* 	font-weight:bold; */
        margin-right:10px;
        /* 	text-align:center; */
        cursor:pointer;
        border-bottom: 1px solid #777777;
        border-top: 1px solid #FFFFFF;
        display:block;
        margin:0;
    }
    ul.sidebar li a.last { border-bottom-width:0px;}
    ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}

    /* ---------------------------------------------------
    RESPONSIVENESS
    Nuke it from orbit. It's the only way to be sure.
    ------------------------------------------------------ */
    /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
    .container {
        display:block!important;
        max-width:600px!important;
        margin:0 auto!important; /* makes it centered */
        clear:both!important;
    }

    /* This should also be a block element, so that it will fill 100% of the .container */
    .content {
        padding:15px;
        max-width:600px;
        margin:0 auto;
        display:block;
    }

    /* Let's make sure tables in the content area are 100% wide */
    .content table { width: 100%; }

    /* Odds and ends */
    .column {
        width: 300px;
        float:left;
    }
    .column tr td { padding: 15px; }
    .column-wrap {
        padding:0!important;
        margin:0 auto;
        max-width:600px!important;
    }
    .column table { width:100%;}
    .social .column {
        width: 280px;
        min-width: 279px;
        float:left;
    }

    /* Be sure to place a .clear element after each set of columns, just to be safe */
    .clear { display: block; clear: both; }

    /* -------------------------------------------
    PHONE
    For clients that support media queries.
    Nothing fancy.
    -------------------------------------------- */
    @media only screen and (max-width: 600px) {
        a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}
        div[class="column"] { width: auto!important; float:none!important;}
        table.social div[class="column"] {
            width:auto!important;
        }
    }
    </style>
</head>

<body bgcolor="#FFFFFF">
    <table class="head-wrap">
        <tr>
            <td></td>
            <td class="header container" >
                <div class="content">
                    <table>
                        <tr>
                            <td><img src="https://infinityfit.io/wp-content/uploads/2020/04/02.png" class="logo"></td>
                        </tr>
                    </table>
                </div>
            </td>
            <td></td>
        </tr>
    </table>

    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container" bgcolor="#FFFFFF">
                <div class="content">
                    <table>
                        <tr>
                            <td>
                                <br>
                                <br>
                                <br>

                                @yield('content')

                                <br>
                                <br>
                                <br>
                                <br>
                                <br>

                                <table class="social" width="100%">
                                    <tr>
                                        <td>
                                            <table align="left" class="column">
                                                <tr>
                                                    <td>
                                                        <h5 class="">Contact Info:</h5>
                                                        <p>Address: <strong>Zrenjaninski put 150a, 11000 Belgrade, Serbia</strong><br/>
                                                            E-mail: <strong><a href="mailto:office@infinityfit.io" class="email">office@infinityfit.io</a></strong>
                                                        </p>
                                                    </td>
                                                </tr>
                                            </table>

                                            <table align="left" class="column">
                                                <tr>
                                                    <td>
                                                        <h5 class="">Useful Links:</h5>
                                                        <p class=""><a href="https://infinityfit.io" class="soc-btn hp">Homepage</a> <a href="https://play.google.com/store/apps/details?id=rs.telego.fitness&hl=en" class="soc-btn ps">Play Store</a> <a href="https://apps.apple.com/us/app/infinity-fit/id1486585228" class="soc-btn as">App Store</a></p>
                                                    </td>
                                                </tr>
                                            </table>

                                            <span class="clear"></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td></td>
        </tr>
    </table>

    <table class="footer-wrap">
        <tr>
            <td></td>
            <td class="container">
                <div class="content">
                    <table>
                        <tr>
                            <td align="center">
                                <p class="small">
                                    This e-mail message may contain confidential or legally privileged information and is intended only for the use of the intended recipient(s). Any unauthorized disclosure, dissemination, distribution, copying or the taking of any action in reliance on the information herein is prohibited. E-mails are not secure and cannot be guaranteed to be error free as they can be intercepted, amended, or contain viruses. Anyone who communicates with us by e-mail is deemed to have accepted these risks. InfinityFit is not responsible for errors or omissions in this message and denies any responsibility for any damage arising from the use of e-mail. Any opinion and other statement contained in this message and any attachment are solely those of the author and do not necessarily represent those of the company.
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td></td>
        </tr>
    </table>
</body>
</html>
