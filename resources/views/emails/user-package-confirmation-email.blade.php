@extends('emails.layouts.fit')

@section('content')
<h2>Thank you.</h2>
<p>
    You've made a subscription purchase from InfinityFit for <b>{{$packageName}}</b>. Your subscription will
    automatically renew on <b>{{$renewDate}}</b> unless you cancel <b>at least one day before</b> then.
<br />
    Your subscription is valid until <b>{{$validUntil}}</b>
</p>

<p>
    Time: {{$subscriptionTime}}
</p>
@endsection
