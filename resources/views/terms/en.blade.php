<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, shrink-to-fit=no, user-scalable=no">
    <meta property=“fb:app_id” content=“2565240367037984">
    <meta property=“og:description” content=“Infinity Fit”>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Roboto&display=swap');
        html * {
            font-size: 14px;
            font-family: Roboto;
            color: #BBBBBB;
            font-weight: 400;
        }

        body {
            -webkit-text-size-adjust: none;
        }

        .image img {
            width: 100%;
            height: auto;
            border-radius: 10px;
        }

        .image {
            margin-block-start: 0;
            margin-block-end: 0;
            margin-inline-start: 0;
            margin-inline-end: 0;
        }

        .image-style-align-center {
            margin-block-start: 1em;
            margin-block-end: 1em;
            margin-inline-start: 40px;
            margin-inline-end: 40px;
        }

        .image-style-align-left {
            float: left;
            margin: 0 16px 0 0;
            width: 50%;
        }

        .image-style-align-right {
            float: right;
            margin: 0 0 0 16px;
            width: 50%;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            color: #0b2441;
            font-weight: 500;
        }

        h1 {
            font-size: 30px;
        }

        h2 {
            font-size: 28px;
        }

        h2 span {
            color: #0b2441;
            font-size: 28px;
        }

        h2 strong {
            font-size: 28px;
            color: #0b2441;
            font-weight: 700;
        }

        h3 {
            font-size: 26px;
        }

        h3 span {
            color: #0b2441;
            font-size: 26px;
        }

        h3 strong {
            font-size: 26px;
            color: #0b2441;
            font-weight: 700;
        }

        h4 {
            font-size: 22px;
        }

        h4 span {
            color: #0b2441;
            font-size: 22px;
        }

        h4 strong {
            font-size: 22px;
            color: #0b2441;
            font-weight: 600;
        }

        h5 {
            font-size: 18px;
        }

        h5 span {
            color: #0b2441;
            font-size: 18px;
        }

        h5 strong {
            font-size: 18px;
            color: #0b2441;
            font-weight: 600;
        }

        h6 {
            font-size: 16px;
        }

        h6 span {
            color: #0b2441;
            font-size: 16px;
        }

        h6 strong {
            font-size: 16px;
            color: #0b2441;
            font-weight: 600;
        }

        p {
            font-size: 16px;
        }

        p strong {
            font-size: 16px;
            color: #0b2441;
            font-weight: 600;
        }

        span.text-tiny {
            font-size: 12px;
        }

        span.text-small {
            font-size: 14px;
        }

        span.text-big {
            font-size: 24px;
        }

        span.text-huge {
            font-size: 28px;
        }

        blockquote {
            margin-block-start: 0;
            margin-block-end: 0;
            margin-inline-start: 0;
            margin-inline-end: 0;
        }

        blockquote p:after {
            content: "";
            background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNC4xOTMiIGhlaWdodD0iMjAuMjg3IiB2aWV3Qm94PSIwIDAgMjQuMTkzIDIwLjI4NyI+CiAgPGcgaWQ9Im5vdW5fUXVvdGVfOTMwNDQyXzFhMWExYSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTE1Ny4yMDcgLTQ2MS44MTUpIj4KICAgIDxwYXRoIGlkPSJQYXRoXzE5MyIgZGF0YS1uYW1lPSJQYXRoIDE5MyIgZD0iTTE2Ny4wOTUsNDYxLjgxNWE5LjMzOSw5LjMzOSwwLDAsMC03LjI1NywyLjg2MXEtMi42MzUsMi44NzMtMi42MzEsOC45NzJWNDgyLjFoMTAuMTY5di05LjEzMWgtNC45MDhhOS4yLDkuMiwwLDAsMSwxLjA4MS00Ljk4NywzLjkyNSwzLjkyNSwwLDAsMSwzLjU0Ni0xLjYzNlptMTMuOTQ1LDBhMTEuNCwxMS40LDAsMCwwLTQuMTIyLjY5Miw3Ljc0NCw3Ljc0NCwwLDAsMC0zLjA5MiwyLjE0OCw5LjkzOSw5LjkzOSwwLDAsMC0xLjk1MywzLjcxMSwxOC40NzgsMTguNDc4LDAsMCwwLS42NzcsNS4yODNWNDgyLjFoMTAuMnYtOS4xMzFoLTQuOWE5LjIxLDkuMjEsMCwwLDEsMS4wNzQtNC45ODcsMy44NDUsMy44NDUsMCwwLDEsMy40NjYtMS42MzZaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwIDApIiBmaWxsPSIjMDBiMDRjIi8+CiAgPC9nPgo8L3N2Zz4=");
            background-size: 30px 30px;
            display: block;
            float: right;
            height: 30px;
            position: relative;
            top: 30px;
            width: 30px;
        }

        blockquote p:before {
            content: "";
            background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNC4xOTMiIGhlaWdodD0iMjAuMjg3IiB2aWV3Qm94PSIwIDAgMjQuMTkzIDIwLjI4NyI+CiAgPGcgaWQ9Im5vdW5fUXVvdGVfOTMwNDQyXzFhMWExYSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTgxLjQgNDgyLjEwMikgcm90YXRlKDE4MCkiPgogICAgPHBhdGggaWQ9IlBhdGhfMTkzIiBkYXRhLW5hbWU9IlBhdGggMTkzIiBkPSJNMTY3LjA5NSw0NjEuODE1YTkuMzM5LDkuMzM5LDAsMCwwLTcuMjU3LDIuODYxcS0yLjYzNSwyLjg3My0yLjYzMSw4Ljk3MlY0ODIuMWgxMC4xNjl2LTkuMTMxaC00LjkwOGE5LjIsOS4yLDAsMCwxLDEuMDgxLTQuOTg3LDMuOTI1LDMuOTI1LDAsMCwxLDMuNTQ2LTEuNjM2Wm0xMy45NDUsMGExMS40LDExLjQsMCwwLDAtNC4xMjIuNjkyLDcuNzQ0LDcuNzQ0LDAsMCwwLTMuMDkyLDIuMTQ4LDkuOTM5LDkuOTM5LDAsMCwwLTEuOTUzLDMuNzExLDE4LjQ3OCwxOC40NzgsMCwwLDAtLjY3Nyw1LjI4M1Y0ODIuMWgxMC4ydi05LjEzMWgtNC45YTkuMjEsOS4yMSwwLDAsMSwxLjA3NC00Ljk4NywzLjg0NSwzLjg0NSwwLDAsMSwzLjQ2Ni0xLjYzNloiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAgMCkiIGZpbGw9IiMwMGIwNGMiLz4KICA8L2c+Cjwvc3ZnPgo=");
            background-size: 30px 30px;
            margin-bottom: 10px;
            display: block;
            height: 30px;
            width: 30px;
        }

        blockquote p {
            color: #0b2441;
            font-weight: 500;
        }

        span.text-tiny * {
            font-size: 12px;
        }

        span.text-small * {
            font-size: 14px;
        }

        span.text-big * {
            font-size: 24px;
        }

        span.text-huge * {
            font-size: 28px;
        }

        ol li span.text-tiny {
            font-size: 12px;
        }

        ul li span.text-tiny {
            font-size: 12px;
        }

        ol li span.text-small {
            font-size: 14px;
        }

        ul li span.text-small {
            font-size: 14px;
        }

        ol li span.text-big {
            font-size: 24px;
        }

        ul li span.text-big {
            font-size: 24px;
        }

        ol li span.text-huge {
            font-size: 28px;
        }

        ul li span.text-huge {
            font-size: 28px;
        }

    </style>
</head>
<body>

<h2>INFINITY FIT TERMS OF SERVICE</h2>


<p>IMPORTANT – READ CAREFULLY BEFORE USING INFINITY FIT. THESE TERMS OF SERVICE (“TOS”) IS A LEGAL AGREEMENT BETWEEN YOU AND INFINITY FIT.</p>

<p>INFINITY FIT d.o.o. is a company registered under the laws of Republic of Serbia and its office is in Serbia.</p>


<h2>[INFINITY FIT IMPORTANT NOTICES]</h2>

<p>INFINITY FIT IS INTENDED FOR FITNESS AND WELLNESS PURPOSES ONLY AND IS NOT INTENDED FOR USE IN THE DIAGNOSIS OF DISEASE OR OTHER CONDITIONS, OR IN THE CURE, MITIGATION, TREATMENT, OR PREVENTION OF DISEASE. INFINITY FIT IS NOT INTENDED FOR USE IN THE DETECTION, DIAGNOSIS, MONITORING, MANAGEMENT, OR TREATMENT OF ANY MEDICAL CONDITION, DISEASE OR VITAL PHYSIOLOGICAL PROCESSES OR FOR THE TRANSMISSION OF TIME-SENSITIVE HEALTH INFORMATION. ANY INFORMATION FOUND, ACQUIRED, OR ACCESSED THROUGH INFINITY FIT IS MADE AVAILABLE ONLY FOR YOUR CONVENIENCE AND SHOULD NOT BE TREATED AS MEDICAL ADVICE. YOU SHOULD SEEK MEDICAL ADVICE FROM A DOCTOR BEFORE STARTING A NEW FITNESS OR LIFESTYLE REGIMEN. YOU UNDERSTAND AND AGREE THAT ANY INFORMATION YOU OBTAIN FROM INFINITY FIT MAY NOT BE SUITABLE, ACCURATE, COMPLETE, OR RELIABLE AND THAT INFINITY FIT, SAVE AS OTHERWISE PROVIDED FOR IN THESE TOS, WILL NOT BE HELD LIABLE FOR ANY INJURIES, DAMAGES, LOSSES, AND/OR COSTS ASSOCIATED WITH INFINITY FIT, NOR FOR THE ACCURACY OR RELIABILITY OF ANY INFORMATION FOUND, ACQUIRED, OR ACCESSED THROUGH INFINITY FIT. YOU ACKNOWLEDGE AND AGREE THAT INFINITY FIT IS NOT ENGAGED IN THE PRACTICE OF MEDICINE AND THAT INFINITY FIT DOES NOT DETERMINE THE APPROPRIATE MEDICAL USE OF INFINITY FIT. INFINITY FIT DOES NOT GENERATE ANY INDIVIDUAL BENEFIT FOR PATIENTS.</p>

<p>These TOS may be revised or updated from time to time subject to prior notice to you. You hereby acknowledge that INFINITY FIT may be changed, updated, or discontinued with or without prior notice. Your use of INFINITY FIT may be suspended or terminated without notice if you breach these TOS or engage in any illegal, inappropriate, or prohibited behaviour, such as the rooting, decompiling, or reverse engineering of your mobile device or any of its software. If there is any conflict between these TOS and any other terms and conditions you have agreed to regarding your use of your mobile device, these TOS will prevail.</p>

<p>INFINITY FIT may help you manage your overall fitness and wellness by providing relevant reference information and tools, including tools to help you track your activities and manage your wellbeing.</p>

<p>INFINITY FIT may make available certain services, connecting devices, websites, and software from third-party service providers (“Third-Party Services”), provided and powered by third-party providers, that will enable you to directly communicate with such provider’s contents, devices, and/or software (“Provider”). The Providers are independent of INFINITY FIT and INFINITY FIT does not furnish any of the Providers’ services itself..
    INFINITY FIT is not practising medicine or any healthcare profession through its offer of such Third-Party Services. Your interactions with Providers are not intended to take the place of your relationship with your regular healthcare providers. INFINITY FIT does not evaluate, recommend, or endorse any Third-Party Services. INFINITY FIT does not protect, monitor, or otherwise have access to any information you share with Third-Party Services.</p>

<p>The use of Third-Party Services will be subject to the terms and conditions provided by the respective Provider. You will be presented with the Provider’s terms of use and/or other legal agreements before using a Third-Party Service for the first time. You should read and understand such a third party’s terms of use and/or other legal agreements before using the Third-Party Services. If you do not agree with the Third-Party Service’s terms, you may not use the Third-Party Service. If you agree, but change your mind later, you should stop using such Third-Party Service.</p>


<h2>1. Creating an account</h2>

<p>Subject to the terms of these TOS, you are granted a limited, non-exclusive, non-transferable, and revocable to install, access, and use the SERVICE for your personal and non-commercial use.</p>

<p>If you are under the age of 14, you may not use the SERVICE. If you are 14 or older but under the majority age in your jurisdiction, you must review these TOS with your parent or legal guardian to be sure you and your parent or guardian understand and consent to these TOS. Your parent or guardian must accept these TOS. If you are a parent or guardian permitting a person under the age of majority in your jurisdiction (“Minor”) to use the SERVICE, you agree to: (i) supervise the Minor’s use of the SERVICE; (ii) assume all risks associated with the Minor’s use of the SERVICE; (iii) assume any liability resulting from the Minor’s use of the SERVICE; (iv) ensure the accuracy and truthfulness of all information submitted by the Minor; and (v) assume responsibility and are bound by these TOS for the Minor’s access and use of the SERVICE.</p>


<h2>2. Reservation of Rights and Ownership</h2>

<p>All rights not expressly granted to you hereunder shall belong to INFINITY FIT. This SERVICE includes software and data provided by third parties. You acknowledge that all articles, text, software, music, sound, photographs, graphics, video, page layouts, design and other material available on or through the SERVICE and its applications and all intellectual property rights therein (collectively, the “INFINITY FIT Contents and IPR”) are proprietary to INFINITY FIT and its third-party licensors, and protected under applicable copyright and other intellectual property laws and treaties. These TOS and your use of the SERVICE do not grant you any ownership interest in or to the SERVICE or the INFINITY FIT Contents and IPR, or any data generated by other users of the SERVICE, and you are granted only a limited licence to use that is revocable in accordance with the terms of these TOS. You may not use the SERVICE to reproduce copyrighted materials, or materials to which you do not have the right or legal approval for reproduction.</p>


<h2>3. Restrictions</h2>

<p>You may not (i) reverse engineer, decompile, disassemble, or otherwise attempt to discover the source code or algorithms of the SERVICE except as permitted by applicable law in the jurisdiction where you are a resident; (ii) modify or disable any features of the SERVICE; (iii) create derivative works based on the SERVICE, except to the extent that your local law requires you to be able to do so; (iv) rent, lease, lend, sublicense, or provide commercial hosting services with the SERVICE; (v) infringe INFINITY FIT’s intellectual property rights or those of any third party in relation to your use of the SERVICE (to the extent that such use is not licensed by these TOS); (vi) use the SERVICE in any unlawful manner, for any unlawful purpose, or in any commercial manner, for any commercial purpose, or in any manner inconsistent with these TOS, or act fraudulently or maliciously, for example, by hacking into or inserting malicious code, including viruses, or harmful data, into the SERVICE or any operating system; (vii) use the SERVICE in a way that could damage, disable, overburden, impair or compromise our systems or security or interfere with other users; and (viii) collect or harvest any information or data from the SERVICE or our systems or attempt to decipher any transmissions to or from the servers running the SERVICE; and/or (ix) do any other act or omit to do any other act which may harm or otherwise prejudice any of the SERVICE and/or the INFINITY FIT Contents and IPR.</p>

<p>Representations</p>

<p>By submitting or posting any audio, visual, images, comments, profiles, or other written or graphic materials (“Content”) to the SERVICE, you make the legally binding representations set forth below about yourself, how the Content was created, what it contains and certain other information, and you understand and intend that INFINITY FIT rely on these representations:</p>

<p>If you are the creator of the Content, the Content is your original work and you own it. If created by or with others, you have, or else you have obtained the consent, authorisation, and all of the legal rights from all of the creators or persons or entities who have rights in the Content which are necessary for you to submit it to INFINITY FIT and for INFINITY FIT to distribute and exercise the other rights in this TOS respecting the Content.</p>

<p>You hereby grant INFINITY FIT the right to use, copy, modify, reproduce, or distribute any and all of your Content. You also acknowledge and agree that INFINITY FIT may modify, delete, or hide your Content without notice.</p>

<p>Listing, marketing, delivery, installation, uploading, and use of your Content on the SERVICE does not violate any agreements to which you are a party or of which you are otherwise aware. If your Content includes any open source software, your Content complies with any corresponding licence obligations for such open source software, and will not cause any software or technology used or comprising the SERVICE to be disclosed or otherwise become part of the public domain.</p>

<p>The Content is not:</p>

<ul>
    <li>libellous or defamatory;</li>
    <li>obscene, pornographic, sexually explicit, or vulgar, and does not depict nudity or any sexual acts;</li>
    <li>predatory, hateful, or intended to intimidate or harass any person, and does not contain derogatory name-calling;</li>
    <li>in poor taste or is otherwise objectionable, and</li>
</ul>

<p>The Content does not:</p>

<ul>
    <li>infringe upon or violate the copyrights, trademarks, or other intellectual property rights of any person or entity.</li>
    <li>violate any person’s right to privacy, publicity, or personality;</li>
    <li>violate any local, state, federal, national, or international law;</li>
    <li>contain any viruses, hidden matter, or other malicious elements (including, for example, any “trap doors”, “worms”, “Trojans”, or “time bombs”) or other unauthorised or hidden programmes</li>
    <li>collect any user information for which You have not obtained the user’s consent;</li>
    <li>contain or advocate illegal or violent acts;</li>
    <li>degrade any person on the basis of gender, race, class, ethnicity, national origin, religion, sexual orientation, disability, or other classification;</li>
    <li>contain any prohibited content (as identified in the Documentation, which is defined below);</li>
    <li>contain advertising;</li>
    <li>contain a solicitation of any kind;</li>
    <li>misrepresent Your identity or affiliation;</li>
    <li>impersonate others.</li>
</ul>

<p>If these representations are false in any way, you will be responsible for any damages or losses that INFINITY FIT suffers as a result. In addition, if you discover that any Content violates these representations or that it is otherwise inappropriate, or INFINITY FIT requests that you do so, you will immediately remove any Content that you have posted from the SERVICE.</p>


<h2>4. Update, Change or Suspension of the SERVICE</h2>

<p>INFINITY FIT may, at any time, provide or make available updates or upgrades to the SERVICE (“Updates”), including without limitation, changing the name of the SERVICE, bug fixes, service enhancements, new features, deletion of existing functions, or modification of the SERVICE. Updates will be governed by these TOS unless separate terms and conditions are provided with such Updates which will be duly and timely notified to you, in which case those separate terms and conditions shall govern the Updates. Updates for security software, critical bug fixes, or other important Updates may be downloaded and installed once you have been notified of the importance of receiving such Updates in a timely manner.</p>

<p>INFINITY FIT shall nevertheless be under no obligation to provide any Updates or modifications to the SERVICE.</p>

<p>INFINITY FIT expressly reserves the right to change, suspend, remove, limit the use of, or disable the access to the SERVICE or any portion thereof at any time without notice or liability. It is essential that you back up your data on your device before using the services to avoid losing any data.</p>


<h2>5. Third party Services and Content</h2>

<p>Third-party services and/or content may also be available to you through the SERVICE. To the extent permitted by law, INFINITY FIT disclaims any warranty, condition or representation in respect of any third-party services or content in any aspects including, but not limited to, quality, accuracy, effectiveness, lack of viruses, non-infringement of third-party rights, and compliance with any applicable laws or regulations. The use of third-party services and content may be governed by the terms of use, any licence agreements, privacy policies, or other such agreements of the third-party providers. Certain portions of the SERVICE may be subject to open source licences, in which case the terms of such open source licences may precede the terms of these TOS with respect to that portion of the SERVICE.</p>


<h2>6. Consent for Collection and Use of Data</h2>

<p>You acknowledge and agree that INFINITY FIT and/or parties that are entrusted by INFINITY FIT may collect and use information including your sensitive personal data, necessary to provide the SERVICE and Updates, and improve or enhance the SERVICE. At all times your information will be treated in accordance with the applicable laws and INFINITY FIT’s Privacy Policy. INFINITY FIT may update the Privacy Policy or the INFINITY FIT Privacy Notice from time to time.</p>


<h2>7. Data Access</h2>

<p>Some features of the SERVICE may require your mobile device to have access to the internet. The SERVICE may require access through your mobile network, which may result in additional charges depending on your payment plan. In addition, your enjoyment of some features of the SERVICE may be affected by the suitability and performance of your device hardware or data access.</p>


<h2>8. Compliance with Law</h2>

<p>You acknowledge and agree to comply with any and all applicable laws and regulations in using the SERVICE including without limitation, all applicable export restriction laws and regulations.</p>


<h2>9. Termination</h2>

<p>These TOS are effective upon your acceptance of these TOS, or upon your downloading, accessing or otherwise using the SERVICE. The SERVICE has no predetermined termination date and may continue until such time as either INFINITY FIT or you terminate these TOS by notice to the respective other party or if it is otherwise terminated consistent with these TOS. You can terminate these TOS by ceasing your use of the SERVICE, and uninstalling, deleting software, documentations, and other materials provided by INFINITY FIT. Should you not comply with these TOS, your rights based on these TOS will be automatically terminated. Upon termination of these TOS, you must immediately cease all use of the SERVICE. For the purposes of clarity, INFINITY FIT may terminate these TOS and the SERVICE with notice to you (e.g. service website, pop-up through application, email, etc.).</p>


<h2>10. Disclaimer of Warranty</h2>

<p>SUBJECT TO CLAUSE 11, INFINITY FIT AND INFINITY FIT CONTENTS AND IPR ARE PROVIDED “AS IS” AND ON AN “AS AVAILABLE” BASIS, WITHOUT WARRANTIES OR CONTRACTUAL CONDITIONS OF ANY KIND FROM INFINITY FIT OR ITS THIRD-PARTY LICENSORS OR SUPPLIERS, EITHER EXPRESS OR IMPLIED. TO THE EXTENT PERMITTED BY LAW, INFINITY FIT DISCLAIMS ALL WARRANTIES EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, SATISFACTORY QUALITY, FITNESS FOR A PARTICULAR PURPOSE, RELIABILITY, AVAILABILITY, ACCURACY, LACK OF VIRUSES, QUIET ENJOYMENT, NON INFRINGEMENT OF THIRD-PARTY RIGHTS, OR OTHER VIOLATION OF RIGHTS. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM INFINITY FIT SHALL BE DEEMED TO ALTER THIS DISCLAIMER, OR TO CREATE ANY WARRANTY OF ANY SORT FROM INFINITY FIT.</p>

<p>WITHOUT LIMITING THE FOREGOING, INFINITY FIT DOES NOT WARRANT THAT ACCESS TO INFINITY FIT, OR ANY FEATURE OR FUNCTION THEREIN, WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT DEFECTS, IF ANY, WILL BE CORRECTED; NOR DOES INFINITY FIT MAKE ANY REPRESENTATIONS ABOUT THE ACCURACY, SAFETY, RELIABILITY, CURRENCY, QUALITY, COMPLETENESS, USEFULNESS, PERFORMANCE, SECURITY, LEGALITY, OR SUITABILITY OF INFINITY FIT OR ANY OF THE INFORMATION CONTAINED THEREIN.</p>


<h2>11. Limitation of Liability</h2>

<p>TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL INFINITY FIT OR ITS LICENSORS BE LIABLE TO YOU OR ANY PARTY YOU ALLOW TO USE THE SERVICE FOR ANY INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL, EXEMPLARY, OR PUNITIVE DAMAGES, OR LOST PROFITS, EVEN IF EACH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. THESE LIMITATIONS MAY NOT APPLY TO YOU IF A LAW OF A CERTAIN JURISDICTION DOES NOT ALLOW SUCH LIMITATIONS. YOU ACKNOWLEDGE AND AGREE THAT THE LIMITATIONS HEREIN ARE REASONABLE GIVEN THE BENEFITS OF THE SERVICES AND YOU WILL ACCEPT SUCH RISK AND/OR INSURE ACCORDINGLY.</p>

</body>
</html>
