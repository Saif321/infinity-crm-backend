<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, shrink-to-fit=no, user-scalable=no">
    <meta property=“fb:app_id” content=“2565240367037984">
    <meta property=“og:description” content=“Infinity Fit”>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Roboto&display=swap');
        html * {
            font-size: 14px;
            font-family: Roboto;
            color: #BBBBBB;
            font-weight: 400;
        }

        body {
            -webkit-text-size-adjust: none;
        }

        .image img {
            width: 100%;
            height: auto;
            border-radius: 10px;
        }

        .image {
            margin-block-start: 0;
            margin-block-end: 0;
            margin-inline-start: 0;
            margin-inline-end: 0;
        }

        .image-style-align-center {
            margin-block-start: 1em;
            margin-block-end: 1em;
            margin-inline-start: 40px;
            margin-inline-end: 40px;
        }

        .image-style-align-left {
            float: left;
            margin: 0 16px 0 0;
            width: 50%;
        }

        .image-style-align-right {
            float: right;
            margin: 0 0 0 16px;
            width: 50%;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            color: #0b2441;
            font-weight: 500;
        }

        h1 {
            font-size: 30px;
        }

        h2 {
            font-size: 28px;
        }

        h2 span {
            color: #0b2441;
            font-size: 28px;
        }

        h2 strong {
            font-size: 28px;
            color: #0b2441;
            font-weight: 700;
        }

        h3 {
            font-size: 26px;
        }

        h3 span {
            color: #0b2441;
            font-size: 26px;
        }

        h3 strong {
            font-size: 26px;
            color: #0b2441;
            font-weight: 700;
        }

        h4 {
            font-size: 22px;
        }

        h4 span {
            color: #0b2441;
            font-size: 22px;
        }

        h4 strong {
            font-size: 22px;
            color: #0b2441;
            font-weight: 600;
        }

        h5 {
            font-size: 18px;
        }

        h5 span {
            color: #0b2441;
            font-size: 18px;
        }

        h5 strong {
            font-size: 18px;
            color: #0b2441;
            font-weight: 600;
        }

        h6 {
            font-size: 16px;
        }

        h6 span {
            color: #0b2441;
            font-size: 16px;
        }

        h6 strong {
            font-size: 16px;
            color: #0b2441;
            font-weight: 600;
        }

        p {
            font-size: 16px;
        }

        p strong {
            font-size: 16px;
            color: #0b2441;
            font-weight: 600;
        }

        span.text-tiny {
            font-size: 12px;
        }

        span.text-small {
            font-size: 14px;
        }

        span.text-big {
            font-size: 24px;
        }

        span.text-huge {
            font-size: 28px;
        }

        blockquote {
            margin-block-start: 0;
            margin-block-end: 0;
            margin-inline-start: 0;
            margin-inline-end: 0;
        }

        blockquote p:after {
            content: "";
            background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNC4xOTMiIGhlaWdodD0iMjAuMjg3IiB2aWV3Qm94PSIwIDAgMjQuMTkzIDIwLjI4NyI+CiAgPGcgaWQ9Im5vdW5fUXVvdGVfOTMwNDQyXzFhMWExYSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTE1Ny4yMDcgLTQ2MS44MTUpIj4KICAgIDxwYXRoIGlkPSJQYXRoXzE5MyIgZGF0YS1uYW1lPSJQYXRoIDE5MyIgZD0iTTE2Ny4wOTUsNDYxLjgxNWE5LjMzOSw5LjMzOSwwLDAsMC03LjI1NywyLjg2MXEtMi42MzUsMi44NzMtMi42MzEsOC45NzJWNDgyLjFoMTAuMTY5di05LjEzMWgtNC45MDhhOS4yLDkuMiwwLDAsMSwxLjA4MS00Ljk4NywzLjkyNSwzLjkyNSwwLDAsMSwzLjU0Ni0xLjYzNlptMTMuOTQ1LDBhMTEuNCwxMS40LDAsMCwwLTQuMTIyLjY5Miw3Ljc0NCw3Ljc0NCwwLDAsMC0zLjA5MiwyLjE0OCw5LjkzOSw5LjkzOSwwLDAsMC0xLjk1MywzLjcxMSwxOC40NzgsMTguNDc4LDAsMCwwLS42NzcsNS4yODNWNDgyLjFoMTAuMnYtOS4xMzFoLTQuOWE5LjIxLDkuMjEsMCwwLDEsMS4wNzQtNC45ODcsMy44NDUsMy44NDUsMCwwLDEsMy40NjYtMS42MzZaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwIDApIiBmaWxsPSIjMDBiMDRjIi8+CiAgPC9nPgo8L3N2Zz4=");
            background-size: 30px 30px;
            display: block;
            float: right;
            height: 30px;
            position: relative;
            top: 30px;
            width: 30px;
        }

        blockquote p:before {
            content: "";
            background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNC4xOTMiIGhlaWdodD0iMjAuMjg3IiB2aWV3Qm94PSIwIDAgMjQuMTkzIDIwLjI4NyI+CiAgPGcgaWQ9Im5vdW5fUXVvdGVfOTMwNDQyXzFhMWExYSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTgxLjQgNDgyLjEwMikgcm90YXRlKDE4MCkiPgogICAgPHBhdGggaWQ9IlBhdGhfMTkzIiBkYXRhLW5hbWU9IlBhdGggMTkzIiBkPSJNMTY3LjA5NSw0NjEuODE1YTkuMzM5LDkuMzM5LDAsMCwwLTcuMjU3LDIuODYxcS0yLjYzNSwyLjg3My0yLjYzMSw4Ljk3MlY0ODIuMWgxMC4xNjl2LTkuMTMxaC00LjkwOGE5LjIsOS4yLDAsMCwxLDEuMDgxLTQuOTg3LDMuOTI1LDMuOTI1LDAsMCwxLDMuNTQ2LTEuNjM2Wm0xMy45NDUsMGExMS40LDExLjQsMCwwLDAtNC4xMjIuNjkyLDcuNzQ0LDcuNzQ0LDAsMCwwLTMuMDkyLDIuMTQ4LDkuOTM5LDkuOTM5LDAsMCwwLTEuOTUzLDMuNzExLDE4LjQ3OCwxOC40NzgsMCwwLDAtLjY3Nyw1LjI4M1Y0ODIuMWgxMC4ydi05LjEzMWgtNC45YTkuMjEsOS4yMSwwLDAsMSwxLjA3NC00Ljk4NywzLjg0NSwzLjg0NSwwLDAsMSwzLjQ2Ni0xLjYzNloiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAgMCkiIGZpbGw9IiMwMGIwNGMiLz4KICA8L2c+Cjwvc3ZnPgo=");
            background-size: 30px 30px;
            margin-bottom: 10px;
            display: block;
            height: 30px;
            width: 30px;
        }

        blockquote p {
            color: #0b2441;
            font-weight: 500;
        }

        span.text-tiny * {
            font-size: 12px;
        }

        span.text-small * {
            font-size: 14px;
        }

        span.text-big * {
            font-size: 24px;
        }

        span.text-huge * {
            font-size: 28px;
        }

        ol li span.text-tiny {
            font-size: 12px;
        }

        ul li span.text-tiny {
            font-size: 12px;
        }

        ol li span.text-small {
            font-size: 14px;
        }

        ul li span.text-small {
            font-size: 14px;
        }

        ol li span.text-big {
            font-size: 24px;
        }

        ul li span.text-big {
            font-size: 24px;
        }

        ol li span.text-huge {
            font-size: 28px;
        }

        ul li span.text-huge {
            font-size: 28px;
        }

    </style>
</head>
<body>

<h2>USLOVI KORIŠĆENJA INFINITY FIT APLIKACIJE</h2>


<p>OVI USLOVI KORIŠĆENJA SU ZAKONIT UGOVOR IZMEĐU VAS I KOMPANIJE INFINITY FIT O KORIŠĆENJU USLUGE INFINITY FIT I ODGOVARAJUĆIH INFORMACIJA, KARAKTERISTIKA. AKO PRIHVATITE OVE USLOVE KORIŠĆENJA ILI AKO INSTALIRATE ILI NA BILO KOJI DRUGI NAČIN KORISTITE INFINITY FIT, PRIHVATATE OVE USLOVE KORIŠĆENJA, KOJI VAS OBAVEZUJU. AKO NE PRIHVATATE OVE USLOVE KORIŠĆENJA, U TOM SLUČAJU NEMOJTE DA INSTALIRATE NITI DA KORISTITE INFINITY FIT.</p>

<p>Infinityfit d.o.o. Beograd je kompanija koja je registrovana u skladu sa zakonom Republike Srbije.</p>


<h2>VAŽNA OBAVEŠTENJA O APLIKACIJI INFINITY FIT</h2>
<p>APLIKACIJA INFINITY FIT JE NAMENJENA SAMO U SVRHE KONDICIJE I OPŠTEG ZDRAVLJA I NIJE NAMENJENA ZA KORIŠĆENJE U DIJAGNOSTICI BOLESTI ILI DRUGIH STANJA NITI ZA LEČENJE, UBLAŽAVANJE SIMPTOMA, TERAPIJU ILI PREVENCIJU BOLESTI. APLIKACIJA INFINITY FIT NIJE NAMENJENA ZA KORIŠĆENJE U OTKRIVANJU, DIJAGNOSTICI, NADGLEDANJU, REGULISANJU ILI LEČENJU BILO KAKVOG MEDICINSKOG STANJA, BOLESTI ILI VITALNIH FIZIOLOŠKIH PROCESA ILI ZA PRENOS VREMENSKI OSETLJIVIH ZDRAVSTVENIH INFORMACIJA. SVE INFORMACIJE KOJE SU PRONAĐENE I PRIBAVLJENE ILI KOJIMA JE PRISTUPANO PUTEM APLIKACIJE INFINITY FIT SU DOSTUPNE SAMO ZA VAŠU UPOTREBU I NE SMEJU SE SMATRATI MEDICINSKIM SAVETOM. TREBALO BI DA POTRAŽITE MEDICINSKI SAVET LEKARA PRE NEGO ŠTO ZAPOČNETE NOVI REŽIM ZA KONDICIJU ILI ŽIVOTNI STIL. RAZUMETE I PRIHVATATE DA SVE INFORMACIJE KOJE DOBIJETE OD APLIKACIJE INFINITY FIT MOŽDA NEĆE BITI POGODNE, TAČNE, KOMPLETNE ILI POUZDANE I DA KOMPANIJA INFINITY FIT, OSIM KADA JE DRUGAČIJE PREDVIĐENO OVIM USLOVIMA KORIŠĆENJA, NEĆE BITI ODGOVORNA ZA BILO KOJE POVREDE, ŠTETU, GUBITKE I/ILI TROŠKOVE U VEZI SA APLIKACIJOM INFINITY FIT, NITI ZA TAČNOST ILI POUZDANOST BILO KOJIH INFORMACIJA KOJE SU PRONAĐENE I PRIBAVLJENE ILI KOJIMA JE PRISTUPANO PUTEM APLIKACIJE INFINITY FIT. POTVRĐUJETE I SLAŽETE SE DA KOMPANIJA INFINITY FIT NIJE ANGAŽOVANA ZA PRUŽANJE USLUGE MEDICINSKE PRAKSE I DA KOMPANIJA INFINITY FIT NE ODREĐUJE ODGOVARAJUĆE MEDICINSKO KORIŠĆENJE APLIKACIJE INFINITY FIT. APLIKACIJA INFINITY FIT NE GENERIŠE NIKAKVE POJEDINAČNE POGODNOSTI ZA PACIJENTE.</p>

<p>U skladu sa prethodnim obaveštenjem, ovi Uslovi Korišćenja mogu da se revidiraju ili ažuriraju s vremena na vreme. Ovim prihvatate da aplikacija INFINITY FIT može da se izmeni, ažurira ili ukine uz prethodno obaveštenje ili bez njega. Vaše korišćenje aplikacije INFINITY FIT može da se obustavi ili prekine bez prethodnog obaveštenja ukoliko prekršite ove Uslove Korišćenja ili ukoliko učestvujete u nekoj nezakonitoj, neprikladnoj ili zabranjenoj radnji kao što je rutovanje, dekompilacija ili reverzni inženjering mobilnog uređaja ili bilo kog softvera uređaja. Ukoliko postoji konflikt između ovih Uslova Korišćenja i drugih prihvaćenih uslova i odredbi koje se tiču korišćenja mobilnog uređaja, ovi Uslovi Korišćenja će imati prednost.</p>

<p>Aplikacija INFINITY FIT može da vam pomogne u regulisanju sveukupne kondicije i opšteg zdravlja pružanjem odgovarajućih referentnih informacija i alatki, uključujući alatke koje će vam pomoći da pratite svoje aktivnosti i regulišete svoje blagostanje.
Možete i da koristite aplikaciju INFINITY FIT i da upravljate svojim podacima tako što ćete se prijaviti na INFINITY FIT nalog.</p>

<p>Aplikacija INFINITY FIT može da učini dostupnim određene usluge, uređaje za povezivanje, sajtove i softver provajdera usluga treće strane („Usluge treće strane“), koje pružaju i obezbeđuju provajderi treće strane, i ona će vam omogućiti da direktno komunicirate sa takvim sadržajima, uređajima i/ili softverom provajdera („Provajder“). Provajderi su nezavisni od kompanije INFINITY FIT i sama aplikacija INFINITY FIT ne oprema usluge Provajdera.
Ovime potvrđujete i slažete se da vam sve Usluge treće strane pruža Provajder, a ne kompanija INFINITY FIT. Kompanija INFINITY FIT nije odgovorna za tačnost, pouzdanost, pravovremenost i potpunost informacija koje se nalaze u svim Uslugama treće strane niti za bezbednost prilikom njihovog korišćenja.
Kompanija INFINITY FIT ne praktikuje medicinu niti bilo koju zdravstvenu profesiju pružajući takve Usluge treće strane. Vaše interakcije sa Provajderima nemaju za cilj da zamene odnos koji imate sa pružaocima redovnih zdravstvenih usluga. Kompanija INFINITY FIT ne procenjuje, ne preporučuje niti zastupa bilo koje Usluge treće strane. Kompanija INFINITY FIT ne štiti, ne nadgleda niti na bilo koji drugi način pristupa bilo kojim informacijama koje delite koristeći Usluge treće strane.</p>

<p>Korišćenje Usluga treće strane će biti predmet uslova i odredbi koje obezbeđuje odgovarajući Provajder. Uslovi korišćenja i/ili drugi pravni ugovori Provajdera će vam biti izloženi pre prvog korišćenja Usluge treće strane. Trebalo bi da pročitate i razumete uslove korišćenja i/ili druge pravne ugovore treće strane pre korišćenja Usluga treće strane. Ukoliko ne prihvatate uslove Usluge treće strane, ne smete da koristite Uslugu treće strane. Ukoliko prihvatate, ali se kasnije predomislite, trebalo bi da prestanete da koristite Uslugu treće strane.</p>


<h2>1. Kreiranje naloga</h2>

<p>U skladu sa uslovima ovih Uslovima Korišćenja, omogućena je instalacija aplikacije i kreiranje jedinstvenog naloga za pristup i korišćenje USLUGE za ličnu i nekomercijalnu upotrebu.</p>

<p>Pored toga, usluge i funkcije koje obezbeđuje USLUGA ili Ažuriranje (koje je definisano u nastavku) mogu se razlikovati ili biti ograničene u zavisnosti od određenih faktora.</p>

<p>Ukoliko imate manje od 14 godina, ne smete da koriste USLUGU. Ukoliko imate 14 godina ili više, a manje od granice punoletstva u jurisdikciji u kojoj živite, morate da pregledate ove Uslove Korišćenja sa roditeljem ili zakonskim starateljem da biste vi i vaš roditelj ili staratelj razumeli ove Uslove Korišćenja i da biste ste saglasili sa njima. Vaš roditelj ili staratelj mora da prihvati ove Uslove Korišćenja. Ukoliko ste roditelj ili staratelj osobe koja ima manje godina od granice punoletstva u jurisdikciji u kojoj živite („Maloletnog lica“), koji joj dozvoljava da koristi USLUGU, prihvatate da: (i) nadzirete korišćenje USLUGE od strane Maloletnog lica; (ii) preuzimate na sebe sve rizike koji proizilaze iz korišćenja USLUGE od strane Maloletnog lica; (iii) preuzimate na sebe sve odgovornosti koje proizilaze iz korišćenja USLUGE od strane Maloletnog lica; (iv) obezbeđujete tačnost i istinitost svih informacija koje je dostavilo Maloletno lice i (v) preuzimate odgovornost i obaveze nametnute ovim Uslovima Korišćenja o pristupanju USLUZI i njenog korišćenja od strane Maloletnog lica.</p>


<h2>2. Garantovanje prava i vlasništva</h2>

<p>Sva prava koja vam nisu ovde izričito dodeljena pripadaju kompaniji INFINITY FIT. Ova USLUGA sadrži softver i podatke koje obezbeđuju treće strane. Potvrđujete da svi članci, tekst, softver, muzika, zvuk, fotografije, grafika, video zapis, rasporedi stranica, dizajn i ostali materijal koji je dostupan putem USLUGE i njenih aplikacija, kao i sva prava na intelektualnu svojinu koja su navedena ovde (zajednički nazvane „INFINITY FIT“) pripadaju kompaniji INFINITY FIT i da su zaštićeni važećim autorskim pravom i drugim zakonima i ugovorima o pravima na intelektualnu svojinu. Ovi Uslove Korišćenja i vaše korišćenje USLUGE vam ne garantuju nikakav vlasnički interes u USLUZI ili INFINITY FIT sadržajima, ili u svim podacima koje generišu drugi korisnici USLUGE. Ne smete da koristite USLUGU za reprodukovanje materijala zaštićenog autorskim pravom ili materijala na koji nemate pravo reprodukovanja niti pravno odobrenje za to.</p>


<h2>3. Ograničenja</h2>

<p>Ne smete da (i) vršite inverzni inženjering, dekompilaciju, demontiranje ili otkrivanje izvornog koda USLUGE ili njenih algoritama na neki drugi način, osim ukoliko to nije dozvoljeno važećim zakonom koji podleže pravosudnom sistemu zemlje čiji ste rezident; (ii) modifikujete ili onemogućite bilo koje funkcije USLUGE; (iii) pravite izvedene proizvode koji se zasnivaju na ovoj USLUZI osim u meri u kojoj vam lokalni zakon omogućava da to učinite; (iv) iznajmljujete, dajete u zakup, pozajmljujete ovu USLUGU ili pružate usluge komercijalnog hostinga pomoću ove USLUGE; (v) kršite prava na intelektualnu svojinu kompanije INFINITY FIT ili prava neke treće strane u vezi sa vašim korišćenjem ove USLUGE (ukoliko takvo korišćenje nije licencirano ovim); (vi) koristite USLUGU na bilo koji nezakonit način, u bilo koje nezakonite svrhe ili na bilo koji komercijalan način, u bilo koje komercijalne svrhe ili na bilo koji način koji nije u skladu sa ovim Uslovima Korišćena ili da se ponašate lažno ili zlonamerno, na primer, tako što ćete da hakujete ili ubacujete zlonamerni kod, uključujući viruse ili štetne podatke u USLUGU ili bilo koji operativni sistem; (vii) koristite USLUGU na način koji bi mogao da ošteti, onemogući, preoptereti, uništi ili ugrozi naše sisteme ili bezbednost ili da ometa druge korisnike; niti da (viii) prikupljate ili sakupljate bilo kakve informacije ili podatke iz USLUGE ili sa naših sistema, ili da pokušavate da dešifrujete prenos sa servera ili na njih pokretanjem USLUGE i/ili da (ix) obavljate ili ne obavljate bilo kakvu radnju čije obavljanje ili neobavljanje može da naškodi ili na neki drugi način stvori predrasude o bilo kojoj USLUZI i/ili bilo kojim INFINITY FIT sadržaju.</p>

<p>Izjave</p>

<p>Slanjem bilo kog zvučnog ili vizuelnog materijala, slika, komentara, profila ili drugog pisanog ili grafičkog materijala („sadržaj“) USLUZI ili njihovim objavljivanjem u USLUZI dajete pravno obavezujuće izjave navedene u nastavku o sebi, o tome kako je sadržaj napravljen, od čega se sadržaj sastoji i neke druge informacije i razumete i izražavate nameru da se kompanija INFINITY FIT oslanja na te izjave:</p>

<p>Ako ste vi kreator sadržaja, sadržaj je originalno delo i vi ste njegov vlasnik. Ako su sadržaj napravila druga lica ili je sadržaj napravljen uz pomoć drugih lica ili u nekom drugom slučaju, pribavili ste saglasnost, odobrenje i sva zakonska prava od svih autora ili osoba ili subjekata koji imaju prava na sadržaj, a koji su neophodni da biste poslali sadržaj kompaniji INFINITY FIT i da kompanija INFINITY FIT distribuira i ostvari druga prava u ovim Uslovima Korišćenja koja se odnose na sadržaj.</p>

<p>Ovim kompaniji INFINITY FIT dodeljujete pravo na korišćenje, kopiranje, menjanje, reprodukovanje ili distribuiranje bilo kog sadržaja i svih njegovih delova. Takođe potvrđujete i prihvatate da kompanija INFINITY FIT može da izmeni, izbriše ili sakrije sadržaj bez obaveštenja.</p>

<p>Navođenje, reklamiranje, isporuka, instaliranje, otpremanje i korišćenje sadržaja u USLUZI ne krši nijedan ugovor u kome ste vi ugovorna strana ili čijeg ste postojanja na drugi način svesni. Ako sadržaj obuhvata bilo koji softver otvorenog koda, sadržaj je u skladu sa odgovarajućim obavezama koje se odnose na licencu za taj softver otvorenog koda i sadržaj neće dovesti do otkrivanja bilo kog korišćenog softvera ili bilo koje korišćene tehnologije ili do njihovog otkrivanja koje ugrožava USLUGU ili do toga da na drugi način postanu deo javnog domena.</p>

<p>Sadržaj nije:</p>

<ul>
    <li>pogrdan ili klevetnički;</li>
    <li>opscen, pornografski, seksualno eksplicitan ili vulgaran i ne prikazuje takav sadržaj;</li>
    <li>usmeren na iskorišćavanje nijedne osobe, pun mržnje niti mu je namena da uplaši ili povredi bilo koju osobu niti sadrži uvredljiva imena;</li>
    <li>sadržaj koji je neprikladan ili na drugi način nepoželjan i</li>
</ul>

<p>Sadržaj ne:</p>

<ul>
    <li>krši i ne povređuje autorska prava, zaštitne znakove ili druga prava na intelektualnu svojinu bilo kog lica ili subjekta;</li>
    <li>povređuje prava bilo koje osobe na privatnost, publicitet ili ličnost;</li>
    <li>krši bilo koji lokalni, državni, federalni, nacionalni ili međunarodni zakon;</li>
    <li>sadrži nijedan virus, sakriven materijal ili drugi zlonameran element (uključujući, na primer, bilo koje „zamke“, „crve“, „trojance“ ili „vremenske bombe“) ili druge neovlašćene ili sakrivene programe;</li>
    <li>sakuplja nikakve korisničke informacije za koje niste pribavili saglasnost korisnika;</li>
    <li>sadrži i zalaže se za nezakonite ili nasilne radnje;</li>
    <li>ponižava nijednu osobu na osnovu pola, rase, klase, etničke grupe, nacionalnog porekla, religije, seksualne orijentacije, invaliditeta ili druge klasifikacije;</li>
    <li>sadrži nijedan zabranjen sadržaj (utvrđen u Dokumentaciju definisanoj u nastavku);</li>
    <li>sadrži oglase;</li>
    <li>uključuje ponude bilo koje vrste;</li>
    <li>predstavlja lažno vaš identitet ili pripadnost i</li>
    <li>koji ne predstavlja lažno druge osobe.</li>
</ul>

<p>Ako su ove izjave netačne na bilo koji način, bićete odgovorni za bilo koju štetu ili gubitke kojima kompanija INFINITY FIT bude posledično podlegla. Nadalje, ako otkrijete da bilo koji sadržaj krši ove izjave ili da je na drugi način neprikladan ili ako kompanija INFINITY FIT od vas zatraži, odmah ćete ukloniti bilo koji sadržaj koji ste objavili iz USLUGE.</p>


<h2>4. Ažuriranje, promena ili obustavljanje USLUGE</h2>

<p>Kompanija INFINITY FIT u svakom trenutku može da obezbedi ili stavi na raspolaganje ažuriranja ili nadogradnje za USLUGU („Ažuriranje“), uključujući bez ograničenja i promenu naziva USLUGE, popravke grešaka, nadogradnje usluga, nove funkcije, brisanje postojećih funkcija ili izmenu USLUGE. Ažuriranja će regulisati ovi Uslovi Korišćenja, osim ako u okviru tih Ažuriranja nisu obezbeđeni posebni uslovi i odredbe o kojima ćete uredno i blagovremeno biti obavešteni, i u tom slučaju ti posebni uslovi korišćenja će regulisati Ažuriranja. Ažuriranja softvera za bezbednost, popravke kritičnih grešaka ili druga važna Ažuriranja mogu da se preuzmu i instaliraju čim budete obavešteni o značaju blagovremenog dobijanja takvih Ažuriranja.</p>

<p>Kompanija INFINITY FIT ipak neće biti u obavezi da pruži bilo kakva Ažuriranja ili izmene USLUGE.</p>

<p>Kompanija INFINITY FIT izričito zadržava pravo da u svakom trenutku promeni, suspenduje, ukloni i ograniči upotrebu USLUGE ili nekog njenog dela ili onemogući pristup istim, bez prethodnog obaveštenja i odgovornosti. Obavezno napravite rezervnu kopiju podataka na uređaju pre korišćenja usluga kako se podaci ne bi izgubili.</p>


<h2>5. Usluge i sadržaj treće strane</h2>

<p>Mogu vam biti dostupne i usluge i/ili sadržaj treće strane putem ove USLUGE. U meri u kojoj to zakon dozvoljava, kompanija INFINITY FIT se odriče svih garancija, uslova ili izjava za sve usluge ili sadržaj treće strane u svim aspektima, uključujući bez ograničenja i kvalitet, preciznost, efikasnost, nepostojanje virusa, pravnu nepovredivost prava treće strane i usklađenost sa važećim zakonima i propisima. Korišćenje usluga i sadržaja treće strane može da bude regulisano uslovima korišćenja, bilo kojim ugovorima o licenciranju, politikama privatnosti ili drugim sličnim ugovorima provajdera treće strane. Određeni delovi USLUGE mogu da podležu licencama otvorenog koda i u tom slučaju uslovi za korišćenje tih licenci otvorenog koda mogu da prethode ovim uslovima Uslovima Korišćenja u vezi sa tim delom USLUGE.</p>


<h2>6. Pristanak na prikupljanje i korišćenje podataka</h2>

<p>Potvrđujete i slažete se da kompanija INFINITY FIT i/ili strane kojima to ona poveri mogu da prikupljaju i koriste informacije, uključujući i osetljive lične podatke neophodne za pružanje USLUGE i Ažuriranja, kao i za unapređenje ili poboljšanje USLUGE. U svakom trenutku, vaše informacije će biti tretirane u skladu sa važećim zakonima i dokumentom Politika privatnosti kompanije INFINITY FIT. Kompanija INFINITY FIT može da, s vremena na vreme, ažurira dokument Politika privatnosti zato periodično pregledajte dokument Politika privatnosti za aplikaciju INFINITY FIT.</p>


<h2>7. Pristup podacima</h2>

<p>Funkcionalnosti USLUGE zahtevaju da vaš mobilni uređaj ima pristup internetu. Ova USLUGA može da zahteva pristup internetu putem mobilne mreže, što može dovesti do dodatnih troškova u zavisnosti od vašeg tarifnog paketa. Pored toga, na vaše uživanje u nekim od funkcija USLUGE mogu da utiču pogodnost i performanse hardvera u uređaju ili pristup podacima.</p>


<h2>8. Usaglašenost sa zakonom</h2>

<p>Potvrđujete i slažete se da ćete poštovati bilo koji zakon i sve važeće zakone i propise prilikom korišćenja USLUGE, uključujući bez ograničenja sve važeće zakone i propise o ograničenju izvoza.</p>


<h2>9. Prestanak ugovora</h2>

<p>Ovi Uslovi Korišćenja stupaju na snagu nakon što preuzmete USLUGU, pristupite joj ili je koristite na bilo koji drugi način. Ne postoji unapred određeni datum prestanka ugovora o korišćenju USLUGE i možete je koristiti do trenutka kada kompanija INFINITY FIT bude prekinula ove Uslove Korišćenja. Ove Uslove Korišćenja možete prekinuti tako što ćete prestati da koristite USLUGU i deinstalirati, izbrisati i vratiti sve odgovarajuće softvere, dokumentaciju i drugi materijal koji vam je obezbedila kompanija INFINITY FIT, kao i sve rezervne kopije istih. Ako se ne budete pridržavali ovih Uslova Korišćenja, vaša prava zasnovana na ovim Uslovima Korišćenja biće automatski okončana. Nakon raskidanja ovih Uslova Korišćebha, morate odmah da prekinete svaku upotrebu ove USLUGE. Da objasnimo bolje, kompanija INFINITY FIT može da prekine ove Uslove Korišćenja i USLUGU uz obaveštenje (npr. putem sajta usluge, iskačućeg prozora u aplikaciji, e-pošte ili na drugi način).</p>


<h2>10. Odricanje odgovornosti za garanciju</h2>

<p>U SKLADU SA KLAUZULOM 11, APLIKACIJA INFINITY FITI SADRŽAJI APLIKACIJE INFINITY FIT SE OBEZBEĐUJU „KAKVI JESU“ I „PO PRINCIPU DOSTUPNOSTI“, BEZ GARANCIJA ILI UGOVORNIH USLOVA BILO KOJE VRSTE KOJE DAJE KOMPANIJA INFINITY FIT ILI NJENI DAVAOCI LICENCI TREĆE STRANE ILI DOBAVLJAČI, BILO DIREKTNO ILI INDIREKTNO. U MERI U KOJOJ TO ZAKON DOZVOLJAVA, KOMPANIJA INFINITY FIT ODRIČE SVE IZREČENE GARANCIJE, PODRAZUMEVANE ILI ZAKONSKE, ŠTO BEZ OGRANIČENJA OBUHVATA PODRAZUMEVANE GARANCIJE KOJE SE ODNOSE NA MOGUĆNOST PRODAJE, ZADOVOLJAVAJUĆI KVALITET, POGODNOST ZA ODREĐENU NAMENU, POUZDANOST, DOSTUPNOST, PRECIZNOST, NEPOSTOJANJE VIRUSA, NEOMETANO KORIŠĆENJE, POŠTOVANJE PRAVA TREĆE STRANE ILI POVREDU DRUGIH PRAVA. NIKAKVI SAVETI ILI INFORMACIJE, BILO USMENE ILI PISANE, KOJE STE DOBILI OD KOMPANIJE INFINITY FIT, NEĆE SE SMATRATI IZMENOM OVOG ODRICANJA ODGOVORNOSTI, KAO NI KREIRANJEM BILO KAKVE GARANCIJE BILO KOJE VRSTE OD STRANE KOMPANIJE INFINITY FIT.</p>

<p>NE OGRANIČAVAJUĆI SE NA GORNJE NAVODE, KOMPANIJA INFINITY FIT NE GARANTUJE DA ĆE PRISTUP APLIKACIJI INFINITY FIT ILI BILO KOJOJ KARAKTERISTICI ILI FUNKCIJI U NJOJ BITI BEZ PREKIDA ILI GREŠKE, ILI DA ĆE DEFEKTI, UKOLIKO IH IMA, BITI ISPRAVLJENI; NITI KOMPANIJA INFINITY FIT DAJE BILO KAKVE IZJAVE O TAČNOSTI, BEZBEDNOSTI, POUZDANOSTI, OPŠTOJ PRIHVAĆENOSTI, KVALITETU, POTPUNOSTI, KORISNOSTI, PERFORMANSAMA, BEZBEDNOSTI, LEGALNOSTI ILI POGODNOSTI APLIKACIJE INFINITY FITILI BILO KOJIH INFORMACIJA KOJE SU SADRŽANE U NJOJ.</p>


<h2>11. Ograničenje odgovornosti</h2>

<p>U MERI U KOJOJ TO ZAKON DOZVOLJAVA, KOMPANIJA INFINITY FIT I NJEN DAVAOCI LICENCI NI U KOM SLUČAJU NEĆE BITI ODGOVORNI VAMA ILI BILO KOJOJ STRANI KOJOJ DOZVOLITE DA KORISTI USLUGU ZA BILO KOJU INDIREKTNU, SLUČAJNU, POSLEDIČNU, POSEBNU, OPOMINJUĆU ILI KAZNENU ŠTETU ILI ZA GUBITAK PROFITA ČAK I U SLUČAJU DA JE SVAKOME SAVETOVANO DA POSTOJI MOGUĆNOST ZA POJAVU TAKVE ŠTETE. OVA OGRANIČENJA SE MOŽDA NE ODNOSE NA VAS UKOLIKO NADLEŽAN SUD NE DOZVOLJAVA TAKVA OGRANIČENJA. POTVRĐUJETE I SLAŽETE SE DA SU OGRANIČENJA KOJA SU NAVEDENA OVDE RAZUMNA, UZEVŠI U OBZIR PREDNOSTI USLUGA I PRIHVATIĆETE TAKAV RIZIK I/ILI SHODNO TOME ĆETE SE OSIGURATI.</p>

</body>
</html>
