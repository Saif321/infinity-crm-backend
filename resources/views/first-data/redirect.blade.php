<html>
<head><title>IPG Connect Sample for PHP</title></head>
<body>

<form id="payment_form" method="post" action="{{ $gatewayUrl }}">
    <input type="hidden" name="txntype" value="{{ $transactionType }}">
    <input type="hidden" name="timezone" value="{{ $timezone }}"/>
    <input type="hidden" name="txndatetime" value="{{ $dateTime }}"/>
    <input type="hidden" name="hash_algorithm" value="{{ $hashAlgorithm }}"/>
    <input type="hidden" name="hash" value="{{ $hash }}"/>
    <input type="hidden" name="storename" value="{{ $storeName }}"/>
    <input type="hidden" name="mode" value="{{ $mode }}"/>
    <input type="hidden" name="chargetotal" value="{{$chargeTotal}}"/>
    <input type="hidden" name="currency" value="{{$currency}}"/>
    <input type="hidden" name="responseSuccessURL" value="{{$successfulPaymentUrl}}"/>
    <input type="hidden" name="responseFailURL" value="{{$failedPaymentUrl}}"/>
    <input type="hidden" name="comments" value="{{$comments}}"/>
    <input type="hidden" name="customerid" value="{{$customerId}}"/>
    <input type="hidden" name="language" value="{{$language}}"/>
    <input type="hidden" name="mobileMode" value="{{$mobileMode}}"/>
    <input type="hidden" name="oid" value="{{$orderId}}"/>
    <input type="hidden" name="hosteddataid" value="{{$hostedDataId}}"/>
</form>
<script>
    document.getElementById('payment_form').submit();
</script>
</body>
</html>
