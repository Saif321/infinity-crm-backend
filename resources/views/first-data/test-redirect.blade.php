<html>
<head><title>IPG Connect Sample for PHP</title></head>
<body>

<form id="payment_form" method="post" action="{{ $gatewayUrl }}">
    <label>txntype<input type="text" size="100" name="txntype" value="{{ $transactionType }}"></label>
    <br />
    <label>timezone<input type="text" size="100" name="timezone" value="{{ $timezone }}"/></label>
    <br />
    <label>txndatetime<input type="text" size="100" name="txndatetime" value="{{ $dateTime }}"/></label>
    <br />
    <label>hash_algorithm<input type="text" size="100" name="hash_algorithm" value="{{ $hashAlgorithm }}"/></label>
    <br />
    <label>hash<input type="text" size="100" name="hash" value="{{ $hash }}"/></label>
    <br />
    <label>storename<input type="text" size="100" name="storename" value="{{ $storeName }}"/></label>
    <br />
    <label>mode<input type="text" size="100" name="mode" value="{{ $mode }}"/></label>
    <br />
    <label>chargetotal<input type="text" size="100" name="chargetotal" value="{{$chargeTotal}}"/></label>
    <br />
    <label>currency<input type="text" size="100" name="currency" value="{{$currency}}"/></label>
    <br />
    <label>responseSuccessURL<input type="text" size="100" name="responseSuccessURL" value="{{$successfulPaymentUrl}}"/></label>
    <br />
    <label>responseFailURL<input type="text" size="100" name="responseFailURL" value="{{$failedPaymentUrl}}"/></label>
    <br />
    <label>comments<input type="text" size="100" name="comments" value="{{$comments}}"/></label>
    <br />
    <label>language<input type="text" size="100" name="language" value="{{$language}}"/></label>
    <br />
    <label>oid<input type="text" size="100" name="oid" value="{{$orderId}}"/></label>
    <br />
    <label>merchantTransactionId<input type="text" size="100" name="merchantTransactionId" value="{{$orderId}}"/></label>
    <br />
    <label>referencedMerchantTransactionId<input type="text" size="100" name="referencedMerchantTransactionId"/></label>
    <br />
    <label>trxOrigin<input type="text" size="100" name="trxOrigin" value="{{$trxOrigin}}"/></label>
    <br />
    <input type="submit" value="Submit">
</form>
</body>
</html>
