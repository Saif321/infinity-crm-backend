<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, shrink-to-fit=no, user-scalable=no">
    <meta property=“fb:app_id” content=“2565240367037984">
    <meta property=“og:description” content=“Infinity Fit”>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Roboto&display=swap');
        html * {
            font-size: 14px;
            font-family: Roboto;
            color: #BBBBBB;
            font-weight: 400;
        }

        body {
            -webkit-text-size-adjust: none;
        }

        .image img {
            width: 100%;
            height: auto;
            border-radius: 10px;
        }

        .image {
            margin-block-start: 0;
            margin-block-end: 0;
            margin-inline-start: 0;
            margin-inline-end: 0;
        }

        .image-style-align-center {
            margin-block-start: 1em;
            margin-block-end: 1em;
            margin-inline-start: 40px;
            margin-inline-end: 40px;
        }

        .image-style-align-left {
            float: left;
            margin: 0 16px 0 0;
            width: 50%;
        }

        .image-style-align-right {
            float: right;
            margin: 0 0 0 16px;
            width: 50%;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            color: #0b2441;
            font-weight: 500;
        }

        h1 {
            font-size: 30px;
        }

        h2 {
            font-size: 28px;
        }

        h2 span {
            color: #0b2441;
            font-size: 28px;
        }

        h2 strong {
            font-size: 28px;
            color: #0b2441;
            font-weight: 700;
        }

        h3 {
            font-size: 26px;
        }

        h3 span {
            color: #0b2441;
            font-size: 26px;
        }

        h3 strong {
            font-size: 26px;
            color: #0b2441;
            font-weight: 700;
        }

        h4 {
            font-size: 22px;
        }

        h4 span {
            color: #0b2441;
            font-size: 22px;
        }

        h4 strong {
            font-size: 22px;
            color: #0b2441;
            font-weight: 600;
        }

        h5 {
            font-size: 18px;
        }

        h5 span {
            color: #0b2441;
            font-size: 18px;
        }

        h5 strong {
            font-size: 18px;
            color: #0b2441;
            font-weight: 600;
        }

        h6 {
            font-size: 16px;
        }

        h6 span {
            color: #0b2441;
            font-size: 16px;
        }

        h6 strong {
            font-size: 16px;
            color: #0b2441;
            font-weight: 600;
        }

        p {
            font-size: 16px;
        }

        p strong {
            font-size: 16px;
            color: #0b2441;
            font-weight: 600;
        }

        span.text-tiny {
            font-size: 12px;
        }

        span.text-small {
            font-size: 14px;
        }

        span.text-big {
            font-size: 24px;
        }

        span.text-huge {
            font-size: 28px;
        }

        blockquote {
            margin-block-start: 0;
            margin-block-end: 0;
            margin-inline-start: 0;
            margin-inline-end: 0;
        }

        blockquote p:after {
            content: "";
            background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNC4xOTMiIGhlaWdodD0iMjAuMjg3IiB2aWV3Qm94PSIwIDAgMjQuMTkzIDIwLjI4NyI+CiAgPGcgaWQ9Im5vdW5fUXVvdGVfOTMwNDQyXzFhMWExYSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTE1Ny4yMDcgLTQ2MS44MTUpIj4KICAgIDxwYXRoIGlkPSJQYXRoXzE5MyIgZGF0YS1uYW1lPSJQYXRoIDE5MyIgZD0iTTE2Ny4wOTUsNDYxLjgxNWE5LjMzOSw5LjMzOSwwLDAsMC03LjI1NywyLjg2MXEtMi42MzUsMi44NzMtMi42MzEsOC45NzJWNDgyLjFoMTAuMTY5di05LjEzMWgtNC45MDhhOS4yLDkuMiwwLDAsMSwxLjA4MS00Ljk4NywzLjkyNSwzLjkyNSwwLDAsMSwzLjU0Ni0xLjYzNlptMTMuOTQ1LDBhMTEuNCwxMS40LDAsMCwwLTQuMTIyLjY5Miw3Ljc0NCw3Ljc0NCwwLDAsMC0zLjA5MiwyLjE0OCw5LjkzOSw5LjkzOSwwLDAsMC0xLjk1MywzLjcxMSwxOC40NzgsMTguNDc4LDAsMCwwLS42NzcsNS4yODNWNDgyLjFoMTAuMnYtOS4xMzFoLTQuOWE5LjIxLDkuMjEsMCwwLDEsMS4wNzQtNC45ODcsMy44NDUsMy44NDUsMCwwLDEsMy40NjYtMS42MzZaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwIDApIiBmaWxsPSIjMDBiMDRjIi8+CiAgPC9nPgo8L3N2Zz4=");
            background-size: 30px 30px;
            display: block;
            float: right;
            height: 30px;
            position: relative;
            top: 30px;
            width: 30px;
        }

        blockquote p:before {
            content: "";
            background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNC4xOTMiIGhlaWdodD0iMjAuMjg3IiB2aWV3Qm94PSIwIDAgMjQuMTkzIDIwLjI4NyI+CiAgPGcgaWQ9Im5vdW5fUXVvdGVfOTMwNDQyXzFhMWExYSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTgxLjQgNDgyLjEwMikgcm90YXRlKDE4MCkiPgogICAgPHBhdGggaWQ9IlBhdGhfMTkzIiBkYXRhLW5hbWU9IlBhdGggMTkzIiBkPSJNMTY3LjA5NSw0NjEuODE1YTkuMzM5LDkuMzM5LDAsMCwwLTcuMjU3LDIuODYxcS0yLjYzNSwyLjg3My0yLjYzMSw4Ljk3MlY0ODIuMWgxMC4xNjl2LTkuMTMxaC00LjkwOGE5LjIsOS4yLDAsMCwxLDEuMDgxLTQuOTg3LDMuOTI1LDMuOTI1LDAsMCwxLDMuNTQ2LTEuNjM2Wm0xMy45NDUsMGExMS40LDExLjQsMCwwLDAtNC4xMjIuNjkyLDcuNzQ0LDcuNzQ0LDAsMCwwLTMuMDkyLDIuMTQ4LDkuOTM5LDkuOTM5LDAsMCwwLTEuOTUzLDMuNzExLDE4LjQ3OCwxOC40NzgsMCwwLDAtLjY3Nyw1LjI4M1Y0ODIuMWgxMC4ydi05LjEzMWgtNC45YTkuMjEsOS4yMSwwLDAsMSwxLjA3NC00Ljk4NywzLjg0NSwzLjg0NSwwLDAsMSwzLjQ2Ni0xLjYzNloiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAgMCkiIGZpbGw9IiMwMGIwNGMiLz4KICA8L2c+Cjwvc3ZnPgo=");
            background-size: 30px 30px;
            margin-bottom: 10px;
            display: block;
            height: 30px;
            width: 30px;
        }

        blockquote p {
            color: #0b2441;
            font-weight: 500;
        }

        span.text-tiny * {
            font-size: 12px;
        }

        span.text-small * {
            font-size: 14px;
        }

        span.text-big * {
            font-size: 24px;
        }

        span.text-huge * {
            font-size: 28px;
        }

        ol li span.text-tiny {
            font-size: 12px;
        }

        ul li span.text-tiny {
            font-size: 12px;
        }

        ol li span.text-small {
            font-size: 14px;
        }

        ul li span.text-small {
            font-size: 14px;
        }

        ol li span.text-big {
            font-size: 24px;
        }

        ul li span.text-big {
            font-size: 24px;
        }

        ol li span.text-huge {
            font-size: 28px;
        }

        ul li span.text-huge {
            font-size: 28px;
        }

    </style>
</head>
<body>

<h2>SOURCES</h2>

<p>The sources we used are:</p>

<p><b>Books</b></p>
<ol>
  <li>Fatburner, Naumman & Gobel Verlagsgesellschaft mbH</li>
  <li>The complete ketogenic diet, Amy Ramos</li>
  <li>Science and development of muscle hypertrophy, Brad Schoenfeld</li>
  <li>Live better, live longer, Sanjiv Chopra</li>
  <li>GluteLab, Bret Contreras and Glen Cordoza</li>
  <li>The Ultimate Body Plan, Gemma Atkinson</li>
  <li>Mini Habits for Weight Loss, Stephen Guise</li>
  <li>Macro Cookbook, Rudy Mawer</li>
</ol>

<p><b>Courses</b></p>
<ol>
  <li>IFBB - International Federation of Bodybuilding</li>
  <li>Faculty of Sports and Pshysical Education, Serbia</li>
  <li>College of Sports and Health, Serbia</li>
</ol>

</body>
</html>
