<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, shrink-to-fit=no, user-scalable=no">
    <meta property=“fb:app_id” content=“2565240367037984">
    <meta property=“og:description” content=“Infinity Fit”>
	<style>
		@import url('https://fonts.googleapis.com/css?family=Roboto&display=swap');
		html * {
			font-size: 14px;
			font-family: Roboto;
			color: #BBBBBB;
			font-weight: 400;
		}

		body {
			-webkit-text-size-adjust: none;
		}

		.image img {
			width: 100%;
			height: auto;
			border-radius: 10px;
		}

		.image {
			margin-block-start: 0;
			margin-block-end: 0;
			margin-inline-start: 0;
			margin-inline-end: 0;
		}

		.image-style-align-center {
			margin-block-start: 1em;
			margin-block-end: 1em;
			margin-inline-start: 40px;
			margin-inline-end: 40px;
		}

		.image-style-align-left {
			float: left;
			margin: 0 16px 0 0;
			width: 50%;
		}

		.image-style-align-right {
			float: right;
			margin: 0 0 0 16px;
			width: 50%;
		}

		h1,
		h2,
		h3,
		h4,
		h5,
		h6 {
			color: #0b2441;
			font-weight: 500;
		}

		h1 {
			font-size: 30px;
		}

		h2 {
			font-size: 28px;
		}

		h2 span {
			color: #0b2441;
			font-size: 28px;
		}

		h2 strong {
			font-size: 28px;
			color: #0b2441;
			font-weight: 700;
		}

		h3 {
			font-size: 26px;
		}

		h3 span {
			color: #0b2441;
			font-size: 26px;
		}

		h3 strong {
			font-size: 26px;
			color: #0b2441;
			font-weight: 700;
		}

		h4 {
			font-size: 22px;
		}

		h4 span {
			color: #0b2441;
			font-size: 22px;
		}

		h4 strong {
			font-size: 22px;
			color: #0b2441;
			font-weight: 600;
		}

		h5 {
			font-size: 18px;
		}

		h5 span {
			color: #0b2441;
			font-size: 18px;
		}

		h5 strong {
			font-size: 18px;
			color: #0b2441;
			font-weight: 600;
		}

		h6 {
			font-size: 16px;
		}

		h6 span {
			color: #0b2441;
			font-size: 16px;
		}

		h6 strong {
			font-size: 16px;
			color: #0b2441;
			font-weight: 600;
		}

		p {
			font-size: 16px;
		}

		p strong {
			font-size: 16px;
			color: #0b2441;
			font-weight: 600;
		}

		span.text-tiny {
			font-size: 12px;
		}

		span.text-small {
			font-size: 14px;
		}

		span.text-big {
			font-size: 24px;
		}

		span.text-huge {
			font-size: 28px;
		}

		blockquote {
			margin-block-start: 0;
			margin-block-end: 0;
			margin-inline-start: 0;
			margin-inline-end: 0;
		}

		blockquote p:after {
			content: "";
			background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNC4xOTMiIGhlaWdodD0iMjAuMjg3IiB2aWV3Qm94PSIwIDAgMjQuMTkzIDIwLjI4NyI+CiAgPGcgaWQ9Im5vdW5fUXVvdGVfOTMwNDQyXzFhMWExYSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTE1Ny4yMDcgLTQ2MS44MTUpIj4KICAgIDxwYXRoIGlkPSJQYXRoXzE5MyIgZGF0YS1uYW1lPSJQYXRoIDE5MyIgZD0iTTE2Ny4wOTUsNDYxLjgxNWE5LjMzOSw5LjMzOSwwLDAsMC03LjI1NywyLjg2MXEtMi42MzUsMi44NzMtMi42MzEsOC45NzJWNDgyLjFoMTAuMTY5di05LjEzMWgtNC45MDhhOS4yLDkuMiwwLDAsMSwxLjA4MS00Ljk4NywzLjkyNSwzLjkyNSwwLDAsMSwzLjU0Ni0xLjYzNlptMTMuOTQ1LDBhMTEuNCwxMS40LDAsMCwwLTQuMTIyLjY5Miw3Ljc0NCw3Ljc0NCwwLDAsMC0zLjA5MiwyLjE0OCw5LjkzOSw5LjkzOSwwLDAsMC0xLjk1MywzLjcxMSwxOC40NzgsMTguNDc4LDAsMCwwLS42NzcsNS4yODNWNDgyLjFoMTAuMnYtOS4xMzFoLTQuOWE5LjIxLDkuMjEsMCwwLDEsMS4wNzQtNC45ODcsMy44NDUsMy44NDUsMCwwLDEsMy40NjYtMS42MzZaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwIDApIiBmaWxsPSIjMDBiMDRjIi8+CiAgPC9nPgo8L3N2Zz4=");
			background-size: 30px 30px;
			display: block;
			float: right;
			height: 30px;
			position: relative;
			top: 30px;
			width: 30px;
		}

		blockquote p:before {
			content: "";
			background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNC4xOTMiIGhlaWdodD0iMjAuMjg3IiB2aWV3Qm94PSIwIDAgMjQuMTkzIDIwLjI4NyI+CiAgPGcgaWQ9Im5vdW5fUXVvdGVfOTMwNDQyXzFhMWExYSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTgxLjQgNDgyLjEwMikgcm90YXRlKDE4MCkiPgogICAgPHBhdGggaWQ9IlBhdGhfMTkzIiBkYXRhLW5hbWU9IlBhdGggMTkzIiBkPSJNMTY3LjA5NSw0NjEuODE1YTkuMzM5LDkuMzM5LDAsMCwwLTcuMjU3LDIuODYxcS0yLjYzNSwyLjg3My0yLjYzMSw4Ljk3MlY0ODIuMWgxMC4xNjl2LTkuMTMxaC00LjkwOGE5LjIsOS4yLDAsMCwxLDEuMDgxLTQuOTg3LDMuOTI1LDMuOTI1LDAsMCwxLDMuNTQ2LTEuNjM2Wm0xMy45NDUsMGExMS40LDExLjQsMCwwLDAtNC4xMjIuNjkyLDcuNzQ0LDcuNzQ0LDAsMCwwLTMuMDkyLDIuMTQ4LDkuOTM5LDkuOTM5LDAsMCwwLTEuOTUzLDMuNzExLDE4LjQ3OCwxOC40NzgsMCwwLDAtLjY3Nyw1LjI4M1Y0ODIuMWgxMC4ydi05LjEzMWgtNC45YTkuMjEsOS4yMSwwLDAsMSwxLjA3NC00Ljk4NywzLjg0NSwzLjg0NSwwLDAsMSwzLjQ2Ni0xLjYzNloiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAgMCkiIGZpbGw9IiMwMGIwNGMiLz4KICA8L2c+Cjwvc3ZnPgo=");
			background-size: 30px 30px;
			margin-bottom: 10px;
			display: block;
			height: 30px;
			width: 30px;
		}

		blockquote p {
			color: #0b2441;
			font-weight: 500;
		}

		span.text-tiny * {
			font-size: 12px;
		}

		span.text-small * {
			font-size: 14px;
		}

		span.text-big * {
			font-size: 24px;
		}

		span.text-huge * {
			font-size: 28px;
		}

		ol li span.text-tiny {
			font-size: 12px;
		}

		ul li span.text-tiny {
		 	font-size: 12px;
		 }

		ol li span.text-small {
			font-size: 14px;
		}

		ul li span.text-small {
			font-size: 14px;
		}

		ol li span.text-big {
			font-size: 24px;
		}

		ul li span.text-big {
			font-size: 24px;
		}

		ol li span.text-huge {
			font-size: 28px;
		}

		ul li span.text-huge {
			font-size: 28px;
		}

	</style>
</head>
<body>

<h2>INFINITY FIT PRIVACY NOTICE</h2>

<p>INFINITY FIT d.o.o. respect your concerns about privacy. This Privacy Notice applies to the personal information we collect through the INFINITY FIT app, connected apps, services and accounts, and associated accessories (the "Services”). INFINITY FIT is a service to manage your overall fitness and wellness. Its features include tracking your activities and managing your well-being.</p>

<p>This Privacy Notice provides further information about how your personal data is used in connection with INFINITY FIT.</p>

<h2>INFORMATION WE OBTAIN</h2>

<p>Through INFINITY FIT, we obtain information about you in various ways. The types of personal information we obtain include:</p>

<ul>
    <li>Contact Information. We may collect and store information associated with the INFINITY FIT account you use to access INFINITY FIT, such as your name, email address, and INFINITY FIT account ID;</li>
    <li>Demographic and Health Data. We may collect demographic information about you, as well as information about your health and wellness, including your date of birth, gender, height, weight, physical activity levels, heart rate, and metabolic rates and levels;</li>
    <li>Fitness and Nutrition Data. We may collect information related to your levels of fitness and nutrition, including your physical activity level and type, and food and nutrient intake;</li>
    <li>INFINITY FIT Interactions. We collect information about your use of INFINITY FIT, including messages sent or received, major setting values for the INFINITY FIT app and in-app usage information;</li>
    <li>Geolocation Data. With your permission, we may receive your device’s precise geolocation and other information related to your location. Your device will notify you when the Services attempt to collect your precise geolocation. Please note that if you do not allow INFINITY FIT to collect your precise geolocation, you may not be able to use all of the INFINITY FIT services or features;</li>
    <li>Device and App Information. We collect information through automated means about your INFINITY FIT-enabled devices, and device information including identifiers, firmware version of device, and application version;</li>
    <li>Third-Party Devices and Apps. We may collect information you choose to connect to INFINITY FIT (such as third-party device manufacturers, models and identifiers; and third-party app identifiers, including social media app identifiers);</li>
</ul>

<h2>HOW WE USE THE INFORMATION WE OBTAIN</h2>

<p>We use the information we obtain through the Services to:</p>

<ul>
    <li>provide you with the Services, such as responding to your requests, questions, and instructions made through INFINITY FIT;</li>
    <li>identify and authenticate you so you may use certain Services;</li>
    <li>improve and customize your experience with the Services, such as delivering content and recommendations tailored to you and the manner in which you interact with INFINITY FIT;</li>
    <li>provide an optimized service combined with compatible INFINITY FIT services;</li>
    <li>respond to your requests and enquiries;</li>
    <li>operate, evaluate, and improve our business (including developing new products; enhancing and improving our products and the services; managing our communications; analyzing our products, customer base, and the services; conducting market research; performing data analytics; and performing accounting, auditing, and other internal functions);</li>
    <li>allow you to interact with and use certain third-party apps and services. When you use INFINITY FIT with third-party applications, you may choose to share or synchronize your information with such services and applications. We will grant access to such services and third-party applications only when you authorize us to do so;</li>
    <li>protect against, identify, and prevent fraud and other criminal activity, claims, and other liabilities.</li>
</ul>

<p>Through INFINITY FIT, we may collect personal information about your online activities on INFINITY FIT-enabled devices over time and across third-party apps, websites, and other online services. We may use third-party analytics services on our Services. The providers that administer these analytics services help us analyze your use of the Services and improve them. The information we obtain may be disclosed to or collected directly by these providers and other relevant third parties who use the information, for example, to evaluate use of the Services, help provide the Services, and diagnose technical issues.</p>

<p>INFINITY FIT processes personal data for the purposes described above. INFINITY FIT’s legal basis to process personal data includes processing that is:</p>


<h2>INFORMATION SHARING</h2>

<p>We do not disclose personal information about you through the Services, except as described in this Privacy Notice. Limited members of the INFINITY FIT service team in INFINITY FIT may access and otherwise process personal data in connection with their job responsibilities or contractual obligations. A limited number of the members of the legal team, customer services team, or other relevant teams may also have limited access to your information as necessary. We may share your personal information with our subsidiaries and affiliates and with service providers who perform services for us. We do not authorize our service providers to use or disclose the information except as necessary for performing services on our behalf or to comply with legal requirements.</p>

<p>INFINITY FIT, our subsidiaries and affiliates, and our third-party service providers may also disclose information about you (1) if we are required to do so by law or legal process (such as a court order or subpoena); (2) in response to requests by government agencies, such as law enforcement authorities; (3) to establish, exercise or defend our legal rights; (4) when we believe disclosure is necessary or appropriate to prevent physical or other harm or financial loss; (5) in connection with an investigation of suspected or actual illegal activity; (6) in the event that we sell or transfer all or a portion of our business or assets (including in the event of a merger, acquisition, sale, transfer, reorganization, dissolution or liquidation); or (7) otherwise with your separate consent.</p>


<h2>HOW WE PROTECT PERSONAL INFORMATION</h2>

<p>We maintain appropriate administrative, technical, and physical safeguards designed to protect personal information we obtain through the Services against accidental, unlawful, or unauthorized destruction, interference, loss, alteration, access, disclosure, or use. INFINITY FIT also maintains reasonable procedures to help ensure that such data is reliable for its intended use and is accurate, complete and current.</p>

<h2>UPDATE TO OUR PRIVACY NOTICE</h2>

<p>This Privacy Notice may be updated periodically to reflect changes in our personal information practices with respect to the Services, or changes in the applicable law.</p>


</body>
</html>
