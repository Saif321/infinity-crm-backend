<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, shrink-to-fit=no, user-scalable=no">
    <meta property=“fb:app_id” content=“2565240367037984">
    <meta property=“og:description” content=“Infinity Fit”>
	<style>
		@import url('https://fonts.googleapis.com/css?family=Roboto&display=swap');
		html * {
			font-size: 14px;
			font-family: Roboto;
			color: #BBBBBB;
			font-weight: 400;
		}

		body {
			-webkit-text-size-adjust: none;
		}

		.image img {
			width: 100%;
			height: auto;
			border-radius: 10px;
		}

		.image {
			margin-block-start: 0;
			margin-block-end: 0;
			margin-inline-start: 0;
			margin-inline-end: 0;
		}

		.image-style-align-center {
			margin-block-start: 1em;
			margin-block-end: 1em;
			margin-inline-start: 40px;
			margin-inline-end: 40px;
		}

		.image-style-align-left {
			float: left;
			margin: 0 16px 0 0;
			width: 50%;
		}

		.image-style-align-right {
			float: right;
			margin: 0 0 0 16px;
			width: 50%;
		}

		h1,
		h2,
		h3,
		h4,
		h5,
		h6 {
			color: #0b2441;
			font-weight: 500;
		}

		h1 {
			font-size: 30px;
		}

		h2 {
			font-size: 28px;
		}

		h2 span {
			color: #0b2441;
			font-size: 28px;
		}

		h2 strong {
			font-size: 28px;
			color: #0b2441;
			font-weight: 700;
		}

		h3 {
			font-size: 26px;
		}

		h3 span {
			color: #0b2441;
			font-size: 26px;
		}

		h3 strong {
			font-size: 26px;
			color: #0b2441;
			font-weight: 700;
		}

		h4 {
			font-size: 22px;
		}

		h4 span {
			color: #0b2441;
			font-size: 22px;
		}

		h4 strong {
			font-size: 22px;
			color: #0b2441;
			font-weight: 600;
		}

		h5 {
			font-size: 18px;
		}

		h5 span {
			color: #0b2441;
			font-size: 18px;
		}

		h5 strong {
			font-size: 18px;
			color: #0b2441;
			font-weight: 600;
		}

		h6 {
			font-size: 16px;
		}

		h6 span {
			color: #0b2441;
			font-size: 16px;
		}

		h6 strong {
			font-size: 16px;
			color: #0b2441;
			font-weight: 600;
		}

		p {
			font-size: 16px;
		}

		p strong {
			font-size: 16px;
			color: #0b2441;
			font-weight: 600;
		}

		span.text-tiny {
			font-size: 12px;
		}

		span.text-small {
			font-size: 14px;
		}

		span.text-big {
			font-size: 24px;
		}

		span.text-huge {
			font-size: 28px;
		}

		blockquote {
			margin-block-start: 0;
			margin-block-end: 0;
			margin-inline-start: 0;
			margin-inline-end: 0;
		}

		blockquote p:after {
			content: "";
			background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNC4xOTMiIGhlaWdodD0iMjAuMjg3IiB2aWV3Qm94PSIwIDAgMjQuMTkzIDIwLjI4NyI+CiAgPGcgaWQ9Im5vdW5fUXVvdGVfOTMwNDQyXzFhMWExYSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTE1Ny4yMDcgLTQ2MS44MTUpIj4KICAgIDxwYXRoIGlkPSJQYXRoXzE5MyIgZGF0YS1uYW1lPSJQYXRoIDE5MyIgZD0iTTE2Ny4wOTUsNDYxLjgxNWE5LjMzOSw5LjMzOSwwLDAsMC03LjI1NywyLjg2MXEtMi42MzUsMi44NzMtMi42MzEsOC45NzJWNDgyLjFoMTAuMTY5di05LjEzMWgtNC45MDhhOS4yLDkuMiwwLDAsMSwxLjA4MS00Ljk4NywzLjkyNSwzLjkyNSwwLDAsMSwzLjU0Ni0xLjYzNlptMTMuOTQ1LDBhMTEuNCwxMS40LDAsMCwwLTQuMTIyLjY5Miw3Ljc0NCw3Ljc0NCwwLDAsMC0zLjA5MiwyLjE0OCw5LjkzOSw5LjkzOSwwLDAsMC0xLjk1MywzLjcxMSwxOC40NzgsMTguNDc4LDAsMCwwLS42NzcsNS4yODNWNDgyLjFoMTAuMnYtOS4xMzFoLTQuOWE5LjIxLDkuMjEsMCwwLDEsMS4wNzQtNC45ODcsMy44NDUsMy44NDUsMCwwLDEsMy40NjYtMS42MzZaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwIDApIiBmaWxsPSIjMDBiMDRjIi8+CiAgPC9nPgo8L3N2Zz4=");
			background-size: 30px 30px;
			display: block;
			float: right;
			height: 30px;
			position: relative;
			top: 30px;
			width: 30px;
		}

		blockquote p:before {
			content: "";
			background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNC4xOTMiIGhlaWdodD0iMjAuMjg3IiB2aWV3Qm94PSIwIDAgMjQuMTkzIDIwLjI4NyI+CiAgPGcgaWQ9Im5vdW5fUXVvdGVfOTMwNDQyXzFhMWExYSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTgxLjQgNDgyLjEwMikgcm90YXRlKDE4MCkiPgogICAgPHBhdGggaWQ9IlBhdGhfMTkzIiBkYXRhLW5hbWU9IlBhdGggMTkzIiBkPSJNMTY3LjA5NSw0NjEuODE1YTkuMzM5LDkuMzM5LDAsMCwwLTcuMjU3LDIuODYxcS0yLjYzNSwyLjg3My0yLjYzMSw4Ljk3MlY0ODIuMWgxMC4xNjl2LTkuMTMxaC00LjkwOGE5LjIsOS4yLDAsMCwxLDEuMDgxLTQuOTg3LDMuOTI1LDMuOTI1LDAsMCwxLDMuNTQ2LTEuNjM2Wm0xMy45NDUsMGExMS40LDExLjQsMCwwLDAtNC4xMjIuNjkyLDcuNzQ0LDcuNzQ0LDAsMCwwLTMuMDkyLDIuMTQ4LDkuOTM5LDkuOTM5LDAsMCwwLTEuOTUzLDMuNzExLDE4LjQ3OCwxOC40NzgsMCwwLDAtLjY3Nyw1LjI4M1Y0ODIuMWgxMC4ydi05LjEzMWgtNC45YTkuMjEsOS4yMSwwLDAsMSwxLjA3NC00Ljk4NywzLjg0NSwzLjg0NSwwLDAsMSwzLjQ2Ni0xLjYzNloiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAgMCkiIGZpbGw9IiMwMGIwNGMiLz4KICA8L2c+Cjwvc3ZnPgo=");
			background-size: 30px 30px;
			margin-bottom: 10px;
			display: block;
			height: 30px;
			width: 30px;
		}

		blockquote p {
			color: #0b2441;
			font-weight: 500;
		}

		span.text-tiny * {
			font-size: 12px;
		}

		span.text-small * {
			font-size: 14px;
		}

		span.text-big * {
			font-size: 24px;
		}

		span.text-huge * {
			font-size: 28px;
		}

		ol li span.text-tiny {
			font-size: 12px;
		}

		ul li span.text-tiny {
		 	font-size: 12px;
		 }

		ol li span.text-small {
			font-size: 14px;
		}

		ul li span.text-small {
			font-size: 14px;
		}

		ol li span.text-big {
			font-size: 24px;
		}

		ul li span.text-big {
			font-size: 24px;
		}

		ol li span.text-huge {
			font-size: 28px;
		}

		ul li span.text-huge {
			font-size: 28px;
		}

	</style>
</head>
<body>

<h2>INFINITY FIT POLITIKA PRIVATNOSTI</h2>

<p>Ova Politika privatnosti za INFINITY FIT objašnjava postupke koje se tiču privatnosti kompanije INFINITY FIT d.o.o. i njenih podružnica i povezanih kompanija, a odnosi se na informacije koje se prikupljaju putem aplikacije INFINITY FIT.</p>

<h2>1. INFORMACIJE KOJE PRIKUPLJAMO</h2>

<p>Možemo da prikupljamo i čuvamo sledeće informacije koje nam dajete korišćenjem aplikacije INFINITY FIT:</p>
<ul>
    <li>kontakt podaci (kao što su e-adresa, poštanski broj, država);</li>
    <li>podaci koji se odnose na zdravlje i zdrav život (kao što su visina, težina, puls, telesna mast, mišićno tkivo, mišićna masa, stepen bazalnog metabolizma, nivo šećera u krvi, visina krvnog pritiska i informacije koje se odnose na spavanje);</li>
    <li>informacije koje se odnose na kondiciju i ishranu (kao što su nivoi aktivnosti, vrste vežbi, programi za vežbanje, broj pređenih koraka, brzina, pređena razdaljina, unos hrane, kofeina i vode);</li>
    <li>informacije o korišćenju aplikacije INFINITY FIT (kao što su poruke poslate ili primljene u aplikaciji INFINITY FIT, podešavanja aplikacije INFINITY FIT i informacije koje koristi aplikacija, uključujući menije i podešavanja na koje ste kliknuli, korišćene funkcije, učestalost i trajanje korišćenja aplikacije i bilo koje prikazane poruke o greškama);</li>
    <li>podaci o profilu u aplikaciji INFINITY FIT (kao što su korisničko ime, datum rođenja, pol, profilna fotografija, objave);</li>
    <li>informacije o uređaju (kao što su IP adresa, verzija firmvera, operativni sistem i identifikatori uređaja, uključujući MAC adresu, ID operativnog sistema Android, serijski broj i oglašivački identifikator mobilnog uređaja);</li>
    <li>informacije o uređajima i aplikacijama treće strane koje odaberete za povezivanje sa aplikacijom INFINITY FIT (kao što su proizvođač, model i identifikatori uređaja treće strane, identifikatori aplikacija treće strane, uključujući identifikatore aplikacija za društvene mreže).</li>
</ul>

<h2>2. KAKO KORISTIMO INFORMACIJE KOJE PRIKUPIMO</h2>

<p>Te informacije možemo da koristimo za:</p>
<ul>
    <li>odgovore na zahteve i upite;</li>
    <li>komunikaciju sa vama o aplikaciji INFINITY FIT;</li>
    <li>pružanje usluga ili funkcija koje zatražite, uključujući usluge pravljenja rezervne kopije podataka i sinhronizacije podataka za korisnike INFINITY FIT account-a;</li>
    <li>oglašavanje, kao što je prikazivanje prilagođenih reklama, sponzorisanog sadržaja i slanje promotivne komunikacije;</li>
    <li>dostavljanje prilagođenog sadržaja i preporuke prilagođene vašim interesovanjima i načinu na koji stupate u interakciju sa aplikacijom INFINITY FIT;</li>
    <li>rad, procenu i poboljšanje našeg poslovanja (uključujući razvoj novih proizvoda, unapređenje i poboljšanje naših proizvoda i aplikacije INFINITY FIT, upravljanje našim komunikacijama, analiziranje naših proizvoda, baze korisnika i aplikacije INFINITY FIT, vršenje analize podataka i obavljanje računovodstvenih, revizorskih i drugih internih poslova);</li>
    <li>identifikaciju i sprečavanje prevara i drugih kriminalnih aktivnosti, potraživanja i drugih obaveza i zaštitu od njih.</li>
</ul>

<p>Možemo da kombinujemo informacije koje pribavimo o vama putem aplikacije INFINITY FIT sa informacijama koje prikupimo o vama putem naših drugih usluga u prethodno navedene svrhe. Možemo da prikupljamo i druge informacije o vama, uređajima i aplikacijama i korišćenju aplikacije INFINITY FIT na način koji smo vam opisali u trenutku prikupljanja ili na drugi način uz vašu saglasnost.</p>

<h2>3. DELJENJE INFORMACIJA</h2>

<p>Kada koristite aplikaciju INFINITY FIT sa aplikacijama treće strane, možda ćete odlučiti da delite ili sinhronizujete svoje informacije sa tim uslugama i aplikacijama. Mi ćemo vam dati pristup tim drugim uslugama i aplikacijama treće strane samo kada nas za to ovlastite.<br />
Nećemo otkrivati vaše lične podatke trećim stranama radi njihovih nezavisnih marketinških ili poslovnih aktivnosti bez vaše saglasnosti. Možemo da delimo vaše lične podatke sa našim podružnicama, povezanim kompanijama i provajderima usluga treće strane kad je to neophodno za pružanje INFINITY FIT usluga i funkcija kao što je prethodno opisano.<br />
Mi, naše podružnice i povezane kompanije i naši provajderi usluga treće strane takođe možemo da otkrijemo informacije o vama u sledećim slučajevima: (1) ako nas na to obavezuju zakon ili pravni postupci (kao što je sudski nalog ili sudski poziv); (2) kao odgovor na zahteve vladinih agencija, kao što su pravosudni organi; (3) za uspostavljanje, ostvarivanje ili odbranu naših zakonskih prava; (4) kada smatramo da je otkrivanje podataka neophodno ili prikladno za sprečavanje telesnih povreda ili druge štete ili finansijskog gubitka; (5) u vezi sa istragom mogućih ili stvarnih nezakonitih aktivnosti; (6) u slučaju potencijalne ili stvarne prodaje ili prenosa našeg poslovanja ili imovine delimično ili u celini (što obuhvata i slučaj udruživanja, pripajanja, udruženog poduhvata, reorganizacije, oduzimanja, raspuštanja ili likvidacije) ili (7) u drugim slučajevima uz vašu saglasnost.</p>

<h2>4. VEZE SA UREĐAJIMA, APLIKACIJAMA I FUNKCIJAMA TREĆE STRANE</h2>

<p>INFINITY FIT može da ima mogućnost povezivanja sa uređajima, aplikacijama i funkcijama treće strane. Ti uređaji, aplikacije i funkcije mogu da funkcionišu nezavisno od nas i mogu da imaju svoje politike privatnosti koje vam preporučujemo da pregledate. U meri u kojoj ne posedujemo nijedan povezani uređaj, aplikaciju ni funkciju treće strane niti upravljamo njima, nismo odgovorni za njihov sadržaj, nijedno korišćenje uređaja, aplikacije ili funkcije ili postupke koji se tiču privatnosti korisnika uređaja, aplikacije ili funkcije.</p>

<h2>5. KAKO ŠTITIMO LIČNE PODATKE</h2>

<p>Pružamo administrativne, tehničke i fizičke zaštitne mere koje su osmišljene za zaštitu ličnih podataka koje prikupljamo putem aplikacije INFINITY FIT od slučajnog, nezakonitog ili neovlašćenog uništenja, gubitka, izmene, pristupa, otkrivanja ili korišćenja.</p>

<h2>6. AŽURIRANJA OVE POLITIKE PRIVATNOSTI</h2>

<p>Ova Politika privatnosti može povremeno da se ažurira i bez slanja prethodnog obaveštenja vama kako bi odrazila promene u postupcima koji se tiču ličnih podataka koji se odnose na aplikaciju INFINITY FIT.</p>


</body>
</html>
