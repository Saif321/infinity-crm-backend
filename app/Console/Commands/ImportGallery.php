<?php

namespace App\Console\Commands;

use App\Repositories\GalleryRepository;
use App\Services\Thumbnailer\Thumbnailer;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ImportGallery extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gallery:import {directory}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import images in gallery from directory (relative to storage/app/public/)';

    /**
     * @var GalleryRepository
     */
    protected $galleryRepository;

    /**
     * @var Thumbnailer
     */
    protected $thumbnailer;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(GalleryRepository $galleryRepository, Thumbnailer $thumbnailer)
    {
        parent::__construct();

        $this->galleryRepository = $galleryRepository;
        $this->thumbnailer = $thumbnailer;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userDirectory = $this->argument('directory');
        $storagePath = storage_path('app/public/' . $userDirectory);
        $dir = dir($storagePath);
        while(($file = $dir->read()) !== false) {
            if(preg_match('/^\./', $file)) {
                continue;
            }

            if(preg_match('/_thumb\.(.*?)$/is', $file)) {
                continue;
            }

            $url = 'public/' . $userDirectory . '/' . $file;
            $this->info('File: ' . $url);

            try {
                $item = $this->galleryRepository->findByField('url', $url)->first();
                if($item) {
                    throw new \Exception('File already exists.');
                }

                $mimeType = File::mimeType($storagePath . '/' . $file);
                $extension = File::extension($storagePath . '/' . $file);
                $fileName = File::name($storagePath . '/' . $file);
                $fileSize = File::size($storagePath . '/' . $file);

                $item = $this->galleryRepository->makeModel();
                $item->name = $fileName;
                $item->mime_type = $mimeType;
                $item->extension = $extension;
                $item->size = $fileSize;
                $item->url = $url;
                $item->type = preg_match('/^image/is', $mimeType) ? 'image' : 'video';
                $item->is_user_avatar = false;

                $this->info('Thumbnailer: ' . $url);

                $item->thumbnail = $this->thumbnailer->setGallery($item)->generateThumbnail();
                $item->save();

                $this->info('Success: ' . $url);
            } catch (\Exception $e) {
                $this->error('Fail: ' . $url . '(' . $e->getMessage() . ')');
            }
            finally {
                $this->line("");
            }
        }

        echo "\n";
    }
}
