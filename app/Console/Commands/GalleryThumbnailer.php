<?php

namespace App\Console\Commands;

use App\Repositories\GalleryRepository;
use App\Services\Thumbnailer\Thumbnailer;
use Illuminate\Console\Command;

class GalleryThumbnailer extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'gallery:thumbnailer {--recreate-all}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate thumbnails for gallery';

	/**
	 * @var GalleryRepository
	 */
	protected $galleryRepository;

	/**
	 * @var Thumbnailer
	 */
	protected $thumbnailer;

	/**
	 * Create a new command instance.
	 *
	 * @param GalleryRepository $galleryRepository
	 * @param Thumbnailer $thumbnailer
	 */
	public function __construct( GalleryRepository $galleryRepository, Thumbnailer $thumbnailer ) {
		parent::__construct();

		$this->galleryRepository = $galleryRepository;
		$this->thumbnailer       = $thumbnailer;
	}

	/**
	 * Execute the console command.
	 */
	public function handle() {
		if ( $this->option( 'recreate-all' ) ) {
			$galleries = $this->galleryRepository->all();
		} else {
			$galleries = $this->galleryRepository->findWhere( [ 'thumbnail' => null ] );
		}
		foreach ( $galleries as $gallery ) {
			try {
				$thumbnailPath = $this->thumbnailer->setGallery( $gallery )->generateThumbnail();
				$gallery->thumbnail = $thumbnailPath;
				echo $thumbnailPath . PHP_EOL;
				echo $gallery->thumbnail . PHP_EOL;
				$gallery->save();
			} catch ( \Exception $e ) {
				echo $e->getMessage() . "\n" . $gallery->url . "\n\n";
			}
		}
	}
}
