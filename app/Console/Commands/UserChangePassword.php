<?php

namespace App\Console\Commands;

use App\Repositories\UserRepository;
use Illuminate\Console\Command;
use Illuminate\Contracts\Container\BindingResolutionException;

class UserChangePassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:change-password {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change password for user by email';

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
    }

    /**
     * Change user password
     * @throws BindingResolutionException
     */
    public function handle()
    {
        $email = $this->argument('email');
        $user = $this->userRepository->getUserByEmail($email);
        if (!$user) {
            $this->error(sprintf('User with email %s not found', $email));
            return;
        }

        $newPassword = $this->secret('New password?');

        if ($this->confirm('Are you sure you want to change password for user '.$email)) {
            $user->setPasswordAttribute($newPassword);
            $user->save();
            $this->info(sprintf('Password for user with email %s successfully changed', $email));
        } else {
            $this->info(sprintf('Password for user with email %s not changed', $email));
        }
    }
}
