<?php

namespace App\Console\Commands;

use App\Models\Payment;
use App\Services\UserPackageRenew as UserPackageRenewService;
use Illuminate\Console\Command;

class UserPackageRenew extends Command
{
    private $userPackageRenew;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user-package:renew';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Renew packages due for renewal';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserPackageRenewService $userPackageRenew)
    {
        parent::__construct();
        $this->userPackageRenew = $userPackageRenew;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->userPackageRenew->renewBulk()
            ->each(function (Payment $payment) {
                $this->alert(sprintf(
                    'Renew UserPackage(%s) Payment(%s) finished in status -> %s',
                    $payment->user_package_id,
                    $payment->id,
                    $payment->status
                ));
            });
    }
}
