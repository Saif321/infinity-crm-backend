<?php

namespace App\Console\Commands;

use App\Services\Payment\GooglePay;
use Illuminate\Console\Command;

class PullPaymentData extends Command
{
    public $googlePay;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pubsub:pull';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull messages from PubSub';

    /**
     * Create a new command instance.
     *
     * @param GooglePay $googlePay
     */
    public function __construct( GooglePay $googlePay)
    {
        parent::__construct();
        $this->googlePay = $googlePay;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(config('infinity-fit.use_packages')){
            $this->googlePay->pull();
        }
    }
}
