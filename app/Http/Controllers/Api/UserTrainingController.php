<?php

namespace App\Http\Controllers\Api;

use App\Models\PurchasedProgram;
use App\Models\UserExerciseRoundItem;
use App\Models\UserRound;
use App\Repositories\ExerciseRepository;
use App\Repositories\LevelRepository;
use App\Repositories\PurchasedProgramRepository;
use App\Repositories\UserExerciseSetRepository;
use App\Repositories\UserProgramRepository;
use App\Repositories\UserExerciseRoundItemRepository;
use App\Repositories\UserRoundRepository;
use App\Repositories\UserTrainingRepository;
use App\Services\Exceptions\NotCurrentUserProgramException;
use App\Services\Program;
use App\Services\Training;
use App\Services\UserPackage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class UserTrainingController extends Controller
{
    private $userTrainingRepository;
    private $userExerciseSetRepository;
    private $userRoundRepository;
    private $userExerciseRoundItemRepository;
    private $userProgramRepository;
    private $purchasedProgramRepository;
    private $levelRepository;
    private $trainingService;
    private $programService;
    private $userPackageService;

    public function __construct(
        UserTrainingRepository $userTrainingRepository,
        UserExerciseSetRepository $userExerciseSetRepository,
        UserRoundRepository $userRoundRepository,
        UserExerciseRoundItemRepository $userExerciseRoundItemRepository,
        UserProgramRepository $userProgramRepository,
        PurchasedProgramRepository $purchasedProgramRepository,
        LevelRepository $levelRepository,
        Training $trainingService,
        Program $programService,
        UserPackage $userPackageService
    ) {
        $this->userTrainingRepository = $userTrainingRepository;
        $this->userExerciseSetRepository = $userExerciseSetRepository;
        $this->userRoundRepository = $userRoundRepository;
        $this->userExerciseRoundItemRepository = $userExerciseRoundItemRepository;
        $this->userProgramRepository = $userProgramRepository;
        $this->purchasedProgramRepository = $purchasedProgramRepository;
        $this->levelRepository = $levelRepository;
        $this->trainingService = $trainingService;
        $this->programService = $programService;
        $this->userPackageService = $userPackageService;
    }

    /**
     * @OA\Get(
     *     path="/user-training/{user_training}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"User training"},
     *     @OA\Parameter(
     *         in="path",
     *         name="user_training",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(ref="#/components/schemas/Training"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request, ExerciseRepository $exerciseRepository)
    {
        $training = $this->userTrainingRepository->findTranslation($request->user_training,
            Config::get('language')['id']);

        if (!$training) {
            return response()->json(new JsonResponse(null, false, 6, trans('errors.6')));
        }

        $program = $this->userProgramRepository->getProgramFromTraining($training->object_id);
        /** @var PurchasedProgram $purchasedProgram */
        $purchasedProgram = $this->purchasedProgramRepository->findByField('user_program_id',
            $program->getId())->first();

        $canMakeProgress = true;
        if (!$purchasedProgram->sourceProgram->is_free) {
            $userPackage = $this->userPackageService->getCurrentActiveUserPackage(Auth::id());
            if (!$userPackage) {
                $canMakeProgress = false;
            }
        }

        if ($purchasedProgram->user_id != Auth::user()->getAuthIdentifier()) {
            return response()->json(new JsonResponse(null, false, 8, trans('errors.8')));
        }

        $training->program_id = $program->getId();
        $training->exercise_sets = $this->userExerciseSetRepository->getSetsForTraining($training->object_id)->get();

        foreach ($training->exercise_sets as &$exerciseSet) {
            $exerciseSet->rounds = $this->userRoundRepository->getRoundsForExerciseSet($exerciseSet->object_id)->get();
            foreach ($exerciseSet->rounds as &$round) {
                $round->items = $this->userExerciseRoundItemRepository->getRoundItems($round->id)->get();
                $round->is_finished = $round->items->every(function (UserExerciseRoundItem $item) {
                    return $item->is_finished;
                });
                foreach ($round->items as &$item) {
                    if ($item->exercise) {
                        $translatedExercise = $exerciseRepository->findTranslated($item->exercise->id);

                        $item->exercise->name = $translatedExercise->name;
                        $item->exercise->description = $translatedExercise->description;
                        $item->exercise->load('image', 'video');
                        $item->exercise->levels = $this->levelRepository->getLevelsForExercise($item->exercise->object_id)->get();
                    }
                }
            }
            $exerciseSet->is_finished = $exerciseSet->rounds->every(function (UserRound $round) {
                return $round->is_finished;
            });
        }

        $training->current_week = $this->programService->getProgramDayWeekForTraining($program, $training);
        $training->program_statistics = null;
        $training->training_statistics = null;
        $training->can_make_progress = $canMakeProgress;
        if ($training->current_week['last_training_in_program']) {
            $training->program_statistics = $this->programService->getProgramStatistics($program);
        } else {
            $training->training_statistics = $this->trainingService->getTrainingStatistics($training);
        }

        return response()->json(
            new JsonResponse($training)
        );
    }

    /**
     * @OA\Post(
     *     path="/user-training/{user_training}/finish",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"User training"},
     *     @OA\Parameter(
     *         in="path",
     *         name="user_training",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(type="object",
     *              @OA\Property(property="success", type="boolean"),
     *              @OA\Property(property="next_training_object_id", @OA\Schema(type="integer"))
     *         )
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function finishCompleteTraining(Request $request)
    {
        try {
            $this->trainingService->finishTraining($request->user_training);
        } catch (NotCurrentUserProgramException $e) {
            return response()->json(new JsonResponse(null, false, 8, trans('errors.8')));
        }

        $this->programService->clearUserProgressCache(Auth::user()->getAuthIdentifier());
        //$this->programService->clearUserActivityDatesCache( Auth::user()->getAuthIdentifier() );

        $training = $this->trainingService->getNextTrainingForProgramWithTraining($request->user_training);
        $next_training_object_id = $training ? $training->object_id : 0;

        return response()->json(
            new JsonResponse(['success' => true, 'next_training_object_id' => $next_training_object_id])
        );
    }

    /**
     * @OA\Post(
     *     path="/user-training/{user_training}/finish/{user_exercise_round_item}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"User training"},
     *     @OA\Parameter(
     *         in="path",
     *         name="user_training",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Parameter(
     *         in="path",
     *         name="user_exercise_round_item",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(type="object",
     *              @OA\Property(property="success", type="boolean"),
     *              @OA\Property(property="next_training_object_id", @OA\Schema(type="integer"))
     *         )
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function finish(Request $request)
    {
        try {
            $this->trainingService->finishTraining($request->user_training, $request->user_exercise_round_item);
        } catch (NotCurrentUserProgramException $e) {
            return response()->json(new JsonResponse(null, false, 8, trans('errors.8')));
        }

        $this->programService->clearUserProgressCache(Auth::user()->getAuthIdentifier());
        //$this->programService->clearUserActivityDatesCache( Auth::user()->getAuthIdentifier() );

        $training = $this->trainingService->getNextTrainingForProgramWithTraining($request->user_training);
        $next_training_object_id = $training ? $training->object_id : 0;

        return response()->json(
            new JsonResponse(['success' => true, 'next_training_object_id' => $next_training_object_id])
        );
    }

    /**
     * @OA\Post(
     *     path="/user-training/{user_training}/set-user-data/{user_exercise_round_item}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"User training"},
     *     @OA\Parameter(
     *         in="path",
     *         name="user_training",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Parameter(
     *         in="path",
     *         name="user_exercise_round_item",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(type="object",
     *              @OA\Property(property="user_number_of_reps", type="integer"),
     *              @OA\Property(property="user_strength_kilos", type="integer")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(type="object",
     *              @OA\Property(property="success", type="boolean")
     *         )
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function setRoundItemUserData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_number_of_reps' => 'required',
            'user_strength_kilos' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        try {
            $this->trainingService->setRoundItemUserData(
                $request->user_training,
                $request->user_exercise_round_item,
                $request->user_number_of_reps,
                $request->user_strength_kilos
            );
        } catch (NotCurrentUserProgramException $e) {
            return response()->json(new JsonResponse(null, false, 8, trans('errors.8')));
        }

        return response()->json(
            new JsonResponse(['success' => true])
        );
    }

    /**
     * @OA\Post(
     *     path="/user-training/{user_training}/reset",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"User training"},
     *     @OA\Parameter(
     *         in="path",
     *         name="user_training",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(type="object",
     *              @OA\Property(property="success", type="boolean"),
     *              @OA\Property(property="next_training_object_id", @OA\Schema(type="integer"))
     *         )
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function reset(Request $request)
    {
        try {
            $this->trainingService->resetTraining($request->user_training);
        } catch (NotCurrentUserProgramException $e) {
            return response()->json(new JsonResponse(null, false, 8, trans('errors.8')));
        }

        $this->programService->clearUserProgressCache(Auth::user()->getAuthIdentifier());
        //$this->programService->clearUserActivityDatesCache( Auth::user()->getAuthIdentifier() );

        $training = $this->trainingService->getNextTrainingForProgramWithTraining($request->user_training);
        $next_training_object_id = $training ? $training->object_id : 0;

        return response()->json(
            new JsonResponse(['success' => true, 'next_training_object_id' => $next_training_object_id])
        );
    }
}
