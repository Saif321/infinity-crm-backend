<?php

namespace App\Http\Controllers\Api;

use App\Models\PurchasedProgram;
use App\Models\UserProgram;
use App\Repositories\ExerciseEnvironmentRepository;
use App\Repositories\GoalRepository;
use App\Repositories\LevelRepository;
use App\Repositories\LikeRepository;
use App\Repositories\ProgramRepository;
use App\Repositories\UserProgramWeekRepository;
use App\Repositories\UserTrainingRepository;
use App\Services\Program;
use App\Services\UserPackage;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use App\Repositories\UserProgramRepository;
use App\Repositories\PurchasedProgramRepository;
use App\Repositories\DeactivatedProgramRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class PurchasedProgramController extends Controller
{
    private $userProgramRepository;
    private $likeRepository;
    private $goalRepository;
    private $levelRepository;
    private $exerciseEnvironmentRepository;
    private $programRepository;
    private $userProgramWeekRepository;
    private $userTrainingRepository;
    private $programService;
    private $purchasedProgramRepository;
    private $deactivatedProgramRepository;
    private $userPackageService;

    public function __construct(
        UserProgramRepository $userProgramRepository,
        LikeRepository $likeRepository,
        GoalRepository $goalRepository,
        LevelRepository $levelRepository,
        ExerciseEnvironmentRepository $exerciseEnvironmentRepository,
        ProgramRepository $programRepository,
        UserProgramWeekRepository $userProgramWeekRepository,
        UserTrainingRepository $userTrainingRepository,
        Program $programService,
        PurchasedProgramRepository $purchasedProgramRepository,
        DeactivatedProgramRepository $deactivatedProgramRepository,
        UserPackage $userPackageService
    ) {
        $this->userProgramRepository = $userProgramRepository;
        $this->likeRepository = $likeRepository;
        $this->goalRepository = $goalRepository;
        $this->levelRepository = $levelRepository;
        $this->exerciseEnvironmentRepository = $exerciseEnvironmentRepository;
        $this->programRepository = $programRepository;
        $this->userProgramWeekRepository = $userProgramWeekRepository;
        $this->userTrainingRepository = $userTrainingRepository;
        $this->programService = $programService;
        $this->purchasedProgramRepository = $purchasedProgramRepository;
        $this->deactivatedProgramRepository = $deactivatedProgramRepository;
        $this->userPackageService = $userPackageService;
    }

    /**
     * @OA\Get(
     *     path="/program/purchased",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Program"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/PurchasedProgram")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        /** @var LengthAwarePaginator $list */
        $list = $this->userProgramRepository
            ->getPurchasedPrograms(Auth::user()->getAuthIdentifier(), $request->search)
            ->with(['image'])
            ->paginate()
            ->appends(['search' => $request->search]);

        $list->setCollection(
            $list->getCollection()->transform(function ($el) {
                return $this->programService->mapUserProgramToPurchasedProgram($el);
            })
        );

        return response()->json(
            new JsonResponse($list)
        );
    }

    /**
     * @OA\Get(
     *     path="/program/purchased/{purchased}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Program"},
     *     @OA\Parameter(
     *         in="path",
     *         name="purchased",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(ref="#/components/schemas/ProgramDetails"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        /** @var UserProgram $program */
        $program = $this->userProgramRepository->getPurchasedProgram($request->purchased);
        if (!$program) {
            return response()->json(new JsonResponse(null, false, 6, trans('errors.6')));
        }

        $program->load(['image', 'video']);
        $program->level = $this->levelRepository
            ->findTranslation($program->level_id, Config::get('language')['id']);
        $program->is_purchased = true;
        $program->exercise_environment_id = 1;

        $canMakeProgress = true;
        $purchasedProgram = $this->purchasedProgramRepository->getPurchasedProgramByUserProgramId($program->object_id ?? $program->id);
        if (!$purchasedProgram->sourceProgram->is_free) {
            $userPackage = $this->userPackageService->getCurrentActiveUserPackage(Auth::id());
            if (!$userPackage) {
                $canMakeProgress = false;
            }
        }
        $program->can_make_progress = $canMakeProgress;

        try {
            $sourceProgram = $this->programRepository->find($program->source_program_id);
            $program->is_liked = $this->likeRepository->isUserLiked($sourceProgram, Auth::user());
        } catch (ModelNotFoundException $exception) {
            Log::warning(sprintf('Program (%s) seems to be deleted check error -> %s', $program->source_program_id,
                $exception->getMessage()));
        }

        $program->weeks = $this->userProgramWeekRepository->getProgramWeeks($program->object_id)->get();
        foreach ($program->weeks as $week) {
            $week->trainings = $this->userTrainingRepository->getTrainingsForWeek($week->id)->get();
            $week->is_completed = true;
            foreach ($week->trainings as $training) {
                if ($training->is_rest_day) {
                    continue;
                }
                $training->is_completed = $this->userTrainingRepository->isTrainingCompleted($training->object_id);
                if (!$training->is_completed) {
                    $week->is_completed = false;
                }
            }
        }

        $training = $this->programService->getNextTraining($program->object_id);

        $program->next_training_object_id = $training ? $training->object_id : 0;

        return response()->json(
            new JsonResponse($program)
        );
    }

    /**
     * @OA\Put(
     *     path="/program/purchased/{purchased}/reset",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Program"},
     *     @OA\Parameter(
     *         in="path",
     *         name="purchased",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(ref="#/components/schemas/ProgramDetails"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function reset(Request $request)
    {
        $program = $this->userProgramRepository->getPurchasedProgram($request->purchased);
        if (!$program) {
            return response()->json(new JsonResponse(null, false, 6, trans('errors.6')));
        }
        $this->programService->clearPurchasedProgramCache($program->getId());
        $this->programService->clearUserProgressCache(Auth::user()->getAuthIdentifier());

        try {
            $affected = $this->userProgramRepository->resetProgress($program->getId());
            return response()->json(
                new JsonResponse(['affected' => $affected], true)
            );
        } catch (\Exception $e) {
            return response()->json(
                new JsonResponse(null, false)
            );
        }
    }

    /**
     * @OA\Put(
     *     path="/program/purchased/{purchased}/deactivate",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Program"},
     *     @OA\Parameter(
     *         in="path",
     *         name="purchased",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function destroy(Request $request)
    {
        $userId = Auth::user()->getAuthIdentifier();
        $userProgramId = $request->purchased;

        /** @var PurchasedProgram $purchasedProgram */
        $purchasedProgram = $this->purchasedProgramRepository->findWhere([
            'user_id' => $userId,
            'user_program_id' => $userProgramId
        ])->first();

        if (!$purchasedProgram) {
            return response()->json(new JsonResponse(null, false, 6, trans('errors.6')));
        }

        $this->programService->clearPurchasedProgramCache($userProgramId);
        $this->programService->clearUserProgressCache($userId);

        try {
            $this->deactivatedProgramRepository->create([
                'user_id' => $purchasedProgram->user_id,
                'user_program_id' => $purchasedProgram->user_program_id,
                'source_program_id' => $purchasedProgram->source_program_id,
                'user_package_id' => $purchasedProgram->user_package_id
            ]);
            $response = $this->purchasedProgramRepository->delete($purchasedProgram->id);
            return response()->json(
                new JsonResponse(null, $response ? true : false)
            );
        } catch (\Exception $e) {
            Log::warning($e->getMessage(), ['userProgramId' => $userProgramId]);
            Log::debug($e->getTraceAsString());
            return response()->json(
                new JsonResponse(null, false, 500, $e->getMessage())
            );
        }
    }
}
