<?php

namespace App\Http\Controllers\Api;

use App\Repositories\ExerciseRepository;
use App\Repositories\ExerciseRoundItemRepository;
use App\Repositories\LevelRepository;
use App\Repositories\RoundRepository;
use App\Repositories\ExerciseSetRepository;
use App\Repositories\TrainingRepository;
use App\Services\Program as ProgramService;
use App\Services\Training as TrainingService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class TrainingController extends Controller {

	private $trainingRepository;
	private $exerciseSetRepository;
	private $roundRepository;
	private $exerciseRoundItemRepository;
	private $levelRepository;

	private $programService;
	private $trainingService;

	public function __construct(
		TrainingRepository $trainingRepository,
        ExerciseSetRepository $exerciseSetRepository,
        RoundRepository $roundRepository,
        ExerciseRoundItemRepository $exerciseRoundItemRepository,
        LevelRepository $levelRepository,
        ProgramService $programService,
        TrainingService $trainingService
	) {
		$this->trainingRepository = $trainingRepository;
		$this->exerciseSetRepository = $exerciseSetRepository;
		$this->roundRepository = $roundRepository;
		$this->exerciseRoundItemRepository = $exerciseRoundItemRepository;
		$this->levelRepository = $levelRepository;

		$this->programService = $programService;
		$this->trainingService = $trainingService;
	}

	/**
	 * @OA\Get(
	 *     path="/training/{training}",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Free training"},
	 *     @OA\Parameter(
	 *         in="path",
	 *         name="training",
	 *         required=true,
	 *         @OA\Schema(type="integer", format="int64")
	 *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="success",
	 *         @OA\Schema(ref="#/components/schemas/Training"),
	 *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
	public function show( Request $request, ExerciseRepository $exerciseRepository ) {
		$training = $this->trainingRepository->findTranslation( $request->training, Config::get( 'language' )['id'] );

		if ( ! $training ) {
			return response()->json( new JsonResponse( null, false, 6, trans( 'errors.6' ) ) );
		}

        $training->exercise_sets = $this->exerciseSetRepository->getSetsForTraining($training->object_id)->get();

		foreach ( $training->exercise_sets as &$exerciseSet ) {
		    $exerciseSet->rounds = $this->roundRepository->getRoundsForExerciseSet( $exerciseSet->object_id )->get();
			foreach ( $exerciseSet->rounds as &$round ) {
				$round->items = $this->exerciseRoundItemRepository
                    ->with( 'exercise' )
                    ->findWhere( [ 'round_id' => $round->id ] );

				foreach ( $round->items as &$item ) {
					if ( $item->exercise ) {
						$translatedExercise = $exerciseRepository->findTranslated($item->exercise->id);

						$item->exercise->name = $translatedExercise->name;
						$item->exercise->description = $translatedExercise->description;
						$item->exercise->load( 'image', 'video' );
						$item->exercise->levels = $this->levelRepository->getLevelsForExercise( $item->exercise->id )
						                                                ->get();
					}
				}
			}
		}

        $training->training_statistics = $this->trainingService->getFreeTrainingStatistics( $training, $training->exercise_sets );

		return response()->json(
			new JsonResponse( $training )
		);
	}
}
