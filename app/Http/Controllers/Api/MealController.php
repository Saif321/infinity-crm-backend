<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\Meal;
use App\Repositories\LanguageRepository;
use App\Repositories\LikeRepository;
use App\Repositories\MealGroupRepository;
use App\Repositories\MealIngredientRepository;
use App\Repositories\MealRecipeStepRepository;
use App\Repositories\MealRepository;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class MealController extends Controller
{
    private $mealRepository;
    private $languageRepository;
    private $likeRepository;
    private $mealIngredientRepository;
    private $mealRecipeStepRepository;
    private $mealGroupRepository;

    public function __construct(
        MealRepository $mealRepository,
        LanguageRepository $languageRepository,
        LikeRepository $likeRepository,
        MealIngredientRepository $mealIngredientRepository,
        MealRecipeStepRepository $mealRecipeStepRepository,
        MealGroupRepository $mealGroupRepository
    )
    {
        $this->mealRepository = $mealRepository;
        $this->languageRepository = $languageRepository;
        $this->likeRepository = $likeRepository;
        $this->mealIngredientRepository = $mealIngredientRepository;
        $this->mealRecipeStepRepository = $mealRecipeStepRepository;
        $this->mealGroupRepository = $mealGroupRepository;
    }

    /**
     * @OA\Get(
     *     path="/meal",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Meal"},
     *     @OA\Parameter(
     *         in="query",
     *         name="nutrition_program_id",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="meal_group_id",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Meal")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        //todo zabrani pristup ako nije medju kupljenim
        $list = $this->mealRepository
            ->getMeal($request->nutrition_program_id, $request->meal_group_id, $request->search)
            ->with('image', 'video')
            ->paginate($request->get('limit', 15));

        $mealGroup = null;
        foreach($list as &$item) {
            $item->is_liked = $this->likeRepository->isUserLiked($item, Auth::user());
            $item->ingredients = $this->mealIngredientRepository->getTranslatedForMealId($item->object_id, Config::get('language')['id'])->get();
            $item->recipes = $this->mealRecipeStepRepository->getTranslatedForMealId($item->object_id, Config::get('language')['id'])->get();

            if(!$mealGroup) {
                $mealGroup = $this->mealGroupRepository->find($item->meal_group_id);
            }
            $item->meal_group = $mealGroup;
        }

        return response()->json(new JsonResponse($list));
    }

    /**
     * @OA\Get(
     *     path="/meal/{meal}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Meal"},
     *     @OA\Parameter(
     *         in="path",
     *         name="meal",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Meal")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        $meal = $this->mealRepository->setTranslatableTable()->find($request->meal);

        if(!$meal->is_published) {
            return response()->json(new JsonResponse(null, false, 10, trans('errors.10')));
        }

        $meal->is_liked = $this->likeRepository->isUserLiked($meal, Auth::user());
        $meal->ingredients = $this->mealIngredientRepository->getTranslatedForMealId($meal->id, Config::get('language')['id'])->get();
        $meal->recipes = $this->mealRecipeStepRepository->getTranslatedForMealId($meal->id, Config::get('language')['id'])->get();
        $meal->load('image', 'video');

        return response()->json(
            new JsonResponse($meal)
        );

    }
}
