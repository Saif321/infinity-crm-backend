<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Repositories\ExerciseEnvironmentRepository;
use App\Repositories\ExerciseRepository;
use App\Repositories\LevelRepository;
use App\Repositories\MuscleGroupRepository;
use App\Services\Program;
use Illuminate\Http\Request;

class ExerciseController extends Controller {
	private $exerciseRepository;
	private $muscleGroupRepository;
	private $exerciseEnvironmentRepository;
	private $levelRepository;
	private $programService;

	public function __construct(
		ExerciseRepository $exerciseRepository,
		ExerciseEnvironmentRepository $exerciseEnvironmentRepository,
		MuscleGroupRepository $muscleGroupRepository,
		LevelRepository $levelRepository,
		Program $programService
	) {
		$this->exerciseRepository            = $exerciseRepository;
		$this->muscleGroupRepository         = $muscleGroupRepository;
		$this->exerciseEnvironmentRepository = $exerciseEnvironmentRepository;
		$this->levelRepository               = $levelRepository;
		$this->programService                = $programService;
	}

	/**
	 * @OA\Get(
	 *     path="/exercise",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Exercise"},
	 *     @OA\Parameter(
	 *         in="query",
	 *         name="muscle_group_id",
	 *         required=true,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Parameter(
	 *         in="query",
	 *         name="exercise_environment_id",
	 *         required=true,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Parameter(
	 *         in="query",
	 *         name="search",
	 *         required=false,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="",
	 *         @OA\Schema(
	 *             type="array",
	 *             @OA\Items(ref="#/components/schemas/Exercise")
	 *         ),
	 *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
	public function index( Request $request ) {
		$muscleGroup         = $this->muscleGroupRepository->find( $request->muscle_group_id );
		$exerciseEnvironment = $this->exerciseEnvironmentRepository->find( $request->exercise_environment_id );

		$list = $this->exerciseRepository
			->getFreeExercisesForMuscleGroupAndEnvironment( $muscleGroup->id, $exerciseEnvironment->id,
				$request->search )
			->with( 'image', 'video' )->paginate()->appends( [
				'muscle_group_id'         => $request->muscle_group_id,
				'exercise_environment_id' => $request->exercise_environment_id,
				'search'                  => $request->search
			] );
		foreach ( $list as &$item ) {
//            $item->level = $this->levelRepository->findTranslation($item->level_id, Config::get('language')['id']);
			$item->levels = $this->levelRepository->getLevelsForExercise( $item->object_id )->get();
		}

		return response()->json(
			new JsonResponse( $list )
		);
	}

	/**
	 * @OA\Get(
	 *     path="/exercise/{exercise_id}",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Exercise"},
	 *     @OA\Parameter(
	 *         in="path",
	 *         name="exercise_id",
	 *         required=true,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="",
	 *         @OA\Schema(ref="#/components/schemas/Exercise"),
	 *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
	public function show( Request $request ) {
		$item = $this->exerciseRepository->find( $request->exercise_id );

		if ( ! $item ) {
			return response()->json( new JsonResponse( null, false, 6, trans( 'errors.6' ) ) );
		}

		if ( ! $item->is_free ) {
			return response()->json( new JsonResponse( null, false, 9, trans( 'errors.9' ) ) );
		}

		$translatedItem = $this->exerciseRepository->findTranslated( $request->exercise_id );
		$translatedItem->load( 'image', 'video' );
		$translatedItem->levels = $this->levelRepository->getLevelsForExercise( $item->id )->get();

		return response()->json( new JsonResponse( $translatedItem ) );
	}

	/**
	 * @OA\Get(
	 *     path="/exercise/{exercise_id}/program/{program_id}",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Exercise"},
	 *     @OA\Parameter(
	 *         in="path",
	 *         name="exercise_id",
	 *         required=true,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Parameter(
	 *         in="path",
	 *         name="program_id",
	 *         required=true,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="",
	 *         @OA\Schema(ref="#/components/schemas/Exercise"),
	 *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
	public function showExerciseInProgram( Request $request ) {
		$item = $this->exerciseRepository->find( $request->exercise_id );

		if ( ! $item ) {
			return response()->json( new JsonResponse( null, false, 6, trans( 'errors.6' ) ) );
		}

		if ( $item->is_free || $this->programService->canShowExerciseForProgram( $request->exercise_id,
				$request->program_id ) ) {
			$translatedItem = $this->exerciseRepository->findTranslated( $request->exercise_id );
			$translatedItem->load( 'image', 'video' );
			$translatedItem->levels = $this->levelRepository->getLevelsForExercise( $item->id )->get();

			return response()->json( new JsonResponse( $translatedItem ) );
		}

		return response()->json( new JsonResponse( null, false, 9, trans( 'errors.9' ) ) );
	}

}
