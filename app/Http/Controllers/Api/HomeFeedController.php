<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\LikeRepository;
use App\Repositories\HomeFeedRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Responses\JsonResponse;
use App\Models\HomeFeed;
use App\Models\Follower;
use App\Models\UserTraining;
use App\Models\Training;
use PDO;
use \Illuminate\Support\Facades\DB;

class HomeFeedController extends Controller
{
    private $likeRepository;
    private $homeFeedRepository;

    public function __construct(
        LikeRepository $likeRepository,
        HomeFeedRepository $homeFeedRepository
    ) {
        $this->likeRepository = $likeRepository;
        $this->homeFeedRepository = $homeFeedRepository;
    }

    /**
     * @OA\Post(
     *     path="/create_home_feed",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="",
     *     tags={"Home Feed"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(
        *           @OA\Property(property="image_id", type="integer"),
        *           @OA\Property(property="description", type="string"),
        *           @OA\Property(property="training_id", type="integer"),
        *           @OA\Property(property="private", type="integer"),
        *        )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function createHomeFeed(Request $request)
    {
        if(Auth::user()->blocked) return response()->json(new JsonResponse(null, false, 401, 'Unauthorized.'), 401);
        $data = $request->all();
        if ($data) {
            $user = Auth::user();
            if ($user) {
                $newHomeFeed = new HomeFeed;
                $newHomeFeed->image_id = $data["image_id"];

                $user_training = UserTraining::find($data["training_id"]);
                if($user_training != null) {
                    if($user_training->parent_id){
                        $newHomeFeed->training_id = $user_training->parent_id;
                    }                    
                    else {
                        $training = Training::where('name', $user_training->name)->first();
                        if($training != null){
                            $newHomeFeed->training_id = $training->id;
                        }
                    }
                }

                $newHomeFeed->description = $data["description"];
                $newHomeFeed->users_id = $user->id;
                $newHomeFeed->private = $data["private"];
                $newHomeFeed->save();

                return response()->json(new JsonResponse($newHomeFeed));
            } else {
                return response()->json(new JsonResponse(null, false, 401, 'Unauthorized.'), 401);
            }
        }
    }

    /**
	 * @OA\Delete(
	 *     path="/delete_home_feed/{home_feed_id}",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Home Feed"},
	 *     @OA\Parameter(
	 *         in="path",
	 *         name="home_feed_id",
	 *         required=true,
	 *         @OA\Schema(type="integer")
	 *     ),
	 *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
    public function deleteHomeFeed($id)
    {
        $user = Auth::user();
        if ($user) {
            $homeFeed = HomeFeed::find($id);
            if ($homeFeed) {
                $homeFeed->delete();
                return response()->json(new JsonResponse($homeFeed));
            } else {
                return response()->json(new JsonResponse(null, false, 401, 'Unauthorized.'), 401);
            }
        } else {
            return response()->json(new JsonResponse(null, false, 401, 'Unauthorized.'), 401);
        }
    }

    /**
	 * @OA\Get(
	 *     path="/explore_feed",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Home Feed"},
     *     @OA\Parameter(
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
	 *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
    public function getExploreHomeFeed(Request $request)
    {
        $user = Auth::user();
        if ($user) {
               
            $homeFeeds = $this->homeFeedRepository
            ->getHomeFeed($request->search)
            ->with('user')
            ->with('image')
            ->with('training')
            ->where('private', 0)
            ->paginate($request->get('limit', 15))
            ->appends(['search' => $request->search, 'limit' => $request->limit]);
            
            foreach ($homeFeeds as &$item) {
                $item->is_liked = $this->likeRepository->isUserLiked($item, Auth::user());
                $item->user_followed_by_me = Follower::where("follower_id", Auth::user()->id)->where("followed_id", $item->users_id)->count();
            }
            if ($homeFeeds) {
                return response()->json(new JsonResponse($homeFeeds));
            } else {
                return response()->json(new JsonResponse(null, false, 401, 'Unauthorized.'), 401);
            }
        } else {
            return response()->json(new JsonResponse(null, false, 401, 'Unauthorized.'), 401);
        }
    }

    /**
	 * @OA\Get(
	 *     path="/home_feed",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Home Feed"},
     *     @OA\Parameter(
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
	 *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
    public function getHomeFeed(Request $request)
    {
        $user = Auth::user();
        if ($user) {
               
            $homeFeeds = $this->homeFeedRepository
            ->getHomeFeed($request->search)
            ->join('followers', 'followed_id', 'users_id')
            ->with('user')
            ->with('image')
            ->with('training')
            ->where('followers.follower_id', $user->id)
            ->paginate($request->get('limit', 15))
            ->appends(['search' => $request->search, 'limit' => $request->limit]);
            
            foreach ($homeFeeds as &$item) {
                $item->is_liked = $this->likeRepository->isUserLiked($item, Auth::user());
                $item->user_followed_by_me = Follower::where("follower_id", Auth::user()->id)->where("followed_id", $item->users_id)->count();
            }
            if ($homeFeeds) {
                return response()->json(new JsonResponse($homeFeeds));
            } else {
                return response()->json(new JsonResponse(null, false, 401, 'Unauthorized.'), 401);
            }
        } else {
            return response()->json(new JsonResponse(null, false, 401, 'Unauthorized.'), 401);
        }
    }

    /**
	 * @OA\Get(
	 *     path="/my_home_feed",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Home Feed"},
     *     @OA\Parameter(
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
	 *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
    public function getMyHomeFeed(Request $request)
    {
        $user = Auth::user();
        if ($user) {
               
            $homeFeeds = $this->homeFeedRepository
            ->getHomeFeed($request->search)
            ->with('user')
            ->with('training')
            ->with('image')
            ->where('users_id', $user->id)
            ->paginate($request->get('limit', 15))
            ->appends(['search' => $request->search, 'limit' => $request->limit]);
            
            foreach ($homeFeeds as &$item) {
                $item->is_liked = $this->likeRepository->isUserLiked($item, Auth::user());
                $item->user_followed_by_me = Follower::where("follower_id", Auth::user()->id)->where("followed_id", $item->users_id)->count();
            }
            if ($homeFeeds) {
                return response()->json(new JsonResponse($homeFeeds));
            } else {
                return response()->json(new JsonResponse(null, false, 401, 'Unauthorized.'), 401);
            }
        } else {
            return response()->json(new JsonResponse(null, false, 401, 'Unauthorized.'), 401);
        }
    }

    /**
	 * @OA\Get(
	 *     path="/home_feed/{home_feed_id}",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Home Feed"},
	 *     @OA\Parameter(
	 *         in="path",
	 *         name="home_feed_id",
	 *         required=true,
	 *         @OA\Schema(type="integer")
	 *     ),
	 *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
    public function getHomeFeedById($id)
    {
        $user = Auth::user();
        if ($user) {
            $homeFeed = HomeFeed::with('image')
                                ->with('user')
                                ->with('training')
                                ->find($id);
            $homeFeed->is_liked = $this->likeRepository->isUserLiked($homeFeed, Auth::user());
            $homeFeed->user_followed_by_me = Follower::where("follower_id", Auth::user()->id)->where("followed_id", $homeFeed->users_id)->count();
            if ($homeFeed) {
                return response()->json(new JsonResponse($homeFeed));
            } else {
                return response()->json(new JsonResponse(null, false, 420, 'INVALID ARGUMENTS.'), 420);
            }
        } else {
            return response()->json(new JsonResponse(null, false, 401, 'Unauthorized'), 401);
        }
    }

}
