<?php

namespace App\Http\Controllers\Api;

use App\Http\Responses\JsonResponse;
use App\Services\Payment\ApplePay;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ApplePayController extends Controller
{
    private $applePayService;

    public function __construct(ApplePay $applePayService)
    {
        $this->applePayService = $applePayService;
    }


    /**
     * @OA\POST(
     *     path="/apple/verify-payment",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Payment"},
     *   @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="receipt",
     *                     type="string",
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="string",
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateReceipt(Request $request)
    {
        try {
            $receipt = $request->receipt;
            $user = Auth::user();
            $this->applePayService->validateReceipt($receipt, $user);
            return response()->json(new JsonResponse(true));
        } catch (\Throwable $throwable) {
            Log::error($throwable->getMessage(), $throwable->getTrace());
            return response()->json(new JsonResponse($throwable->getMessage(), false, 500));
        }

    }
}
