<?php


namespace App\Http\Controllers\Api\Vendor;


use App\Http\Responses\JsonResponse;
use App\Models\User;
use Laminas\Diactoros\Response as Psr7Response;
use Laravel\Passport\Exceptions\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;
use Laravel\Passport\Http\Controllers\AccessTokenController;

class CustomAccessTokenController extends AccessTokenController
{
    /**
     * Hooks in before the AccessTokenController issues a token
     *
     *
     * @param ServerRequestInterface $request
     * @return mixed
     * @throws OAuthServerException
     */
    public function issueUserToken(ServerRequestInterface $request)
    {
        $httpRequest = request();
        if ($httpRequest->grant_type == 'refresh_token' && !$httpRequest->refresh_token) {
            return response()->json(new JsonResponse(null, false, 401, 'Please login'), 401);
        }
        if ($httpRequest->grant_type == 'password') {
            $user = User::where('email', $httpRequest->username)->first();
            if ($user && !$user->is_active) {
                return response()->json(new JsonResponse(null, false, 406, 'User not activated!'), 406);
            }
            if ($user && $user->blocked) {
                return response()->json(new JsonResponse(null, false, 406, 'User blocked!'), 406);
            }
        }
        return $this->withErrorHandling(function () use ($request) {
            return $this->convertResponse(
                $this->server->respondToAccessTokenRequest($request, new Psr7Response)
            );
        });
    }
}
