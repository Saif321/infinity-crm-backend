<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Repositories\FruitRepository;
use App\Repositories\MeatRepository;
use App\Repositories\NutritionQuestionnaireRepository;
use App\Repositories\VegetableRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class NutritionQuestionnaireQuestionController extends Controller
{
    private $nutritionQuestionnaireRepository;
    private $vegetableRepository;
    private $fruitRepository;
    private $meatRepository;

    public function __construct(NutritionQuestionnaireRepository $nutritionQuestionnaireRepository,
                                VegetableRepository $vegetableRepository,
                                FruitRepository $fruitRepository,
                                MeatRepository $meatRepository)
    {
        $this->nutritionQuestionnaireRepository = $nutritionQuestionnaireRepository;
        $this->vegetableRepository = $vegetableRepository;
        $this->fruitRepository = $fruitRepository;
        $this->meatRepository = $meatRepository;
    }

    /**
     * @OA\Get(
     *     path="/nutrition-questionnaire-questions",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Home"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        $questionnarie = Lang::get('nutrition-questionnaire');

        $questionnarie[3]['answers'] = $this->vegetableRepository->setTranslatableTable()->select()->get();
        $questionnarie[4]['answers'] = $this->meatRepository->setTranslatableTable()->select()->get();
        $questionnarie[5]['answers'] = $this->fruitRepository->setTranslatableTable()->select()->get();

        foreach($questionnarie as $index => &$question) {
            $question['id'] = $index + 1;

            if($question['question_type'] == 'text') {
                foreach($question['answers'] as $i => $answer) {
                    $question['answers'][$i] = [
                        'id' => $i + 1,
                        'name' => $answer,
                        'image' => null,
                        'order' => $i + 1,
                        'created_at' => null,
                        'updated_at' => null
                    ];
                }
            }
        }

        return response()->json(
            new JsonResponse($questionnarie)
        );
    }

}
