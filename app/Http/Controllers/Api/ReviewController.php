<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Responses\JsonResponse;
use App\Repositories\ReviewRepository;

class ReviewController extends Controller
{
    private $reviewRepository;

    public function __construct(ReviewRepository $reviewRepository)
    {
        $this->reviewRepository = $reviewRepository;
    }

    /**
     * @OA\Get(
     *     path="/review",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Review"},
     *     @OA\Parameter(
     * 			name="object_id",
     * 			in="query",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 	   ),
     *     @OA\Parameter(
     * 			name="object_type",
     * 			in="query",
     * 			required=true,
     * 			@OA\Schema(type="string"),
     * 	   ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Review")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        return response()->json(
            new JsonResponse(
                $this->reviewRepository
                    ->getPublishedReviews($request->object_id, 'App\Models\\' . $request->object_type)
                    ->paginate()
                    ->appends(['object_id' => $request->object_id, 'object_type' => $request->object_type])
            )
        );
    }

    /**
     * @OA\Post(
     *     path="/review",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Review of model",
     *     tags={"Review"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(ref="#/components/schemas/ReviewRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(ref="#/components/schemas/Review"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function store(Request $request)
    {
        $rules = [
            'object_id' => 'required|integer',
            'object_type' => 'required',
            'description' => '',
            'rating' => 'required|integer|min:1|max:5',
        ];

        $input = $request->all();
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        $object_type = 'App\Models\\' . $request->object_type;
        if(!class_exists($object_type)) return response()->json(
            new JsonResponse(null, false, 6, trans('errors.6'))
        );

        //todo check two more things:
        //object_id of object_type must be published
        //object_id of object_type must be purchased by user_id
        //limit 1 review per user

        $commentCountByUser = $this->reviewRepository->findWhere(['user_id' => Auth::user()->id,
            'object_id' => $request->object_id,
            'object_type' => $object_type])->count();
        if($commentCountByUser) {
            return response()->json(
                new JsonResponse(null, false, 8, trans('errors.8'))
            );
        }

        return response()->json(
            new JsonResponse($this->reviewRepository->create([
                'user_id' => Auth::user()->id,
                'object_id' => $request->object_id,
                'object_type' => $object_type,
                'description' => $request->description,
                'rating' => $request->rating,
            ]))
        );
    }
}
