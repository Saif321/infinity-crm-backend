<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Responses\JsonResponse;
use App\Models\Follower;
use PDO;

class FollowerController extends Controller
{

    public function __construct(
    ) {
        
    }

    /**
	 * @OA\Post(
	 *     path="/follow/{user_id}",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Followers"},
	 *     @OA\Parameter(
	 *         in="path",
	 *         name="user_id",
	 *         required=true,
	 *         @OA\Schema(type="integer")
	 *     ),
	 *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
    public function follow(Request $request){
        $follower = Follower::where('follower_id', Auth::user()->id)->where('followed_id', $request->id)->first();
        if($follower == null){
            $follower = new Follower;
            $follower->followed_id = $request->id;
            $follower->follower_id = Auth::user()->id;
    
            $follower->save();
        }
        
        return response()->json(new JsonResponse($follower));
    }

    /**
	 * @OA\Post(
	 *     path="/unfollow/{user_id}",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Followers"},
	 *     @OA\Parameter(
	 *         in="path",
	 *         name="user_id",
	 *         required=true,
	 *         @OA\Schema(type="integer")
	 *     ),
	 *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
    public function unfollow(Request $request){
        $followers = Follower::where('follower_id', Auth::user()->id)->where('followed_id', $request->id)->first();
        if($followers != null){
            $followers->delete();

            return response()->json(new JsonResponse(array("result" => "User unfollowed.")));
        }
        
        return response()->json(new JsonResponse(array("result" => "Follow link not found.")));
    }

}
