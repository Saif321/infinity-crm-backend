<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Responses\JsonResponse;
use App\Repositories\CommentRepository;

class CommentController extends Controller
{
    private $commentRepository;

    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    /**
     * @OA\Get(
     *     path="/comment",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Comment"},
     *     @OA\Parameter(
     *         in="query",
     *         name="object_type",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="object_id",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Comment")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        $objectType = 'App\Models\\' . $request->object_type;
        $list = $this->commentRepository
            ->getPublishedComments($request->object_id, $objectType)
            ->with('user')
            ->paginate()
            ->appends($request->all());

        return response()->json(
            new JsonResponse($list)
        );
    }

    /**
     * @OA\Post(
     *     path="/comment",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Comment on model",
     *     tags={"Comment"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(ref="#/components/schemas/CommentRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(ref="#/components/schemas/Comment"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function store(Request $request)
    {
        $rules = [
            'object_id' => 'required|integer',
            'object_type' => 'required',
            'comment' => 'required',
        ];

        $input = $request->all();
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        //$object_type = app()->make('App\\Models\\' . $request->object_type);
        $object_type = 'App\Models\\' . $request->object_type;
        if(!class_exists($object_type)) return response()->json(
            new JsonResponse(null, false, 400, "Unknown object " . $request->object_type)
        );

        $comment = $this->commentRepository->create([
            'user_id' => Auth::user()->id,
            'object_id' => $request->object_id,
            'object_type' => $object_type,
            'comment' => $request->comment,
        ]);

        $comment->user = $comment->user()->first();

        return response()->json(
            new JsonResponse($comment)
        );
    }
}
