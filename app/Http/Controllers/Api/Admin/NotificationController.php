<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Services\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    private $notificationService;

    public function __construct(Notification $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * @OA\Post(
     *     path="/admin/notification",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Send notification",
     *     tags={"Admin"},
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="object_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="object_type",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/JsonResponse"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="User not logged in"
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function notifyUsers(Request $request)
    {
        return $this->notificationService->sendNotification($request->object_id, $request->object_type);
    }
}
