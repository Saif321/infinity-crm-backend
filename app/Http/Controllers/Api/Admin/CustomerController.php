<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\NutritionQuestionnaire;
use App\Models\User;
use App\Repositories\BasicQuestionnaireRepository;
use App\Repositories\NutritionQuestionnaireRepository;
use App\Repositories\ProgramRepository;
use App\Repositories\UserProgramRepository;
use App\Repositories\UserRepository;
use App\Repositories\NutritionProgramRepository;
use App\Repositories\PurchasedProgramRepository;
//use App\Rules\StrongPassword;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class CustomerController extends Controller
{
    private $userRepository;
    private $userProgramRepository;
    private $basicQuestionnaireRepository;
    private $nutritionQuestionnaireRepository;
    private $programRepository;
    private $nutritionProgramRepository;

    public function __construct(UserRepository $userRepository,
                                UserProgramRepository $userProgramRepository,
                                BasicQuestionnaireRepository $basicQuestionnaireRepository,
                                NutritionQuestionnaireRepository $nutritionQuestionnaireRepository,
                                ProgramRepository $programRepository,
                                NutritionProgramRepository $nutritionProgramRepository
                                )
    {
        $this->userRepository = $userRepository;
        $this->basicQuestionnaireRepository = $basicQuestionnaireRepository;
        $this->nutritionQuestionnaireRepository = $nutritionQuestionnaireRepository;
        $this->userProgramRepository = $userProgramRepository;
        $this->programRepository = $programRepository;
        $this->nutritionProgramRepository = $nutritionProgramRepository;
    }

    /**
     * @OA\Get(
     *     path="/admin/customer",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/User")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        $list = $this->userRepository->getAllCustomer($request->search)->paginate()->appends(['search' => $request->search]);
        foreach($list as &$item) {
            $item->questionnaire = $this->basicQuestionnaireRepository->findByField('user_id', $item->id)->first();
            $item->nutrition_questionnaire = $this->nutritionQuestionnaireRepository->findByField('user_id', $item->id)->first();
        }

        return response()->json(new JsonResponse($list));
    }

    /**
     * @OA\Get(
     *     path="/admin/customer/{customer}",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="customer",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/User")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        $item = $this->userRepository->find($request->customer);
        $item->questionnaire = $this->basicQuestionnaireRepository->findByField('user_id', $item->id)->first();
        $item->nutrition_questionnaire = $this->nutritionQuestionnaireRepository->findByField('user_id', $item->id)->first();

        return response()->json(
            new JsonResponse($item)
        );
    }

    /**
     * @OA\Put(
     *     path="/admin/customer/{customer}",

     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="customer",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Parameter(
     *         in="path",
     *         name="body",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/UserRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/UserRequest"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function update(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => ['required', $request->customer ? Rule::unique('users')->ignore($request->customer) : ''],
            'password' => [!$request->customer ? 'required' : '', new StrongPassword()],
        ]);
        if($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        if($request->customer) {
            $item = $this->userRepository->find($request->customer);

            if(!$item->hasRole('customer')) {
                return response()->json(new JsonResponse(null, false, 8, trans('errors.8')));
            }

            $item->fill($data);
            $item->save();
        }else {
            $item = $this->userRepository->create($data);

            $item->assignRole(Role::findByName('customer'));
        }


        return response()->json(new JsonResponse($item));
    }

    /**
     * @OA\Delete(
     *     path="/admin/customer/{customer}",

     *     @OA\MediaType(mediaType="application/json"),
     *     description="customer",
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="customer",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/User"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function destroy(Request $request)
    {
        if(!Auth::user()->hasRole('root')) {
            return response()->json(new JsonResponse(null, false, 400, 'You don\'t have permissions for this action'));
        }

        $item = $this->userRepository->find($request->customer);

        if(!$item->hasRole('customer')) {
            return response()->json(new JsonResponse(null, false, 400, 'You don\'t have permissions for this action'));
        }

        return response()->json(new JsonResponse(
            $this->userRepository->delete($request->customer) ? true : false
        ));
    }


    /**
     * @OA\Get(
     *     path="/admin/customer/{customer}/programs",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="customer",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Program")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function programs(Request $request)
    {
        return response()->json(new JsonResponse(
            $this->programRepository->getPurchasedProgramsForAdmin($request->customer)->get()
        ));

    }

    /**
     * @OA\Get(
     *     path="/admin/customer/{customer}/nutrition-programs",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="customer",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/NutritionProgram")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function nutritionPrograms(Request $request)
    {
        return response()->json(new JsonResponse(
            $this->nutritionProgramRepository->getPurchasedNutritionProgramsForAdmin($request->customer)->get()
        ));

    }

}
