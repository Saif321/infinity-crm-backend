<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\Exercise;
use App\Repositories\ExerciseRepository;
use App\Repositories\ExerciseRoundItemRepository;
use App\Repositories\LanguageRepository;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use App\Services\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ExerciseController extends Controller
{
    private $exerciseRepository;
    private $languageRepository;
    private $exerciseRoundItemRepository;

    public function __construct(
        ExerciseRepository $exerciseRepository,
        LanguageRepository $languageRepository,
        ExerciseRoundItemRepository $exerciseRoundItemRepository
    )
    {
        $this->exerciseRepository = $exerciseRepository;
        $this->languageRepository = $languageRepository;
        $this->exerciseRoundItemRepository = $exerciseRoundItemRepository;
    }

    /**
     * @OA\Get(
     *     path="/admin/exercise",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="muscle_group_ids",
     *         required=false,
     
     *         @OA\Items(type="integer", format="int32")
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="environment_ids",
     *         required=false,
     
     *         @OA\Items(type="integer", format="int32")
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="is_published",
     *         required=false,
     
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="is_free",
     *         required=false,
     
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Exercise")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        $muscleGroupIds = $request->muscle_group_ids ? explode(',', $request->muscle_group_ids) : null;
        $environmentIds = $request->environment_ids ? explode(',', $request->environment_ids) : null;

        return response()->json(
            new JsonResponse($this->exerciseRepository
                ->getExercises($request->search, $muscleGroupIds, $environmentIds, $request->is_published, $request->is_free)
                ->with('image','video', 'exerciseEnvironments', 'levels', 'muscleGroups')
                ->paginate()
                ->appends(['search' => $request->search])
            )
        );

    }

    /**
     * @OA\Get(
     *     path="/admin/exercise/{exercise}",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="exercise",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Exercise")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        $exercise = $this->exerciseRepository->find($request->exercise);
        $exercise->load('image','video', 'exerciseEnvironments', 'levels', 'muscleGroups');
        $exercise->translations = $this->exerciseRepository->getTranslations($exercise->id)->with('image', 'video')->get();

        return response()->json(
            new JsonResponse($exercise)
        );
    }

    /**
     * @OA\Put(
     *     path="/admin/exercise/{exercise}",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="exercise",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Parameter(
     *         in="path",
     *         name="body",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/ExerciseRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Exercise"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function update(Request $request, Translation $translation)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'image_id' => 'integer',
//            'video_id' => 'integer',
            'translations.*' => 'required',
            'translations.*.image_id' => 'required|integer',
//            'translations.*.video_id' => 'required|integer',
            'translations.*.name' => 'required',
            'translations.*.description' => 'required',

        ]);
        if($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        if($request->exercise) {
            $item = $this->exerciseRepository->find($request->exercise);

            if(isset($data['is_published'])) {
                if (!$item->is_published && $data['is_published']) {
                    $data['published_at'] = date('Y-m-d H:i:s');
                } else if($item->is_published && !$data['is_published']) {
                    $data['published_at'] = null;
                } else {
                    $data['published_at'] = $item->published_at;
                }
            }

            $item->fill($data);
            $item->save();
        } else {
            if(isset($data['is_published']) && $data['is_published']) {
                $data['published_at'] = date('Y-m-d H:i:s');
            }

            $item = $this->exerciseRepository->create($data);
        }

        if(array_key_exists('muscle_group_ids', $data)) {
            $item->muscleGroups()->sync($data['muscle_group_ids']);
        }

        if(array_key_exists('exercise_environment_ids', $data)) {
            $item->exerciseEnvironments()->sync($data['exercise_environment_ids']);
        }

        if(array_key_exists('level_ids', $data)) {
            $item->levels()->sync($data['level_ids']);
        }

        $item->load('image','video', 'exerciseEnvironments', 'levels', 'muscleGroups');

        $response = $item->toArray();
        $response['translations'] = $translation->translateObject($item, $request->translations);

        return response()->json(new JsonResponse($response));
    }

    /**
     * @OA\Put(
     *     path="/admin/exercise/{exercise}/publish",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Change publish status for exercise",
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="exercise",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Exercise"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function publish(Request $request)
    {
        $item = $this->exerciseRepository->find($request->exercise);
        $item->is_published = !$item->is_published;
        $item->published_at = $item->is_published ? date("Y-m-d H:i:s") : null;
        $item->save();

        $translations = $this->exerciseRepository->getTranslations($item->id)->get();
        foreach($translations as $translation) {
            $this->exerciseRepository->saveTranslation($item->id, $translation->language_id, ['is_published' => $item->is_published]);
        }

        return response()->json(new JsonResponse($item));
    }


    /**
     * @OA\Delete(
     *     path="/admin/exercise/{exercise}",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Delete exercise",
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="exercise",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Exercise"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */

    public function destroy(Request $request)
    {
        if($this->exerciseRoundItemRepository->findWhere(['exercise_id' => $request->exercise])->count()) {
            return response()->json(new JsonResponse(null, false, 200, trans('errors.11')), 200);
        }

        $deleteExerciseResponse= $this->exerciseRepository->delete($request->exercise);
        $this->exerciseRepository->deleteTranslations($request->exercise);

        return response()->json(new JsonResponse(
            $deleteExerciseResponse ? true : false
        ));
    }
}
