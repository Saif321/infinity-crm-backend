<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Repositories\ExerciseRoundItemRepository;
use App\Repositories\RoundRepository;
use App\Repositories\ExerciseRepository;
use App\Repositories\ExerciseSetRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\TrainingRepository;
use App\Repositories\WeekItemRepository;
use App\Services\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class WorkoutController extends Controller
{
    private $exerciseRepository;
    private $languageRepository;
    private $exerciseSetRepository;
    private $roundRepository;
    private $trainingRepository;
    private $exerciseRoundItemRepository;
    private $weekItemRepository;

    public function __construct(
        ExerciseRepository $exerciseRepository,
        LanguageRepository $languageRepository,
        ExerciseSetRepository $exerciseSetRepository,
        RoundRepository $roundRepository,
        TrainingRepository $trainingRepository,
        ExerciseRoundItemRepository $exerciseRoundItemRepository,
        WeekItemRepository $weekItemRepository
    ) {
        $this->exerciseRepository = $exerciseRepository;
        $this->languageRepository = $languageRepository;
        $this->exerciseSetRepository = $exerciseSetRepository;
        $this->roundRepository = $roundRepository;
        $this->trainingRepository = $trainingRepository;
        $this->exerciseRoundItemRepository = $exerciseRoundItemRepository;
        $this->weekItemRepository = $weekItemRepository;
    }

    /**
     * @OA\Get(
     *     path="/admin/workout",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Training")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {

        return response()->json(
            new JsonResponse(
                $this->trainingRepository
                    ->getTraining($request->search)
                    ->paginate()
                    ->appends(['search' => $request->search])
            )
        );

    }

    /**
     * @OA\Get(
     *     path="/admin/workout/{workout}",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="workout",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        $training = $this->trainingRepository->find($request->workout);
        $training->translations = $this->trainingRepository->getTranslations($training->id)->get();

        $training->exercise_sets = $this->exerciseSetRepository->findWhere(['training_id' => $training->id]);
        foreach ($training->exercise_sets as &$exerciseSet) {
            $exerciseSet->translations = $this->exerciseSetRepository->getTranslations($exerciseSet->id)->get();
            $exerciseSet->rounds = $this->roundRepository->findWhere(['exercise_set_id' => $exerciseSet->id]);
            foreach ($exerciseSet->rounds as &$round) {
                $round->items = $this->exerciseRoundItemRepository->with('exercise')->findWhere(['round_id' => $round->id]);
            }
        }

        return response()->json(
            new JsonResponse($training)
        );

    }


    /**
     * @OA\Put(
     *     path="/admin/workout/{workout}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *            name="workout",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="integer"),
     *        ),
     *     @OA\Parameter(
     *         in="path",
     *         name="body",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/TrainingRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Training"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function update(Request $request, Translation $translation)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'number_of_calories' => 'required|integer',
            'translations.*' => 'required',
            'translations.*.name' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        try {
            DB::beginTransaction();

            if ($request->workout) {
                $item = $this->trainingRepository->find($request->workout);
                $item->fill($data);
                $item->save();
                $translations = $translation->translateObject($item, $request->translations);

            } else {
                $item = $this->trainingRepository->create($data);
                $translations = $translation->translateObject($item, $request->translations);

            }


            // delete old data
            $exerciseSets = $this->exerciseSetRepository->findWhere(['training_id' => $item->id]);
            foreach ($exerciseSets as $exerciseSet) {
                $this->exerciseSetRepository->deleteTranslations($exerciseSet->id);

                $rounds = $this->roundRepository->findWhere(['exercise_set_id' => $exerciseSet->id]);
                foreach ($rounds as $round) {
                    $this->exerciseRoundItemRepository->deleteWhere(['round_id' => $round->id]);
                    $round->delete();
                }

                $exerciseSet->delete();
            }

            foreach ($request->exercise_sets as $exerciseSetData) {
                $exerciseSetData['training_id'] = $item->id;
                $exerciseSet = $this->exerciseSetRepository->create($exerciseSetData);

                $translation->translateObject($exerciseSet, $exerciseSetData['translations']);

                foreach ($exerciseSetData['rounds'] as $roundData) {
                    $roundData['exercise_set_id'] = $exerciseSet->id;
                    $round = $this->roundRepository->create($roundData);

                    foreach ($roundData['items'] as $exerciseRoundItemData) {
                        if ($exerciseSet->type == 'time' && $exerciseRoundItemData['type'] == 'pause') {
                            continue;
                        }

                        $exerciseRoundItemData['round_id'] = $round->id;
                        $this->exerciseRoundItemRepository->create($exerciseRoundItemData);
                    }
                }
            }

            $response = $item->toArray();
            $response['translations'] = $translations;

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            throw $e;
        }
        return response()->json(new JsonResponse($response));

    }

    /**
     * @OA\Delete(
     *     path="/admin/workout/{workout}",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Delete workout",
     *     tags={"Admin"},
     *     @OA\Parameter(
     *            name="workout",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="integer"),
     *        ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Training"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */

    public function destroy(Request $request)
    {
        $deleteWorkoutResponse = $this->trainingRepository->delete($request->workout);
        $this->trainingRepository->deleteTranslations($request->workout);

        $exerciseSets = $this->exerciseSetRepository->findWhere(['training_id' => $request->workout]);
        foreach ($exerciseSets as $exerciseSet) {
            $this->exerciseSetRepository->deleteTranslations($exerciseSet->id);

            $rounds = $this->roundRepository->findWhere(['exercise_set_id' => $exerciseSet->id]);
            foreach ($rounds as $round) {
                $this->exerciseRoundItemRepository->deleteWhere(['round_id' => $round->id]);
            }
            $this->roundRepository->deleteWhere(['exercise_set_id' => $exerciseSet->id]);
        }
        $this->exerciseSetRepository->deleteWhere(['training_id' => $request->workout]);
        $this->weekItemRepository->deleteWhere(['training_id' => $request->workout]);

        return response()->json(new JsonResponse(
            $deleteWorkoutResponse ? true : false
        ));

    }
}
