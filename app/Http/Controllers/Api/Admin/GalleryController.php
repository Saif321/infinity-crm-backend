<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\Blog;
use App\Repositories\BlogRepository;
use App\Repositories\BlogTagRepository;
use App\Repositories\CommentRepository;
use App\Repositories\GalleryRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\LikeRepository;
use App\Repositories\UserRepository;
//use App\Rules\StrongPassword;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use App\Services\Thumbnailer\Thumbnailer;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;

class GalleryController extends Controller {
	private static $imageMimeTypes = [ 'jpeg', 'png' ];
	private static $videoMimeTypes = [ 'mpeg4', 'mpeg', 'mp4', 'm4v' ];

	private $galleryRepository;
	private $thumbnailer;

	public function __construct( GalleryRepository $galleryRepository, Thumbnailer $thumbnailer ) {
		$this->galleryRepository = $galleryRepository;
		$this->thumbnailer = $thumbnailer;
	}

	/**
	 * @OA\Get(
	 *     path="/admin/gallery",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Admin"},
	 *     @OA\Parameter(
	 *         in="query",
	 *         name="search",
	 *         required=false,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Parameter(
	 *         in="query",
	 *         name="type",
	 *         required=false,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Parameter(
	 *         in="query",
	 *         name="limit",
	 *         required=false,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="",
	 *         @OA\Schema(
	 *             type="array",
	 *             @OA\Items(ref="#/components/schemas/Gallery")
	 *         ),
	 *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
	public function index( Request $request ) {
		$list = $this->galleryRepository
			->getFiles( $request->search, $request->type )
			->paginate( $request->get( 'limit', 15 ) )
			->appends( [ 'search' => $request->search, 'type' => $request->type ] );

		return response()->json( new JsonResponse( $list ) );
	}

	/**
	 * @OA\Get(
	 *     path="/admin/gallery/avatar",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Admin"},
	 *     @OA\Parameter(
	 *         in="query",
	 *         name="search",
	 *         required=false,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Parameter(
	 *         in="query",
	 *         name="type",
	 *         required=false,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Parameter(
	 *         in="query",
	 *         name="limit",
	 *         required=false,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="",
	 *         @OA\Schema(
	 *             type="array",
	 *             @OA\Items(ref="#/components/schemas/Gallery")
	 *         ),
	 *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
	public function getAvatars( Request $request ) {
		$list = $this->galleryRepository
			->getFiles( $request->search, $request->type, true )
			->paginate( $request->get( 'limit', 15 ) )
			->appends( [ 'search' => $request->search, 'type' => $request->type ] );

		return response()->json( new JsonResponse( $list ) );
	}

	/**
	 * @OA\Get(
	 *     path="/admin/gallery/{gallery}",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Admin"},
	 *     @OA\Parameter(
	 *         in="path",
	 *         name="gallery",
	 *         required=true,
	 *         @OA\Schema(type="integer", format="int64")
	 *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="",
	 *         @OA\Schema(
	 *             type="array",
	 *             @OA\Items(ref="#/components/schemas/Blog")
	 *         ),
	 *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
	public function show( Request $request ) {
		return response()->json(
			new JsonResponse(
				$this->galleryRepository->find( $request->gallery )
			)
		);
	}

	/**
	 * @OA\Post(
	 *     path="/admin/gallery",
	 *     description="Uploads images and videos into gallery (currently not working on Swagger)",
	 *     tags={"Admin"},
	 *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="name",
     *                     description="Image/video name",
     *                     type="string",
     *                 ),
	 *				   @OA\Property(
     *                     property="url",
     *                     description="Image/video itself",
     *                     type="file",
     *                 ),
     *                 @OA\Property(
     *                     property="is_user_avatar",
     *                     description="Shall we use it as avatar",
     *                     type="integer",
     *                 )
     *             )
     *         )
     *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="",
	 *         @OA\Schema(ref="#/components/schemas/Gallery"),
	 *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
	public function store( Request $request ) {
		$videoMimeTypes = join( ',', static::$videoMimeTypes );
		$imageMimeTypes = join( ',', static::$imageMimeTypes );
		$validator      = Validator::make( $request->all(), [
			'name' => 'required',
			'url'  => 'required|mimes:' . $imageMimeTypes . ',' . $videoMimeTypes
		] );
		if ( $validator->fails() ) {
			return response()->json( new JsonResponse( null, false, 2, $validator->messages() ) );
		}

		$extension = $request->url->extension();
		$fileName  = md5( microtime() );
		$directory = $fileName[0] . '/' . $fileName[1] . '/' . $fileName[2];

        $imageUrl = $request->file( 'url' )->storeAs( 'public/' . $directory, $fileName . '.' . $extension );

		$item                 = $this->galleryRepository->makeModel();
		$item->name           = $request->name;
		$item->mime_type      = $request->file( 'url' )->getMimeType();
		$item->extension      = $extension;
		$item->size           = $request->file( 'url' )->getSize();
		$item->url            = $imageUrl;
		$item->type           = preg_match( '/^image/is', $request->file( 'url' )->getMimeType() ) ? 'image' : 'video';
		$item->is_user_avatar = $request->is_user_avatar == 1 ? true : false;

		$item->thumbnail = $this->thumbnailer->setGallery($item)->generateThumbnail();

		$item->save();

		return response()->json( new JsonResponse( $item ) );
	}

	/**
	 * @OA\Put(
	 *     path="/admin/gallery/{gallery}",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Admin"},
	 *     @OA\Parameter(
	 *            name="gallery",
	 *            in="path",
	 *            required=true,
	 *            @OA\Schema(type="integer"),
	 *        ),
	 *     @OA\Parameter(
	 *         in="path",
	 *         name="body",
	 *         required=true,
	 *         @OA\Schema(ref="#/components/schemas/GalleryEditRequest")
	 *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="",
	 *         @OA\Schema(ref="#/components/schemas/Gallery"),
	 *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
	public function update( Request $request ) {
		$validator = Validator::make( $request->all(), [
			'name' => 'required',
		] );
		if ( $validator->fails() ) {
			return response()->json( new JsonResponse( null, false, 2, $validator->messages() ) );
		}

		$item       = $this->galleryRepository->find( $request->gallery );
		$item->name = $request->name;
		$item->save();

		return response()->json( new JsonResponse( $item ) );
	}

	/**
	 * @OA\Delete(
	 *     path="/admin/gallery/{gallery}",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     description="",
	 *     tags={"Admin"},
	 *     @OA\Parameter(
	 *            name="gallery",
	 *            in="path",
	 *            required=true,
	 *            @OA\Schema(type="integer"),
	 *        ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="",
	 *         @OA\Schema(ref="#/components/schemas/JsonResponse"),
	 *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
	public function destroy( Request $request ) {
		$item = $this->galleryRepository->find( $request->gallery );

		if ( $item ) {
			$item->deleteFile()->delete();
		}

		return response()->json( new JsonResponse( null ) );
	}


	/**
	 * @OA\Get(
	 *     path="/admin/gallery/upload-limits",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     description="Returns gallery upload limits",
	 *     tags={"Admin"},
	 *     @OA\Response(
	 *         response=200,
	 *         description="success",
	 *         @OA\Schema(type="object",
	 *              @OA\Property(property="image_types", type="array", @OA\Items(type="string")),
	 *              @OA\Property(property="video_types", type="array", @OA\Items(type="string")),
	 *              @OA\Property(property="upload_limit_in_bytes", @OA\Schema(type="integer"))
	 *         )
	 *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
	public function uploadLimits() {
		return response()
			->json( new JsonResponse( [
				'image_types'           => static::$imageMimeTypes,
				'video_types'           => static::$videoMimeTypes,
				'upload_limit_in_bytes' => $this->file_upload_max_size()
			] ) );
	}

	/**
	 * @return int
	 */
	private function file_upload_max_size() {
		static $max_size = - 1;

		if ( $max_size < 0 ) {
			// Start with post_max_size.
			$post_max_size = $this->parse_size( ini_get( 'post_max_size' ) );
			if ( $post_max_size > 0 ) {
				$max_size = $post_max_size;
			}

			// If upload_max_size is less, then reduce. Except if upload_max_size is
			// zero, which indicates no limit.
			$upload_max = $this->parse_size( ini_get( 'upload_max_filesize' ) );
			if ( $upload_max > 0 && $upload_max < $max_size ) {
				$max_size = $upload_max;
			}
		}

		return $max_size;
	}

	private function parse_size( $size ) {
		$unit = preg_replace( '/[^bkmgtpezy]/i', '', $size ); // Remove the non-unit characters from the size.
		$size = preg_replace( '/[^0-9\.]/', '', $size ); // Remove the non-numeric characters from the size.
		if ( $unit ) {
			// Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
			return round( $size * pow( 1024, stripos( 'bkmgtpezy', $unit[0] ) ) );
		} else {
			return round( $size );
		}
	}
}
