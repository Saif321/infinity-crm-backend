<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\Blog;
use App\Models\Program;
use App\Repositories\BlogRepository;
use App\Repositories\BlogTagRepository;
use App\Repositories\CommentRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\LikeRepository;
use App\Repositories\PurchasedProgramRepository;
use App\Repositories\UserProgramRepository;
use App\Repositories\UserRepository;
//use App\Rules\StrongPassword;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    private $commentRepository;

    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    /**
     * @OA\Get(
     *     path="/admin/comment",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Blog")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        $list = $this->commentRepository->getComments($request->search)->with('user')->paginate()->appends(['search' => $request->search]);
        foreach ($list as &$item) {
            $model = app()->make($item->object_type);
            $commentForItem = $model->find($item->object_id);

            $item->root = $commentForItem;
        }

        return response()->json(
            new JsonResponse($list)
        );
    }

    /**
     * @OA\Delete(
     *     path="/admin/comment/{comment}",

     *     @OA\MediaType(mediaType="application/json"),
     *     description="Change publish status for comment",
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="comment",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/JsonResponse"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function destroy(Request $request)
    {
        $item = $this->commentRepository->find($request->comment);
        if(class_exists($item->object_type)) {
            $object = app()->make($item->object_type)->find($item->object_id);
            $repositoryClass = 'App\\Repositories\\' . class_basename($item->object_type) . 'Repository';
            $repository = app()->make($repositoryClass);

            if($object) {
                $object->comment_count = $this->commentRepository->getPublishedComments($item->object_id, $item->object_type)->count();
                $object->save();

                foreach($repository->getTranslations($object->id)->get() as $translation) {
                    $repository->saveTranslation($object->id, $translation->language_id, ['comment_count' => $object->comment_count]);
                }
            }
        }

        return response()->json(new JsonResponse(
            $this->commentRepository->delete($request->comment) ? true : false
        ));
    }

    /**
     * @OA\Put(
     *     path="/admin/comment/{comment}/publish",

     *     @OA\MediaType(mediaType="application/json"),
     *     description="Change publish status for comment",
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="comment",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Comment"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function publish(Request $request)
    {
        $item = $this->commentRepository->find($request->comment);
        $item->is_published = !$item->is_published;
        $item->published_at = $item->is_published ? date("Y-m-d H:i:s") : null;
        $item->save();

        if(class_exists($item->object_type)) {
            $object = app()->make($item->object_type)->find($item->object_id);
            $repositoryClass = 'App\\Repositories\\' . class_basename($item->object_type) . 'Repository';
            $repository = app()->make($repositoryClass);

            if($object) {
                $object->comment_count = $this->commentRepository->getPublishedComments($item->object_id, $item->object_type)->count();
                $object->save();

                foreach($repository->getTranslations($object->id)->get() as $translation) {
                    $repository->saveTranslation($object->id, $translation->language_id, ['comment_count' => $object->comment_count]);
                }

                if($object instanceof Program) {
                    /** @var UserProgramRepository $userProgramRepository */
                    $userProgramRepository = app()->make(UserProgramRepository::class);
                    $userPrograms = $userProgramRepository->getPurchasedProgramsBySourceProgramId($object->id)->get();

                    foreach($userPrograms as $userProgram) {
                        $userProgram->comment_count = $object->comment_count;
                        $userProgram->save();

                        foreach($userProgramRepository->getTranslations($userProgram->id)->get() as $userProgramTranslation) {
                            $userProgramRepository->saveTranslation($userProgram->id, $userProgramTranslation->language_id, ['comment_count' => $object->comment_count]);
                        }
                    }
                }
            }
        }

        return response()->json(new JsonResponse($item));
    }
}
