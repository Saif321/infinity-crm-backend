<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\Meal;
use App\Repositories\LanguageRepository;
use App\Repositories\MealIngredientRepository;
use App\Repositories\MealRecipeStepRepository;
use App\Repositories\MealRepository;
use App\Repositories\NutritionProgramItemRepository;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use App\Services\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MealController extends Controller
{
    private $mealRepository;
    private $languageRepository;
    private $mealIngredientRepository;
    private $mealRecipeStepRepository;
    private $nutritionProgramItemRepository;

    public function __construct(
        MealRepository $mealRepository,
        LanguageRepository $languageRepository,
        MealIngredientRepository $mealIngredientRepository,
        MealRecipeStepRepository $mealRecipeStepRepository,
        NutritionProgramItemRepository $nutritionProgramItemRepository
    )
    {
        $this->mealRepository = $mealRepository;
        $this->languageRepository = $languageRepository;
        $this->mealIngredientRepository = $mealIngredientRepository;
        $this->mealRecipeStepRepository = $mealRecipeStepRepository;
        $this->nutritionProgramItemRepository = $nutritionProgramItemRepository;
    }

    /**
     * @OA\Get(
     *     path="/admin/meal",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Meal")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        return response()->json(
            new JsonResponse(
                $this->mealRepository
                    ->getMealsForAdmin($request->search)
                    ->with('image', 'video')
                    ->paginate()
                    ->appends(['search' => $request->search])
            )

        );
    }

    /**
     * @OA\Get(
     *     path="/admin/meal/{meal}",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="meal",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Meal")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        $meal = $this->mealRepository->find($request->meal);
        $meal->load('image', 'video');
        $meal->translations = $this->mealRepository->getTranslations($meal->id)->with('image', 'video')->get();

        $meal->ingredients = $this->mealIngredientRepository->findWhere(['meal_id' => $meal->id]);
        foreach($meal->ingredients as &$ingredient) {
            $ingredient->translations = $this->mealIngredientRepository->getTranslations($ingredient->id)->get();
        }

        $meal->recipes = $this->mealRecipeStepRepository->findWhere(['meal_id' => $meal->id]);
        foreach($meal->recipes as &$recipe) {
            $recipe->translations = $this->mealRecipeStepRepository->getTranslations($recipe->id)->get();
        }

        return response()->json(
            new JsonResponse($meal)
        );

    }

    /**
     * @OA\Put(
     *     path="/admin/meal/{meal}",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="meal",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Parameter(
     *         in="path",
     *         name="body",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/MealRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Meal"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function update(Request $request, Translation $translation)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'image_id' => 'required',
//            'video_id' => 'required',
            'preparation_time_in_minutes' => 'required',
            'number_of_calories' => 'required',
            'fat' => 'required',
            'carbohydrates' => 'required',
            'proteins' => 'required',
            'translations' => 'required',
            'ingredients.*' => 'required',
            'steps.*' => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        if($request->meal) {
            $item = $this->mealRepository->find($request->meal);
            $item->fill($data);
            $item->save();
        } else {
            $item = $this->mealRepository->create($data);
        }

        $translation->translateObject($item, $request->translations); // save translations for meal

        $this->mealIngredientRepository->deleteWhere(['meal_id' => $item->id]);
        $this->mealIngredientRepository->deleteTranslationsWhere(['meal_id' => $item->id]);
        foreach($request->ingredients as $ingredient) {
            $mealIngredient = $this->mealIngredientRepository->create([
                'meal_id' => $item->id,
                'name' => $ingredient['name']
            ]);

            $translation->translateObject($mealIngredient, $ingredient['translations']); // save translations for ingredients
        }

        $this->mealRecipeStepRepository->deleteWhere(['meal_id' => $item->id]);
        $this->mealRecipeStepRepository->deleteTranslationsWhere(['meal_id' => $item->id]);
        foreach($request->steps as $step) {
            $mealStep = $this->mealRecipeStepRepository->create([
                'meal_id' => $item->id,
                'description' => $step['description'],
                'order' => $step['order']
            ]);

            $translation->translateObject($mealStep, $step['translations']); // save translations for meal steps
        }

        $item->translations = $this->mealRepository->getTranslations($item->id)->get();

        return response()->json(new JsonResponse($item));
    }


    /**
     * @OA\Delete(
     *     path="/admin/meal/{meal}",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Delete meal",
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="meal",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Meal"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */

    public function destroy(Request $request)
    {
        if($this->nutritionProgramItemRepository->findWhere(['meal_id' => $request->meal])->count()) {
            return response()->json(new JsonResponse(null, false, 200, trans('errors.11')), 200);
        }

        $deleteMealResponse= $this->mealRepository->delete($request->meal);
        $this->mealRepository->deleteTranslations($request->meal);

        return response()->json(new JsonResponse(
            $deleteMealResponse ? true : false
        ));
    }
}
