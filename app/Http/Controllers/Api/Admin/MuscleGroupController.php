<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\MuscleGroup;
use App\Repositories\MuscleGroupRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\UserRepository;
use App\Rules\StrongPassword;
use App\Services\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class MuscleGroupController extends Controller
{
    private $languageRepository;
    private $muscleGroupRepository;

    public function __construct(MuscleGroupRepository $muscleGroupRepository, LanguageRepository $languageRepository)
    {
        $this->muscleGroupRepository = $muscleGroupRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * @OA\Get(
     *     path="/admin/muscle-group",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/MuscleGroup")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */

    public function index(Request $request)
    {
        return response()->json(
            new JsonResponse($this->muscleGroupRepository->getMuscleGroups($request->search)->paginate()->appends(['search' => $request->search]))
        );
    }

    /**
     * @OA\Get(
     *     path="/admin/muscle-group/list",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/MuscleGroup")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */

    public function fullList(Request $request)
    {
        return response()->json(
            new JsonResponse($this->muscleGroupRepository->getMuscleGroups($request->search)->get())
        );
    }

    /**
     * @OA\Get(
     *     path="/admin/muscle-group/{muscle_group}",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="muscle_group",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/MuscleGroup")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        $item = $this->muscleGroupRepository->find($request->muscle_group);
        $item->translations = $this->muscleGroupRepository->getTranslations($item->id)->get();

        return response()->json(
            new JsonResponse($item)
        );
    }

    /**
     * @OA\Put(
     *     path="/admin/muscle-group/{muscle_group}",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="muscle_group",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Parameter(
     *         in="path",
     *         name="body",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/MuscleGroupRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/MuscleGroup"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function update(Request $request, Translation $translation)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        if($request->muscle_group) {
            $item = $this->muscleGroupRepository->find($request->muscle_group);
            $item->fill($data);
            $item->save();
        } else {
            $data['slug'] = Str::slug($data['name']);

            $item = $this->muscleGroupRepository->create($data);
        }

        $response = $item->toArray();
        $response['translations'] = $translation->translateObject($item, $request->translations);

        return response()->json(new JsonResponse($response));
    }


    /**
     * @OA\Delete(
     *     path="/admin/muscle-group/{muscle_group}",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="muscle_group",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/MuscleGroup"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */

    public function destroy(Request $request)
    {
        $deleteMuscleGroupResponse = $this->muscleGroupRepository->delete($request->muscle_group);
        $this->muscleGroupRepository->deleteTranslations($request->muscle_group);

        return response()->json(new JsonResponse(
            $deleteMuscleGroupResponse ? true : false
        ));
    }


}
