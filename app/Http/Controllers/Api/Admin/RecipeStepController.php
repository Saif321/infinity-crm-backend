<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\RecipeStep;
use App\Repositories\LanguageRepository;
use App\Repositories\MealRecipeStepRepository;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use App\Services\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class RecipeStepController extends Controller
{
    private $recipeStepRepository;
    private $languageRepository;

    public function __construct(MealRecipeStepRepository $recipeStepRepository, LanguageRepository $languageRepository)
    {
        $this->recipeStepRepository = $recipeStepRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * @OA\Get(
     *     path="/admin/recipe-step",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/RecipeStep")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {

        return response()->json(
            new JsonResponse($this->recipeStepRepository->paginate())
        );

    }

    /**
     * @OA\Get(
     *     path="/admin/recipe-step/{recipe_step}",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="recipe_step",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/RecipeStep")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        $recipeStep = $this->recipeStepRepository->find($request->recipe_step);
        $recipeStep->translations = $this->recipeStepRepository->getTranslations($recipeStep->id)->get();

        return response()->json(
            new JsonResponse($recipeStep)
        );
    }

    /**
     * @OA\Put(
     *     path="/admin/recipe-step/{recipe_step}",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="recipe_step",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Parameter(
     *         in="path",
     *         name="recipe_step",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/RecipeStepRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/RecipeStep"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function update(Request $request, Translation $translation)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'description' => 'required',
        ]);
        if($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        if($request->recipeStep) {
            $item = $this->recipeStepRepository->find($request->recipeStep);
            $item->fill($data);
            $item->save();
        } else {
            $item = $this->recipeStepRepository->create($data);
        }

        $response = $item->toArray();
        $response['translations'] = $translation->translateObject($item, $request->translations);

        return response()->json(new JsonResponse($response));
    }


    /**
     * @OA\Delete(
     *     path="/admin/recipe-step/{recipe_step}",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Delete Recipe Step",
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="recipe_step",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/RecipeStep"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */

    public function destroy(Request $request)
    {
        $deleteRecipeStepResponse= $this->recipeStepRepository->delete($request->recipe_step);
        $this->recipeStepRepository->deleteTranslations($request->recipe_step);

        return response()->json(new JsonResponse(
            $deleteRecipeStepResponse ? true : false
        ));
    }
}
