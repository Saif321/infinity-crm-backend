<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Repositories\LanguageRepository;
use App\Repositories\NutritionProgramItemRepository;
use App\Repositories\NutritionProgramMealGroupRepository;
use App\Repositories\NutritionProgramRepository;
use App\Repositories\PurchasedNutritionProgramRepository;
use App\Repositories\UserRepository;
use App\Services\BuyNutritionProgram;
use App\Services\Translation;
use App\Services\WordPressService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class NutritionProgramController extends Controller
{
    private $nutritionProgramRepository;
    private $languageRepository;
    private $nutritionProgramMealGroupRepository;
    private $nutritionProgramItemRepository;
    private $userRepository;
    private $purchasedNutritionProgramRepository;
    private $wordPressService;
    private $buyNutritionProgram;
    private $sync;

    public function __construct(
        NutritionProgramRepository $nutritionProgramRepository,
        LanguageRepository $languageRepository,
        NutritionProgramMealGroupRepository $nutritionProgramMealGroupRepository,
        NutritionProgramItemRepository $nutritionProgramItemRepository,
        UserRepository $userRepository,
        PurchasedNutritionProgramRepository $purchasedNutritionProgramRepository,
        WordPressService $wordPressService,
        BuyNutritionProgram $buyNutritionProgram
    ) {
        $this->nutritionProgramRepository = $nutritionProgramRepository;
        $this->languageRepository = $languageRepository;
        $this->nutritionProgramMealGroupRepository = $nutritionProgramMealGroupRepository;
        $this->nutritionProgramItemRepository = $nutritionProgramItemRepository;
        $this->userRepository = $userRepository;
        $this->purchasedNutritionProgramRepository = $purchasedNutritionProgramRepository;
        $this->wordPressService = $wordPressService;
        $this->buyNutritionProgram = $buyNutritionProgram;
        $this->sync = config('infinity-fit.wordpress.wordpress_sync');
    }

    /**
     * @OA\Get(
     *     path="/admin/nutrition-program",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="nutrition_program",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/NutritionProgram")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        return response()->json(
            new JsonResponse(
                $this->nutritionProgramRepository
                    ->searchNutritionPrograms($request->search)
                    ->paginate()
                    ->appends(['search' => $request->search])
            )
        );

    }

    /**
     * @OA\Get(
     *     path="/admin/nutrition-program/{nutrition_program}",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="nutrition_program",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/NutritionProgram")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        $nutritionProgram = $this->nutritionProgramRepository->find($request->nutrition_program);
        $nutritionProgram->groups = $this->nutritionProgramMealGroupRepository
            ->orderBy('order')
            ->with('mealGroup')
            ->findWhere(['nutrition_program_id' => $nutritionProgram->id]);
        foreach ($nutritionProgram->groups as &$group) {
            $group->items = $this->nutritionProgramItemRepository
                ->orderBy('order')
                ->with('meal')
                ->findWhere(['nutrition_program_meal_group_id' => $group->id]);
        }

        $nutritionProgram->load('goals', 'image', 'video');
        $nutritionProgram->translations = $this->nutritionProgramRepository->getTranslations($nutritionProgram->id)->with('image',
            'video')->get();

        return response()->json(
            new JsonResponse($nutritionProgram)
        );
    }

    /**
     * @OA\Put(
     *     path="/admin/nutrition-program/{nutrition_program}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *            name="nutrition_program",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="integer"),
     *        ),
     *     @OA\Parameter(
     *         in="path",
     *         name="body",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/NutritionProgramRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/NutritionProgram"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function update(Request $request, Translation $translation)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'image_id' => 'required',
//            'video_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        if ($request->nutrition_program) {
            $item = $this->nutritionProgramRepository->find($request->nutrition_program);
            $translates = $this->nutritionProgramRepository->getTranslations($item->id)->get();
            $oldNames = [];
            foreach ($translates as $translate) {
                $oldNames[] = $translate->name;
            }
            $item->fill($data);
            $item->save();
            $translations = $translation->translateObject($item, $request->translations);
            if ($this->sync) {
                $translationIds = [];
                for ($i = 0; $i < count($translations); $i++) {
                    $nutritionProgramData = [
                        'title' => $translations[$i]->name,
                        'description' => $translations[$i]->description,
                        'image_id' => $item->image_id,
                        'language_id' => $translations[$i]->language_id
                    ];
                    $translationIds[] = $this->wordPressService->updatePost(
                        $nutritionProgramData,
                        'nutrition',
                        $oldNames[$i],
                        $item->is_published
                    );
                }
                $this->wordPressService->matchPostTranslation($translationIds[0], $translationIds[1]);
            }
        } else {
            $item = $this->nutritionProgramRepository->create($data);
            $translations = $translation->translateObject($item, $request->translations);
            if ($this->sync) {
                $translationsIds = [];
                foreach ($translations as $translation) {
                    $nutritionProgramData = [
                        'title' => $translation->name,
                        'description' => $translation->description,
                        'image_id' => $item->image_id,
                        'language_id' => $translation->language_id
                    ];
                    $translationsIds[] = $this->wordPressService->updatePost(
                        $nutritionProgramData,
                        'nutrition',
                        $item->is_published
                    );
                }
                $this->wordPressService->matchPostTranslation($translationsIds[0], $translationsIds[1]);
            }
        }

        if (array_key_exists('goal_ids', $data)) {
            $item->goals()->sync($data['goal_ids']);
        }

        $item->translations = $this->nutritionProgramRepository->getTranslations($item->id)->get();

        $nutritionProgramMealGroups = $this->nutritionProgramMealGroupRepository->findWhere(['nutrition_program_id' => $item->id]);
        foreach ($nutritionProgramMealGroups as $nutritionProgramMealGroup) {
            $this->nutritionProgramItemRepository->deleteWhere(['nutrition_program_meal_group_id' => $nutritionProgramMealGroup->id]);
        }
        $this->nutritionProgramMealGroupRepository->deleteWhere(['nutrition_program_id' => $item->id]);

        foreach ($data['groups'] as $order => $nutritionProgramMealGroupData) {
            $nutritionProgramMealGroupData['nutrition_program_id'] = $item->id;
            $nutritionProgramMealGroupData['order'] = $order;
            $nutritionProgramMealGroup = $this->nutritionProgramMealGroupRepository->create($nutritionProgramMealGroupData);
            foreach ($nutritionProgramMealGroupData['items'] as $nutritionProgramItemData) {
                $nutritionProgramItemData['nutrition_program_meal_group_id'] = $nutritionProgramMealGroup->id;
                $this->nutritionProgramItemRepository->create($nutritionProgramItemData);
            }
        }

        return response()->json(new JsonResponse($item));
    }

    /**
     * @OA\Put(
     *     path="/admin/nutrition-program/{nutrition_program}/publish",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Change publish status for nutrition programs",
     *     tags={"Admin"},
     *     @OA\Parameter(
     *            name="nutrition_program",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="integer"),
     *        ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/NutritionProgram"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function publish(Request $request)
    {
        $item = $this->nutritionProgramRepository->find($request->nutrition_program);
        $item->is_published = !$item->is_published;
        $item->save();
        $translations = $this->nutritionProgramRepository->getTranslations($item->id)->get();
        $translationIds = [];
        foreach ($translations as $translation) {
            $this->nutritionProgramRepository
                ->saveTranslation($item->id, $translation->language_id, ['is_published' => $item->is_published]);
            if ($this->sync) {
                $postId = $this->wordPressService->publishPost($translation->name, $item->is_published,
                    $translation->language_id);
                if (!$postId) {
                    $nutritionData = [
                        'title' => $translation->name,
                        'description' => $translation->description,
                        'image_id' => $item->image_id,
                        'language_id' => $translation->language_id
                    ];
                    $translationIds[] = $this->wordPressService->updatePost($nutritionData, 'nutrition', null, null,
                        $item->is_published);
                    $this->wordPressService->publishPost($translation->title, $item->is_published,
                        $translation->language_id);
                }
            }
        }
        if (count($translationIds)) {
            $this->wordPressService->matchPostTranslation($translationIds[0], $translationIds[1]);
        }

        return response()->json(new JsonResponse($item));
    }


    /**
     * @OA\Delete(
     *     path="/admin/nutrition-program/{nutrition_program}",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Delete Nutrition Program",
     *     tags={"Admin"},
     *     @OA\Parameter(
     *            name="nutrition_program",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="integer"),
     *        ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/NutritionProgram"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */

    public function destroy(Request $request)
    {
        if ($this->purchasedNutritionProgramRepository->findWhere(['nutrition_program_id' => $request->nutrition_program])->count()) {
            return response()->json(new JsonResponse(null, false, 200, trans('errors.11')), 200);
        }

        $deleteNutritionProgramResponse = $this->nutritionProgramRepository->delete($request->nutrition_program);
        $translations = $this->nutritionProgramRepository->getTranslations($deleteNutritionProgramResponse->name)->get();
        if ($this->sync) {
            foreach ($translations as $translation) {
                $this->wordPressService->deletePost($translation->name, $translation->language_id);
            }
        }
        $this->nutritionProgramRepository->deleteTranslations($request->nutrition_program);
        return response()->json(new JsonResponse(
            $deleteNutritionProgramResponse ? true : false
        ));
    }

    /**
     * @OA\Get(
     *     path="/admin/nutrition-program/{nutrition_program}/users",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *            name="nutrition_program",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="integer"),
     *       ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/User")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function users(Request $request)
    {
        return response()->json(new JsonResponse(
            $this->userRepository->getUsersForNutritionProgram($request->nutrition_program)->with('image')->get()
        ));
    }

    /**
     * @OA\Put(
     *     path="/admin/nutrition-program/{nutrition_program}/user/{user}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *            name="nutrition_program",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="integer"),
     *       ),
     *     @OA\Parameter(
     *            name="user",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="integer"),
     *       ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/User")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function buyForUser(Request $request)
    {
        $program = $this->nutritionProgramRepository->find($request->nutrition_program);
        $user = $this->userRepository->find($request->user);

        if (!$program || !$user) {
            return response()->json(new JsonResponse(null, false, 6, trans('errors.6')));
        }

        try {
            return response()->json(new JsonResponse(
                $this->buyNutritionProgram->buy($program, $user, true)
            ));
        } catch (\Exception $e) {
            Log::debug($e->getMessage(), $e->getTrace());
            return response()->json(new JsonResponse(null, false, 500, $e->getMessage()));
        }
    }
}
