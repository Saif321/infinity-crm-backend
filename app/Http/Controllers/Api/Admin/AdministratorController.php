<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\User;
use App\Repositories\UserRepository;
//use App\Rules\StrongPassword;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class AdministratorController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;

    }

    /**
     * @OA\Get(
     *     path="/admin/administrator",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/User")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        return response()->json(
            new JsonResponse($this->userRepository->getAllAdministrators($request->search)->with('roles', 'image')->paginate()->appends(['search' => $request->search]))
        );
    }

    /**
     * @OA\Get(
     *     path="/admin/administrator/{administrator}",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="administrator",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/User")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        $item = $this->userRepository->with('roles', 'image')->find($request->administrator);

        if(!$item->hasRole('administrator')) {
            return response()->json(new JsonResponse(null, false, 8, trans('errors.8')));
        }

        return response()->json(
            new JsonResponse($item)
        );
    }

    /**
     * @OA\Put(
     *     path="/admin/administrator/{administrator}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="administrator",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Parameter(
     *         in="path",
     *         name="body",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/UserRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/UserRequest"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function update(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => ['required', $request->administrator ? Rule::unique('users')->ignore($request->administrator) : ''],
            'password' => [!$request->administrator ? 'required' : '', new StrongPassword()],
        ]);
        if($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        if($request->administrator) {
            $item = $this->userRepository->find($request->administrator);
            $item->fill($data);
            $item->save();
        }else {
            try {
                DB::beginTransaction();

                $data['is_active'] = 1;
                $item = $this->userRepository->create($data);
                $item->assignRole('administrator');

                DB::commit();
            } catch(\Exception $e) {
                DB::rollback();

                throw $e;
            }
        }

        $item->load('image');
        $item->load('roles');

        return response()->json(new JsonResponse($item));
    }

    /**
     * @OA\Delete(
     *     path="/admin/administrator/{administrator}",

     *     @OA\MediaType(mediaType="application/json"),
     *     description="administrator",
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="administrator",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/User"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function destroy(Request $request)
    {
        if(!Auth::user()->hasRole('root')) {
            return response()->json(new JsonResponse(null, false, 8, trans('errors.8')));
        }

        return response()->json(new JsonResponse(
            $this->userRepository->delete($request->administrator) ? true : false
        ));
    }
}
