<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\Blog;
use App\Repositories\BlogRepository;
use App\Repositories\CommentRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\LikeRepository;
use App\Services\Blog as BlogService;
use App\Services\Translation;
use App\Services\WordPressService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Prettus\Validator\Exceptions\ValidatorException;

class BlogController extends Controller
{
    private $blogRepository;
    private $likeRepository;
    private $commentRepository;
    private $languageRepository;
    private $wordpressService;
    private $blogService;
    private $sync;

    public function __construct(
        BlogRepository $blogRepository,
        LikeRepository $likeRepository,
        CommentRepository $commentRepository,
        LanguageRepository $languageRepository,
        WordPressService $wordPressService,
        BlogService $blogService
    ) {
        $this->blogRepository = $blogRepository;
        $this->likeRepository = $likeRepository;
        $this->commentRepository = $commentRepository;
        $this->languageRepository = $languageRepository;
        $this->wordpressService = $wordPressService;
        $this->blogService = $blogService;
        $this->sync = config('infinity-fit.wordpress.wordpress_sync');
    }

    /**
     * @OA\Get(
     *     path="/admin/blog",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Blog")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        return response()->json(
            new JsonResponse($this->blogRepository->getPosts($request->search)->paginate()->appends(['search' => $request->search]))
        );
    }

    /**
     * @OA\Get(
     *     path="/admin/blog/{blog}",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="blog",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Blog")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        $blog = $this->blogRepository->with('image')->find($request->blog);
        $blog->translations = $this->blogRepository->getTranslations($blog->id)->get();

        return response()->json(
            new JsonResponse($blog)
        );
    }

    /**
     * @OA\Put(
     *     path="/admin/blog/{blog}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *            name="blog",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="integer"),
     *        ),
     *     @OA\Parameter(
     *         in="path",
     *         name="body",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/BlogRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Blog"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     * @param Request $request
     * @param Translation $translation
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidatorException
     */
    public function update(Request $request, Translation $translation)
    {
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'translations' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        if ($request->blog) {

            $item = $this->blogRepository->find($request->blog);
            $translates = $this->blogRepository->getTranslationsForPost($item->id)->get();
            $oldNames = [];
            foreach ($translates as $translate) {
                $oldNames[] = $translate->title;
            }
            $oldImageID = $item->image_id;
            $item->fill($data);
            $item->save();
            $translations = $translation->translateObject($item, $request->translations);
            if ($this->sync) {
                $translationIds = [];
                for ($i = 0; $i < count($translations); $i++) {
                    $blogData = [
                        'title' => $translations[$i]->title,
                        'description' => $this->blogService->parseContentAndReplaceOembed($translations[$i]->description),
                        'image_id' => $item->image_id,
                        'language_id' => $translations[$i]->language_id
                    ];
                    if (isset($item->published_at)) {
                        $blogData['date'] = $item->published_at;
                    }
                    $translationIds[] = $this->wordpressService->updatePost(
                        $blogData,
                        'blog',
                        $oldNames[$i],
                        $item->is_published
                    );
                }
                $this->wordpressService->matchPostTranslation($translationIds[0], $translationIds[1]);
            }
        } else {

            $item = $this->blogRepository->create($data);
            $translations = $translation->translateObject($item, $request->translations);
            if ($this->sync) {
                $translationIds = [];
                foreach ($translations as $translation) {
                    $blogData = [
                        'title' => $translation->title,
                        'description' => $this->blogService->parseContentAndReplaceOembed($translation->description),
                        'image_id' => $item->image_id,
                        'language_id' => $translation->language_id
                    ];
                    $translationIds[] = $this->wordpressService->updatePost(
                        $blogData,
                        'blog',
                        null,
                        $translation->is_published
                    );
                }
                $this->wordpressService->matchPostTranslation($translationIds[0], $translationIds[1]);
            }
        }

        $response = $item->toArray();
        $response['translations'] = $translations;

        return response()->json(new JsonResponse($response));
    }

    /**
     * @OA\Put(
     *     path="/admin/blog/{blog}/publish",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Change publish status for blog post",
     *     tags={"Admin"},
     *     @OA\Parameter(
     *            name="blog",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="integer"),
     *        ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Blog"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function publish(Request $request)
    {
        $item = $this->blogRepository->find($request->blog);
        $item->is_published = !$item->is_published;
        $item->published_at = $item->is_published ? date("Y-m-d H:i:s") : null;
        $item->save();
        $translationIds = [];
        foreach ($this->blogRepository->getTranslations($item->id)->get() as $translation) {
            if ($this->sync) {
                $postId = $this->wordpressService->publishPost($translation->title, $item->is_published,
                    $translation->language_id);
                if (!$postId) {
                    $blogData = [
                        'title' => $translation->title,
                        'description' => $this->blogService->parseContentAndReplaceOembed($translation->description),
                        'image_id' => $item->image_id,
                        'language_id' => $translation->language_id
                    ];
                    if (isset($item->published_at)) {
                        $blogData['date'] = $item->published_at;
                    }
                    $translationIds[] = $this->wordpressService->updatePost($blogData, 'blog');
                    $this->wordpressService->publishPost($translation->title, $item->is_published,
                        $translation->language_id);
                }
                $this->blogRepository->saveTranslation($item->id, $translation->language_id, [
                    'is_published' => $item->is_published,
                    'published_at' => $item->published_at
                ]);
            }
        }
        if (count($translationIds)) {
            $this->wordpressService->matchPostTranslation($translationIds[0], $translationIds[1]);
        }

        return response()->json(new JsonResponse($item));
    }

    /**
     * @OA\Delete(
     *     path="/admin/blog/{blog}",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Change publish status for comment",
     *     tags={"Admin"},
     *     @OA\Parameter(
     *            name="blog",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="integer"),
     *        ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/JsonResponse"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function destroy(Request $request)
    {
        $blog = $this->blogRepository->find($request->blog);
        $translations = $this->blogRepository->getTranslations($blog->id)->get();
        if ($this->sync) {
            foreach ($translations as $translation) {
                $this->wordpressService->deletePost($translation->title, $translation->language_id);
            }
        }
        $deleteBlogResponse = $this->blogRepository->delete($request->blog);
        $this->blogRepository->deleteTranslations($request->blog);
        $this->commentRepository->deleteWhere(['object_type' => Blog::class, 'object_id' => $request->blog]);
        $this->likeRepository->deleteWhere(['object_type' => Blog::class, 'object_id' => $request->blog]);
        return response()->json(new JsonResponse(
            $deleteBlogResponse ? true : false
        ));
    }
}
