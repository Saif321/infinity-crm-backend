<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\Ingredient;
use App\Repositories\MealIngredientRepository;
use App\Repositories\LanguageRepository;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use App\Services\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class IngredientController extends Controller
{
    private $ingredientRepository;
    private $languageRepository;

    public function __construct(MealIngredientRepository $ingredientRepository, LanguageRepository $languageRepository)
    {
        $this->ingredientRepository = $ingredientRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * @OA\Get(
     *     path="/admin/ingredient",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Ingredient")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {

        return response()->json(
            new JsonResponse($this->ingredientRepository->paginate())
        );
    }

    /**
     * @OA\Get(
     *     path="/admin/ingredient/{ingredient}",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="ingredient",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Ingredient")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        $ingredient = $this->ingredientRepository->find($request->ingredient);
        $ingredient->translations = $this->ingredientRepository->getTranslations($ingredient->id)->get();

        return response()->json(
            new JsonResponse($ingredient)
        );
    }

    /**
     * @OA\Put(
     *     path="/admin/ingredient/{ingredient}",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="ingredient",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Parameter(
     *         in="path",
     *         name="body",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/IngredientRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Ingredient"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function update(Request $request, Translation $translation)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'description' => 'required',
        ]);
        if($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        if($request->ingredient) {
            $item = $this->ingredientRepository->find($request->ingredient);
            $item->fill($data);
            $item->save();
        } else {
            $item = $this->ingredientRepository->create($data);
        }

        $response = $item->toArray();
        $response['translations'] = $translation->translateObject($item, $request->translations);

        return response()->json(new JsonResponse($response));
    }


    /**
     * @OA\Delete(
     *     path="/admin/ingredient/{ingredient}",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Delete ingredient",
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="ingredient",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Ingredient"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */

    public function destroy(Request $request)
    {
        $deleteIngredientResponse= $this->ingredientRepository->delete($request->ingredient);
        $this->ingredientRepository->deleteTranslations($request->ingredient);

        return response()->json(new JsonResponse(
            $deleteIngredientResponse ? true : false
        ));
    }
}
