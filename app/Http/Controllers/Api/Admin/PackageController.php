<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\Package;
use App\Models\User;
use App\Repositories\UserPackageRepository;
use App\Services\UserPackage as UserPackageService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DateTime;

class PackageController extends Controller
{
    private $userPackageRepository;
    private $userPackageService;

    public function __construct(
        UserPackageRepository $userPackageRepository,
        UserPackageService $userPackageService
    ) {
        $this->userPackageRepository = $userPackageRepository;
        $this->userPackageService = $userPackageService;
    }

    /**
     * @OA\Post(
     *     path="/admin/package/customer",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"user_id", "package_id"},
     *                 @OA\Property(
     *                     property="user_id",
     *                     description="User",
     *                     type="integer",
     *                 ),
     *				   @OA\Property(
     *                     property="package_id",
     *                     description="Package",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="valid_until",
     *                     description="Valid until",
     *                     type="string",
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\JsonContent(ref="#/components/schemas/UserPackage"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function assignToCustomer(Request $request)
    {
        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(
                new JsonResponse(null, false, 1, trans('errors.1'))
            );
        }

        $package = Package::find($request->package_id);
        if (!$package) {
            return response()->json(
                new JsonResponse(null, false, 6, trans('errors.6'))
            );
        }
        $days = $package->recurring_period_in_days + 1;
        $validUntil = $request->valid_until ? new DateTime($request->valid_until) : Carbon::now()->addDays($days);

        return response()->json(
            new JsonResponse(
                $this->userPackageService->storePackage($user, $package, 0, false, false, $validUntil)
            )
        );
    }
}
