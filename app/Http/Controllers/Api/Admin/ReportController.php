<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Services\Payment;
use App\Services\UserPackage;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ReportController extends Controller
{
    private $userService;
    private $userPackageService;
    private $payment;

    public function __construct(UserService $userService, UserPackage $userPackageService, Payment $payment)
    {
        $this->userService = $userService;
        $this->userPackageService = $userPackageService;
        $this->payment = $payment;
    }

    /**
     * @OA\Get(
     *     path="/admin/reports/customers",
     *     @OA\MediaType(mediaType="multipart/form-data"),
     *     tags={"Admin"},
     *       @OA\Parameter(
     *         in="query",
     *         name="levelIds[]",
     *         required=false,
     *         @OA\Schema(
     *          type="array",
     *          @OA\Items(type="string"),
     *          ),
     *         style="form"
     *     ),
     *
     *      @OA\Parameter(
     *         in="query",
     *         name="goalIds[]",
     *         required=false,
     *         @OA\Schema(
     *          type="array",
     *          @OA\Items(type="string"),
     *          ),
     *         style="form"
     *     ),
     *         @OA\Parameter(
     *         in="query",
     *         name="environmentIds[]",
     *         required=false,
     *         @OA\Schema(
     *          type="array",
     *          @OA\Items(type="string"),
     *          ),
     *         style="form"
     *     ),
     *       @OA\Parameter(
     *         in="query",
     *         name="gender",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="age_from",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="age_to",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *       @OA\Response(
     *         response=200,
     *         description=""
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     *
     */
    public function getCustomerReport(Request $request)
    {
        $levels = $request->levelIds;
        $goals = $request->goalIds;
        $environment = $request->environmentIds;
        $gender = $request->gender;
        $ageFrom = $request->age_from;
        $agoTo = $request->age_to;

        /** @var LengthAwarePaginator $list */
        $list = $this->userService->getCustomerReport($levels, $goals, $environment, $gender, $ageFrom,
            $agoTo)->paginate();

        $list->setCollection(
            $list->getCollection()->transform(function ($el) {
                return $this->userService->mapToCustomerReportRow($el);
            })
        );
        return response()->json(new JsonResponse($list));
    }

    /**
     * @OA\Get(
     *     path="/admin/reports/user-packages",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="only_active",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="include_free_trial",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/UserPackageReport")
     *         ),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="User not logged in"
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     *
     */
    public function getUserPackageReport(Request $request)
    {
        /** @var LengthAwarePaginator $list */
        $list = $this->userPackageService->getUserPackageReport(
            $request->search,
            $request->only_active == 1,
             $request->include_free_trial == 1
        )->paginate();

        $list->setCollection(
            $list->getCollection()->transform(function ($el) {
                return $this->userPackageService->mapToUserPackageReportRow($el);
            })
        );
        return response()->json(new JsonResponse($list));
    }

    /**
     * @OA\Get(
     *     path="/admin/reports/payments",
     *     @OA\MediaType(mediaType="multipart/form-data"),
     *     tags={"Admin"},
     *     @OA\Response(
     *         response=200,
     *         description=""
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     *
     */
    public function getPaymentReport()
    {
        /** @var LengthAwarePaginator $list */
        $list = $this->payment->getPaymentReport()->paginate();

        $list->setCollection(
            $list->getCollection()->transform(function ($el) {
                return $this->payment->mapPaymentReportRow($el);
            })
        );
        return response()->json(new JsonResponse($list));
    }

    /**
     * @OA\Get(
     *     path="/admin/reports/payment-history/{transaction_id}",
     *     @OA\MediaType(mediaType="multipart/form-data"),
     *     tags={"Admin"},
     *     @OA\Response(
     *         response=200,
     *         description=""
     *     ),
     *     @OA\Parameter(
     *            name="transaction_id",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="string"),
     *        ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     *
     */
    public function getPaymentHistory(string $transaction_id)
    {
        /** @var LengthAwarePaginator $list */
        $list = $this->payment->getPaymentHistory($transaction_id)->paginate();

        $list->setCollection(
            $list->getCollection()->transform(function ($el) {
                return $this->payment->mapPaymentReportRow($el);
            })
        );
        return response()->json(new JsonResponse($list));
    }
}
