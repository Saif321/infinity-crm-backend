<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\Program;
use App\Models\Language;
use App\Models\WeekItem;
use App\Repositories\LanguageRepository;
use App\Repositories\LevelRepository;
use App\Repositories\ProgramRepository;
use App\Repositories\PurchasedProgramRepository;
use App\Repositories\UserRepository;
use App\Repositories\WeekItemRepository;
use App\Repositories\ProgramWeekRepository;
use App\Repositories\GoalRepository;
use App\Rules\StrongPassword;
use App\Services\BuyProgram;
use App\Services\Otp\OtpService;
use App\Services\Translation;
use App\Services\WordPressService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

class ProgramController extends Controller
{
    private $languageRepository;
    private $programRepository;
    private $weekItemRepository;
    private $programWeekRepository;
    private $goalRepository;
    private $userRepository;
    private $buyProgram;
    private $wordPressService;
    private $sync;

    public function __construct(
        LanguageRepository $languageRepository,
        ProgramRepository $programRepository,
        WeekItemRepository $weekItemRepository,
        ProgramWeekRepository $programWeekRepository,
        GoalRepository $goalRepository,
        UserRepository $userRepository,
        BuyProgram $buyProgram,
        WordPressService $wordPressService
    ) {
        $this->languageRepository = $languageRepository;
        $this->programRepository = $programRepository;
        $this->weekItemRepository = $weekItemRepository;
        $this->programWeekRepository = $programWeekRepository;
        $this->goalRepository = $goalRepository;
        $this->userRepository = $userRepository;
        $this->buyProgram = $buyProgram;
        $this->wordPressService = $wordPressService;
        $this->sync = config('infinity-fit.wordpress.wordpress_sync');

    }

    /**
     * @OA\Get(
     *     path="/admin/program",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Program")
     *         ),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="User not logged in"
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        return response()->json(
            new JsonResponse(
                $this->programRepository
                    ->getPrograms($request->search)
                    ->with('image', 'video', 'levels', 'exerciseEnvironments')
                    ->paginate()
                    ->appends(['search' => $request->search])
            )
        );
    }
    
    public function all_programs(Request $request)
    {
        return response()->json(
            new JsonResponse(
                $this->programRepository
                    ->getPrograms($request->search)
                    ->with('image', 'video', 'levels', 'exerciseEnvironments')
                    ->get()
            )
        );

    }

    /**
     * @OA\Get(
     *     path="/admin/program/{program}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="program",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        $program = $this->programRepository->with('image', 'video')->find($request->program);
        $program->translations = $this->programRepository->getTranslations($program->id)->with('image', 'video')->get();
        foreach ($program->translations as &$translation) {
            $translation->load('image', 'video');
        }

        $program->weeks = $this->programWeekRepository->findWhere(['program_id' => $program->id]);

        $program->load('goals', 'levels', 'exerciseEnvironments');


        foreach ($program->weeks as &$weekItem) {
            $weekItem->items = $this->weekItemRepository->getWeekItems($weekItem->id)->get();
        }

        return response()->json(
            new JsonResponse($program)
        );
    }

    /**
     * @OA\Put(
     *     path="/admin/program/{program}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *            name="program",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="integer"),
     *        ),
     *     @OA\Parameter(
     *         in="path",
     *         name="body",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/ProgramRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Program"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */

    public function update(Request $request, Translation $translation)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'program_length_in_weeks' => 'required',
            'workout_days_per_week' => 'required|integer',
//            'video_id' => 'required',
            'image_id' => 'required|integer',
            'translations.*' => 'required',
            'translations.*.name' => 'required',
            'translations.*.description' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        try {
            DB::beginTransaction();

            if ($request->program) {
                $item = $this->programRepository->find($request->program);

                if (isset($data['is_published'])) {
                    if (!$item->is_published && $data['is_published']) {
                        $data['published_at'] = date('Y-m-d H:i:s');
                    } else {
                        if ($item->is_published && !$data['is_published']) {
                            $data['published_at'] = null;
                        } else {
                            $data['published_at'] = $item->published_at;
                        }
                    }
                }
                $translates = $this->programRepository->getTranslations($item->id)->get();
                $oldNames = [];
                foreach ($translates as $translate) {
                    $oldNames[] = $translate->name;
                }
                $item->fill($data);
                $item->save();
                $translations = $translation->translateObject($item, $request->translations);
                if ($this->sync) {
                    $translationIds = [];
                    for ($i = 0; $i < count($translations); $i++) {
                        $postData = [
                            'title' => $translations[$i]->name,
                            'description' => $translations[$i]->description,
                            'image_id' => $item->image_id,
                            'language_id' => $translations[$i]->language_id
                        ];
                        $translationIds[] = $this->wordPressService->updatePost($postData, 'workout', $oldNames[$i],
                            $item->is_published);
                    }
                    $this->wordPressService->matchPostTranslation($translationIds[0], $translationIds[1]);
                }

            } else {
                if (isset($data['is_published']) && $data['is_published']) {
                    $data['published_at'] = date('Y-m-d H:i:s');
                }

                $item = $this->programRepository->create($data);
                $translations = $translation->translateObject($item, $request->translations);
                if ($this->sync) {
                    $translationsIds = [];
                    foreach ($translations as $translation) {
                        $postData = [
                            'title' => $translation->name,
                            'description' => $translation->description,
                            'image_id' => $item->image_id,
                            'language_id' => $translation->language_id
                        ];
                        $translationsIds[] = $this->wordPressService->updatePost($postData, 'workout', null,
                            $item->is_published);
                    }
                    $this->wordPressService->matchPostTranslation($translationsIds[0], $translationsIds[1]);
                }
            }

            if (array_key_exists('goal_ids', $data)) {
                $item->goals()->sync($data['goal_ids']);
            }

            if (array_key_exists('level_ids', $data)) {
                $item->levels()->sync($data['level_ids']);
            }

            if (array_key_exists('exercise_environment_ids', $data)) {
                $item->exerciseEnvironments()->sync($data['exercise_environment_ids']);
            }

//            $itemTranslations = $translation->translateObject($item, $request->translations);

            // delete old data
            $programWeeks = $this->programWeekRepository->findWhere(['program_id' => $item->id]);
            foreach ($programWeeks as $programWeek) {
                $weeks = $this->weekItemRepository->findWhere(['program_week_id' => $programWeek->id]);
                foreach ($weeks as $week) {
                    $week->delete();
                }

                $programWeek->delete();
            }

            foreach ($request->weeks as $weekData) {
                $weekData['program_id'] = $item->id;
                $programWeek = $this->programWeekRepository->create($weekData);

                foreach ($weekData['items'] as $itemData) {
                    $itemData['program_week_id'] = $programWeek->id;
                    $this->weekItemRepository->create($itemData);
                }
            }

            $response = $item->toArray();
            //$response['translations'] = $itemTranslations;

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            throw $e;
        }

        return response()->json(new JsonResponse($response));
    }

    /**
     * @OA\Put(
     *     path="/admin/program/{program}/publish",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Change publish status for programs",
     *     tags={"Admin"},
     *     @OA\Parameter(
     *            name="program",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="integer"),
     *        ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Program"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function publish(Request $request)
    {
        $item = $this->programRepository->find($request->program);
        $item->is_published = !$item->is_published;
        $item->save();
        $translationIds = [];
        foreach ($this->programRepository->getTranslations($item->id)->get() as $translation) {
            if ($this->sync) {
                $postId = $this->wordPressService->publishPost($translation->name, $item->is_published,
                    $translation->language_id);
                if (!$postId) {
                    $programData = [
                        'title' => $translation->name,
                        'description' => $translation->description,
                        'image_id' => $item->image_id,
                        'language_id' => $translation->language_id
                    ];
                    $translationIds[] = $this->wordPressService->updatePost($programData, 'workout', null, null,
                        $item->is_published);
                    $this->wordPressService->publishPost($translation->title, $item->is_published,
                        $translation->language_id);
                }
                $this->programRepository->saveTranslation($item->id, $translation->language_id, [
                    'is_published' => $item->is_published
                ]);
            }
        }
        if (count($translationIds)) {
            $this->wordPressService->matchPostTranslation($translationIds[0], $translationIds[1]);
        }

        return response()->json(new JsonResponse($item));
    }

    /**
     * @OA\Get(
     *     path="/admin/program/{program}/user",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *            name="program",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="integer"),
     *       ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/User")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function user(Request $request)
    {
        return response()->json(new JsonResponse(
            $this->userRepository->getUsersForProgram($request->program)->with('image')->get()
        ));
    }

    /**
     * @OA\Put(
     *     path="/admin/program/{program}/user/{user}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *            name="program",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="integer"),
     *       ),
     *     @OA\Parameter(
     *            name="user",
     *            in="path",
     *            required=true,
     * 			@OA\Schema(type="integer"),
     *       ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/PurchasedProgram")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function buyForUser(Request $request)
    {
        $program = $this->programRepository->find($request->program);
        $user = $this->userRepository->find($request->user);

        if (!$program || !$user) {
            return response()->json(new JsonResponse(null, false, 6, trans('errors.6')));
        }

        try {
            return response()->json(new JsonResponse(
                $this->buyProgram->buy($program, $user, true)
            ));
        } catch (\Exception $e) {
            Log::debug($e->getMessage(), $e->getTrace());
            return response()->json(new JsonResponse(null, false, 500, $e->getMessage()));
        }
    }
}
