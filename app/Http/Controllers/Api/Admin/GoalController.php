<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\Goal;
use App\Repositories\GoalRepository;
use App\Repositories\LanguageRepository;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use App\Services\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class GoalController extends Controller
{
    private $goalRepository;
    private $languageRepository;

    public function __construct(GoalRepository $goalRepository, LanguageRepository $languageRepository)
    {
        $this->goalRepository = $goalRepository;
        $this->languageRepository = $languageRepository;

    }

    /**
     * @OA\Get(
     *     path="/admin/goal",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Goal")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        return response()->json(
            new JsonResponse($this->goalRepository->getGoals($request->search)->paginate())
        );
    }

    /**
     * @OA\Get(
     *     path="/admin/goal/{goal}",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="goal",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Goal")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        $goal = $this->goalRepository->find($request->goal);
        $goal->translations = $this->goalRepository->getTranslations($goal->id)->get();

        return response()->json(
            new JsonResponse($goal)
        );
    }

    /**
     * @OA\Put(
     *     path="/admin/goal/{goal}",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="goal",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Parameter(
     *         in="path",
     *         name="body",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/GoalRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Goal"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function update(Request $request, Translation $translation)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
        ]);
        if($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        if($request->goal) {
            $item = $this->goalRepository->find($request->goal);
            $item->fill($data);
            $item->save();
        } else {
            $item = $this->goalRepository->create($data);
        }

        $response = $item->toArray();
        $response['translations'] = $translation->translateObject($item, $request->translations);

        return response()->json(new JsonResponse($response));
    }


    /**
     * @OA\Delete(
     *     path="/admin/goal/{goal}",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Delete status for goal",
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="goal",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Goal"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */

    public function destroy(Request $request)
    {
        $deleteGoalResponse= $this->goalRepository->delete($request->goal);
        $this->goalRepository->deleteTranslations($request->goal);

        return response()->json(new JsonResponse(
            $deleteGoalResponse ? true : false
        ));
    }
}
