<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\MealGroup;
use App\Repositories\LanguageRepository;
use App\Repositories\MealGroupRepository;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use App\Services\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MealGroupController extends Controller
{
    private $mealGroupRepository;
    private $languageRepository;

    public function __construct(MealGroupRepository $mealGroupRepository, LanguageRepository $languageRepository)
    {
        $this->mealGroupRepository = $mealGroupRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * @OA\Get(
     *     path="/admin/meal-group",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="meal_group",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/MealGroup")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
	    return response()->json(
		    new JsonResponse(
			    $this->mealGroupRepository
				    ->searchMealGroups($request->search)
				    ->paginate()
				    ->appends(['search' => $request->search])
		    )
	    );
    }

    /**
     * @OA\Get(
     *     path="/admin/meal-group/{meal_group}",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="meal_group",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/MealGroup")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        $mealGroup = $this->mealGroupRepository->find($request->meal_group);
        $mealGroup->translations = $this->mealGroupRepository->getTranslations($mealGroup->id)->get();

        return response()->json(
            new JsonResponse($mealGroup)
        );
    }

    /**
     * @OA\Put(
     *     path="/admin/meal-group/{meal_group}",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="meal_group",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Parameter(
     *         in="path",
     *         name="meal_group",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/MealGroupRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/MealGroup"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function update(Request $request, Translation $translation)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'eating_time' => 'required',
            //'order' => 'required',
        ]);
        if($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        if($request->meal_group) {
            $item = $this->mealGroupRepository->find($request->meal_group);
            $item->fill($data);
            $item->save();
        } else {
            $item = $this->mealGroupRepository->create($data);
        }

        $response = $item->toArray();
        $response['translations'] = $translation->translateObject($item, $request->translations);

        return response()->json(new JsonResponse($response));
    }


    /**
     * @OA\Delete(
     *     path="/admin/meal-group/{meal_group}",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Delete Meal Group",
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="meal_group",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/MealGroup"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */

    public function destroy(Request $request)
    {
        $deleteMealGroupResponse= $this->mealGroupRepository->delete($request->meal_group);
        $this->mealGroupRepository->deleteTranslations($request->meal_group);

        return response()->json(new JsonResponse(
            $deleteMealGroupResponse ? true : false
        ));
    }
}
