<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Program;
use App\Models\Review;
use App\Repositories\UserProgramRepository;
use Illuminate\Http\Request;
use App\Http\Responses\JsonResponse;
use App\Repositories\ReviewRepository;


class ReviewController extends Controller
{
    private $reviewRepository;

    public function __construct(ReviewRepository $reviewRepository)
    {
        $this->reviewRepository = $reviewRepository;
    }

    /**
     * @OA\Get(
     *     path="/admin/review",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Review")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        $list = $this->reviewRepository->getReview($request->search)->with('user')->paginate()->appends(['search' => $request->search]);
        foreach ($list as &$item) {
            $item->root = app()->make($item->object_type)->find($item->object_id);
        }
        return response()->json(new JsonResponse($list));
    }

    /**
     * @OA\Get(
     *     path="/admin/review/{review}",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Show selected review",
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="review",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Review")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        $item = $this->reviewRepository->with('user')->find($request->review);

        $model = app()->make($item->object_type);
        $commentForItem = $model->find($item->object_id);
        $item->root = $commentForItem;

        return response()->json(
            new JsonResponse($item)
        );
    }

    /**
     * @OA\Delete(
     *     path="/admin/review/{review}",

     *     @OA\MediaType(mediaType="application/json"),
     *     description="Delete publish status for comment",
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="review",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/JsonResponse"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function destroy(Request $request)
    {
        $item = $this->reviewRepository->find($request->review);
        $deleteReviewReponse = $this->reviewRepository->delete($request->review);

        if(class_exists($item->object_type)){
            $object = app()->make($item->object_type)->find($item->object_id);
            $repositoryClass = 'App\\Repositories\\' . class_basename($item->object_type) . 'Repository';
            $repository = app()->make($repositoryClass);

            if($object) {
                $publishedReviews = $this->reviewRepository->getPublishedReviews($item->object_id, $item->object_type);
                $object->review_count = $publishedReviews->count();
                $object->avg_rating = $publishedReviews->avg('rating');
                $object->save();

                foreach($repository->getTranslations($object->id)->get() as $translation) {
                    $repository->saveTranslation($object->id, $translation->language_id, ['review_count' => $object->review_count, 'avg_rating' => $object->avg_rating]);
                }
            }
        }

        return response()->json(new JsonResponse(
            $deleteReviewReponse ? true : false
        ));
    }

    /**
     * @OA\Put(
     *     path="/admin/review/{review}/publish",

     *     @OA\MediaType(mediaType="application/json"),
     *     description="Change publish status for review",
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="review",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/Review"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function publish(Request $request)
    {
        $item = $this->reviewRepository->find($request->review);
        $item->is_published = !$item->is_published;
        $item->published_at = $item->is_published ? date("Y-m-d H:i:s") : null;
        $item->save();

        if(class_exists($item->object_type)){
            $object = app()->make($item->object_type)->find($item->object_id);
            $repositoryClass = 'App\\Repositories\\' . class_basename($item->object_type) . 'Repository';
            $repository = app()->make($repositoryClass);

            if($object) {
                $publishedReviews = $this->reviewRepository->getPublishedReviews($item->object_id, $item->object_type);
                $object->review_count = $publishedReviews->count();
                $object->avg_rating = $publishedReviews->avg('rating');
                $object->save();

                foreach($repository->getTranslations($object->id)->get() as $translation) {
                    $repository->saveTranslation($object->id, $translation->language_id, ['review_count' => $object->review_count, 'avg_rating' => $object->avg_rating]);
                }

                if($object instanceof Program) {
                    /** @var UserProgramRepository $userProgramRepository */
                    $userProgramRepository = app()->make(UserProgramRepository::class);
                    $userPrograms = $userProgramRepository->getPurchasedProgramsBySourceProgramId($object->id)->get();

                    foreach($userPrograms as $userProgram) {
                        $userProgram->review_count = $object->review_count;
                        $userProgram->avg_rating = $object->avg_rating;
                        $userProgram->save();

                        foreach($userProgramRepository->getTranslations($userProgram->id)->get() as $userProgramTranslation) {
                            $userProgramRepository->saveTranslation($userProgram->id, $userProgramTranslation->language_id, ['review_count' => $object->review_count, 'avg_rating' => $object->avg_rating]);
                        }
                    }
                }
            }
        }

        return response()->json(new JsonResponse($item));
    }

}
