<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\Blog;
use App\Models\ExerciseEnvironment;
use App\Repositories\BlogRepository;
use App\Repositories\BlogTagRepository;
use App\Repositories\CommentRepository;
use App\Repositories\ExerciseEnvironmentRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\LikeRepository;
use App\Repositories\UserRepository;
//use App\Rules\StrongPassword;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use App\Services\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ExerciseEnvironmentController extends Controller
{
    private $exerciseEnvironmentRepository;
    private $languageRepository;

    public function __construct(ExerciseEnvironmentRepository $exerciseEnvironmentRepository, LanguageRepository $languageRepository)
    {
        $this->exerciseEnvironmentRepository = $exerciseEnvironmentRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * @OA\Get(
     *     path="/admin/exercise-environment",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/ExerciseEnvironment")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        $list = $this->exerciseEnvironmentRepository
	        ->getExerciseEnvironments($request->search)
	        ->paginate()
	        ->appends(['search' => $request->search]);

        return response()->json(
            new JsonResponse($list)
        );
    }

    /**
     * @OA\Get(
     *     path="/admin/exercise-environment/list",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/ExerciseEnvironment")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function fullList(Request $request)
    {
        return response()->json(
            new JsonResponse($this->exerciseEnvironmentRepository->getPosts()->get())
        );
    }

    /**
     * @OA\Get(
     *     path="/admin/exercise-environment/{exercise_environment}",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     *         in="path",
     *         name="exercise_environment",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/ExerciseEnvironment")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        $item = $this->exerciseEnvironmentRepository->find($request->exercise_environment);
        $item->translations = $this->exerciseEnvironmentRepository->getTranslations($item->id)->get();

        return response()->json(
            new JsonResponse($item)
        );
    }

    /**
     * @OA\Put(
     *     path="/admin/exercise-environment/{exercise_environment}",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="exercise_environment",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Parameter(
     *         in="path",
     *         name="body",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/ExerciseEnvironmentRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/ExerciseEnvironment"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function update(Request $request, Translation $translation)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'order' => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        if($request->exercise_environment) {
            $item = $this->exerciseEnvironmentRepository->find($request->exercise_environment);
            $item->fill($data);
            $item->save();
        } else {
            $item = $this->exerciseEnvironmentRepository->create($data);
        }

        $response = $item->toArray();
        $response['translations'] = $translation->translateObject($item, $request->translations);

        return response()->json(new JsonResponse($response));
    }

    /**
     * @OA\Delete(
     *     path="/admin/exercise-environment/{exercise_environment}",
     
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Admin"},
     *     @OA\Parameter(
     * 			name="exercise_environment",
     * 			in="path",
     * 			required=true,
     * 			@OA\Schema(type="integer"),
     * 		),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/JsonResponse"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function destroy(Request $request)
    {
        $item = $this->exerciseEnvironmentRepository->find($request->exercise_environment);
        $this->exerciseEnvironmentRepository->deleteTranslations($item->id);

        return response()->json(new JsonResponse(
            $this->exerciseEnvironmentRepository->delete($request->exercise_environment) ? true : false
        ));
    }
}
