<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * @OA\SecurityScheme(
 *      securityScheme="default",
 *      type="apiKey",
 *      in="header",
 *      name="Authorization"
 * )
 *
 * @OA\Schema(
 *   schema="Token",
 *   allOf={
 *       @OA\Schema(
 *           @OA\Property(property="access_token", type="string"),
 *           @OA\Property(property="token_type", type="string"),
 *           @OA\Property(property="user", type="object", ref="#/components/schemas/User"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="ForgottenPasswordRequest",
 *   allOf={
 *       @OA\Schema(
 *           @OA\Property(property="destination", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="CheckOtpRequest",
 *   allOf={
 *       @OA\Schema(
 *           @OA\Property(property="destination", type="string"),
 *           @OA\Property(property="code", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="ResetPasswordRequest",
 *   allOf={
 *       @OA\Schema(
 *           @OA\Property(property="destination", type="string"),
 *           @OA\Property(property="code", type="string"),
 *           @OA\Property(property="password", type="string"),
 *       )
 *   }
 * )
 *
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
