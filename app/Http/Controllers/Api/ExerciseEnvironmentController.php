<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\Blog;
use App\Models\ExerciseEnvironment;
use App\Repositories\BlogRepository;
use App\Repositories\BlogTagRepository;
use App\Repositories\CommentRepository;
use App\Repositories\ExerciseEnvironmentRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\LikeRepository;
use App\Repositories\UserRepository;
//use App\Rules\StrongPassword;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ExerciseEnvironmentController extends Controller
{
    private $exerciseEnvironmentRepository;
    private $languageRepository;

    public function __construct(ExerciseEnvironmentRepository $exerciseEnvironmentRepository, LanguageRepository $languageRepository)
    {
        $this->exerciseEnvironmentRepository = $exerciseEnvironmentRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * @OA\Get(
     *     path="/exercise-environment",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Exercise environment"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/ExerciseEnvironment")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        return response()->json(
            new JsonResponse(
            	$this->exerciseEnvironmentRepository
		            ->getTranslatedExerciseEnvironments($request->search)
		            ->get()
            )
        );
    }
}
