<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\Package;
use App\Models\PackagePaymentMethod;
use App\Models\User;
use App\Models\UserPackage;
use App\Repositories\UserPackageRepository;
use App\Services\IosHeadersParser;
use App\Services\Payment\FirstData;
use App\Services\Payment\Sms;
use App\Services\UserPackageRenew as UserPackageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class PackageController extends Controller
{
    private $userPackageRepository;
    private $smsPaymentService;
    private $firstDataPaymentService;
    private $userPackageService;
    private $iosHeadersParser;
    private $paymentAfterBuildNumber;

    public function __construct(
        UserPackageRepository $userPackageRepository,
        Sms $smsPaymentService,
        FirstData $firstDataPaymentService,
        UserPackageService $userPackageService,
        IosHeadersParser $iosHeadersParser
    ) {
        $this->userPackageRepository = $userPackageRepository;
        $this->smsPaymentService = $smsPaymentService;
        $this->firstDataPaymentService = $firstDataPaymentService;
        $this->userPackageService = $userPackageService;
        $this->iosHeadersParser = $iosHeadersParser;
        $this->paymentAfterBuildNumber = config('infinity-fit.apple_pay.payment_after_build_number');
    }

    /**
     * @OA\Get(
     *     path="/package",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Package"},
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Package")
     *         ),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="User not logged in"
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        $packages = Package::with('paymentMethods')->orderBy('order', 'asc')->get();

        /** @var User $user */
        $user = Auth::user();

        $userPackage = $this->userPackageRepository->getCurrentActiveUserPackage(Auth::user()->id);
        $properties = Lang::get('package-properties');

        $packages->each(function (Package $package) use ($user, $userPackage, $properties) {
            $package->credit_card_payment_url = $this->firstDataPaymentService->generateRedirectionUrl($user, $package);
            $package->sms_payment_body = $this->smsPaymentService->generateSmsPaymentMessageBody($user, $package);
            $package->sms_payment_number = $this->smsPaymentService->getShortNumber();
            $package->is_active = false;
            $package->can_activate = true;
            $package->can_deactivate = false;
            $package->free_trial = false;
            $package->valid_until = null;
            $package->name = $properties[$package->id]['plan_name'];
            if ($userPackage) {
                $isActive = $package->id == $userPackage->package->id;
                $package->free_trial = $isActive ? $userPackage->free_trial : false;
                $package->valid_until = $isActive ? $userPackage->valid_until->format('c') : null;
                $package->can_deactivate = !$userPackage->renew_stopped && $isActive;
                $package->is_active = $isActive;
                $package->can_activate = $userPackage->renew_stopped && $package->recurring_period_in_days > $userPackage->package->recurring_period_in_days;
            }
            $package->properties = $properties[$package->id];
        });

        $packages = $packages->toArray();
        foreach ($packages as &$package) {
            if ($this->iosHeadersParser->isIosUserAgent() && $this->iosHeadersParser->getBuildNo() > $this->paymentAfterBuildNumber ) {
                $package['payment_methods'] = array_values(array_filter($package['payment_methods'], function (
                    $paymentMethod
                ) {
                    return $paymentMethod['payment_method'] === PackagePaymentMethod::PAYMENT_METHOD_APPLE_PAY;
                }));
            }
        }

        return response()->json(
            new JsonResponse($packages)
        );
    }

    /**
     * @OA\Get(
     *     path="/package/active",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Package"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/UserPackage"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="User not logged in"
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function showActivePackage()
    {
        $userPackage = $this->userPackageRepository->getCurrentActiveUserPackage(Auth::user()->id);
        if (!$userPackage) {
            return response()->json(new JsonResponse($userPackage));
        }

        $numberOfPurchasedPrograms = UserPackage::find($userPackage->id)->purchasedPrograms->count();
        $numberOfPurchasedNutritionPrograms = UserPackage::find($userPackage->id)->purchasedNutritionPrograms->count();

        $userPackage->load('package');
        $properties = Lang::get('package-properties');

        $userPackage->package->name = $properties[$userPackage->package->id]['plan_name'];
        if ($userPackage->free_trial) {
            $userPackage->programs_left = (int) $userPackage->package->number_of_free_trial_programs - $numberOfPurchasedPrograms;
            $userPackage->nutrition_programs_left = (int) $userPackage->package->number_of_free_trial_nutrition_programs - $numberOfPurchasedNutritionPrograms;
        } else {
            $userPackage->programs_left = (int) $userPackage->package->number_of_programs - $numberOfPurchasedPrograms;
            $userPackage->nutrition_programs_left = (int) $userPackage->package->number_of_nutrition_programs - $numberOfPurchasedNutritionPrograms;
        }

        return response()->json(
            new JsonResponse($userPackage)
        );
    }

    /**
     * @OA\Put(
     *     path="/package/{package_id}/unsubscribe",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Package"},
     *     @OA\Parameter(
     *         in="path",
     *         name="package_id",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function unsubscribe(Request $request)
    {
        try {
            $activePackage = $this->userPackageRepository->getCurrentActiveUserPackage(Auth::user()->id);

            if (!$activePackage) {
                throw new \Exception('Currently no active packages for user');
            }

            if ($activePackage->id != $request->package_id) {
                throw new \Exception('Only active packages can be deactivated');
            }

            if ($activePackage->renew_stopped) {
                throw new \Exception('Already unsubscribed');
            }

            $result = $this->userPackageService->unsubscribe($activePackage->id);
            return response()->json(
                new JsonResponse($result, true)
            );
        } catch (\Exception $e) {
            Log::warning($e->getMessage(), ['package_id' => $request->package_id, 'user_id' => Auth::id()]);
            Log::debug($e->getTraceAsString());
            return response()->json(
                new JsonResponse(null, false, 500, $e->getMessage())
            );
        }
    }
}
