<?php

namespace App\Http\Controllers\Api;

use App\Models\Program;
use App\Models\HomeFeed;
use App\Repositories\UserProgramRepository;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\Like;
use App\Repositories\LikeRepository;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use Illuminate\Support\Facades\Validator;

class LikeController extends Controller
{

    private $likeRepository;

    public function __construct(LikeRepository $likeRepository)
    {
        $this->likeRepository = $likeRepository;
    }

    /**
     * @OA\Post(
     *     path="/like",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Like/dislike ",
     *     tags={"Like"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(ref="#/components/schemas/LikeRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Like")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function store(Request $request)
    {
        $objectType = 'App\Models\\' . $request->object_type;
        $repositoryType = 'App\Repositories\\' . $request->object_type . 'Repository';
        if(!class_exists($objectType)) return response()->json(
            new JsonResponse(null, false, 6, trans('errors.6'))
        );
        $objectId = $request->object_id;

        if($like = $this->likeRepository->getUserLikes($objectId, $objectType, Auth::user()->id)->count()){
            $this->likeRepository->deleteWhere(['object_id' => $objectId, 'object_type' => $objectType, 'user_id' => Auth::user()->id]);
            $like = null;
        } else{
            $like = $this->likeRepository->create([
                'user_id' => Auth::user()->id,
                'object_id' => $objectId,
                'object_type' => $objectType,
            ]);
        }

        if($object = $objectType::where('id', $objectId)->first()){
            $repository = app()->make($repositoryType);
            $likeCount = $this->likeRepository->getLikes($objectId, $objectType)->count();
            $object->like_count = $likeCount;
            $object->save();

            if(!($object instanceof HomeFeed)) {
                foreach($repository->getTranslations($object->id)->get() as $translation) {
                    $repository->saveTranslation($object->id, $translation->language_id, ['like_count' => $likeCount]);
                }
            }
        }

        /*if($object instanceof Program) {
            /** @var UserProgramRepository $userProgramRepository */
            /*$userProgramRepository = app()->make(UserProgramRepository::class);
            $userPrograms = $userProgramRepository->getPurchasedProgramsBySourceProgramId($object->id)->get();

            foreach($userPrograms as $userProgram) {
                $userProgram->like_count = $object->like_count;
                $userProgram->save();

                foreach($userProgramRepository->getTranslations($userProgram->id)->get() as $userProgramTranslation) {
                    $userProgramRepository->saveTranslation($userProgram->id, $userProgramTranslation->language_id, ['like_count' => $likeCount]);
                }
            }
        }*/

        return response()->json(
            new JsonResponse($like)
        );
    }
}
