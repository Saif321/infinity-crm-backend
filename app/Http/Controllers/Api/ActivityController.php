<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Repositories\GoalRepository;
use App\Repositories\NutritionQuestionnaireRepository;
use App\Repositories\UserProgramRepository;
use App\Repositories\UserRepository;
use App\Repositories\BasicQuestionnaireRepository;
use App\Services\Program as ProgramService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ActivityController extends Controller
{
    private $userRepository;
    private $basicQuestionnaireRepository;
    private $nutritionQuestionnaireRepository;
    private $goalRepository;
    private $userProgramRepository;
    private $programService;

    public function __construct(
        UserRepository $userRepository,
        UserProgramRepository $userProgramRepository,
        BasicQuestionnaireRepository $basicQuestionnaireRepository,
        NutritionQuestionnaireRepository $nutritionQuestionnaireRepository,
        GoalRepository $goalRepository,
        ProgramService $programService
    ) {
        $this->userRepository = $userRepository;
        $this->userProgramRepository = $userProgramRepository;
        $this->basicQuestionnaireRepository = $basicQuestionnaireRepository;
        $this->nutritionQuestionnaireRepository = $nutritionQuestionnaireRepository;
        $this->goalRepository = $goalRepository;
        $this->programService = $programService;
    }

    /**
     * @OA\Get(
     *     path="/activity/dates/{year}/{month}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Activity"},
     *     @OA\Parameter(
     *         in="path",
     *         name="year",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         in="path",
     *         name="month",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success"
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function dates(Request $request)
    {
        $response = $this->programService->getUserActivityDatesForMonth($request->year, $request->month);

        return response()->json(
            new JsonResponse($response)
        );
    }

    /**
     * @OA\Get(
     *     path="/activity/{date}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Activity"},
     *     @OA\Parameter(
     *         in="path",
     *         name="date",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success"
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */

    public function date(Request $request)
    {
        $activity = $this->programService->getUserActivityOnDate(Auth::id(), $request->date);

        return response()->json(
            new JsonResponse($activity)
        );
    }
}
