<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Services\Home;
use Illuminate\Http\Request;

class HomeController extends Controller {
	private $homeService;

	public function __construct( Home $homeService ) {
		$this->homeService = $homeService;
	}

	/**
	 * @OA\Schema(
	 *   schema="Home",
	 *   allOf={
	 *       @OA\Schema(ref="#/components/schemas/Home"),
	 *       @OA\Schema(
	 *          @OA\Property(
	 *             property="programs",
	 *             type="array",
	 *             @OA\Items(ref="#/components/schemas/Program")
	 *          ),
	 *          @OA\Property(
	 *             property="nutrition",
	 *             type="array",
	 *             @OA\Items(ref="#/components/schemas/NutritionProgram")
	 *         ),
	 *       )
	 *   }
	 * )
	 *
	 * @OA\Get(
	 *     path="/home",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Home"},
	 *
	 *     @OA\Response(
	 *         response=200,
	 *         description="",
	 *         @OA\Schema(
	 *             type="array",
	 *             @OA\Items(ref="#/components/schemas/Home")
	 *         ),
	 *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
	public function index( Request $request ) {
		return response()->json(
			new JsonResponse( $this->homeService->getHomeResponse() )
		);
	}
}
