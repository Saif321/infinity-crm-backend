<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Repositories\BlogRepository;
use App\Repositories\CommentRepository;
use App\Repositories\LikeRepository;
use App\Services\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Throwable;

class BlogController extends Controller
{
    private $blogRepository;
    private $likeRepository;
    private $commentRepository;
    private $blogService;

    public function __construct(
        BlogRepository $blogRepository,
        LikeRepository $likeRepository,
        CommentRepository $commentRepository,
        Blog $blogService
    ) {
        $this->blogRepository = $blogRepository;
        $this->likeRepository = $likeRepository;
        $this->commentRepository = $commentRepository;
        $this->blogService = $blogService;
    }

    /**
     * @OA\Get(
     *     path="/blog",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Blog"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Blog")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        $list = $this->blogRepository
            ->getPublishedPosts($request->search)
            ->with('user')
            ->paginate($request->get('limit', 15))
            ->appends(['search' => $request->search, 'limit' => $request->limit]);

        foreach ($list as &$item) {
            $item->is_liked = $this->likeRepository->isUserLiked($item, Auth::user());
        }

        return response()->json(new JsonResponse($list));
    }

    /**
     *
     * @OA\Get(
     *     path="/blog/{blog}",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Blog"},
     *     @OA\Parameter(
     *         in="path",
     *         name="blog",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Blog")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $blog = $this->blogRepository->findTranslated($request->blog);

        if (!$blog) {
            return response()->json(new JsonResponse(null, false, 6, trans('errors.6')));
        }

        $blog->is_liked = $this->likeRepository->isUserLiked($blog, Auth::user());
        $blog->load('user', 'image');

        try {
            $blog->description = view('blog.description',
                ['description' => $this->blogService->parseContentAndReplaceOembed($blog->description)])
                ->render();
        } catch (Throwable $e) {
            Log::warning($e->getMessage());
        }

        return response()->json(
            new JsonResponse($blog)
        );
    }
}
