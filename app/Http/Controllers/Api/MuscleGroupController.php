<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\MuscleGroup;
use App\Models\Language;
use App\Repositories\MuscleGroupRepository;
use App\Repositories\LanguageRepository;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;



class MuscleGroupController extends Controller
{
    private $muscleGroupRepository;
    private $languageRepository;

    public function __construct(MuscleGroupRepository $muscleGroupRepository, LanguageRepository $languageRepository)
    {
        $this->muscleGroupRepository = $muscleGroupRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * @OA\Get(
     *     path="/muscle-group",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Muscle group"},
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/MuscleGroup")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {

        return response()->json(
            new JsonResponse($this->muscleGroupRepository
                ->getTranslatedMuscleGroups($request->search)
                ->get()
            )
        );
    }
}
