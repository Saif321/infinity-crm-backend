<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Mail\UserConfirmationMail;
use App\Repositories\GalleryRepository;
use App\Repositories\GoalRepository;
use App\Repositories\LevelRepository;
use App\Repositories\NutritionQuestionnaireRepository;
use App\Repositories\UserProgramRepository;
use App\Repositories\UserWeightHistoryRepository;
use App\Repositories\UserRepository;
use App\Repositories\BasicQuestionnaireRepository;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use App\Services\UserService;
use App\Services\UserPackage;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use App\Models\User;
use App\Models\UserBlock;

class UserController extends Controller
{
    private $userRepository;
    private $basicQuestionnaireRepository;
    private $nutritionQuestionnaireRepository;
    private $goalRepository;
    private $userProgramRepository;
    private $userWeightHistoryRepository;
    private $userService;
    private $userPackage;

    public function __construct(
        UserRepository $userRepository,
        UserProgramRepository $userProgramRepository,
        BasicQuestionnaireRepository $basicQuestionnaireRepository,
        NutritionQuestionnaireRepository $nutritionQuestionnaireRepository,
        GoalRepository $goalRepository,
        UserWeightHistoryRepository $userWeightHistoryRepository,
        UserService $userService,
        UserPackage $userPackage
    ) {
        $this->userRepository = $userRepository;
        $this->userProgramRepository = $userProgramRepository;
        $this->basicQuestionnaireRepository = $basicQuestionnaireRepository;
        $this->nutritionQuestionnaireRepository = $nutritionQuestionnaireRepository;
        $this->goalRepository = $goalRepository;
        $this->userWeightHistoryRepository = $userWeightHistoryRepository;
        $this->userService = $userService;
        $this->userPackage = $userPackage;
    }

    /**
     * @OA\Post(
     *     path="/user",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Change user profile",
     *     tags={"User"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(ref="#/components/schemas/UserRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(ref="#/components/schemas/User"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function store(Request $request)
    {
        $rules = [];
        $rules['birthday'] = 'date';
        $rules['gender'] = 'in:male,female';

        $data = $request->all();

        if (array_key_exists('email', $data) && $request->email != Auth::user()->email) {
            $rules['email'] = 'required|unique:users';
        }

        if (array_key_exists('password', $data) && $request->password != '') {
            $rules['password'] = [
                new StrongPassword()
            ];
        }

        $input = $request->all();
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $error = $validator->messages();
            return response()->json(new JsonResponse(null, false, 400, $error));
        }

        if (array_key_exists('date_of_birth', $data)) {
            $request->date_of_birth = date('Y-m-d', strtotime($request->date_of_birth));
        }

        $user = Auth::user();
        $user->fill($request->all());
        $user->save();

        $basic = $this->basicQuestionnaireRepository->findByField('user_id', $user->id)->first();
        if (!$basic) {
            $basic = $this->basicQuestionnaireRepository->makeModel();
            $basic->user_id = $user->id;
            $basic->save();
        }

        $nutrition = $this->nutritionQuestionnaireRepository->findByField('user_id', $user->id)->first();
        if (!$nutrition) {
            $nutrition = $this->nutritionQuestionnaireRepository->makeModel();
            $nutrition->user_id = $user->id;
            $nutrition->save();
        }

        $user->questionnaire = $basic;
        $user->nutrition_questionnaire = $nutrition;
        $user->statistics = $this->userService->getUserStatistics($user->id);
        $user->free_trial_eligible = $this->userPackage->isUserEligibleForFreeTrial(Auth::user());

        return response()->json(new JsonResponse($user));
    }

    /**
     * @OA\Post(
     *     path="/user/register",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Create user account",
     *     tags={"User"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(ref="#/components/schemas/UserRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/User"),
     *     ),
     * )
     */
    public function register(Request $request, OtpService $otpService)
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => ['required', new StrongPassword()],
        ];

        $input = $request->all();
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $error = $validator->messages();
            return response()->json(new JsonResponse(null, false, 400, $error));
        }

        $input['is_active'] = false;

        $user = $this->userRepository->create($input);
        $user->assignRole(Role::findByName('customer'));

        $otp = $otpService->setDestination($user->email)->generate();
        $subject =  trans('app.WEB_CONFIRMATION_EMAIL_SUBJECT');
        Mail::to($user->email)->send(new UserConfirmationMail([
            'code' => $otp->getVerificationCode(),
            'subject' => $subject
        ]));


        return response()->json(new JsonResponse($user));
    }

    /**
     * @OA\Post(
     *     path="/user/change-password",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Change password for account",
     *     tags={"User"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(ref="#/components/schemas/UserChangePasswordRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/User"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'password' => ['required', 'confirmed', new StrongPassword()],
            'password_confirmation' => ['required'],
        ]);
        if ($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 400, $validator->messages()));
        }

        $user = Auth::user();
        $hasher = app()->make(BcryptHasher::class);
        if (!$hasher->check($request->old_password, $user->getAuthPassword())) {
            return response()->json(new JsonResponse(null, false, 401, 'Old password is not valid.'), 401);
        }

        $user->password = $request->password;
        $user->save();

        return response()->json(new JsonResponse($user));
    }

    /**
     * @OA\Get(
     *     path="/user",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"User"},
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(ref="#/components/schemas/User"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index()
    {
        $user = Auth::user();
        $user->questionnaire = $this->basicQuestionnaireRepository->findByField('user_id', $user->id)->first();
        $user->nutrition_questionnaire = $this->nutritionQuestionnaireRepository->findByField('user_id',
            $user->id)->first();

        if (!$user->questionnaire) {
            $user->questionnaire = $this->basicQuestionnaireRepository->makeModel();
            $user->questionnaire->user_id = $user->id;
            $user->questionnaire->save();

            $user->questionnaire = $this->basicQuestionnaireRepository->findByField('user_id', $user->id)->first();
        }

        if (!$user->nutrition_questionnaire) {
            $user->nutrition_questionnaire = $this->nutritionQuestionnaireRepository->makeModel();
            $user->nutrition_questionnaire->user_id = $user->id;
            $user->nutrition_questionnaire->save();

            $user->nutrition_questionnaire = $this->nutritionQuestionnaireRepository->findByField('user_id',
                $user->id)->first();
        }

        $user->statistics = $this->userService->getUserStatistics($user->id);

        $user->free_trial_eligible = $this->userPackage->isUserEligibleForFreeTrial(Auth::user());

        return response()->json(
            new JsonResponse($user)
        );
    }

    /**
	 * @OA\Post(
	 *     path="/blockuser/{user_id}",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"User"},
	 *     @OA\Parameter(
	 *         in="path",
	 *         name="user_id",
	 *         required=true,
	 *         @OA\Schema(type="integer")
	 *     ),
	 *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
    public function blockuser(Request $request){
        $user = User::find($request->user_id);
        if($user != null){
            if($user->id == Auth::user()->id){
                new JsonResponse(null, false, 7, trans('errors.7'));
            }

            $user_block = new UserBlock;
            $user_block->users_id = Auth::user()->id;
            $user_block->blocked_users_id = $request->user_id;
            $user_block->save();

            /*$user->blocked = 1;
            $user->tokens->each(function($token, $key) {
                $token->delete();
            });
            $this->revokeTokens($user->tokens);
            $user->save();*/

        }

        return response()->json(new JsonResponse($user));
    }

    /**
	 * @OA\Post(
	 *     path="/unblockuser/{user_id}",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"User"},
	 *     @OA\Parameter(
	 *         in="path",
	 *         name="user_id",
	 *         required=true,
	 *         @OA\Schema(type="integer")
	 *     ),
	 *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
    public function unblockuser(Request $request){
        $user = UserBlock::where('blocked_users_id', $request->user_id)->where('users_id', Auth::user()->id)->delete();

        return response()->json(new JsonResponse($user));
    }

    public function blockedusers(Request $request){
        $users = UserBlock::where('users_id', Auth::user()->id)->with('user')->get();

        return response()->json(new JsonResponse($users));
    }

    /**
     * @OA\Post(
     *     path="/user/photo",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"User"},
     *     @OA\Parameter(
     *         in="query",
     *         name="image",
     *         required=true,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/User"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function uploadPhoto(Request $request, GalleryRepository $galleryRepository)
    {
        $rules = [
            'image' => 'required|image'
        ];

        $input = $request->all();
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $error = $validator->messages()->toJson();
            return response()->json(['success' => false, 'error' => $error]);
        }

        if ($request->file('image')) {
            $user = Auth::user();

            $extension = $request->image->extension();
            $fileName = md5(microtime());
            $directory = $user->id.'/'.$fileName[0].'/'.$fileName[1].'/'.$fileName[2];

            $gallery = $galleryRepository->makeModel();
            $gallery->name = $request->file('image')->getClientOriginalName();
            $gallery->mime_type = $request->file('image')->getMimeType();
            $gallery->extension = $extension;
            $gallery->size = $request->file('image')->getSize();
            $gallery->url = $request->file('image')->storeAs('public/avatars/'.$directory, $fileName.'.'.$extension);
            $gallery->type = preg_match('/^image/is', $request->file('image')->getMimeType()) ? 'image' : 'video';
            $gallery->is_user_avatar = true;
            $gallery->save();

            $user->image_id = $gallery->id;
            $user->save();

            $user->load('image');

            return response()->json(new JsonResponse($user));
        }

        return response()->json(
            new JsonResponse(null, false, 7, trans('errors.7'))
        );
    }

    /**
     * @OA\Post(
     *     path="/user/photo_base64",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="",
     *     tags={"User"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(
        *           @OA\Property(property="image", type="string"),
        *           @OA\Property(property="extension", type="string"),
        *           @OA\Property(property="client_original_name", type="string"),
        *           @OA\Property(property="mime_type", type="string"),
        *        )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function uploadPhotoBase64(Request $request, GalleryRepository $galleryRepository)
    {
        $rules = [
            'image' => 'required'
        ];

        $input = $request->all();
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $error = $validator->messages()->toJson();
            return response()->json(['success' => false, 'error' => $error]);
        }

        if ($request->input('image')) {
            $user = Auth::user();

            $extension = $request->input('extension');
            $fileName = md5(microtime());
            $directory = $user->id.'/'.$fileName[0].'/'.$fileName[1].'/'.$fileName[2];

            $path = 'public/';
            $items = explode("/", $directory);
            foreach($items as $item){
                $path .= $item  . '/';
            }
            $path .= $fileName.'.'.$extension;
            Storage::put($path, base64_decode($request->input('image'), true));

            $gallery = $galleryRepository->makeModel();
            $gallery->name = $request->input('client_original_name');
            $gallery->mime_type = $request->input('mime_type');
            $gallery->extension = $extension;
            $gallery->size = Storage::size($path);
            $gallery->url = $path;
            $gallery->type = preg_match('/^image/is', $request->input('mime_type')) ? 'image' : 'video';
            $gallery->save();

            $user->image_id = $gallery->id;
            $user->save();

            $user->load('image');

            return response()->json(new JsonResponse($user));
        }

        return response()->json(
            new JsonResponse(null, false, 7, trans('errors.7'))
        );
    }

    /**
     * @OA\Post(
     *     path="/upload_image_base64",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="",
     *     tags={"Home Feed"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(
        *           @OA\Property(property="image", type="string"),
        *           @OA\Property(property="extension", type="string"),
        *           @OA\Property(property="client_original_name", type="string"),
        *           @OA\Property(property="mime_type", type="string"),
        *        )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function uploadImageBase64(Request $request, GalleryRepository $galleryRepository)
    {
        $rules = [
            'image' => 'required'
        ];

        $input = $request->all();
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $error = $validator->messages()->toJson();
            return response()->json(['success' => false, 'error' => $error]);
        }

        if ($request->input('image')) {
            $user = Auth::user();

            $extension = $request->input('extension');
            $fileName = md5(microtime());
            $directory = $user->id.'/'.$fileName[0].'/'.$fileName[1].'/'.$fileName[2];

            $path = 'public/';
            $items = explode("/", $directory);
            foreach($items as $item){
                $path .= $item  . '/';
            }
            $path .= $fileName.'.'.$extension;
            Storage::put($path, base64_decode($request->input('image'), true));

            $gallery = $galleryRepository->makeModel();
            $gallery->name = $request->input('client_original_name');
            $gallery->mime_type = $request->input('mime_type');
            $gallery->extension = $extension;
            $gallery->size = Storage::size($path);
            $gallery->url = $path;
            $gallery->type = preg_match('/^image/is', $request->input('mime_type')) ? 'image' : 'video';
            $gallery->save();

            return response()->json(new JsonResponse($gallery));
        }

        return response()->json(
            new JsonResponse(null, false, 7, trans('errors.7'))
        );
    }

    /**
     * @OA\Post(
     *     path="/user/questionnaire",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"User"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(ref="#/components/schemas/BasicQuestionnaireRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/BasicQuestionnaireRequest"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function questionnaire(Request $request, LevelRepository $levelRepository)
    {
        $input = $request->all();
        $rules = [];

        if (array_key_exists('level_id', $input)) {
            $rules['level_id'] = 'required|integer';
        }

        if (array_key_exists('goals', $input)) {
            if (!is_array($input['goals'])) {
                $input['goals'] = explode(",", $input['goals']);
            }
            $rules['goals'] = 'required|array|min:1';
            $rules['goals.*'] = 'required|integer|distinct|min:1';
        }

        if (array_key_exists('exercise_environment_id', $input)) {
            $rules['exercise_environment_id'] = 'required|integer';
        }

        if (array_key_exists('gender', $input)) {
            $rules['gender'] = 'required|in:male,female';
        }

        if (array_key_exists('date_of_birth', $input)) {
            $rules['date_of_birth'] = 'required|date';
        }

        if (array_key_exists('height_in_centimeters', $input)) {
            $rules['height_in_centimeters'] = 'required|integer';
        }

        if (array_key_exists('weight_in_kilograms', $input)) {
            $rules['weight_in_kilograms'] = 'required|integer';
        }

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $error = $validator->messages()->toJson();
            return response()->json(['success' => false, 'error' => $error]);
        }

        $user = Auth::user();
        $questionnaire = $this->basicQuestionnaireRepository->getForUser($user->id)->first();
        if (!$questionnaire) {
            $questionnaire = $this->basicQuestionnaireRepository->makeModel();
            $input['user_id'] = $user->id;
        }

        $questionnaire->fill($input);
        $questionnaire->save();

        if (array_key_exists('weight_in_kilograms', $input)) {
            $this->userWeightHistoryRepository->create([
                'user_id' => $user->id,
                'weight_in_kilograms' => $input['weight_in_kilograms'],
            ]);
        }

        if (array_key_exists('goals', $input)) {
            $questionnaire->goals()->sync($input['goals']);
        }

        if (array_key_exists('level_id', $input)) {
            $user->level_id = $input['level_id'];
        }

        if (array_key_exists('date_of_birth', $input)) {
            $input['date_of_birth'] = date('Y-m-d', strtotime($input['date_of_birth']));
            $user->date_of_birth = $input['date_of_birth'];
        }

        if (array_key_exists('gender', $input)) {
            $user->gender = $input['gender'];
        }

        if (count($validator->validated()) == 7) {
            $user->is_survey_completed = 1;
        }
        $user->save();

        $questionnaire->load('goals');
        $questionnaire->load('environment');

        return response()->json(new JsonResponse($questionnaire));
    }

    /**
     * @OA\Post(
     *     path="/user/nutrition",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"User"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(ref="#/components/schemas/NutritionQuestionnaire")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/NutritionQuestionnaire"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function nutrition(Request $request)
    {
        $rules = [
            'how_active' => 'required|string',
            'typical_day' => 'required|string',
            'mine_description' => 'required|string',
            'meat_ids' => 'required',
            'fruit_ids' => 'required',
            'vegetable_ids' => 'required',
        ];

        $input = $request->all();
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $error = $validator->messages()->toJson();
            return response()->json(['success' => false, 'error' => $error]);
        }

        $user = Auth::user();
        $questionnaire = $this->nutritionQuestionnaireRepository->getForUser($user->id)->first();
        if (!$questionnaire) {
            $questionnaire = $this->nutritionQuestionnaireRepository->makeModel();

            $input['user_id'] = $user->id;
        }

        $questionnaire->fill($input);
        $questionnaire->save();

        if (array_key_exists('meat_ids', $input)) {
            $questionnaire->meats()->sync($input['meat_ids']);
        }

        if (array_key_exists('fruit_ids', $input)) {
            $questionnaire->fruits()->sync($input['fruit_ids']);
        }

        if (array_key_exists('vegetable_ids', $input)) {
            $questionnaire->vegetables()->sync($input['vegetable_ids']);
        }

        return response()->json(new JsonResponse($questionnaire));
    }

    /**
     * @OA\Post(
     *     path="/user/update-weight",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Update user's weight in kilograms",
     *     tags={"User"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(ref="#/components/schemas/UpdateWeightRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/User"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function updateWeight(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'weight' => 'required|integer|min:1',
        ]);
        if ($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 400, $validator->messages()));
        }

        return response()->json(
            new JsonResponse(
                $this->userWeightHistoryRepository->create([
                    'user_id' => Auth::user()->id,
                    'weight_in_kilograms' => $request->weight,
                ])
            )
        );
    }
}
