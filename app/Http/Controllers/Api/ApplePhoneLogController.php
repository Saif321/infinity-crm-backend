<?php

namespace App\Http\Controllers\Api;

use App\Http\Responses\JsonResponse;
use App\Services\Payment\ApplePay;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ApplePhoneLogController extends Controller
{
    /**
     * @OA\POST(
     *     path="/apple/log",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Log"},
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="message",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="level",
     *                     type="string",
     *                     description="Possible levels (emergency, alert, critical, error, warning, notice, info, debug, log)",
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="string",
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function log(Request $request)
    {
        try {
            $message = $request->message;
            $level = $request->level;
            Log::channel('apple-phone')->log($level, sprintf('%s - %s', $request->ip(), $message));
            return response()->json(new JsonResponse(true));
        } catch (\Throwable $throwable) {
            Log::error($throwable->getMessage(), $throwable->getTrace());
            return response()->json(new JsonResponse($throwable->getMessage(), false, 500));
        }

    }
}
