<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Services\Home;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;

class SourcesController extends Controller {
	public function __construct( Home $homeService ) {
	}

	/**
	 * @OA\Get(
	 *     path="/sources",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Sources"},
	 *
	 *     @OA\Response(
	 *         response=200,
	 *         description="",
	 *         @OA\Schema(
	 *             type="array",
	 *             @OA\Items(ref="#/components/schemas/JsonResponse")
	 *         ),
	 *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
	public function index( Request $request ) {
	    $language = Config::get('language');
	    $viewFile = 'sources.' . $language['iso'];

		return response()->json(
			new JsonResponse( view( $viewFile )->render() )
		);
	}
}
