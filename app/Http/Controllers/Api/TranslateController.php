<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use Illuminate\Support\Facades\Lang;

/**
 * @OA\Get(
 *     path="/translate",
 *     @OA\MediaType(mediaType="application/json"),
 *     tags={"Translate"},
 *     @OA\Response(
 *         response=200,
 *         description="",
 *         @OA\Schema(
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/JsonResponse")
 *         ),
 *     ),
 * )
 */
class TranslateController extends Controller
{
    public function __construct()
    {
    }


    public function translate()
    {
        return response()->json(
            new JsonResponse(Lang::get('app'))
        );
    }


}
