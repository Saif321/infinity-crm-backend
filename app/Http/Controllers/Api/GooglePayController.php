<?php

namespace App\Http\Controllers\Api;

use App\Services\Payment\GooglePay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GooglePayController extends Controller
{
    protected $googlePayService;

    public function __construct(GooglePay $googlePayService)
    {
        $this->googlePayService = $googlePayService;
    }

    /**
     * @OA\POST(
     *     path="/google/verify-payment",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Payment"},
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="purchase_token",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="product_id",
     *                     type="string",
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="string",
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function token(Request $request)
    {
        $purchaseToken = $request->purchase_token;
        $productId = $request->product_id;
        $user = Auth::user();
        return  $this->googlePayService->acknowledge($purchaseToken,$productId,$user);
    }
}
