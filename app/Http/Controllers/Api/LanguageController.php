<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Repositories\LanguageRepository;
use App\Services\Home;
use Illuminate\Http\Request;

class LanguageController extends Controller {
    private $languageRepository;

	public function __construct( LanguageRepository $languageRepository ) {
		$this->languageRepository = $languageRepository;
	}

	/**
	 * @OA\Get(
	 *     path="/language",
	 *     @OA\MediaType(mediaType="application/json"),
	 *     tags={"Language"},
	 *
	 *     @OA\Response(
	 *         response=200,
	 *         description="",
     *         @OA\JsonContent(ref="#/components/schemas/Language"),
	 *         @OA\Schema(
	 *             type="array",
	 *             @OA\Items(ref="#/components/schemas/Language")
	 *         ),
	 *     ),
	 *     security={{
	 *         "default":{}
	 *     }}
	 * )
	 */
	public function index( Request $request ) {
	    return response()->json(
			new JsonResponse( $this->languageRepository->getLanguages() )
		);
	}
}
