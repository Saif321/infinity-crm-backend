<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\MuscleGroup;
use App\Models\Language;
use App\Repositories\MealGroupRepository;
use App\Repositories\MuscleGroupRepository;
use App\Repositories\LanguageRepository;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;



class MealGroupController extends Controller
{
    private $mealGroupRepository;
    private $languageRepository;

    public function __construct(MealGroupRepository $mealGroupRepository, LanguageRepository $languageRepository)
    {
        $this->mealGroupRepository = $mealGroupRepository;
        $this->languageRepository = $languageRepository;
    }

    //todo ovo da se proveri zbog izmena za order
    /**
     * @OA\Get(
     *     path="/meal-group",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Meal group"},
     *     @OA\Parameter(
     *         in="query",
     *         name="nutrition_program",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/MealGroup")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        return response()->json(
            new JsonResponse($this->mealGroupRepository
                ->searchTranslatedMealGroups($request->nutrition_program)
                ->get()
            )
        );
    }
}
