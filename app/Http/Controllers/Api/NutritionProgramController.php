<?php

namespace App\Http\Controllers\Api;

use App\Repositories\GoalRepository;
use App\Repositories\LikeRepository;
use App\Repositories\MealGroupRepository;
use App\Repositories\MealRepository;
use App\Repositories\NutritionProgramItemRepository;
use App\Repositories\NutritionProgramMealGroupRepository;
use App\Repositories\NutritionProgramRepository;
use App\Repositories\PurchasedNutritionProgramRepository;
use App\Services\BuyNutritionProgram;
use App\Services\NutritionProgram;
use App\Services\UserPackage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class NutritionProgramController extends Controller
{
    private $nutritionProgramRepository;
    private $likeRepository;
    private $goalRepository;
    private $purchasedNutritionProgramRepository;
    private $nutritionProgramMealGroupRepository;
    private $nutritionProgramItemRepository;
    private $mealGroupRepository;
    private $mealRepository;
    private $nutritionProgramService;
    private $buyNutritionProgram;
    private $userPackageService;

    public function __construct(
        NutritionProgramRepository $nutritionProgramRepository,
        LikeRepository $likeRepository,
        GoalRepository $goalRepository,
        PurchasedNutritionProgramRepository $purchasedNutritionProgramRepository,
        NutritionProgramMealGroupRepository $nutritionProgramMealGroupRepository,
        NutritionProgramItemRepository $nutritionProgramItemRepository,
        MealGroupRepository $mealGroupRepository,
        MealRepository $mealRepository,
        NutritionProgram $nutritionProgramService,
        BuyNutritionProgram $buyNutritionProgram,
        UserPackage $userPackageService
    ) {
        $this->nutritionProgramRepository = $nutritionProgramRepository;
        $this->likeRepository = $likeRepository;
        $this->goalRepository = $goalRepository;
        $this->purchasedNutritionProgramRepository = $purchasedNutritionProgramRepository;
        $this->nutritionProgramMealGroupRepository = $nutritionProgramMealGroupRepository;
        $this->nutritionProgramItemRepository = $nutritionProgramItemRepository;
        $this->mealGroupRepository = $mealGroupRepository;
        $this->mealRepository = $mealRepository;
        $this->nutritionProgramService = $nutritionProgramService;
        $this->buyNutritionProgram = $buyNutritionProgram;
        $this->userPackageService = $userPackageService;
    }

    /**
     * @OA\Get(
     *     path="/nutrition-program",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Nutrition program"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/NutritionProgram")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        /** @var LengthAwarePaginator $list */
        $list = $this->nutritionProgramRepository
            ->getNutritionProgram(Auth::user()->getAuthIdentifier(), $request->search)
            ->with(['image'])
            ->paginate($request->get('limit', 15))
            ->appends(['search' => $request->search]);

        $list->setCollection(
            $list->getCollection()->transform(function ($el) {
                return $this->nutritionProgramService->mapNutritionProgramModelToNutritionProgram($el);
            })
        );

        return response()->json(new JsonResponse($list));
    }

    /**
     * @OA\Get(
     *     path="/nutrition-program/{nutrition_program}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Nutrition program"},
     *     @OA\Parameter(
     *         in="path",
     *         name="nutrition_program",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(ref="#/components/schemas/NutritionProgram"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        /** @var \App\Models\NutritionProgram $item */
        $item = $this->nutritionProgramRepository->find($request->nutrition_program);

        if (!$item) {
            return response()->json(new JsonResponse(null, false, 6, trans('errors.6')));
        }

        if (!$item->is_published) {
            return response()->json(new JsonResponse(null, false, 10, trans('errors.10')));
        }

        $canMakeProgress = true;
        if (!$item->is_free) {
            $userPackage = $this->userPackageService->getCurrentActiveUserPackage(Auth::id());
            $hasBought = $this->nutritionProgramService->hasUserBoughtProgramWithId(Auth::user(), $item->id);
            if (!$userPackage || !$hasBought) {
                $canMakeProgress = false;
            }
        }

        $translatedItem = $this->nutritionProgramRepository->findTranslation($item->id,
            Config::get('language')['id']);
        $translatedItem->load('image', 'video');
        $translatedItem->is_liked = $this->likeRepository->isUserLiked($translatedItem, Auth::user());
        $translatedItem->can_make_progress = $canMakeProgress;
        $translatedItem->can_activate_state = $this->buyNutritionProgram->getCanActivateState($translatedItem,
            Auth::user());
        $translatedItem->is_purchased = BuyNutritionProgram::NUTRITION_PROGRAM_STATE_ALREADY_ACTIVE === $translatedItem->can_activate_state;

        $translatedItem->goals = $this->goalRepository->getTranslatedGoalsForNutrition($translatedItem->object_id)->get();

        $translatedItem->groups = $this->nutritionProgramMealGroupRepository
            ->orderBy('order')
            ->findWhere(['nutrition_program_id' => $item->id]);
        foreach ($translatedItem->groups as &$group) {
            $group->meal_group = $this->mealGroupRepository->findTranslation($group->id,
                Config::get('language')['id']);

            $group->items = $this->nutritionProgramItemRepository
                ->orderBy('order')
                ->findWhere(['nutrition_program_meal_group_id' => $group->id]);
            foreach ($group->items as &$nutritionProgramItem) {
                $nutritionProgramItem->meal = $this->mealRepository->findTranslation($nutritionProgramItem->meal_id,
                    Config::get('language')['id']);
                $nutritionProgramItem->meal->load('image', 'video');
            }
        }

        return response()->json(
            new JsonResponse($translatedItem)
        );
    }

    /**
     * @OA\Post(
     *     path="/nutrition-program/{nutrition_program}/buy",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Nutrition program"},
     *     @OA\Parameter(
     *         in="path",
     *         name="nutrition_program",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(ref="#/components/schemas/NutritionProgram"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function buy(Request $request)
    {
        $program = $this->nutritionProgramRepository->find($request->nutrition_program);
        if (!$program) {
            return response()->json(new JsonResponse(null, false, 6, trans('errors.6')));
        }

        try {
            $response = new JsonResponse($this->buyNutritionProgram->buy($program, Auth::user()));
        } catch (\Exception $exception) {
            Log::warning($exception->getMessage(), ['nutritionProgramId' => $program->id]);
            Log::debug($exception->getTraceAsString());
            $response = new JsonResponse(null, false, 500, $exception->getMessage());
        }

        return response()->json(
            $response
        );
    }
}
