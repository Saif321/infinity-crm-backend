<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\Goal;
use App\Repositories\ExerciseEnvironmentRepository;
use App\Repositories\GoalRepository;
use App\Repositories\LevelRepository;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use Illuminate\Http\Request;

class BasicQuestionnaireQuestionsController extends Controller
{
    private $levelRepository;
    private $goalRepository;
    private $exerciseEnvironmentRepository;

    public function __construct(LevelRepository $levelRepository, GoalRepository $goalRepository, ExerciseEnvironmentRepository $exerciseEnvironmentRepository)
    {
        $this->levelRepository = $levelRepository;
        $this->goalRepository = $goalRepository;
        $this->exerciseEnvironmentRepository = $exerciseEnvironmentRepository;
    }

    /**
     * @OA\Schema(
     *   schema="BasicQuestionnaireQuestions",
     *   allOf={
     *       @OA\Schema(ref="#/components/schemas/BasicQuestionnaireQuestions"),
     *       @OA\Schema(
     *          @OA\Property(
     *             property="levels",
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Level")
     *          ),
     *          @OA\Property(
     *             property="goals",
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Goal")
     *         ),
     *         @OA\Property(
     *             property="environments",
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/ExerciseEnvironment")
     *         ),
     *       )
     *   }
     * )
     *
     * @OA\Get(
     *     path="/basic-questionnaire-questions",
     *     @OA\MediaType(mediaType="application/json"),
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Home"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/BasicQuestionnaireQuestions")
     *         ),
     *     ),
     * )
     */
    public function index(Request $request)
    {
        return response()->json(
            new JsonResponse([
                'levels' => $this->levelRepository->getLevelTranslations()->get(),
                'goals' => $this->goalRepository->getGoalsTranslations()->get(),
                'environments' => $this->exerciseEnvironmentRepository->getEnvironmentTranslations()->get()
            ])
        );
    }
}
