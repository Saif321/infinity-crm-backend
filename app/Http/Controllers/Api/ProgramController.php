<?php

namespace App\Http\Controllers\Api;

use App\Models\UserProgram;
use App\Repositories\ExerciseEnvironmentRepository;
use App\Repositories\LevelRepository;
use App\Repositories\TrainingRepository;
use App\Services\BuyProgram;
use App\Services\Program;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Repositories\ProgramRepository;
use App\Repositories\ProgramWeekRepository;
use App\Repositories\LikeRepository;
use Illuminate\Support\Facades\Log;

class ProgramController extends Controller
{
    private $programRepository;
    private $programWeekRepository;
    private $likeRepository;
    private $levelRepository;
    private $trainingRepository;

    private $exerciseEnvironmentRepository;

    private $buyProgram;
    private $programService;

    public function __construct(
        ProgramRepository $programRepository,
        ProgramWeekRepository $programWeekRepository,
        LikeRepository $likeRepository,
        LevelRepository $levelRepository,
        TrainingRepository $trainingRepository,
        ExerciseEnvironmentRepository $exerciseEnvironmentRepository,
        BuyProgram $buyProgram,
        Program $programService
    ) {
        $this->programRepository = $programRepository;
        $this->programWeekRepository = $programWeekRepository;
        $this->likeRepository = $likeRepository;
        $this->levelRepository = $levelRepository;
        $this->trainingRepository = $trainingRepository;
        $this->exerciseEnvironmentRepository = $exerciseEnvironmentRepository;
        $this->buyProgram = $buyProgram;
        $this->programService = $programService;
    }

    /**
     * @OA\Get(
     *     path="/program",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Program"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Program")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        /** @var LengthAwarePaginator $list */
        $list = $this->programRepository
            ->getProgramsAvailableForPurchase(Auth::user()->getAuthIdentifier(), $request->search)
            ->with(['image'])
            ->paginate()
            ->appends(['search' => $request->search]);

        $list->setCollection(
            $list->getCollection()->transform(function ($el) {
                return $this->programService->mapProgramModelToProgram($el);
            })
        );

        return response()->json(
            new JsonResponse($list)
        );
    }

    /**
     * @OA\Get(
     *     path="/program/{program}",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Program"},
     *     @OA\Parameter(
     *         in="path",
     *         name="program",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(ref="#/components/schemas/ProgramDetails"),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function show(Request $request)
    {
        /** @var \App\Models\Program $program */
        $program = $this->programRepository->findTranslation($request->program, Config::get('language')['id']);
        if (!$program) {
            return response()->json(new JsonResponse(null, false, 6, trans('errors.6')));
        }

        $purchasedPrograms = $this->programService->getUserPrograms();
        /** @var UserProgram $purchasedProgram */
        $purchasedProgram = $purchasedPrograms->filter(function (UserProgram $purchasedProgram) use ($program) {
            return $purchasedProgram->source_program_id == $program->object_id;
        })->first();

        $program->load('image', 'video');
        $program->levels = $this->levelRepository->getLevelsForProgram($program->object_id)->get();
        $program->is_liked = $this->likeRepository->isUserLiked($program, Auth::user());
        $program->is_purchased = is_object($purchasedProgram);
        $program->purchased_program_id = is_object($purchasedProgram) ? $purchasedProgram->object_id : null;
        $program->exercise_environment_id = 1;
        $program->can_activate_state = $this->buyProgram->getCanActivateState($program, Auth::user());
        $program->weeks = $this->programWeekRepository->getProgramWeeks($request->program)->get();
        foreach ($program->weeks as $week) {
            $week->trainings = $this->trainingRepository->getTrainingsForWeek($week->id)->get();
        }

        return response()->json(
            new JsonResponse($program)
        );
    }

    /**
     * @OA\Post(
     *     path="/program/{program}/buy",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Program"},
     *     @OA\Parameter(
     *         in="path",
     *         name="program",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\JsonContent(ref="#/components/schemas/PurchasedProgram"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="User not logged in"
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function buy(Request $request)
    {
        $program = $this->programRepository->find($request->program);
        if (!$program) {
            return response()->json(new JsonResponse(null, false, 6, trans('errors.6')));
        }

        try {
            $response = new JsonResponse($this->buyProgram->buy($program, Auth::user()));
        } catch (\Exception $exception) {
            Log::warning($exception->getMessage(), ['programId' => $program->id]);
            Log::debug($exception->getTraceAsString());
            $response = new JsonResponse(null, false, 500, $exception->getMessage());
        }

        return response()->json(
            $response
        );
    }
}
