<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Repositories\UserRepository;
use App\Rules\StrongPassword;
use App\Services\Otp\OtpService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ForgottenPasswordController extends Controller
{
    private $otpService;
    private $userRepository;

    public function __construct(OtpService $otpService, UserRepository $userRepository)
    {
        $this->otpService = $otpService;
        $this->userRepository = $userRepository;
    }

    /**
     * @OA\Post(
     *     path="/forgotten-password/send-otp",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Change user profile",
     *     tags={"Forgotten password"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(ref="#/components/schemas/ForgottenPasswordRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/JsonResponse"),
     *     )
     * )
     */
    public function sendOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'destination' => 'required|email',
        ]);
        if($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 400, $validator->messages()));
        }

        $user = $this->userRepository->findByField('email', $request->destination)->first();

        if(!$user) {
            return response()->json(
                new JsonResponse([], false, 5, trans('errors.5'))
            );
        }

        return response()->json(new JsonResponse(
            null,
            $this->otpService->setDestination($request->destination)->setTransportForDestination()->generate()->send()
        ));
    }

    /**
     * @OA\Post(
     *     path="/forgotten-password/check-otp",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="Check OTP validity",
     *     tags={"Forgotten password"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(ref="#/components/schemas/CheckOtpRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(ref="#/components/schemas/JsonResponse"),
     *     )
     * )
     */
    public function checkOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'destination' => 'required|email',
            'code' => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        if($this->otpService->verify($request->destination, $request->code) == 0){
            sleep(1);
            return response()->json(
                new JsonResponse([], false, 3, trans('errors.3'))
            );
        } else{
            return response()->json(
                new JsonResponse([], true)
            );
        }
    }

    /**
     * @OA\Post(
     *     path="/forgotten-password/change-password",
     *     @OA\MediaType(mediaType="application/json"),
     *     description="This method is used to reset forgotten password for user. Required parameters are code (from otp-forgot-password request), destination (user MSISDN) and new password. If code and MSISDN are valid, password will be changed for user with selected MSISDN.",
     *     tags={"Forgotten password"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(ref="#/components/schemas/ResetPasswordRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     )
     * )
     */
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'destination' => 'required|email',
            'code' => 'required',
            'password' => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2, $validator->messages()));
        }

        if(!$this->otpService->verify($request->destination, $request->code)) {
            return response()->json(
                new JsonResponse([], false, 4, trans('errors.4'))
            );
        }

        $rules['password'] = [
            new StrongPassword()
        ];

        $input = $request->all();
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json(new JsonResponse(null, false, 2,  $validator->messages()));
        }

        $this->otpService->invalidate($request->destination, $request->code);

        $user = $this->userRepository->findByField('email', $request->destination)->first();

        if(!$user) {
            return response()->json(
                new JsonResponse([], false, 5, trans('errors.5')), 403
            );
        }

        $user->password = $request->password;
        $user->is_active = 1;
        $user->save();

        return response()->json(
            new JsonResponse([])
        );
    }
}
