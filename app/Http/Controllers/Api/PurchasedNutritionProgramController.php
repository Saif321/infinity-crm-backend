<?php

namespace App\Http\Controllers\Api;

use App\Models\PurchasedNutritionProgram;
use App\Repositories\NutritionProgramRepository;
use App\Services\NutritionProgram;
use App\Repositories\PurchasedNutritionProgramRepository;
use App\Repositories\DeactivatedNutritionProgramRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PurchasedNutritionProgramController extends Controller
{
    private $nutritionProgramRepository;
    private $nutritionProgramService;
    private $purchasedNutritionProgramRepository;
    private $deactivatedNutritionProgramRepository;

    public function __construct(
        NutritionProgramRepository $nutritionProgramRepository,
        NutritionProgram $nutritionProgramService,
        PurchasedNutritionProgramRepository $purchasedNutritionProgramRepository,
        DeactivatedNutritionProgramRepository $deactivatedNutritionProgramRepository
    ) {
        $this->nutritionProgramRepository = $nutritionProgramRepository;
        $this->nutritionProgramService = $nutritionProgramService;
        $this->purchasedNutritionProgramRepository = $purchasedNutritionProgramRepository;
        $this->deactivatedNutritionProgramRepository = $deactivatedNutritionProgramRepository;
    }

    /**
     * @OA\Get(
     *     path="/nutrition-program/purchased",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Nutrition program"},
     *     @OA\Parameter(
     *         in="query",
     *         name="search",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/PurchasedProgram")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function index(Request $request)
    {
        /** @var LengthAwarePaginator $list */
        $list = $this->nutritionProgramRepository
            ->getPurchasedNutritionProgram(Auth::user()->id, $request->search)
            ->with(['image'])
            ->paginate()
            ->appends(['search' => $request->search]);

        $list->setCollection(
            $list->getCollection()->transform(function ($el) {
                return $this->nutritionProgramService->mapNutritionProgramModelToNutritionProgram($el);
            })
        );

        return response()->json(
            new JsonResponse($list)
        );
    }

    /**
     * @OA\Put(
     *     path="/nutrition-program/purchased/{nutrition_program}/deactivate",
     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Nutrition program"},
     *     @OA\Parameter(
     *         in="path",
     *         name="nutrition_program",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/JsonResponse")
     *         ),
     *     ),
     *     security={{
     *         "default":{}
     *     }}
     * )
     */
    public function deactivate(Request $request)
    {
        try {
            /** @var PurchasedNutritionProgram $purchasedNutritionProgram */
            $purchasedNutritionProgram = $this->purchasedNutritionProgramRepository->findWhere([
                'user_id' => Auth::user()->id,
                'nutrition_program_id' => $request->nutrition_program
            ])->first();

            if (!$purchasedNutritionProgram) {
                return response()->json(
                    new JsonResponse(null, false)
                );
            }

            $this->deactivatedNutritionProgramRepository->create([
                'user_id' => $purchasedNutritionProgram->user_id,
                'nutrition_program_id' => $purchasedNutritionProgram->nutrition_program_id,
                'user_package_id' => $purchasedNutritionProgram->user_package_id
            ]);
            $response = $this->purchasedNutritionProgramRepository->deleteWhere([
                'user_id' => $purchasedNutritionProgram->user_id,
                'nutrition_program_id' => $purchasedNutritionProgram->nutrition_program_id,
            ]);
            return response()->json(
                new JsonResponse(null, $response ? true : false)
            );
        } catch (\Exception $e) {
            Log::warning($e->getMessage(), ['purchasedNutritionProgram' => $request->nutrition_program]);
            Log::debug($e->getTraceAsString());
            return response()->json(
                new JsonResponse(null, false, 500, $e->getMessage())
            );
        }
    }
}
