<?php

namespace App\Http\Controllers\Api;

use App\Http\Responses\JsonResponse;
use App\Mail\UserConfirmationMail;
use App\Repositories\UserRepository;
use App\Services\Otp\ExceptionDestinationRequired;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use App\Services\Otp\OtpService;


class AuthController extends Controller
{
    private $userRepository;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->middleware('auth:api', ['only' => ['logout', 'refresh']]);
        $this->userRepository = $userRepository;
    }

    /**
     * @OA\Post(
     *     path="/resend-activation-email",

     *     @OA\MediaType(mediaType="application/json"),
     *     tags={"Auth"},
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=true,
     *         description="Email",
     *         @OA\Schema(type="string"),
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         required=true,
     *         description="Pass",
     *         @OA\Schema(type="string"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="object",
     *             @OA\Property(property="activation_email_sent", type="boolean"),
     *             @OA\Property(property="user_is_active", type="boolean"),
     *         ),
     *     ),
     *
     * )
     * @param Request $request
     * @param OtpService $otpService
     * @param UserRepository $userRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendActivationEmail(Request $request, OtpService $otpService, UserRepository $userRepository)
    {
        $user = $userRepository->getUserByEmail($request->email);

        try {
            if (!$user || !$user->verifyPassword($request->password)) {
                return response()->json(new JsonResponse(null, false, 1, trans('errors.403')), 403);
            }
        } catch (BindingResolutionException $e) {
            return response()->json(new JsonResponse(null, false, 1, trans('errors.403')), 403);
        }

        if ($user->is_active) {
            return response()->json(new JsonResponse([
                'activation_email_sent' => false,
                'user_is_active' => true
            ], true), 200);
        }

        try {
            $otp = $otpService->setDestination($user->email)->generate();
        } catch (ExceptionDestinationRequired $e) {
            return response()->json(new JsonResponse(null, false, 1, trans('errors.12')), 200);
        }
        $subject = Lang::get('app.WEB_CONFIRMATION_EMAIL_SUBJECT');
        Mail::to($user->email)->send(new UserConfirmationMail([
            'code' => $otp->getVerificationCode(),
            'subject' => $subject
        ]));

        return response()->json(new JsonResponse([
            'activation_email_sent' => true,
            'user_is_active' => false
        ], true), 200);
    }

    /**
     * @OA\Post(
     *      path="/logout",
     *      description="Logout user",
     *      @OA\MediaType(mediaType="application/json"),
     *      tags={"Auth"},
     *  @OA\Response(
     *         response=200,
     *         description="",
     *
     *  ),
     *     security={{
     *         "default":{}
     *     }}
     *)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function log_out(Request $request)
    {
        $token = Auth::user()->token();
        $token->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }


}
