<?php

namespace App\Http\Controllers;

use App\Http\Responses\JsonResponse;
use App\Services\Payment\ApplePay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use ReceiptValidator\iTunes\PurchaseItem;

class AppleController extends Controller
{
    private static $expiration_intent = [
        "1" => "The customer voluntarily canceled their subscription.",
        "2" => "Billing error; for example, the customer's payment information was no longer valid.",
        "3" => "The customer did not agree to a recent price increase.",
        "4" => "The product was not available for purchase at the time of renewal.",
        "5" => "Unknown error.",
    ];
    private static $auto_renew_status = [
        "true" => "The subscription will renew at the end of the current subscription period.",
        "false" => "The customer has turned off automatic renewal for the subscription."
    ];

    private $applePayService;
    private $logger;

    public function __construct(ApplePay $applePayService)
    {
        $this->applePayService = $applePayService;
        $this->logger = Log::channel('payment-apple');
    }

    public function getData(Request $request)
    {
        try {
            $appleReceiptEventData = $request->toArray();
            $this->logger->debug('Apple receipt event data', ['eventData', $appleReceiptEventData]);
            $this->logger->debug('Apple event: '.$appleReceiptEventData['notification_type']);
            if (isset($appleReceiptEventData['expiration_intent'])) {
                $this->logger->debug('Apple expiration intent: '.AppleController::$expiration_intent[(string) $appleReceiptEventData['notification_type']]);
            }
            if (isset($appleReceiptEventData['auto_renew_status'])) {
                $this->logger->debug('Apple expiration intent: '.AppleController::$auto_renew_status[(string) $appleReceiptEventData['auto_renew_status']]);
            }

            $user = null;
            $purchaseIndex = count($appleReceiptEventData['unified_receipt']['latest_receipt_info']) - 1;
            while ($purchaseIndex >= 0) {
                if (!$user && isset($appleReceiptEventData['unified_receipt']['latest_receipt_info'][$purchaseIndex])) {
                    $userPackage = $this->applePayService->findUserPackageByOriginalTransactionId($appleReceiptEventData['unified_receipt']['latest_receipt_info'][$purchaseIndex]['transaction_id']);
                    if ($userPackage) {
                        $user = $userPackage->user;
                    }
                }
                if ($user) {
                    break;
                }

                $purchaseIndex--;
            }

            if (!$user) {
                // TODO: create custom log file for receipts we must log all receipts
                $this->logger->error('Apple error: receipt user not found');
                return response()->json(new JsonResponse(true));
            }

            $this->applePayService->validatePurchases(array_map(function ($data) {
                return new PurchaseItem($data);
            }, $appleReceiptEventData['unified_receipt']['latest_receipt_info']),
                $user);
            $this->logger->debug(sprintf('Apple receipt for User(%s) verified successfully', $user->id));

            if (!$userPackage) {
                // TODO: create custom log file for receipts we must log all receipts
                $this->logger->error('Apple error: user package not found');
                return response()->json(new JsonResponse(true));
            }

            if ($appleReceiptEventData['notification_type'] === ApplePay::NOTIFICATION_TYPE_DID_CHANGE_RENEWAL_STATUS) {
                $result = $this->applePayService->onChangeRenewalStatus(
                    $appleReceiptEventData['auto_renew_status'] === "true",
                    $userPackage
                );
                if (!$result) {
                    $this->logger->debug(sprintf('Trouble changing renewal status on UserPackage(%s)', $userPackage->id));
                }
            }

            return response()->json(new JsonResponse(true));
        } catch (\Exception $e) {
            $this->logger->error(sprintf('Handling Apple receipt failed with message %s', $e->getMessage()),
                ['trace' => $e->getTraceAsString()]);
            return response()->json(new JsonResponse(false, false, 500), 500);
        }
    }
}
