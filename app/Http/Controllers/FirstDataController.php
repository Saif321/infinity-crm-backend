<?php

namespace App\Http\Controllers;

use App\Models\Package;
use App\Models\User;
use App\Services\Payment\FirstData;
use Illuminate\Http\Request;

class FirstDataController extends Controller
{
    private $firstData;

    public function __construct(FirstData $firstData)
    {
        $this->firstData = $firstData;
    }

    public function redirect(Request $request)
    {
        $package = Package::find($request->package_id);
        if (!$package) {
            return view('first-data.redirect-error', ['error' => 'Unexisting package']);
        }
        $user = User::find($request->user_id);
        if (!$user) {
            return view('first-data.redirect-error', ['error' => 'Unexisting user']);
        }
        $this->firstData->setPackage($package);
        $this->firstData->setUser($user);
        $redirectionData = $this->firstData->getRedirectionData();
        $this->firstData->initPayment();
        return view('first-data.redirect', $redirectionData);
    }

    public function successfulPayment(Request $request)
    {
        $response = $this->firstData->handleRedirectionData($request->all());
        return view('first-data.finish-success', ['message' => 'Transaction successful']);
    }

    public function failedPayment(Request $request)
    {
        $response = $this->firstData->handleRedirectionData($request->all());
        return view('first-data.finish-error', ['message' => $response->fail_reason]);
    }

    public function redirectTest(Request $request)
    {
        $redirectionData = $this->firstData->getTestRedirectionData($request->test_case);
        return view('first-data.test-redirect', $redirectionData);
    }

    public function successfulPaymentTest(Request $request)
    {
        return view('first-data.test-finish-success', ['post' => $request->all()]);
    }

    public function failedPaymentTest(Request $request)
    {
        return view('first-data.test-finish-error', ['post' => $request->all()]);
    }
}
