<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Responses\JsonResponse;
use App\Models\Package;
use App\Models\PackagePaymentMethod;
use App\Models\User;
use App\Models\Payment;
use App\Services\Payment\Sms;
use App\Services\UserPackage;
use App\Repositories\UserPackageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SmsPaymentController extends Controller
{
    private $sms;
    private $userPackage;
    private $userPackageRepository;

    public function __construct(Sms $sms, UserPackage $userPackage, UserPackageRepository $userPackageRepository)
    {
        $this->sms = $sms;
        $this->userPackage = $userPackage;
        $this->userPackageRepository = $userPackageRepository;
    }

    public function checkAvailability(Request $request)
    {
        $packageExists = PackagePaymentMethod::where('package_id', $request->package_id)
            ->where('payment_method', Payment::METHOD_SMS)
            ->exists();
        if (!$packageExists) {
            return response()->json(
                new JsonResponse(null, false, 400, 'Unexisting package')
            );
        }

        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(
                new JsonResponse(null, false, 400, 'Unexisting user')
            );
        }

        $activePackage = $this->userPackageRepository->getCurrentActiveUserPackage($request->user_id);
        if($activePackage && $activePackage->package_id == $request->package_id){
            return response()->json(
                new JsonResponse(null, false, 400, 'Package already owned by user')
            );
        }

        $package = Package::find($request->package_id);
        if($this->userPackage->isUserEligibleForFreeTrial($user)){
            $response = ['free' => $package->free_trial_in_days];
        } else{
            $response = ['charge' => $package->recurring_period_in_days];
        }

        return response()->json(
            new JsonResponse($response)
        );
    }

    public function successfulPayment(Request $request)
    {
        $package = Package::find($request->package_id);
        if (!$package) {
            return response()->json(
                new JsonResponse(null, false, 400, 'Unexisting package')
            );
        }

        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(
                new JsonResponse(null, false, 400, 'Unexisting user')
            );
        }

        if (!isset($request->transaction_id)) {
            return response()->json(
                new JsonResponse(null, false, 400, 'Transaction ID not provided')
            );
        }

        $this->sms->setPackage($package);
        $this->sms->setUser($user);
        $this->sms->setTransactionId($request->transaction_id);

        $paymentMethod = $package->paymentMethods->filter(function (
            PackagePaymentMethod $packagePaymentMethod
        ) {
            return $packagePaymentMethod->payment_method === Payment::METHOD_SMS;
        })->first();

        if (!$paymentMethod) {
            Log::error(sprintf('No SMS payment method for package %s', $package->id));
        }

        if(isset($paymentMethod->currency_code)) $this->sms->setCurrencyCode($paymentMethod->currency_code);
        if(isset($paymentMethod->price)) $this->sms->setChargedValue($paymentMethod->price);

        $this->sms->initPayment();
        $this->sms->updatePayment($request->transaction_id, Payment::STATUS_SUCCESS, '', false);

        return response()->json(
            new JsonResponse([])
        );
    }

    public function unsubscribe(Request $request)
    {
        $packageExists = PackagePaymentMethod::where('package_id', $request->package_id)
            ->where('payment_method', Payment::METHOD_SMS)
            ->exists();
        if (!$packageExists) {
            return response()->json(
                new JsonResponse(null, false, 400, 'Unexisting package')
            );
        }

        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(
                new JsonResponse(null, false, 400, 'Unexisting user')
            );
        }

        $package = Package::find($request->package_id);
        $this->sms->setPackage($package);
        $this->sms->setUser($user);
        $result = $this->sms->unsubscribe();

        if($result != '1'){
            return response()->json(
                new JsonResponse(null, false, 400, $result)
            );
        } else{
            return response()->json(
                new JsonResponse([])
            );
        }
    }
}
