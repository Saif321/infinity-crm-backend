<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Repositories\VerificationCodeRepository;
use App\Services\Otp\OtpService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $userRepository;
    private $verificationCodeRepository;
    private $otpService;

    public function __construct(UserRepository $userRepository, VerificationCodeRepository $verificationCodeRepository, OtpService $otpService)
    {
        $this->userRepository = $userRepository;
        $this->verificationCodeRepository = $verificationCodeRepository;
        $this->otpService = $otpService;
    }

    public function privacy(Request $request)
    {
        return view('privacy.' . $request->get('language', 'en'));
    }

    public function terms(Request $request)
    {
        return view('terms.' . $request->get('language', 'en'));
    }

    public function confirmation(Request $request)
    {
        try {
            $verificationCode = $this->verificationCodeRepository->find($request->id);
        } catch (\Exception $e) {
            return view('user-confirmation.invalid-code');
        }

        if($verificationCode->code != $request->code) {
            return view('user-confirmation.invalid-code');
        }

        if(!$this->otpService->verify($verificationCode->destination, $request->code)) {
            return view('user-confirmation.invalid-code');
        }

        $user = $this->userRepository->findByField('email', $verificationCode->destination)->first();
        if(!$user) {
            return view('user-confirmation.invalid-code');
        }

        $user->is_active = true;
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->save();

        $this->otpService->invalidate($verificationCode->destination, $request->code);

        return view('user-confirmation.success');
    }
}
