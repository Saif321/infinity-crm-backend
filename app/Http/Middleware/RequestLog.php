<?php

namespace App\Http\Middleware;

use App\Services\IosHeadersParser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Log\Logger;
use League\OAuth2\Server\CryptTrait;

final class RequestLog
{

    use CryptTrait;

    private $logger;
    private $iosHeadersParser;

    public function __construct(
        Logger $logger,
        IosHeadersParser $iosHeadersParser
    ) {
        $this->logger = $logger;
        $this->iosHeadersParser = $iosHeadersParser;
    }

    public function handle(
        Request $request,
        \Closure $next
    ) {
        /** @var Response $response */
        $response = $next($request);

        if ($this->iosHeadersParser->isIosOauthRequest()) {
            $decryptedToken = null;
            $refreshToken = $request->get('refresh_token');
            if ($refreshToken) {
                try {
                    $keyE = app('encrypter')->getKey();
                    $this->setEncryptionKey($keyE);
                    $decryptedToken = $this->decrypt($refreshToken);
                    $decryptedToken = json_decode($decryptedToken, true);
                } catch (\Exception $e) {
                    $this->logger->info('Refresh token decrypt error', ['message' => $e->getMessage()]);
                }
            }

            $this->logger->info('oAuth req/res', [
                'ip' => $request->ip(),
                'request' => $request->__toString(),
                'response' => $response->__toString(),
                'decrypted_token' => $decryptedToken
            ]);
        }

        return $response;
    }
}
