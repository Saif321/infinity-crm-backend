<?php

namespace App\Http\Middleware;

use App\Repositories\LanguageRepository;
use Closure;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Http\Responses\JsonResponse;
use Illuminate\Support\Facades\Auth;

class Localize
{
	/**
	 * Handle an incoming request
	 *
	 * @param Request $request
	 * @param Closure $next
	 *
	 * @return mixed
	 * @throws BindingResolutionException
	 */
    public function handle(Request $request, Closure $next)
    {
        if($request->user() && $request->user()->blocked){
            return response()->json(new JsonResponse(null, false, 406, 'User blocked!'), 406);
        }
        $language = null;
        $headerLocale = $request->header('X-Locale');
        /** @var LanguageRepository $languageRepository */
        $languageRepository = app()->make(LanguageRepository::class);

        if($headerLocale) {
            $language = $languageRepository->findWhere(['iso' => $headerLocale])->first();
        }

        $language = $language ? $language : $languageRepository->getDefaultLanguage();

        Config::set('language', $language->toArray());
        app()->setLocale($language->iso);

        $response = $next($request);

        return $response;
    }
}
