<?php

namespace App\Http\Responses;

use Illuminate\Support\MessageBag;

/**
 *  @OA\Schema(
 *   schema="JsonResponse",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/JsonResponse"),
 *       @OA\Schema(
 *           @OA\Property(property="success", type="boolean"),
 *           @OA\Property(property="errorCode", type="integer"),
 *           @OA\Property(property="errorMessage", type="string"),
 *           @OA\Property(property="data", type="array", @OA\Items(type="string")),
 *       )
 *   }
 * )
 */
class JsonResponse
{
    public $success = true;
    public $errorCode = '';
    public $errorMessage = [];
    public $data = [];

    public function __construct($data, $success = true, $errorCode = null, $errorMessage = null)
    {
        $this->success = $success;
        $this->data = $data;
        $this->errorCode = $errorCode;

        if(!$success) {
            $this->data = null;
        }

        $this->prepareErrorMessages($errorMessage);
    }

    private function prepareErrorMessages($errorMessages)
    {
        if(!$errorMessages) {
            return $this;
        }

        $this->errorMessage = [];

        if(is_string($errorMessages)) {
            $this->errorMessage[] = $errorMessages;
        }

        if($errorMessages instanceof MessageBag) {
            $errorMessages = $errorMessages->toArray();
        }

        if(is_array($errorMessages)) {
            foreach($errorMessages as $key => $value) {
                if(is_array($value)) {
                    foreach($value as $subValue) {
                        $this->errorMessage[] = $subValue;
                    }
                } else {
                    $this->errorMessage[] = $value;
                }
            }
        }

        return $this;
    }
}
