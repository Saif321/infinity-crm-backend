<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @OA\Schema(
 *   schema="Gallery",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/Gallery"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="mime_type", type="string"),
 *           @OA\Property(property="extension", type="string"),
 *           @OA\Property(property="size", type="string"),
 *           @OA\Property(property="url", type="string"),
 *           @OA\Property(property="type", type="string", enum={"image", "video"}),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="GalleryEditRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/GalleryEditRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="GalleryCreateRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/GalleryCreateRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="url", type="string"),
 *       )
 *   }
 * )
 *
 * @property int $id
 * @property string $name
 */
class Gallery extends Model implements Auditable {
	use \OwenIt\Auditing\Auditable;

	protected $fillable = [ 'name', 'mime_type', 'extension', 'size', 'url', 'type', 'is_user_avatar', 'thumbnail' ];

	protected $casts = [ 'is_user_avatar' => 'boolean', 'updated_at' => 'datetime:c', 'created_at' => 'datetime:c' ];

    public function getUrl(){
        return $this->attributes['url'];
    }

    public function geThumbnail(){
        return $this->attributes['thumbnail'];
    }

	public function deleteFile() {
		if ( $this->attributes['url'] ) {
			Storage::delete( $this->attributes['url'] );
			Storage::delete( $this->attributes['thumbnail'] );
		}

		$this->attributes['url']       = '';
		$this->attributes['thumbnail'] = '';

		return $this;
	}

	public function getUrlAttribute() {
		if ( ! array_key_exists( 'url', $this->attributes ) || empty( $this->attributes['url'] ) ) {
			return '';
		}

		if ( preg_match( '/^http(s|)/is', $this->attributes['url'] ) ) {
			return $this->attributes['url'];
		}

		return env( 'APP_URL' ) . implode( "/", array_map( "rawurlencode",
				explode( "/", Storage::url( $this->attributes['url'] ) ) ) ) . '?v=' . env( 'STORAGE_VERSION', 1 );
	}

	public function getThumbnailAttribute() {
		if ( ! array_key_exists( 'thumbnail', $this->attributes ) || empty( $this->attributes['thumbnail'] ) ) {
			return '';
		}

		if ( preg_match( '/^http(s|)/is', $this->attributes['thumbnail'] ) ) {
			return $this->attributes['thumbnail'];
		}

		return env( 'APP_URL' ) . implode( "/", array_map( "rawurlencode",
				explode( "/", Storage::url( $this->attributes['thumbnail'] ) ) ) ) . '?v=' . env( 'STORAGE_VERSION',
				1 );
	}

	public function getSizeAttribute() {
		$bytes = $this->attributes['size'];

		if ( $bytes >= 1073741824 ) {
			return number_format( $bytes / 1073741824, 2 ) . ' GB';
		} elseif ( $bytes >= 1048576 ) {
			return number_format( $bytes / 1048576, 2 ) . ' MB';
		} elseif ( $bytes >= 1024 ) {
			return number_format( $bytes / 1024, 2 ) . ' KB';
		} elseif ( $bytes > 1 ) {
			return $bytes . ' bytes';
		} elseif ( $bytes == 1 ) {
			return $bytes . ' byte';
		} else {
			$bytes = '0 bytes';
		}

		return $bytes;
	}
}
