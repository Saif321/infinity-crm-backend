<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PaymentLog
 * @package App\Models
 *
 * @property integer $id
 * @property string $method
 * @property string $transaction_id
 * @property string $status
 * @property object $details
 * @property integer $user_package_id
 * @property string $google_purchase_token
 * @property string $google_product_id
 * @property float $charged_value
 * @property string $currency_code
 * @property string $created_at
 * @property string $updated_at
 */
class PaymentLog extends Model
{
    protected $fillable = [
        'method', 'transaction_id', 'status', 'details', 'user_package_id', 'charged_value', 'currency_code',
        'google_purchase_token', 'google_product_id'
    ];

    protected $casts = [
        'charged_value' => 'float',
        'details' => 'json',
        'updated_at' => 'datetime:c',
        'created_at' => 'datetime:c'
    ];
}
