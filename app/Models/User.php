<?php

namespace App\Models;

use App\Services\Dto\UserStatistics;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Permission\Traits\HasRoles;

/**
 * @OA\Schema(
 *   schema="User",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/User"),
 *       @OA\Schema(
 *           @OA\Property(property="level_id", type="string"),
 *           @OA\Property(property="first_name", type="string"),
 *           @OA\Property(property="last_name", type="string"),
 *           @OA\Property(property="email", type="string"),
 *           @OA\Property(property="phone", type="string"),
 *           @OA\Property(property="image", type="string"),
 *           @OA\Property(property="date_of_birth", type="string"),
 *           @OA\Property(property="gender", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="UserRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/UserRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="level_id", type="string"),
 *           @OA\Property(property="image_id", type="string"),
 *           @OA\Property(property="first_name", type="string"),
 *           @OA\Property(property="last_name", type="string"),
 *           @OA\Property(property="email", type="string"),
 *           @OA\Property(property="password", type="string"),
 *           @OA\Property(property="phone", type="string"),
 *           @OA\Property(property="date_of_birth", type="string"),
 *           @OA\Property(property="gender", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="UserChangePasswordRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/UserChangePasswordRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="old_password", type="string"),
 *           @OA\Property(property="password", type="string"),
 *           @OA\Property(property="password_confirmation", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="UserDetails",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/UserDetails"),
 *       @OA\Schema(
 *           @OA\Property(property="id", type="integer"),
 *           @OA\Property(property="google_id", type="string"),
 *           @OA\Property(property="apple_id", type="string"),
 *           @OA\Property(property="level_id", type="integer"),
 *           @OA\Property(property="first_name", type="string"),
 *           @OA\Property(property="last_name", type="string"),
 *           @OA\Property(property="email", type="string"),
 *           @OA\Property(property="phone", type="string"),
 *           @OA\Property(property="image", type="string"),
 *           @OA\Property(property="email_verified_at", type="string"),
 *           @OA\Property(property="date_of_birth", type="string"),
 *           @OA\Property(property="gender", type="string"),
 *           @OA\Property(property="is_active", type="integer"),
 *           @OA\Property(property="is_survey_completed", type="integer"),
 *           @OA\Property(property="level", type="array", @OA\Items(ref="#/components/schemas/Level")),
 *           @OA\Property(property="questionnaire", type="array", @OA\Items(ref="#/components/schemas/BasicQuestionnaireRequest")),
 *           @OA\Property(property="nutrition_questionnaire", type="array", @OA\Items(ref="#/components/schemas/NutritionQuestionnaire")),
 *           @OA\Property(property="goals", type="array", @OA\Items(ref="#/components/schemas/Goal")),
 *           @OA\Property(property="roles", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="UpdateWeightRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/UpdateWeightRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="weight", type="integer"),
 *       )
 *   }
 * )
 *
 * @property integer $id
 * @property string $gender
 * @property string $email
 * @property boolean $is_active
 * @property Level $level
 * @property BasicQuestionnaire $questionnaire
 * @property NutritionQuestionnaire $nutrition_questionnaire
 * @property UserStatistics $statistics
 */
class User extends Authenticatable implements Auditable
{
    use Notifiable, HasRoles;
    use \OwenIt\Auditing\Auditable;
    use HasApiTokens, Notifiable;

    protected $with = ['image', 'level'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'google_id',
        'apple_id',
        'fb_id',
        'level_id',
        'first_name',
        'last_name',
        'email',
        'phone',
        'password',
        'is_active',
        'image_id',
        'date_of_birth',
        'gender',
        'blocked'
    ];

    protected $casts = ['email_verified_at' => 'datetime:c', 'updated_at' => 'datetime:c', 'created_at' => 'datetime:c'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];
    /**
     * @param $value
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function setPasswordAttribute( $value ) {
        $this->attributes['password'] = $this->getHasher()->make( $value );
    }

    /**
     * @return BcryptHasher
     * @throws BindingResolutionException
     */

    private function getHasher()
    {
        return app()->make(BcryptHasher::class);
    }

    /**
     * @param $password
     *
     * @return bool
     * @throws BindingResolutionException
     */
    public function verifyPassword($password)
    {
        return $this->getHasher()->check($password, $this->attributes['password']);
    }


    /**
     * @return HasOne
     */
    public function image()
    {
        return $this->hasOne(Gallery::class, 'id', 'image_id');
    }

    /**
     * @return HasOne
     */
    public function level()
    {
        return $this->hasOne(Level::class, 'id', 'level_id');
    }

    /**
     * @return BelongsTo
     */
    public function questionnaire()
    {
        return $this->belongsTo(BasicQuestionnaire::class, 'id', 'user_id');
    }
}
