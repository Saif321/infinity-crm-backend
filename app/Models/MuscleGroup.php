<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


/**
 * @OA\Schema(
 *   schema="MuscleGroup",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/MuscleGroup"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="slug", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="MuscleGroupTranslation",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/MuscleGroupTranslation"),
 *       @OA\Schema(
 *           @OA\Property(property="language_id", type="integer"),
 *           @OA\Property(property="name", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="MuscleGroupRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/MuscleGroupRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(
 *             property="translations",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/MuscleGroupTranslation")
 *         ),
 *       )
 *   }
 * )
 */
class MuscleGroup extends Model implements Auditable {
	//
	use TranslatableModel;
	use \OwenIt\Auditing\Auditable;

	protected $fillable = [ 'name', 'slug' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];
}
