<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *   schema="Package",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/Package"),
 *       @OA\Schema(
 *           @OA\Property(property="id", type="integer"),
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="number_of_programs", type="integer"),
 *           @OA\Property(property="number_of_nutrition_programs", type="integer"),
 *           @OA\Property(property="recurring_period_in_days", type="integer"),
 *           @OA\Property(property="free_trial_in_days", type="integer"),
 *           @OA\Property(property="payment_methods", type="array", @OA\Items(ref="#/components/schemas/PackagePaymentMethod")),
 *       )
 *   }
 * )
 *
 * Class Package
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property int $number_of_programs
 * @property int $number_of_nutrition_programs
 * @property int $recurring_period_in_days
 * @property int $number_of_free_trial_nutrition_programs
 * @property int $number_of_free_trial_programs
 * @property int $free_trial_in_days
 * @property string $ios_store_package_id
 * @property string $android_store_package_id
 * @property int $order
 * @property string $created_at
 * @property string $update_at
 *
 * @property Collection $paymentMethods
 */
class Package extends Model
{
    protected $fillable = [
        'name', 'number_of_programs', 'number_of_nutrition_programs', 'recurring_period_in_days', 'free_trial_in_days',
        'android_store_package_id', 'ios_store_package_id', 'order' , 'number_of_free_trial_programs', 'number_of_free_trial_nutrition_programs'
    ];

    protected $casts = [
        'updated_at' => 'datetime:c',
        'created_at' => 'datetime:c'
    ];

    public function paymentMethods()
    {
        return $this->hasMany(PackagePaymentMethod::class, 'package_id', 'id');
    }
}
