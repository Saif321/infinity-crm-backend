<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *   schema="Language",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/Language"),
 *		 @OA\Schema(
 *           @OA\Property(property="id", type="integer"),
 *           @OA\Property(property="language", type="string"),
 *           @OA\Property(property="iso", type="string"),
 *           @OA\Property(property="is_default", type="boolean"),
 *       )
 *   }
 * )
 */

/**
 * Class Language
 * @package App\Models
 * @property string $language
 * @property string $iso
 * @property boolean $is_default
 */
class Language extends Model {

	protected $casts = [
		'is_default' => 'boolean',
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];
}
