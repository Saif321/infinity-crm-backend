<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Payment
 * @package App\Models
 *
 * @property integer $id
 * @property string $method
 * @property string $transaction_id
 * @property string $status
 * @property object $details
 * @property integer $user_package_id
 * @property string $google_purchase_token
 * @property string $google_product_id
 * @property float $charged_value
 * @property string $currency_code
 * @property string $created_at
 * @property string $updated_at
 * @property UserPackage $userPackage
 */
class Payment extends Model
{
    const METHOD_CREDIT_CARD = 'CC';
    const METHOD_APPLE_PAY = 'AP';
    const METHOD_GOOGLE_PAY = 'GP';
    const METHOD_SMS = 'SMS';

    const STATUS_PROCESSING = 'PROCESSING';
    const STATUS_SUCCESS = 'SUCCESS';
    const STATUS_FAILED = 'FAILED';
    const STATUS_VOID = 'VOID';

    protected $fillable = [
        'method', 'transaction_id', 'status', 'details', 'user_package_id', 'charged_value', 'currency_code',
        'google_purchase_token', 'google_product_id'
    ];

    protected $casts = [
        'charged_value' => 'float',
        'details' => 'json',
        'updated_at' => 'datetime:c',
        'created_at' => 'datetime:c'
    ];

    public function userPackage()
    {
        return $this->belongsTo(UserPackage::class, 'user_package_id', 'id');
    }
}
