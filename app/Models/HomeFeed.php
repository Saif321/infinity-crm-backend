<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


class HomeFeed  extends Model implements Auditable
{
    use HasApiTokens, Notifiable;
	use \OwenIt\Auditing\Auditable;


    protected $table = "home_feed";


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */  
    
	public function getId() {
		return $this->id;
	}

	public function image() {
		return $this->hasOne( Gallery::class, 'id', 'image_id' );
	}

	public function user() {
		return $this->hasOne( User::class, 'id', 'users_id' );
	}

	public function training() {
		return $this->hasOne( Training::class, 'id', 'training_id' );
	}
}