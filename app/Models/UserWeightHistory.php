<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserWeightHistory extends Model
{
    protected $table = 'user_weight_history';

    protected $fillable = [ 'user_id', 'weight_in_kilograms' ];

	protected $casts = [
		'updated_at'  => 'datetime:c',
		'created_at'  => 'datetime:c'
	];
}
