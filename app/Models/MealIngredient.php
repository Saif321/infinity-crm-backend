<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *   schema="MealIngredient",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/MealIngredient"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *     )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="MealIngredientTranslation",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/MealIngredientTranslation"),
 *       @OA\Schema(
 *           @OA\Property(property="language_id", type="integer"),
 *           @OA\Property(property="name", type="string"),
 *        )
 *     }
 *   )
 *
 * @OA\Schema(
 *   schema="MealIngredientRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/MealIngredientRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(
 *             property="translations",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/MealIngredientTranslation"),
 *       ),
 *     )
 *   }
 * )
 */
class MealIngredient extends Model {
	//
	use TranslatableModel;

	protected $fillable = [ 'meal_id', 'name' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];
}
