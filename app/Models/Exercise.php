<?php

namespace App\Models;

use App\Models\Gallery;
use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


/**
 * @OA\Schema(
 *   schema="Exercise",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/Exercise"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(property="image_id", type="integer"),
 *           @OA\Property(property="video_id", type="integer"),
 *           @OA\Property(property="is_free", type="boolean"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="ExerciseTranslation",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/ExerciseTranslation"),
 *       @OA\Schema(
 *           @OA\Property(property="language_id", type="integer"),
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(property="image_id", type="integer"),
 *           @OA\Property(property="video_id", type="integer")
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="ExerciseRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/ExerciseRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(property="image_id", type="integer"),
 *           @OA\Property(property="video_id", type="integer"),
 *           @OA\Property(property="is_free", type="boolean"),
 *           @OA\Property(property="muscle_group_ids", type="array", @OA\Items(type="integer", format="int32")),
 *           @OA\Property(property="exercise_environment_ids", type="array", @OA\Items(type="integer", format="int32")),
 *           @OA\Property(property="level_ids", type="array", @OA\Items(type="integer", format="int32")),
 *           @OA\Property(
 *             property="translations",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/ExerciseTranslation")
 *         ),
 *       )
 *   }
 * )
 *
 * @property int $id
 * @property string $name
 * @property string $description
 */
class Exercise extends Model implements Auditable {
	//
	use TranslatableModel;
	use \OwenIt\Auditing\Auditable;

	protected $fillable = [ 'video_id', 'image_id', 'level_id', 'name', 'description', 'is_published', 'is_free' ];

	protected $casts = [
		'is_free'      => 'boolean',
		'is_published' => 'boolean',
		'published_at' => 'datetime:c',
		'updated_at'   => 'datetime:c',
		'created_at'   => 'datetime:c'
	];

	public function image() {
		return $this->hasOne( Gallery::class, 'id', 'image_id' );
	}

	public function video() {
		return $this->hasOne( Gallery::class, 'id', 'video_id' );
	}

	public function exerciseEnvironments() {
//        return $this->belongsTo(ExerciseEnvironment::class);
		return $this->belongsToMany( ExerciseEnvironment::class, 'exercise_environment_items', 'exercise_id',
			'exercise_environment_id' );
	}

	public function level() {
		return $this->belongsTo( Level::class );
	}

	public function levels() {
		return $this->belongsToMany( Level::class, 'exercise_level_items', 'exercise_id', 'level_id' );
	}

	public function muscleGroups() {
		return $this->belongsToMany( MuscleGroup::class, 'exercise_muscle_group_items', 'exercise_id',
			'muscle_group_id' );
	}

}
