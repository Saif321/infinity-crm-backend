<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProgramWeek extends Model {
	protected $fillable = [ 'user_program_id', 'order' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];
}
