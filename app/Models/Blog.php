<?php

namespace App\Models;

use App\Models\Gallery;
use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @OA\Schema(
 *   schema="Blog",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/Blog"),
 *       @OA\Schema(
 *           @OA\Property(property="id", type="integer"),
 *           @OA\Property(property="image_id", type="integer"),
 *           @OA\Property(property="title", type="string"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(property="is_published", type="integer"),
 *           @OA\Property(property="published_at", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="BlogTranslation",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/BlogTranslation"),
 *       @OA\Schema(
 *           @OA\Property(property="language_id", type="integer"),
 *           @OA\Property(property="title", type="string"),
 *           @OA\Property(property="description", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="BlogRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/BlogRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="image_id", type="integer"),
 *           @OA\Property(property="title", type="string"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(property="is_published", type="integer"),
 *           @OA\Property(
 *             property="translations",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/BlogTranslation")
 *         ),
 *       )
 *   }
 * )
 */
class Blog extends Model implements Auditable {
	use TranslatableModel;
	use \OwenIt\Auditing\Auditable;


	protected $with = [ 'user', 'image' ];

	protected $fillable = [
		'image_id',
		'title',
		'description',
		'is_published',
		'published_at',
		'like_count',
		'review_count',
		'user_id'
	];

	protected $casts = [
		'is_published' => 'boolean',
		'published_at' => 'datetime:c',
		'updated_at'   => 'datetime:c',
		'created_at'   => 'datetime:c'
	];

	public function comments() {
		return $this->hasMany( Comment::class, 'object_id' )
		            ->where( 'object_type', self::class )
		            ->where( 'is_published', true )
		            ->orderBy( 'published_at', 'desc' );
	}

	public function tags() {
		return $this->belongsToMany( BlogTag::class, 'blog_blog_tags', 'blog_id', 'tag_id' );
	}

	public function image() {
		return $this->hasOne( Gallery::class, 'id', 'image_id' );
	}

	public function user() {
		return $this->belongsTo( User::class );
	}
}
