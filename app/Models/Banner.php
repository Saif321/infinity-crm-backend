<?php

namespace App\Models;

use App\Models\Gallery;
use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Banner extends Model implements Auditable {
	use TranslatableModel;
	use \OwenIt\Auditing\Auditable;

	protected $fillable = [
		'photo_url',
		'link_url'
	];

}
