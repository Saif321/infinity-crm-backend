<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserTraining
 * @package App\Models
 *
 * @property integer $id
 * @property integer $object_id
 * @property integer $number_of_calories
 * @property boolean $is_rest_day
 */
class UserTraining extends Model {
	use TranslatableModel;

	protected $fillable = [ 'name', 'description', 'number_of_calories', 'parent_id' ];

	protected $casts = [
		'is_rest_day' => 'boolean',
		'is_locked'   => 'boolean',
		'updated_at'  => 'datetime:c',
		'created_at'  => 'datetime:c'
	];
}
