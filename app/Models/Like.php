<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *   schema="Like",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/Like"),
 *       @OA\Schema(
 *           @OA\Property(property="user_id", type="integer"),
 *           @OA\Property(property="object_id", type="integer"),
 *           @OA\Property(property="object_type", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="LikeRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/LikeRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="object_id", type="integer"),
 *           @OA\Property(property="object_type", type="string"),
 *       )
 *   }
 * )
 */
class Like extends Model {
	protected $fillable = [ 'user_id', 'object_id', 'object_type' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];
}
