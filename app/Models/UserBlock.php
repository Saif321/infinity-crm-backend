<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


class UserBlock  extends Model implements Auditable
{
    use HasApiTokens, Notifiable;
	use \OwenIt\Auditing\Auditable;


    protected $table = "users_blocks";


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */  
    
	public function getId() {
		return $this->id;
	}

    
    /**
     * @return HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'users_id');
    }

}