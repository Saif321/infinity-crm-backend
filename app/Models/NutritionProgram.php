<?php

namespace App\Models;

use App\Models\Gallery;
use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @OA\Schema(
 *   schema="NutritionProgram",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/NutritionProgram"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string", description=""),
 *           @OA\Property(property="description", type="string", description=""),
 *           @OA\Property(property="image_id", type="integer"),
 *           @OA\Property(property="video_id", type="integer"),
 *           @OA\Property(property="program_length", type="integer", description=""),
 *           @OA\Property(property="meals_per_day", type="integer", description=""),
 *           @OA\Property(property="like_count", type="integer", description=""),
 *           @OA\Property(property="review_count", type="integer", description=""),
 *           @OA\Property(property="avg_rating", type="integer", description=""),
 *           @OA\Property(property="is_published", type="boolean", description=""),
 *           @OA\Property(property="is_free", type="boolean", description=""),
 *       )
 *   }
 * )
 * @OA\Schema(
 *   schema="NutritionProgramTranslation",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/NutritionProgramTranslation"),
 *       @OA\Schema(
 *           @OA\Property(property="language_id", type="integer"),
 *           @OA\Property(property="name", type="string", description=""),
 *           @OA\Property(property="description", type="string", description=""),
 *           @OA\Property(property="image_id", type="integer"),
 *           @OA\Property(property="video_id", type="integer"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="NutritionProgramRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/NutritionProgramRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string", description=""),
 *           @OA\Property(property="description", type="string", description=""),
 *           @OA\Property(property="image_id", type="integer"),
 *           @OA\Property(property="video_id", type="integer"),
 *           @OA\Property(property="program_length", type="integer", description=""),
 *           @OA\Property(property="meals_per_day", type="integer", description=""),
 *           @OA\Property(property="is_published", type="boolean", description=""),
 *           @OA\Property(property="is_free", type="boolean", description=""),
 *           @OA\Property(property="goals", type="array", @OA\Items(type="integer", format="int32")),
 *           @OA\Property(
 *             property="translations",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/NutritionProgramTranslation"),
 *       ),
 *     )
 *   }
 * )
 *
 * @property int $id
 * @property boolean $is_free
 */
class NutritionProgram extends Model implements Auditable {
	use TranslatableModel;
	use \OwenIt\Auditing\Auditable;

	protected $fillable = [
		'name',
		'description',
		'like_count',
		'review_count',
		'avg_rating',
		'is_published',
		'is_free',
		'image_id',
		'video_id',
		'program_length',
		'meals_per_day'
	];

	protected $casts = [
		'is_published' => 'boolean',
		'is_free'      => 'boolean',
		'updated_at'   => 'datetime:c',
		'created_at'   => 'datetime:c'
	];

	public function goals() {
		return $this->belongsToMany( Goal::class, 'goal_nutrition_program_items', 'nutrition_program_id', 'goal_id' );
	}

	public function image() {
		return $this->hasOne( Gallery::class, 'id', 'image_id' );
	}

	public function video() {
		return $this->hasOne( Gallery::class, 'id', 'video_id' );
	}

	public function getNumberOfReviews() {
		return $this->getAttribute( 'review_count' );
	}

	public function getAverageRating() {
		return $this->getAttribute( 'avg_rating' );
	}

}
