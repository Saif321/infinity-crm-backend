<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;

class UserProgramItem extends Model
{
    //
    use TranslatableModel;

    protected $fillable = ['is_locked', 'order_day', 'user_training_id', 'started_at'];
}
