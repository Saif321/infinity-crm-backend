<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 *  @OA\Schema(
 *   schema="VerificationCode",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/VerificationCode"),
 *	     @OA\Schema(
 *           @OA\Property(property="user_id", type="integer"),
 *           @OA\Property(property="destination", type="string"),
 *           @OA\Property(property="code", type="string"),
 *           @OA\Property(property="is_used", type="boolean"),
 *	     )
 *   }
 * )
 *
 * @property integer $user_id
 * @property string $destination
 * @property string $code
 * @property bool $is_used
 */
class VerificationCode extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'destination', 'code', 'is_used'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
