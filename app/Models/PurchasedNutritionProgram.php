<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PurchasedNutritionProgram
 * @package App\Models
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $nutrition_program_id
 * @property integer $user_package_id
 */
class PurchasedNutritionProgram extends Model
{
    use TranslatableModel;

    protected $fillable = ['user_id', 'nutrition_program_id', 'user_package_id'];

    protected $casts = [
        'updated_at' => 'datetime:c',
        'created_at' => 'datetime:c'
    ];
}
