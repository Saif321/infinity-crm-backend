<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TranslatableModel;

class GoalBasicQuestionnaireItem extends Model {
	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];
}