<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramLevel extends Model {
	protected $fillable = [ 'program_id', 'level_id' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];

	public function level() {
		return $this->hasOne( Level::class, 'id', 'level_id' );
	}
}
