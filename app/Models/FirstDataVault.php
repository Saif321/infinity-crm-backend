<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 *
 * @property int $id
 * @property string $hosted_id;
 * @property string $credit_card_number;
 * @property User $user
 */
class FirstDataVault extends Model
{
    protected $fillable = ['hosted_id', 'credit_card_number'];

    protected $casts = [
        'updated_at' => 'datetime:c',
        'created_at' => 'datetime:c'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class, 'payment_id', 'id');
    }
}
