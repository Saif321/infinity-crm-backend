<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *   schema="BasicQuestionnaireRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/BasicQuestionnaireRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="level_id", type="integer"),
 *           @OA\Property(property="goals", type="array", @OA\Items(type="integer", format="int32")),
 *           @OA\Property(property="exercise_environment_id", type="integer"),
 *           @OA\Property(property="gender", type="string"),
 *           @OA\Property(property="date_of_birth", type="string"),
 *           @OA\Property(property="height_in_centimeters", type="integer"),
 *           @OA\Property(property="weight_in_kilograms", type="integer"),
 *       )
 *   }
 * )
 *
 * @property Goal[]|Collection $goals
 * @property ExerciseEnvironment $environment
 */
class BasicQuestionnaire extends Model {
	protected $with = [ 'goals', 'environment' ];

	protected $fillable = [
		'user_id',
		'weight_in_kilograms',
		'height_in_centimeters',
		'exercise_environment_id',
		'goal_id'
	];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];

	public function goals() {
		return $this->belongsToMany( Goal::class, 'goal_basic_questionnaire_items', 'basic_questionnaire_id',
			'goal_id' );
	}

	public function environment() {
		return $this->belongsTo( ExerciseEnvironment::class, 'exercise_environment_id', 'id' );
	}
}
