<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;

class UserExerciseSetItem extends Model {
	use TranslatableModel;

	protected $fillable = [
		'type',
		'duration_in_seconds',
		'strength_percentage',
		'number_of_reps',
		'exercise_id',
		'weight_in_kilograms',
		'is_finished',
		'finished_at',
		'order'
	];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];
}
