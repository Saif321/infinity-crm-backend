<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


/**
 * @OA\Schema(
 *   schema="MealGroup",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/MealGroup"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="eating_time", type="string"),
 *     )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="MealGroupTranslation",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/MealGroupTranslation"),
 *       @OA\Schema(
 *           @OA\Property(property="language_id", type="integer"),
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="eating_time", type="string"),
 *        )
 *     }
 *   )
 *
 * @OA\Schema(
 *   schema="MealGroupRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/MealGroupRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="eating_time", type="string"),
 *           @OA\Property(
 *             property="translations",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/MealGroupTranslation"),
 *       ),
 *     )
 *   }
 * )
 */
class MealGroup extends Model implements Auditable {
	//
	use TranslatableModel;
	use \OwenIt\Auditing\Auditable;

	protected $fillable = [ 'name', 'eating_time' ];

	protected $casts = [ 'updated_at' => 'datetime:c', 'created_at' => 'datetime:c' ];
}
