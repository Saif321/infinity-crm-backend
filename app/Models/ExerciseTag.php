<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;

class ExerciseTag extends Model
{
    //
    use TranslatableModel;

    protected $fillable = ['name'];

}
