<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 *  @OA\Schema(
 *   schema="Ingredient",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/Ingredient"),
 *       @OA\Schema(
 *           @OA\Property(property="description", type="string"),
 *     )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="IngredientTranslation",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/IngredientTranslation"),
 *       @OA\Schema(
 *           @OA\Property(property="language_id", type="integer"),
 *           @OA\Property(property="description", type="string"),
 *        )
 *     }
 *   )
 *
 * @OA\Schema(
 *   schema="IngredientRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/IngredientRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(
 *             property="translations",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/IngredientTranslation"),
 *       ),
 *     )
 *   }
 * )
 */

class Ingredient extends Model implements Auditable
{
    //
    use TranslatableModel;
    use \OwenIt\Auditing\Auditable;

    protected $fillable = ['description'];
}
