<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRound extends Model {
	protected $fillable = [ 'user_exercise_set_id', 'order' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];
}
