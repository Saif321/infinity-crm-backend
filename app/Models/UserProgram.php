<?php

namespace App\Models;

use App\Models\Gallery;
use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserProgram
 * @package App\Models
 *
 * @property int $id
 * @property int $name
 * @property int $object_id
 * @property boolean $is_free
 * @property PurchasedProgram $purchasedProgram
 */
class UserProgram extends Model {
	//
	use TranslatableModel;

	protected $fillable = [
		'is_published',
		'name',
		'level_id',
		'description',
		'video_id',
		'image_id',
		'like_count',
		'review_count',
		'workout_days_per_week',
		'program_length',
		'comment_count',
		'avg_rating',
		'exercise_environment_id',
		'gender'
	];

	protected $casts = [
		'is_published' => 'boolean',
		'is_free'      => 'boolean',
		'updated_at'   => 'datetime:c',
		'created_at'   => 'datetime:c'
	];

	public function image() {
		return $this->hasOne( Gallery::class, 'id', 'image_id' );
	}

	public function video() {
		return $this->hasOne( Gallery::class, 'id', 'video_id' );
	}

	public function exerciseEnvironments() {
		return $this->belongsToMany( ExerciseEnvironment::class, 'environment_user_program_items', 'user_program_id',
			'environment_id' );
	}

    public function purchasedProgram()
    {
        return $this->belongsTo(PurchasedProgram::class, 'id', 'user_program_id');
    }
}
