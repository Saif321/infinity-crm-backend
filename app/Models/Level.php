<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @OA\Schema(
 *   schema="Level",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/Level"),
 *       @OA\Schema(
 *           @OA\Property(property="id", type="integer", description=""),
 *           @OA\Property(property="name", type="string", description=""),
 *           @OA\Property(property="description", type="string", description=""),
 *           @OA\Property(property="order", type="integer", description=""),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="LevelTranslation",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/LevelTranslation"),
 *       @OA\Schema(
 *           @OA\Property(property="language_id", type="integer"),
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="description", type="string", description=""),
 *           @OA\Property(property="order", type="integer", description=""),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="LevelRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/LevelRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(
 *             property="translations",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/LevelTranslation")
 *         ),
 *       )
 *   }
 * )
 */
class Level extends Model implements Auditable {
	use TranslatableModel;
	use \OwenIt\Auditing\Auditable;

	protected $fillable = [ 'name', 'description', 'order' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];
}
