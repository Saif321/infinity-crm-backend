<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TranslatableModel;
use Illuminate\Support\Facades\Storage;

class Meat extends Model {
	//
	use TranslatableModel;

	protected $fillable = [ 'name', 'image', 'order' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];

	public function getImageAttribute() {
		if ( ! array_key_exists( 'image', $this->attributes ) || empty( $this->attributes['image'] ) ) {
			return '';
		}

		if ( preg_match( '/^http(s|)/is', $this->attributes['image'] ) ) {
			return $this->attributes['image'];
		}

		return ( isset( $_SERVER['HTTPS'] ) ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . Storage::url( $this->attributes['image'] );
	}
}
