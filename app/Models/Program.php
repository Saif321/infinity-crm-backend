<?php

namespace App\Models;

use App\Models\Gallery;
use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @OA\Schema(
 *   schema="Program",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/Program"),
 *       @OA\Schema(
 *           @OA\Property(property="id", type="integer", description=""),
 *           @OA\Property(property="exercise_environment_id", type="integer", description=""),
 *           @OA\Property(property="name", type="string", description=""),
 *           @OA\Property(property="description", type="string", description=""),
 *           @OA\Property(property="program_length_in_weeks", type="integer", description=""),
 *           @OA\Property(property="workout_days_per_week", type="integer", description=""),
 *           @OA\Property(property="image_id", type="integer", description=""),
 *           @OA\Property(property="video_id", type="integer", description=""),
 *           @OA\Property(property="like_count", type="integer", description=""),
 *           @OA\Property(property="review_count", type="integer", description=""),
 *           @OA\Property(property="avg_rating", type="integer", description=""),
 *           @OA\Property(property="is_published", type="integer", description=""),
 *           @OA\Property(property="is_free", type="integer", description=""),
 *           @OA\Property(property="type", type="string", enum={"image", "video"}),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="ProgramDetails",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/ProgramDetails"),
 *       @OA\Schema(
 *           @OA\Property(property="id", type="integer", description=""),
 *           @OA\Property(property="object_id", type="integer", description=""),
 *           @OA\Property(property="language_id", type="integer", description=""),
 *           @OA\Property(property="exercise_environment_id", type="integer", description=""),
 *           @OA\Property(property="name", type="string", description=""),
 *           @OA\Property(property="description", type="string", description=""),
 *           @OA\Property(property="program_length_in_weeks", type="integer", description=""),
 *           @OA\Property(property="workout_days_per_week", type="integer", description=""),
 *           @OA\Property(property="video_id", type="integer", description=""),
 *           @OA\Property(property="image_id", type="integer", description=""),
 *           @OA\Property(property="comment_count", type="integer", description=""),
 *           @OA\Property(property="like_count", type="integer", description=""),
 *           @OA\Property(property="review_count", type="integer", description=""),
 *           @OA\Property(property="avg_rating", type="integer", description=""),
 *           @OA\Property(property="is_published", type="boolean", description=""),
 *           @OA\Property(property="is_free", type="boolean", description=""),
 *           @OA\Property(property="level", type="array", @OA\Items(ref="#/components/schemas/Level")),
 *           @OA\Property(property="image", type="array", @OA\Items(ref="#/components/schemas/Gallery")),
 *           @OA\Property(property="comments", type="array", @OA\Items(ref="#/components/schemas/Comment")),
 *           @OA\Property(property="is_liked", type="integer", description=""),
 *           @OA\Property(property="weeks", type="array", @OA\Items(ref="#/components/schemas/ProgramWeek")),
 *           @OA\Property(property="levels", type="array", @OA\Items(ref="#/components/schemas/Level")),
 *           @OA\Property(property="type", type="string", enum={"image", "video"}),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="ProgramTranslation",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/ProgramTranslation"),
 *       @OA\Schema(
 *           @OA\Property(property="language_id", type="integer"),
 *           @OA\Property(property="name", type="string", description=""),
 *           @OA\Property(property="description", type="string", description=""),
 *           @OA\Property(property="image_id", type="integer", description=""),
 *           @OA\Property(property="video_id", type="integer", description=""),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="ProgramRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/ProgramRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="id", type="integer", description=""),
 *           @OA\Property(property="exercise_environment_id", type="integer", description=""),
 *           @OA\Property(property="name", type="string", description=""),
 *           @OA\Property(property="description", type="string", description=""),
 *           @OA\Property(property="program_length_in_weeks", type="integer", description=""),
 *           @OA\Property(property="workout_days_per_week", type="integer", description=""),
 *           @OA\Property(property="image_id", type="integer", description=""),
 *           @OA\Property(property="video_id", type="integer", description=""),
 *           @OA\Property(property="is_published", type="boolean", description=""),
 *           @OA\Property(property="is_free", type="boolean", description=""),
 *           @OA\Property(property="goal_ids", type="array", @OA\Items(type="integer", format="int32")),
 *           @OA\Property(property="level_ids", type="array", @OA\Items(type="integer", format="int32")),
 *           @OA\Property(property="type", type="string", enum={"image", "video"}),
 *           @OA\Property(
 *             property="translations",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/ProgramTranslation")
 *         ),
 *       )
 *   }
 * )
 *
 * @property integer $id
 * @property boolean $is_free
 *
 */
class Program extends Model implements Auditable {
	//
	use TranslatableModel;
	use \OwenIt\Auditing\Auditable;

	protected $fillable = [
		'is_published',
		'is_free',
		'name',
		'level_id',
		'description',
		'video_id',
		'image_id',
		'like_count',
		'review_count',
		'workout_days_per_week',
		'program_length_in_weeks',
		'comment_count',
		'avg_rating',
		'gender',
		'previous_level_program_id'
	];

	protected $casts = [ 'is_published' => 'boolean', 'is_free' => 'boolean' ];

	public function image() {
		return $this->hasOne( Gallery::class, 'id', 'image_id' );
	}

	public function video() {
		return $this->hasOne( Gallery::class, 'id', 'video_id' );
	}

	public function level() {
		return $this->hasOne( Level::class, 'id', 'level_id' );
	}

	public function exerciseEnvironments() {
		return $this->belongsToMany( ExerciseEnvironment::class, 'environment_program_items', 'program_id',
			'environment_id' );
	}

	public function goals() {
		return $this->belongsToMany( Goal::class, 'goal_program_items', 'program_id', 'goal_id' );
	}

	public function levels() {
		return $this->belongsToMany( Level::class, 'program_level_items', 'program_id', 'level_id' );
	}

	public function programWeeks() {
		$queue = [ 'mail1', 'mail2' ];

		return $this->belongsToMany( ProgramWeek::class, 'id', 'program_id' );
	}

}
