<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NutritionProgramItem extends Model {
	protected $fillable = [ 'nutrition_program_meal_group_id', 'meal_id', 'order' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];

	public function meal() {
		return $this->belongsTo( Meal::class );
	}
}
