<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *   schema="PackagePaymentMethod",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/PackagePaymentMethod"),
 *       @OA\Schema(
 *           @OA\Property(property="id", type="integer"),
 *           @OA\Property(property="package_id", type="integer"),
 *           @OA\Property(property="payment_method", type="string", enum={"CC", "AP", "GP", "SMS"}),
 *       )
 *   }
 * )
 *
 * Class PackagePaymentMethod
 * @package App\Models
 *
 * @property int $id
 * @property string $payment_method
 * @property Package $package
 * @property string $price
 * @property string $currency_code
 * @property string $created_at
 * @property string $update_at
 */
class PackagePaymentMethod extends Model
{

    const PAYMENT_METHOD_CREDIT_CARD = 'CC';
    const PAYMENT_METHOD_APPLE_PAY = 'AP';
    const PAYMENT_METHOD_GOOGLE_PAY = 'GP';
    const PAYMENT_METHOD_SMS = 'SMS';

    protected $fillable = ['payment_method', 'price', 'currency_code'];

    public function package()
    {
        return $this->hasOne(Package::class, 'id', 'package_id');
    }
}
