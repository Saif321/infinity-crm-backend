<?php

namespace App\Models;

use App\Models\Gallery;
use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;

class UserProgramWeekItem extends Model {
	protected $fillable = [
		'user_program_week_id',
		'user_training_id',
		'is_locked',
		'is_rest_day',
		'order',
		'started_at'
	];

	protected $casts = [ 'started_at' => 'datetime:c', 'updated_at' => 'datetime:c', 'created_at' => 'datetime:c' ];
}
