<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *   schema="PurchasedProgram",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/PurchasedProgram"),
 *       @OA\Schema(
 *           @OA\Property(property="id", type="integer", description=""),
 *           @OA\Property(property="user_id", type="integer", description=""),
 *           @OA\Property(property="user_program_id", type="integer", description=""),
 *           @OA\Property(property="source_program_id", type="integer", description=""),
 *           @OA\Property(property="user_package_id", type="integer", description=""),
 *       )
 *   }
 * )
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $user_program_id
 * @property integer $source_program_id
 * @property integer $user_package_id
 * @property integer $updated_at
 * @property integer $created_at
 *
 * @property Program $sourceProgram
 * @property UserProgram $userProgram
 */
class PurchasedProgram extends Model
{
    protected $fillable = ['user_id', 'user_program_id', 'source_program_id', 'user_package_id'];

    protected $casts = [
        'updated_at' => 'datetime:c',
        'created_at' => 'datetime:c'
    ];

    public function sourceProgram()
    {
        return $this->belongsTo(Program::class, 'source_program_id', 'id');
    }

    public function userProgram()
    {
        return $this->belongsTo(UserProgram::class, 'user_program_id', 'id');
    }
}
