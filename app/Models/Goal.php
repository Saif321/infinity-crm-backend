<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @OA\Schema(
 *   schema="Goal",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/Goal"),
 *       @OA\Schema(
 *           @OA\Property(property="order", type="integer"),
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="description", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="GoalTranslation",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/GoalTranslation"),
 *       @OA\Schema(
 *           @OA\Property(property="language_id", type="integer"),
 *           @OA\Property(property="order", type="integer"),
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="description", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="GoalRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/GoalRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(
 *             property="translations",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/GoalTranslation")
 *         ),
 *       )
 *   }
 * )
 */
class Goal extends Model implements Auditable {
	//
	use TranslatableModel;
	use \OwenIt\Auditing\Auditable;

	protected $fillable = [ 'user_program_id', 'order', 'name', 'description' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];

	public function basicQuestionaires() {
		return $this->belongsToMany( BasicQuestionnaire::class, 'goal_basic_questionnaire_items', 'goal_id',
			'basic_questionnaire_id' );
	}
}
