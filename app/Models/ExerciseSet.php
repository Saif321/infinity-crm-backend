<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;

class ExerciseSet extends Model {
	//
	use TranslatableModel;

	protected $fillable = [ 'training_id', 'name', 'description', 'type', 'duration_in_seconds', 'order' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];

}
