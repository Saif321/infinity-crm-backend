<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WeekItem
 * @package App\Models
 *
 * @property int $order
 * @property bool $is_locked
 * @property bool $is_rest_day
 */
class WeekItem extends Model {
	use TranslatableModel;

	protected $fillable = [
		'program_week_id',
		'training_id',
		'is_locked',
		'is_rest_day',
		'order'
	];

	protected $casts = [
		'is_locked'   => 'boolean',
		'is_rest_day' => 'boolean',
		'updated_at'  => 'datetime:c',
		'created_at'  => 'datetime:c'
	];

	public function training() {
		return $this->belongsTo( Training::class );
	}
}
