<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *   schema="Training",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/Training"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(property="number_of_calories", type="integer"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="TrainingTranslation",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/TrainingTranslation"),
 *       @OA\Schema(
 *           @OA\Property(property="language_id", type="integer"),
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(property="number_of_calories", type="integer"),
 *
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="TrainingRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/TrainingRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="parent_id", type="integer"),
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(property="number_of_calories", type="integer"),
 *           @OA\Property(property="training_ids", type="array", @OA\Items(type="integer", format="int32")),
 *           @OA\Property(
 *             property="translations",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/TrainingTranslation")
 *         ),
 *       )
 *   }
 * )
 */
class Training extends Model {
	//
	use TranslatableModel;

	protected $fillable = [ 'name', 'description', 'number_of_calories', 'parent_id' ];

	protected $casts = [
		'is_rest_day' => 'boolean',
		'is_locked'   => 'boolean',
		'updated_at'  => 'datetime:c',
		'created_at'  => 'datetime:c'
	];

	public function exerciseSets() {
		return $this->belongsToMany( ExerciseSet::class, 'id', 'training_id' );
	}
}
