<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *   schema="ProgramWeek",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/ProgramWeek"),
 *       @OA\Schema(
 *           @OA\Property(property="id", type="integer", description=""),
 *           @OA\Property(property="program_id", type="integer", description=""),
 *           @OA\Property(property="order", type="integer", description=""),
 *           @OA\Property(property="name", type="string", description=""),
 *       )
 *   }
 * )
 */
class ProgramWeek extends Model {
	protected $fillable = [ 'program_id', 'order' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];
}
