<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NutritionProgramMealGroup extends Model {
	protected $fillable = [ 'nutrition_program_id', 'meal_group_id', 'order' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];

	public function mealGroup() {
		return $this->belongsTo( MealGroup::class );
	}
}
