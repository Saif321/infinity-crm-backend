<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;

class UserExercise extends Model {
	//
	use TranslatableModel;

	protected $fillable = [ 'name', 'description', 'type', 'duration_in_seconds', 'order', 'user_round_id' ];

	protected $casts = [ 'published_at' => 'datetime:c', 'updated_at' => 'datetime:c', 'created_at' => 'datetime:c' ];

}
