<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @OA\Schema(
 *   schema="ExerciseEnvironment",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/ExerciseEnvironment"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(property="order", type="integer"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="ExerciseEnvironmentTranslation",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/ExerciseEnvironmentTranslation"),
 *       @OA\Schema(
 *           @OA\Property(property="language_id", type="integer"),
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="description", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="ExerciseEnvironmentRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/ExerciseEnvironmentRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(property="order", type="integer"),
 *           @OA\Property(
 *             property="translations",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/ExerciseEnvironmentTranslation")
 *         ),
 *       )
 *   }
 * )
 */
class ExerciseEnvironment extends Model implements Auditable {
	//
	use TranslatableModel;
	use \OwenIt\Auditing\Auditable;

	protected $fillable = [ 'name', 'order', 'description' ];

	protected $casts = [
		'updated_at'   => 'datetime:c',
		'created_at'   => 'datetime:c'
	];

}
