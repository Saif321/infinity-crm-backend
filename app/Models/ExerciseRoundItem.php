<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TranslatableModel;

class ExerciseRoundItem extends Model {
	//
	use TranslatableModel;

	protected $fillable = [
		'round_id',
		'type',
		'duration_in_seconds',
		'strength_percentage',
		'number_of_reps',
		'exercise_id',
		'order'
	];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];

	public function exercise() {
		return $this->belongsTo( Exercise::class );
	}
}
