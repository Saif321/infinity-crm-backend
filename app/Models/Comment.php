<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @OA\Schema(
 *   schema="Comment",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/Comment"),
 *       @OA\Schema(
 *           @OA\Property(property="id", type="integer"),
 *           @OA\Property(property="user_id", type="integer"),
 *           @OA\Property(property="object_id", type="integer"),
 *           @OA\Property(property="object_type", type="string"),
 *           @OA\Property(property="comment", type="string"),
 *           @OA\Property(property="is_published", type="integer"),
 *           @OA\Property(property="published_at", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="CommentRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/CommentRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="object_id", type="integer"),
 *           @OA\Property(property="object_type", type="string", enum={"Blog", "Program", "NutritionProgram", "Comment"}),
 *           @OA\Property(property="comment", type="string"),
 *       )
 *   }
 * )
 */
class Comment extends Model implements Auditable {
	use \OwenIt\Auditing\Auditable;

	protected $fillable = [ 'user_id', 'object_id', 'object_type', 'comment', 'like_count' ];

	protected $with = [ 'user' ];

	protected $casts = [
		'is_published' => 'boolean',
		'published_at' => 'datetime:c',
		'updated_at'   => 'datetime:c',
		'created_at'   => 'datetime:c'
	];

	public function user() {
		return $this->belongsTo( User::class );
	}

}
