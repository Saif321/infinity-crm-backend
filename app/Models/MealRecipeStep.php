<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *   schema="MealRecipeStep",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/MealRecipeStep"),
 *       @OA\Schema(
 *           @OA\Property(property="order", type="integer"),
 *           @OA\Property(property="description", type="string"),
 *     )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="MealRecipeStepTranslation",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/MealRecipeStepTranslation"),
 *       @OA\Schema(
 *           @OA\Property(property="language_id", type="integer"),
 *           @OA\Property(property="description", type="string"),
 *        )
 *     }
 *   )
 *
 * @OA\Schema(
 *   schema="MealRecipeStepRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/MealRecipeStepRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="order", type="integer"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(
 *             property="translations",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/MealRecipeStepTranslation"),
 *       ),
 *     )
 *   }
 * )
 */
class MealRecipeStep extends Model {
	//
	use TranslatableModel;

	protected $fillable = [ 'meal_id', 'description', 'order' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];
}
