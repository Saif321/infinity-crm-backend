<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *   schema="DeactivatedProgram",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/DeactivatedProgram"),
 *       @OA\Schema(
 *           @OA\Property(property="id", type="integer", description=""),
 *           @OA\Property(property="user_id", type="integer", description=""),
 *           @OA\Property(property="user_program_id", type="integer", description=""),
 *           @OA\Property(property="source_program_id", type="integer", description=""),
 *       )
 *   }
 * )
 */
class DeactivatedProgram extends Model
{
    protected $fillable = [ 'user_id', 'user_program_id', 'source_program_id', 'user_package_id' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];
}
