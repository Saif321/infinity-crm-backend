<?php

namespace App\Models;


use App\Models\Gallery;
use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @OA\Schema(
 *   schema="Meal",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/Meal"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(property="image_id", type="integer"),
 *           @OA\Property(property="video_id", type="integer"),
 *           @OA\Property(property="preparation_time_in_minutes", type="integer"),
 *           @OA\Property(property="number_of_calories", type="integer"),
 *           @OA\Property(property="fat", type="integer"),
 *           @OA\Property(property="proteins", type="integer"),
 *           @OA\Property(property="carbohydrates", type="integer"),
 *
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="MealTranslation",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/MealTranslation"),
 *       @OA\Schema(
 *           @OA\Property(property="language_id", type="integer"),
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(property="image_id", type="integer"),
 *           @OA\Property(property="video_id", type="integer"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="MealRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/MealRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="name", type="string"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(property="image_id", type="integer"),
 *           @OA\Property(property="video_id", type="integer"),
 *           @OA\Property(property="preparation_time_in_minutes", type="integer"),
 *           @OA\Property(property="number_of_calories", type="integer"),
 *           @OA\Property(property="fat", type="integer"),
 *           @OA\Property(property="proteins", type="integer"),
 *           @OA\Property(property="carbohydrates", type="integer"),
 *           @OA\Property(property="ingredients", type="array", @OA\Items(ref="#/components/schemas/MealIngredientRequest")),
 *           @OA\Property(property="steps", type="array", @OA\Items(ref="#/components/schemas/MealRecipeStepRequest")),
 *           @OA\Property(
 *             property="translations",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/MealTranslation")
 *         ),
 *       )
 *   }
 * )
 */
class Meal extends Model implements Auditable {
	//

	use TranslatableModel;
	use \OwenIt\Auditing\Auditable;

	protected $fillable = [
		'name',
		'is_published',
		'description',
		'preparation_time_in_minutes',
		'number_of_calories',
		'fat',
		'proteins',
		'carbohydrates',
		'video_id',
		'image_id'
	];

	protected $casts = [ 'is_published' => 'boolean', 'updated_at' => 'datetime:c', 'created_at' => 'datetime:c' ];

	public function image() {
		return $this->hasOne( Gallery::class, 'id', 'image_id' );
	}

	public function video() {
		return $this->hasOne( Gallery::class, 'id', 'video_id' );
	}

	public function ingredients() {
		$foreignKey = preg_match( '/^__/is', $this->getTable() ) ? 'object_id' : 'id';

		return $this->belongsTo( MealIngredient::class, $foreignKey, 'meal_id' );
	}

	public function recipeSteps() {
		$foreignKey = preg_match( '/^__/is', $this->getTable() ) ? 'object_id' : 'id';

		return $this->belongsTo( MealRecipeStep::class, $foreignKey, 'meal_id' );
	}
}
