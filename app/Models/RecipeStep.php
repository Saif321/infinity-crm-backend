<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *   schema="RecipeStep",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/RecipeStep"),
 *       @OA\Schema(
 *           @OA\Property(property="order", type="integer"),
 *           @OA\Property(property="description", type="string"),
 *     )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="RecipeStepTranslation",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/RecipeStepTranslation"),
 *       @OA\Schema(
 *           @OA\Property(property="language_id", type="integer"),
 *           @OA\Property(property="order", type="integer"),
 *           @OA\Property(property="description", type="string"),
 *        )
 *     }
 *   )
 *
 * @OA\Schema(
 *   schema="RecipeStepRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/RecipeStepRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="order", type="integer"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(
 *             property="translations",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/RecipeStepTranslation"),
 *       ),
 *     )
 *   }
 * )
 */
class RecipeStep extends Model {
	use TranslatableModel;

	protected $fillable = [ 'order', 'description' ];
}
