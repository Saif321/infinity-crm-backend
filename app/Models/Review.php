<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @OA\Schema(
 *   schema="Review",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/Review"),
 *       @OA\Schema(
 *           @OA\Property(property="id", type="integer"),
 *           @OA\Property(property="object_id", type="integer"),
 *           @OA\Property(property="object_type", type="string"),
 *           @OA\Property(property="user_id", type="integer"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(property="rating", type="integer"),
 *           @OA\Property(property="is_published", type="integer"),
 *           @OA\Property(property="published_at", type="string"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="ReviewRequest",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/ReviewRequest"),
 *       @OA\Schema(
 *           @OA\Property(property="object_id", type="integer"),
 *           @OA\Property(property="object_type", type="string", description="Program, NutritionProgram etc"),
 *           @OA\Property(property="description", type="string"),
 *           @OA\Property(property="rating", type="integer"),
 *       )
 *   }
 * )
 */
class Review extends Model implements Auditable {
	use \OwenIt\Auditing\Auditable;

	protected $fillable = [ 'object_id', 'object_type', 'user_id', 'description', 'rating' ];

	protected $casts = [
		'is_published' => 'boolean',
		'published_at' => 'datetime:c',
		'updated_at'   => 'datetime:c',
		'created_at'   => 'datetime:c'
	];

	public function user() {
		return $this->belongsTo( User::class );
	}
}
