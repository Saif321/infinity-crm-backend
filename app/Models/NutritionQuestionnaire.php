<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *   schema="NutritionQuestionnaire",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/NutritionQuestionnaire"),
 *       @OA\Schema(
 *           @OA\Property(property="how_active", type="string"),
 *           @OA\Property(property="typical_day", type="string"),
 *           @OA\Property(property="mine_description", type="string"),
 *           @OA\Property(property="meat_ids", type="array", @OA\Items(type="integer", format="int32")),
 *           @OA\Property(property="fruit_ids", type="array", @OA\Items(type="integer", format="int32")),
 *           @OA\Property(property="vegetable_ids", type="array", @OA\Items(type="integer", format="int32")),
 *       )
 *   }
 * )
 */
class NutritionQuestionnaire extends Model {

	use TranslatableModel;

	protected $fillable = [ 'user_id', 'how_active', 'typical_day', 'mine_description' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];

	public function meats() {
		return $this->belongsToMany( Meat::class, 'nutrition_questionnaire_meat_items', 'nutrition_questionnaire_id',
			'meat_id' );
	}

	public function fruits() {
		return $this->belongsToMany( Fruit::class, 'nutrition_questionnaire_fruit_items', 'nutrition_questionnaire_id',
			'fruit_id' );
	}

	public function vegetables() {
		return $this->belongsToMany( Vegetable::class, 'nutrition_questionnaire_vegetable_items',
			'nutrition_questionnaire_id', 'vegetable_id' );
	}


}
