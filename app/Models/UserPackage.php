<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *   schema="UserPackage",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/UserPackage"),
 *       @OA\Schema(
 *           @OA\Property(property="id", type="integer"),
 *           @OA\Property(property="user_id", type="integer"),
 *           @OA\Property(property="package_id", type="integer"),
 *           @OA\Property(property="valid_until", type="string"),
 *           @OA\Property(property="last_renewal_attempt", type="string"),
 *           @OA\Property(property="renew_attempts", type="integer"),
 *           @OA\Property(property="renew_stopped", type="integer"),
 *           @OA\Property(property="programs_left", type="integer"),
 *           @OA\Property(property="nutrition_programs_left", type="integer"),
 *           @OA\Property(property="free_trial", type="integer"),
 *       )
 *   }
 * )
 *
 * @OA\Schema(
 *   schema="UserPackageReport",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/UserPackageReport"),
 *       @OA\Schema(
 *           @OA\Property(property="user_id", type="integer"),
 *           @OA\Property(property="email", type="string"),
 *           @OA\Property(property="package_name", type="string"),
 *           @OA\Property(property="valid_until", type="string"),
 *           @OA\Property(property="last_renewal_attempt", type="string"),
 *           @OA\Property(property="renew_attempts", type="integer"),
 *           @OA\Property(property="renew_stopped", type="integer"),
 *           @OA\Property(property="payment_method", type="string"),
 *           @OA\Property(property="charged_value", type="string"),
 *           @OA\Property(property="currency_code", type="string"),
 *       )
 *   }
 * )
 *
 * Class UserPackage
 * @package App\Models
 *
 *
 * @property int $id
 * @property Package $package
 * @property User $user
 * @property Payment $payment
 * @property Collection $purchasedPrograms
 * @property Collection $purchasedNutritionPrograms
 * @property int $renew_attempts
 * @property string $valid_until
 * @property string $last_renewal_attempt
 * @property boolean $renew_stopped
 * @property boolean $free_trial
 * @property string $created_at
 * @property string $update_at
 */
class UserPackage extends Model
{
    protected $casts = [
        'renew_stopped' => 'boolean',
        'free_trial' => 'boolean',
        'last_renewal_attempt' => 'datetime:c',
        'valid_until' => 'datetime:c',
        'updated_at' => 'datetime:c',
        'created_at' => 'datetime:c'
    ];

    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function payment()
    {
        return $this->hasOne(Payment::class, 'user_package_id', 'id');
    }

    public function purchasedPrograms()
    {
        return $this->hasMany(PurchasedProgram::class, 'user_package_id', 'id');
    }

    public function purchasedNutritionPrograms()
    {
        return $this->hasMany(PurchasedNutritionProgram::class, 'user_package_id', 'id');
    }
}
