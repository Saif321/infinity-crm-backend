<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *   schema="UserExerciseRoundItemUsersData",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/UserExerciseRoundItemUsersData"),
 *       @OA\Schema(
 *           @OA\Property(property="user_number_of_reps", type="integer"),
 *           @OA\Property(property="user_strength_kilos", type="integer")
 *       )
 *   }
 * )
 *
 * @property int $user_number_of_reps
 * @property int $user_strength_kilos
 * @property boolean $is_finished
 * @property string $finished_at
 * @property string $type
 */
class UserExerciseRoundItem extends Model {
	//
	protected $fillable = [
		'user_round_id',
		'type',
		'duration_in_seconds',
		'strength_percentage',
		'number_of_reps',
		'exercise_id',
		'order',
		'is_finished',
		'finished_at',
		'started_at',
		'user_number_of_reps',
		'user_strength_kilos'
	];

	protected $casts = [
		'is_finished' => 'boolean',
		'started_at'  => 'datetime:c',
		'finished_at' => 'datetime:c',
		'updated_at'  => 'datetime:c',
		'created_at'  => 'datetime:c'
	];

	public function exercise() {
		return $this->belongsTo( Exercise::class );
	}
}
