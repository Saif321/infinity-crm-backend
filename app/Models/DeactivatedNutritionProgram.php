<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *   schema="DeactivatedNutritionProgram",
 *   allOf={
 *       @OA\Schema(ref="#/components/schemas/DeactivatedNutritionProgram"),
 *       @OA\Schema(
 *           @OA\Property(property="id", type="integer", description=""),
 *           @OA\Property(property="user_id", type="integer", description=""),
 *           @OA\Property(property="nutrition_program_id", type="integer", description=""),
 *       )
 *   }
 * )
 */
class DeactivatedNutritionProgram extends Model
{
    protected $fillable = [ 'user_id', 'nutrition_program_id', 'user_package_id' ];

	protected $casts = [
		'updated_at' => 'datetime:c',
		'created_at' => 'datetime:c'
	];
}
