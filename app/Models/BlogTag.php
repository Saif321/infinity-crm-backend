<?php

namespace App\Models;

use App\Traits\TranslatableModel;
use Illuminate\Database\Eloquent\Model;

class BlogTag extends Model
{
    //
    use TranslatableModel;

    protected $fillable = ['name'];

    public function blogs()
    {
        return $this->belongsToMany(Blog::class, 'blog_blog_tags', 'tag_id', 'blog_id');
    }
}
