<?php
declare(strict_types=1);

namespace App\Services\Exceptions;

use Exception;
use Throwable;

class ProgramAlreadyPurchasedException extends Exception
{
    public function __construct(
        $message = 'Program is already purchased',
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
