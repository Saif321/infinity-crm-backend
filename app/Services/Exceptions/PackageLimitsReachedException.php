<?php
declare(strict_types=1);

namespace App\Services\Exceptions;

use Exception;
use Throwable;

class PackageLimitsReachedException extends Exception
{
    public function __construct(
        $message = 'Package limits reached, please subscribe to package with higher limits',
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
