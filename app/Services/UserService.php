<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\Follower;
use App\Repositories\UserRepository;
use App\Repositories\UserWeightHistoryRepository;
use App\Repositories\HomeFeedRepository;
use App\Services\Dto\CustomerReport;
use App\Services\Dto\CustomerReportRow;
use App\Services\Dto\UserStatistics;
use App\Services\Dto\WeightChange;
use App\Services\Dto\ProfileInfo;
use App\Services\Program as ProgramService;

class UserService
{
    private $userWeightHistoryRepository;
    private $programService;
    private $userRepository;
    private $homeFeedRepository;

    public function __construct(
        UserWeightHistoryRepository $userWeightHistoryRepository,
        ProgramService $programService,
        UserRepository $userRepository,
        HomeFeedRepository $homeFeedRepository
    ) {
        $this->userWeightHistoryRepository = $userWeightHistoryRepository;
        $this->programService = $programService;
        $this->userRepository = $userRepository;
        $this->homeFeedRepository = $homeFeedRepository;
    }

    /**
     * @param int $userId
     *
     * @return ProfileInfo
     */
    public function getProfileInfo($userId): ProfileInfo
    {
        $profileInfo = new ProfileInfo();
        $profileInfo->posts = $this->homeFeedRepository->where('users_id', $userId)->count();
        $profileInfo->followers = Follower::where('followed_id', $userId)->count();
        $profileInfo->following = Follower::where('follower_id', $userId)->count();

        return $profileInfo;
    }

    /**
     * @param int $userId
     *
     * @return WeightChange
     */
    public function getWeightChange($userId): WeightChange
    {
        $weightChange = new WeightChange();
        $initial = $this->userWeightHistoryRepository->orderBy('created_at', 'asc')->findByField('user_id',
            $userId)->first();
        $current = $this->userWeightHistoryRepository->orderBy('created_at', 'desc')->findByField('user_id',
            $userId)->first();

        $weightChange->initial = $initial ? $initial->weight_in_kilograms : 0;
        $weightChange->current = $current ? $current->weight_in_kilograms : 0;
        $weightChange->difference = abs($weightChange->initial - $weightChange->current);

        return $weightChange;
    }

    /**
     * @param int $userId
     * @return UserStatistics
     */
    public function getUserStatistics($userId): UserStatistics
    {
        $statistics = new UserStatistics();
        $statistics->weight = $this->getWeightChange($userId);
        $statistics->progress = $this->programService->getUserProgress();
        $statistics->profile = $this->getProfileInfo($userId);
        return $statistics;
    }


    public function getCustomerReport($levels, $goals, $environment, $gender, $ageFrom, $ageTo)
    {
        return $this->userRepository->getCustomerReport($levels, $goals, $environment, $gender, $ageFrom, $ageTo);
    }

    public function mapToCustomerReportRow(object $rowData): CustomerReportRow
    {
        $row = new CustomerReportRow();
        $row->first_name = $rowData->first_name;
        $row->last_name = $rowData->last_name;
        $row->level = $rowData->level;
        $row->goals = $rowData->goals;
        $row->environment = $rowData->environment;
        $row->age = $rowData->age;
        $row->gender = $rowData->gender;
        return $row;
    }
}
