<?php
declare( strict_types=1 );

namespace App\Services;

use App\Services\Dto\HomeResponse;

class Home {
	private $programService;
	private $nutritionProgramService;

	public function __construct(
		Program $programService,
		NutritionProgram $nutritionProgramService
	) {
		$this->programService = $programService;
		$this->nutritionProgramService = $nutritionProgramService;
	}

	/**
	 * @return HomeResponse
	 */
	public function getHomeResponse() {
		$response = new HomeResponse();
		$response->programs = $this->programService->getProgramsForHomePage();
		$response->purchased_programs = $this->programService->getPurchasedPrograms();
		$response->nutrition_programs = $this->nutritionProgramService->getProgramsForHomePage();

		return $response;
	}
}
