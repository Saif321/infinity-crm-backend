<?php


namespace App\Services;

use Coderello\SocialGrant\Resolvers\SocialUserResolverInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use Laravel\Socialite\Facades\Socialite;
use Prettus\Repository\Exceptions\RepositoryException;
use Prettus\Validator\Exceptions\ValidatorException;

class SocialUserResolver implements SocialUserResolverInterface
{

    protected $socialAccountService;

    public function __construct(SocialAccountService $socialAccountsService)
    {
        $this->socialAccountService = $socialAccountsService;

    }

    /**
     * @OA\Post(
     *      path="/oauth/token",
     *      description="Generates access and refresh token for authorization",
     *      tags={"Auth"},
     *      @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="grant_type",
     *                     description="Grant Type (password, refresh_token, social)",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="client_id",
     *                     description="Client ID",
     *                      default=3,
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="client_secret",
     *                     description="Client Secret",
     *                      default="J9mvGV3iVcN2NHWqKEb3yOVFsZIokZRzk1SzOxZY",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="username",
     *                     description="E-mail, use with password grant_type",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     description="Password, use with password grant_type",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="refresh_token",
     *                     description="Refresh token from /oauth/token response, use with refresh_token grant_type",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="access_token",
     *                     description="Authorization token from client app, use with social grant_type",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="provider",
     *                     description="Social login provider (Google, Apple), use with social grant_type",
     *                     type="string",
     *                 )
     *             )
     *         )
     *     ),
     *  @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\Schema(
     *             type="object",
     *             @OA\Property(property="token_type", type="string"),
     *             @OA\Property(property="expires_in", type="integer"),
     *             @OA\Property(property="access_token", type="string"),
     *             @OA\Property(property="refresh_token", type="string"),
     *         ),
     *  )
     *)
     */

     /** @param string $provider
     * @param string $accessToken
     *
     * @return Authenticatable|null
     * @throws RepositoryException
     * @throws ValidatorException
     * @property integer $id
     * @property string $first_name
     * @property string $last_name
     * @property string $email
     * @property string $password
     *
     * Resolve user by provider credentials.
     *
     */
    public function resolveUserByProviderCredentials(string $provider, string $accessToken): ?Authenticatable
    {

        if ($provider == 'apple') {
            $apple = Socialite::driver('sign-in-with-apple');
            $accessToken = $apple->getAccessTokenResponse($accessToken)['id_token'];
            $providerUser = $apple->userFromToken($accessToken);
            return $this->socialAccountService->findOrCreate($providerUser, $provider);
        }
        if ($provider =='google') {
            $driver = Socialite::driver($provider);
            $accessToken = $driver->getAccessTokenResponse($accessToken);
            $providerUser = $driver->userFromToken($accessToken['access_token']);
            return $this->socialAccountService->findOrCreate($providerUser, $provider);
        }
        if ($provider == 'facebook' || $provider == 'Facebook') {
            try {
                $facebook = Socialite::driver($provider);
                $providerUser = $facebook->userFromToken($accessToken);
                return $this->socialAccountService->findOrCreate($providerUser, $provider);
            } catch(Exception $e) {
                return response()->json(array("message" => "Invalid login."));
            }
        }

        return null;
    }
}
