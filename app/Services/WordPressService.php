<?php

namespace App\Services;

use App\Models\Gallery;
use App\Models\Language;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\ImageManagerStatic as Image;

class WordPressService
{
    const POST_ROUTE = '/wp-json/wp/v2/posts';
    const MEDIA_ROUTE = '/wp-json/wp/v2/media';
    private $url;
    private $username;
    private $password;
    private $client;
    private $blogCategoryId;
    private $nutritionCategoryId;
    private $workoutProgramsCategoryId;
    private $uploadImageWidth;

    public function __construct()
    {
        $this->url = config('infinity-fit.wordpress.url');
        $this->username = config('infinity-fit.wordpress.username');
        $this->password = config('infinity-fit.wordpress.password');
        $this->blogCategoryId = config('infinity-fit.wordpress.blog_category_id');
        $this->nutritionCategoryId = config('infinity-fit.wordpress.nutrition_category_id');
        $this->workoutProgramsCategoryId = config('infinity-fit.wordpress.workout_program_category_id');
        $this->uploadImageWidth = config('infinity-fit.wordpress.upload_image_width');
        $this->client = new Client([
            'headers' => ['Accept: application/json'],
            'auth' => [$this->username, $this->password],
        ]);
    }

    public function createPost(array $post, string $categoryName = 'blog', bool $published = false)
    {
        $categoryId = '';
        if ($categoryName == 'blog') {
            $categoryId = $this->blogCategoryId;
        }
        if ($categoryName == 'nutrition') {
            $categoryId = $this->nutritionCategoryId;
        }
        if ($categoryName == 'workout') {
            $categoryId = $this->workoutProgramsCategoryId;
        }
        return $this->savePost($post, $published, $categoryId);
    }


    public function updatePost(
        array $post,
        string $categoryName,
        string $oldName = null,
        bool $published = false
    ) {
        if ($oldName != null) {
            $postId = $this->postIdByName($oldName, $post['language_id']);
        }
        if (isset($postId) && $postId) {
            $language = Language::find($post['language_id']);
            $mediaId = null;
            if (isset($post['image_id'])) {
                /** @var Gallery $image */
                $image = Gallery::find($post['image_id']);
                if ($image) {
                    $sourceUrl = $this->optimizeImageAndGetPath($image);
                    $mediaId = $this->updateMedia($sourceUrl, $image->name, $oldName);
                    $this->removeFileOnUrl($sourceUrl);
                }
            }
            $data = [
                'title' => $post['title'],
                'content' => $post['description'],
                'slug' => Str::slug($post['title'].' '.$language->iso, '-'),
                'featured_media' => $mediaId
            ];
            if (null !== $mediaId) {
                $data['featured_media'] = $mediaId;
            }
            if (isset($post['date'])) {
                $data['date'] = $post['date']->toDateTimeString();
            }
            $data['status'] = $published ? 'publish' : 'draft';
            $this->client->put($this->url.self::POST_ROUTE.'/'.$postId, [
                'form_params' => $data
            ]);
            return $postId;
        } else {
            return $this->createPost($post, $categoryName, $published);
        }
    }

    public function deletePost($postName, $languageId)
    {
        $postId = $this->postIdByName($postName, $languageId);
        if ($postId) {
            $this->client->delete($this->url.self::POST_ROUTE.'/'.$postId);
        }
    }

    public function publishPost($name, bool $publish, $languageId)
    {
        $postId = $this->postIdByName($name, $languageId);
        if ($postId) {
            $this->client->put($this->url.self::POST_ROUTE.'/'.$postId, [
                RequestOptions::FORM_PARAMS => [
                    'status' => $publish ? 'publish' : 'draft',
                ]
            ]);
        }
        return $postId;
    }

    public function postIdByName($name, $languageId)
    {
        $language = Language::find($languageId);
        $post = $this->client->get($this->url.self::POST_ROUTE, [
            RequestOptions::JSON => [
                'slug' => [Str::slug($name.' '.$language->iso, '-')],
                'status' => ['draft', 'publish']
            ]
        ]);
        $posts = \GuzzleHttp\json_decode($post->getBody()->getContents());
        return isset($posts[0]) ? $posts[0]->id : false;
    }

    public function addMedia($sourceUrl, $title)
    {
        $response = $this->client->post($this->url.self::MEDIA_ROUTE, [
            RequestOptions::MULTIPART => [
                [
                    'name' => 'file',
                    'contents' => fopen($sourceUrl, 'r'),
                    'filename' => basename($sourceUrl)
                ],
                [
                    'name' => 'title',
                    'contents' => $title,
                ],
                [
                    'name' => 'slug',
                    'contents' => Str::slug($title, '-')
                ],
                [
                    'name' => 'alt_text',
                    'contents' => $title
                ],
            ]
        ])->getBody()->getContents();
        $post = json_decode($response);
        return $post->id ? $post->id : null;
    }

    public function findMediaIdByName(string $name)
    {
        $response = $this->client->get($this->url.self::MEDIA_ROUTE, [
            RequestOptions::FORM_PARAMS => [
                'title' => $name
            ]
        ])->getBody()->getContents();
        $media = json_decode($response);
        return isset($media[0]) ? $media[0]->id : false;
    }

    public function updateMedia($sourceUrl, $name, $oldName)
    {
        return $this->addMedia($sourceUrl, $name);
    }

    /**
     * @param array $post
     * @param bool $published
     * @param $categoryId
     * @return
     */
    public function savePost(array $post, bool $published, $categoryId)
    {
        /** @var Gallery $image */
        $image = Gallery::find($post['image_id']);
        $language = Language::find($post['language_id']);
        $mediaUrl = $this->optimizeImageAndGetPath($image);
        $mediaId = $this->addMedia($mediaUrl, $image->name);
        $this->removeFileOnUrl($mediaUrl);
        $data = [
            'title' => $post['title'],
            'content' => $post['description'],
            'slug' => Str::slug($post['title'].' '.$language->iso, '-'),
            'featured_media' => $mediaId,
            'lang' => $language->iso
        ];
        if (isset($post['date'])) {
            $data['date'] = $post['date']->toDateTimeString();
        }
        $data['categories'] = [$categoryId];
        $data['status'] = $published ? 'publish' : 'draft';
        $response = $this->client->post($this->url.self::POST_ROUTE, [
            'auth' => [
                $this->username,
                $this->password
            ],
            'form_params' => $data
        ])->getBody()->getContents();
        $post = json_decode($response);
        return $post->id;
    }

    public function matchPostTranslation($postId, $translationId)
    {
        $this->client->post($this->url.self::POST_ROUTE.'/'.$postId, [
            'auth' => [
                $this->username,
                $this->password
            ],
            RequestOptions::QUERY => [
                'lang' => 'en',
                'translations[sr]' => $translationId
            ]
        ]);
        $this->client->post($this->url.self::POST_ROUTE.'/'.$translationId, [
            'auth' => [
                $this->username,
                $this->password
            ],
            RequestOptions::QUERY => [
                'lang' => 'sr',
                'translations[en]' => $postId
            ]
        ]);
    }

    private function optimizeImageAndGetPath(Gallery $gallery): string
    {
        $sourceUrl = storage_path('app/'.$gallery->getUrl());
        $destinationUrl = sys_get_temp_dir().'/'.basename($sourceUrl);
        Image::make($sourceUrl)
            ->resize($this->uploadImageWidth, null, function (Constraint $constraint) {
                $constraint->aspectRatio();
            })
            ->save($destinationUrl);
        return $destinationUrl;
    }

    private function removeFileOnUrl(string $url): bool
    {
        return unlink($url);
    }
}
