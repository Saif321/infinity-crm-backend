<?php
declare(strict_types=1);

namespace App\Services;

use Illuminate\Http\Request;

class IosHeadersParser
{
    private $request;

    public function __construct(
        Request $request
    ) {
        $this->request = $request;
    }

    public function isIosUserAgent(): bool
    {
        $userAgentHeader = $this->request->header('User-Agent');
        return false !== strstr($userAgentHeader, 'Alamofire');
    }

    public function getBuildNo(): int
    {
        // EX: User-Agent:      Infinity Fit/1.0.6 (rs.infinity.fit; build:67; iOS 13.4.0) Alamofire/4.9.1
        $userAgentHeader = $this->request->header('User-Agent');
        $parts = explode('; ', $userAgentHeader);
        if (isset($parts[1]) && strstr($parts[1], 'build:')) {
            return intval(explode(':', $parts[1])[1]);
        } else {
            return 0;
        }
    }

    public function isIosOauthRequest(): bool
    {
        $userAgentHeader = $this->request->header('User-Agent');
        return $userAgentHeader && false !== strstr($this->request->getUri(),
                'oauth') && false !== strstr($userAgentHeader,
                'Alamofire');
    }
}
