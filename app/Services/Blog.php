<?php
declare(strict_types=1);

namespace App\Services;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;
use RicardoFiorani\OEmbed\OEmbed;
use RicardoFiorani\OEmbed\Result\VideoResult;

class Blog
{
    public function parseContentAndReplaceOembed($content)
    {
        $service = new OEmbed(new \Http\Adapter\Guzzle6\Client(new Client([])));
        preg_replace_callback('/<figure class="media">(.*?)<\/figure>/',
            function (array $figureMatches) use ($service, &$content) {
                list($figure, $oembed) = $figureMatches;
                preg_match('/url="(.*?)"/', $oembed, $urlMatches);
                list($attribute, $url) = $urlMatches;
                $uri = new Uri($url);
                $result = $service->get($uri);
                if ($result instanceof VideoResult) {
                    $content = str_replace($figure, $result->getHtml(), $content);
                }
            }, $content);

        return $content;
    }
}
