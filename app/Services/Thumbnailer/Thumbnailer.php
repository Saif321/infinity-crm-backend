<?php
declare(strict_types = 1);

namespace App\Services\Thumbnailer;

use App\Models\Gallery;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use Intervention\Image\ImageManagerStatic as Image;

class Thumbnailer
{
    const GALLERY_TYPE_IMAGE = 1;
    const GALLERY_TYPE_VIDEO = 2;

    /**
     * @var Gallery
     */
    private $gallery = null;

    /**
     * @var FFMpeg
     */
    private $ffmpeg = null;

    public function __construct()
    {
        $this->ffmpeg = FFMpeg::create([
            'ffmpeg.binaries'  => env('FFMPEG_PATH'),
            'ffprobe.binaries' => env('FFPROBE_PATH')
        ]);
    }

    /**
     * @param Gallery $gallery
     * @return Thumbnailer
     */
    public function setGallery(Gallery $gallery): self
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * @param Gallery $gallery
     * @return string
     */
    public function generateThumbnail(): string
    {
        return $this->getGalleryItemType() == self::GALLERY_TYPE_IMAGE ?
            $this->generateThumbnailForImage() :
            $this->generateThumbnailForVideo();
    }

    /**
     * @return string
     */
    private function generateThumbnailForVideo(): string
    {
        $fileName = $this->getFileName();
        $directory = $this->getDirectory();

        $video = $this->ffmpeg->open($this->getRealPath());
        $frame = $video->frame(TimeCode::fromSeconds(2));
        $frame->save(dirname($this->getRealPath()) . '/' . $fileName . '_thumb.jpg');

        return $directory . '/' . $fileName . '_thumb.jpg';
    }

    /**
     * @return string
     */
    private function generateThumbnailForImage(): string
    {
        $fileName = $this->getFileName();
        $directory = $this->getDirectory();
        $extension = $this->getExtension();

        Image::make($this->getRealPath())->resize(600, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(dirname($this->getRealPath()) . '/' . $fileName . '_thumb.' . $extension);

        return $directory . '/' . $fileName . '_thumb.' . $extension;
    }

    /**
     * @return int
     */
    private function getGalleryItemType(): int
    {
        return $this->gallery->type == 'image' ? self::GALLERY_TYPE_IMAGE : self::GALLERY_TYPE_VIDEO;
    }

    /**
     * @return string
     */
    private function getFileName(): string
    {
        return basename($this->gallery->getAttributes()['url'], '.' . $this->gallery->extension);
}

    /**
     * @return string
     */
    private function getDirectory(): string
    {
        return preg_replace('/^(.*?)\/storage\//is', '', dirname($this->gallery->getAttributes()['url']));
    }

    /**
     * @return string
     */
    private function getExtension(): string
    {
        return $this->gallery->extension;
    }

    /**
     * @return string
     */
    private function getRealPath(): string
    {
        return str_replace('//', '/', storage_path('app/' . $this->getDirectory() . '/' . $this->getFileName() . '.' . $this->getExtension()));
    }

}
