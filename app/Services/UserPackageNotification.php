<?php
declare(strict_types=1);

namespace App\Services;

use App\Mail\PaymentConfirmationMail;
use App\Mail\PaymentUnsubscriptionMail;
use App\Mail\UserPackageConfirmationMail;
use App\Models\UserPackage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class UserPackageNotification
{
    public static function sendConfirmationEmail(UserPackage $userPackage)
    {
        try {
            $carbon = new Carbon($userPackage->valid_until);
            $validUntil = $carbon->toDayDateTimeString() . ' ' . $carbon->tzName;
            $renewDate = $carbon->toFormattedDateString();
            $carbon = new Carbon($userPackage->created_at);
            $subscriptionTime = $carbon->toDayDateTimeString() . ' ' . $carbon->tzName;
            $mailTo = $userPackage->user->email;
            Mail::to($mailTo)->send(new UserPackageConfirmationMail([
                'packageName' => $userPackage->package->name,
                'renewDate' => $renewDate,
                'validUntil' => $validUntil,
                'subscriptionTime' => $subscriptionTime
            ]));
        } catch (\Exception $e) {
            Log::error(sprintf('Error while sending %s. Error message: %s', 'emails.user-package-confirmation-email',
                $e->getMessage()));
        }
    }

    public static function sendUnsubscriptionEmail(UserPackage $userPackage)
    {
        try {
            $carbon = new Carbon($userPackage->valid_until);
            $validUntil = $carbon->toDayDateTimeString() . ' ' . $carbon->tzName;
            $carbon = new Carbon();
            $subscriptionTIme = $carbon->toDayDateTimeString() . ' ' . $carbon->tzName;
            Mail::to($userPackage->user->email)->send(new PaymentUnsubscriptionMail([
                'packageName' => $userPackage->package->name,
                'validUntil' => $validUntil,
                'currentTime' => $subscriptionTIme
            ]));
        } catch (\Exception $e) {
            Log::error(sprintf('Error while sending %s. Error message: %s', 'emails.user-package-unsubscription-email',
                $e->getMessage()));
        }
    }
}
