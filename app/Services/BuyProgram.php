<?php

namespace App\Services;

use App\Models\Program;
use App\Models\PurchasedProgram;
use App\Models\User;
use App\Models\UserExerciseSet;
use App\Models\UserProgramWeek;
use App\Models\UserRound;
use App\Models\UserTraining;
use App\Models\WeekItem;
use App\Repositories\ExerciseSetRepository;
use App\Repositories\ProgramRepository;
use App\Repositories\PurchasedProgramRepository;
use App\Repositories\DeactivatedProgramRepository;
use App\Repositories\TrainingRepository;
use App\Repositories\UserExerciseRoundItemRepository;
use App\Repositories\UserExerciseSetRepository;
use App\Repositories\UserPackageRepository;
use App\Repositories\UserProgramRepository;
use App\Repositories\UserProgramWeekItemRepository;
use App\Repositories\UserProgramWeekRepository;
use App\Repositories\UserRoundRepository;
use App\Repositories\UserTrainingRepository;
use App\Services\Exceptions\NotFreeProgramException;
use App\Services\Exceptions\PackageLimitsReachedException;
use App\Services\Exceptions\ProgramAlreadyPurchasedException;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use \App\Models\UserProgram;

class BuyProgram
{
    const PROGRAM_STATE_CAN_ACTIVATE = 'CAN_ACTIVATE';
    const PROGRAM_STATE_BUY_PLAN = 'BUY_PLAN';
    const PROGRAM_STATE_CHANGE_PLAN = 'CHANGE_PLAN';

    private $programRepository;
    private $trainingRepository;

    private $userProgramRepository;
    private $purchasedProgramRepository;
    private $deactivatedProgramRepository;
    private $userProgramWeekRepository;
    private $userTrainingRepository;
    private $userProgramWeekItemRepository;
    private $userExerciseSetRepository;
    private $exerciseSetRepository;
    private $userRoundRepository;
    private $userExerciseRoundItemRepository;

    private $userPackageRepository;

    private $usePackages = false;

    public function __construct(
        ProgramRepository $programRepository,
        TrainingRepository $trainingRepository,

        UserProgramRepository $userProgramRepository,
        PurchasedProgramRepository $purchasedProgramRepository,
        DeactivatedProgramRepository $deactivatedProgramRepository,
        UserProgramWeekRepository $userProgramWeekRepository,
        UserTrainingRepository $userTrainingRepository,
        UserProgramWeekItemRepository $userProgramWeekItemRepository,
        UserExerciseSetRepository $userExerciseSetRepository,
        ExerciseSetRepository $exerciseSetRepository,
        UserRoundRepository $userRoundRepository,
        UserExerciseRoundItemRepository $userExerciseRoundItemRepository,

        UserPackageRepository $userPackageRepository
    ) {
        $this->programRepository = $programRepository;
        $this->trainingRepository = $trainingRepository;

        $this->userProgramRepository = $userProgramRepository;
        $this->purchasedProgramRepository = $purchasedProgramRepository;
        $this->deactivatedProgramRepository = $deactivatedProgramRepository;
        $this->userProgramWeekRepository = $userProgramWeekRepository;
        $this->userTrainingRepository = $userTrainingRepository;
        $this->userProgramWeekItemRepository = $userProgramWeekItemRepository;
        $this->userExerciseSetRepository = $userExerciseSetRepository;
        $this->exerciseSetRepository = $exerciseSetRepository;
        $this->userRoundRepository = $userRoundRepository;
        $this->userExerciseRoundItemRepository = $userExerciseRoundItemRepository;

        $this->userPackageRepository = $userPackageRepository;

        $this->usePackages = config('infinity-fit.use_packages', false);
    }

    private function hasEnoughLimitsToActivateProgram(
        User $user,
        \App\Models\UserPackage $userPackage,
        int $programId
    ): bool {
        $deactivatedPrograms = $this->deactivatedProgramRepository->getDistinctDeactivatedProgramsForPackage($user->id,
            $userPackage->id);
        if ($userPackage->free_trial) {
            if ($userPackage->package->number_of_free_trial_programs <= $userPackage->purchasedPrograms->count() + count($deactivatedPrograms)
                && !in_array($programId, $deactivatedPrograms)) {
                return false;
            }
        } else {
            if ($userPackage->package->number_of_programs <= $userPackage->purchasedPrograms->count() + count($deactivatedPrograms)
                && !in_array($programId, $deactivatedPrograms)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param Program $program
     * @param User $user
     * @param bool $preventUsePackages
     * @return PurchasedProgram
     * @throws NotFreeProgramException
     * @throws PackageLimitsReachedException
     * @throws ProgramAlreadyPurchasedException
     */
    public function buy(Program $program, User $user, bool $preventUsePackages = false): PurchasedProgram
    {
        $userPackage = null;

        if ($this->usePackages && !$preventUsePackages) {
            $userPackage = $this->userPackageRepository->getCurrentActiveUserPackage($user->id);
            if (!$program->is_free && !$userPackage) {
                throw new NotFreeProgramException();
            }

            if (!$program->is_free && !$this->hasEnoughLimitsToActivateProgram($user, $userPackage, $program->id)) {
                throw new PackageLimitsReachedException();
            }
        }

        if ($this->programRepository->findIsPurchased($user->id, $program->id)) {
            throw new ProgramAlreadyPurchasedException();
        }

        try {
            DB::beginTransaction();

            $userProgram = $this->createUserProgram($program);
            $purchasedProgram = $this->createPurchasedProgram(
                $user->id,
                $userProgram->id,
                $program->id,
                !$program->is_free && $userPackage ? $userPackage->id : null);
            $programWeeks = $this->getProgramWeeks($program);

            foreach ($programWeeks as $programWeek) {
                $userProgramWeek = $this->createUserProgramWeek($userProgram, $programWeek);
                $programWeekItems = $this->getProgramWeekItems($programWeek);

                foreach ($programWeekItems as $programWeekItem) {
                    $userTraining = $this->createUserTraining($programWeekItem);

                    if (!$userTraining) {
                        $this->createRestUserProgramWeekItem($userProgramWeek, $programWeekItem);
                        continue;
                    }

                    $this->createUserProgramWeekItem($userProgramWeek, $userTraining, $programWeekItem);

                    $exerciseSets = $this->getExerciseSets($programWeekItem);
                    foreach ($exerciseSets as $exerciseSet) {
                        $userExerciseSet = $this->createUserExerciseSet($exerciseSet, $userTraining);

                        $rounds = $this->getRounds($exerciseSet);
                        foreach ($rounds as $round) {
                            $userRound = $this->createUserRound($userExerciseSet, $round);

                            $exerciseRoundItems = $this->getExerciseRoundItems($round);
                            foreach ($exerciseRoundItems as $exerciseRoundItem) {
                                $this->createUserExerciseRoundItem($userRound, $exerciseRoundItem);
                            }
                        }
                    }
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }

        return $purchasedProgram;
    }

    public function getProgramWeekItems($programWeek)
    {
        return DB::select("SELECT * FROM week_items WHERE program_week_id = ? ORDER BY `order` ASC",
            [$programWeek->id]);
    }

    public function createUserTraining($weekItem)
    {
        $training = DB::select("SELECT * FROM trainings WHERE id = ?", [$weekItem->training_id]);
        if (!$training) {
            return null;
        }

        $training = $training[0];

        $trainingTranslations = $this->trainingRepository->getTranslations($training->id)->get();

        $userTraining = $this->userTrainingRepository->create((array) $training);
        foreach ($trainingTranslations as $trainingTranslation) {
            unset($trainingTranslation->id, $trainingTranslation->object_id);

            $trainingTranslation->object_id = $userTraining->id;

            $this->userTrainingRepository->saveTranslation($userTraining->id, $trainingTranslation->language_id,
                $trainingTranslation->toArray());
        }

        return $userTraining;
    }

    public function getCanActivateState(Program $program, User $user): string
    {
        if (!$this->usePackages || $program->is_free) {
            return static::PROGRAM_STATE_CAN_ACTIVATE;
        }

        $userPackage = null;
        if ($this->usePackages) {
            $userPackage = $this->userPackageRepository->getCurrentActiveUserPackage($user->id);
            if (!$userPackage) {
                return static::PROGRAM_STATE_BUY_PLAN;
            }
            if (!$this->hasEnoughLimitsToActivateProgram($user, $userPackage, $program->object_id ?? $program->id)) {
                return static::PROGRAM_STATE_CHANGE_PLAN;
            }
        }

        return static::PROGRAM_STATE_CAN_ACTIVATE;
    }

    private function createUserProgramWeekItem(
        UserProgramWeek $userProgramWeek,
        UserTraining $userTraining,
        $programWeekItem
    ) {
        return $this->userProgramWeekItemRepository->create([
            'user_program_week_id' => $userProgramWeek->id,
            'user_training_id' => $userTraining->id,
            'is_locked' => $programWeekItem->is_locked,
            'order' => $programWeekItem->order
        ]);
    }

    private function createRestUserProgramWeekItem(UserProgramWeek $userProgramWeek, $programWeekItem)
    {
        return $this->userProgramWeekItemRepository->create([
            'user_program_week_id' => $userProgramWeek->id,
            'user_training_id' => null,
            'is_locked' => false,
            'is_rest_day' => true,
            'order' => $programWeekItem->order
        ]);
    }

    private function getExerciseSets($programWeekItem)
    {
        return DB::select("SELECT * FROM exercise_sets WHERE training_id = ?", [$programWeekItem->training_id]);
    }

    private function createUserExerciseSet($exerciseSet, UserTraining $userTraining)
    {
        $exerciseSetTranslations = $this->exerciseSetRepository->getTranslations($exerciseSet->id)->get();

        $userExerciseSet = $this->userExerciseSetRepository->create([
            'user_training_id' => $userTraining->id,
            'name' => $exerciseSet->name,
            'description' => $exerciseSet->description,
            'type' => $exerciseSet->type,
            'duration_in_seconds' => $exerciseSet->duration_in_seconds,
            'order' => $exerciseSet->order
        ]);

        foreach ($exerciseSetTranslations as $exerciseSetTranslation) {
            unset($exerciseSetTranslation->id, $exerciseSetTranslation->object_id);
            $exerciseSetTranslation->object_id = $userExerciseSet->id;

            $exerciseSetTranslationData = $exerciseSetTranslation->toArray();
            unset($exerciseSetTranslationData['training_id']);
            $exerciseSetTranslationData['user_training_id'] = $userTraining->id;

            $this->userExerciseSetRepository->saveTranslation(
                $userExerciseSet->id,
                $exerciseSetTranslation->language_id,
                $exerciseSetTranslationData
            );
        }

        return $userExerciseSet;
    }

    private function getRounds($exerciseSet)
    {
        return DB::select("SELECT * FROM rounds WHERE exercise_set_id = ?", [$exerciseSet->id]);
    }

    private function createUserRound(UserExerciseSet $userExerciseSet, $round)
    {
        return $this->userRoundRepository->create([
            'user_exercise_set_id' => $userExerciseSet->id, 'order' => $round->order
        ]);
    }

    private function getExerciseRoundItems($round)
    {
        return DB::select("SELECT * FROM exercise_round_items WHERE round_id = ?", [$round->id]);;
    }

    public function createUserExerciseRoundItem(UserRound $userRound, $exerciseRoundItem)
    {
        $this->userExerciseRoundItemRepository->create([
            'user_round_id' => $userRound->id,
            'exercise_id' => $exerciseRoundItem->exercise_id,
            'type' => $exerciseRoundItem->type,
            'duration_in_seconds' => $exerciseRoundItem->duration_in_seconds,
            'strength_percentage' => $exerciseRoundItem->strength_percentage,
            'number_of_reps' => $exerciseRoundItem->number_of_reps,
            'order' => $exerciseRoundItem->order
        ]);
    }

    private function createUserProgram(Program $program)
    {
        $programTranslations = $this->programRepository->getTranslations($program->id)->get();

        $program->load('exerciseEnvironments');
        $programData = $program->toArray();
        unset($programData['id']);

        $userProgram = $this->userProgramRepository->create($programData);
        foreach ($programTranslations as $programTranslation) {
            unset($programTranslation->id, $programTranslation->object_id, $programTranslation->created_at, $programTranslation->updated_at);
            $programTranslation->object_id = $userProgram->id;

            $this->userProgramRepository->saveTranslation($userProgram->id, $programTranslation->language_id,
                $programTranslation->toArray(), true);
        }
        $userProgram->exerciseEnvironments()->sync($program->exerciseEnvironments()->get());

        return $userProgram;
    }

    private function createPurchasedProgram(
        int $userId,
        int $userProgramId,
        int $programId,
        int $userPackageId = null
    ): PurchasedProgram {
        return $this->purchasedProgramRepository->create([
            'user_id' => $userId,
            'user_program_id' => $userProgramId,
            'source_program_id' => $programId,
            'user_package_id' => $userPackageId
        ]);
    }

    private function getProgramWeeks(Program $program)
    {
        return DB::select("SELECT * from program_weeks where program_id = ?", [$program->id]);
    }

    private function createUserProgramWeek(UserProgram $userProgram, $programWeek)
    {
        return $this->userProgramWeekRepository->create([
            'user_program_id' => $userProgram->id, 'order' => $programWeek->order
        ]);;
    }
}
