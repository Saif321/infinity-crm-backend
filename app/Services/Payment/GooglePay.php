<?php

namespace App\Services\Payment;

use App\Http\Responses\JsonResponse;
use App\Models\Package;
use App\Models\Payment;
use App\Models\User;
use App\Models\UserPackage;
use App\Services\Payment\GooglePay\NotificationType;
use App\Services\Payment\GooglePay\PubSubPaymentNotification;
use App\Services\UserPackage as UserPackageService;
use App\Services\UserPackageNotification;
use DateTime;
use Google\Cloud\PubSub\PubSubClient;
use Google_Client;
use Google_Exception;
use Google_Service_AndroidPublisher;
use Google_Service_AndroidPublisher_SubscriptionPurchasesAcknowledgeRequest;
use Illuminate\Support\Facades\Log;

class GooglePay extends AbstractPayment
{
    private $pubSubClient;
    private $googleClient;
    private $googleService;
    private $subscriptionName;
    private $logger;
    private $testMode;

    public function __construct(
        UserPackageService $userPackageService
    ) {
        parent::__construct($userPackageService);
        $this->logger = Log::channel('payment-google');
        $this->pubSubClient = new PubSubClient(config('infinity-fit.google_pay.pub-sub_client'));
        $this->subscriptionName = config('infinity-fit.google_pay.subscription_name');
        $this->googleClient = new Google_Client();
        try {
            $this->googleClient->setAuthConfig(config('infinity-fit.google_pay.google_pay_client'));
        } catch (Google_Exception $e) {
            $this->logger->error($e);
        }
        $this->googleClient->addScope('https://www.googleapis.com/auth/androidpublisher');
        $this->googleService = new Google_Service_AndroidPublisher($this->googleClient);
        $this->method = Payment::METHOD_GOOGLE_PAY;
        $this->testMode = config('infinity-fit.google_pay.test_mode');
    }

    public function pull()
    {
        $subscription = $this->pubSubClient->subscription($this->subscriptionName);
        foreach ($subscription->pull() as $message) {
            $data = json_decode($message->data());
            $this->logger->debug('Message data', (array) $data);
            if (isset($data->subscriptionNotification)) {
                $notification = new PubSubPaymentNotification();
                $notification->version = $data->subscriptionNotification->version;
                $notification->notificationType = $data->subscriptionNotification->notificationType;
                $notification->purchaseToken = $data->subscriptionNotification->purchaseToken;
                $notification->subscriptionId = $data->subscriptionNotification->subscriptionId;
                $notification->isTest = isset($data->subscriptionNotification->purchaseType);
                switch ($notification->notificationType) {
                    case NotificationType::SUBSCRIPTION_RECOVERED:
                        $this->logger->info('SUBSCRIPTION_RECOVERED');
                        break;
                    case NotificationType::SUBSCRIPTION_RENEWED:
                        $this->renewSubscription($notification);
                        break;
                    case NotificationType::SUBSCRIPTION_CANCELED:
                        $this->unsubscribe($notification);
                        break;
                    case NotificationType::SUBSCRIPTION_PURCHASED:
                        $this->logger->info('SUBSCRIPTION_PURCHASED');
                        break;
                    case NotificationType::SUBSCRIPTION_ON_HOLD:
                        $this->logger->info('SUBSCRIPTION_ON_HOLD');
                        break;
                    case NotificationType::SUBSCRIPTION_IN_GRACE_PERIOD:
                        $this->logger->info('SUBSCRIPTION_IN_GRACE_PERIOD');
                        break;
                    case NotificationType::SUBSCRIPTION_RESTARTED:
                        $this->logger->info('SUBSCRIPTION_RESTARTED');
                        break;
                    case NotificationType::SUBSCRIPTION_PRICE_CHANGE_CONFIRMED:
                        $this->logger->info('SUBSCRIPTION_PRICE_CHANGE_CONFIRMED');
                        break;
                    case NotificationType::SUBSCRIPTION_DEFERRED:
                        $this->logger->info('SUBSCRIPTION_DEFERRED');
                        break;
                    case NotificationType::SUBSCRIPTION_PAUSED:
                        $this->logger->info('SUBSCRIPTION_PAUSED');
                        break;
                    case NotificationType::SUBSCRIPTION_PAUSE_SCHEDULE_CHANGED:
                        $this->logger->info('SUBSCRIPTION_PAUSE_SCHEDULE_CHANGED');
                        break;
                    case NotificationType::SUBSCRIPTION_REVOKED:
                        $this->logger->info('SUBSCRIPTION_REVOKED');
                        break;
                    case NotificationType::SUBSCRIPTION_EXPIRED:
                        $this->logger->info('SUBSCRIPTION EXPIRED');
                        break;
                }
                if ($this->testMode && $notification->isTest) {
                    $subscription->acknowledge($message);
                } elseif (!$this->testMode && !$notification->isTest) {
                    $subscription->acknowledge($message);
                }
            }
        }
    }


    public function validate($packageName, $productId, $purchaseToken)
    {
        $purchase = $this->googleService->purchases_subscriptions->get($packageName, $productId, $purchaseToken);
        $this->logger->debug('Purchase data', [
            'ackState' => $purchase->getAcknowledgementState(),
            'orderId' => $purchase->getOrderId(),
            'expiryTime' => date('Y-m-d H:i:s', $purchase->getExpiryTimeMillis() / 1000),
            'priceAmountMicros' => $purchase->getPriceAmountMicros(),
            'currency' => $purchase->getPriceCurrencyCode()
        ]);
        return $purchase;
    }


    /**
     * @param $purchaseToken
     * @param $packageId
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function acknowledge($purchaseToken, $packageId, $user)
    {
        $packageName = 'rs.telego.fitness';
        $purchase = $this->googleService->purchases_subscriptions->get($packageName, $packageId, $purchaseToken);
        if ($purchase->getPurchaseType() == 2) {
            $this->setIsFreeTrial(true);
        } else {
            $this->setIsFreeTrial(false);
        }
        $this->setUser($user);
        $this->setTransactionId($purchase->getOrderId());
        $package = Package::where('android_store_package_id', $packageId)->first();
        $this->setPackage($package);
        $this->setGooglePurchaseToken($purchaseToken);
        $this->setGoogleProductId($packageId);
        $this->setCurrencyCode($purchase->getPriceCurrencyCode());
        $this->setChargedValue($purchase->getPriceAmountMicros() / 1000000);
        $this->initPayment();
        if (!$purchase->getAcknowledgementState()) {
            try {
                $this->googleService->purchases_subscriptions->acknowledge($packageName, $packageId, $purchaseToken,
                    new Google_Service_AndroidPublisher_SubscriptionPurchasesAcknowledgeRequest());
            } catch (\Exception $e) {
                $this->logger->error('Error: '.$e);
                return response()->json(new JsonResponse(null, false, 409, "Subscription expired."), 409);
            }
        } else {
            $this->logger->debug("Subscription already acknowledged.", ['purchase' => $purchase]);
            return response()->json(new JsonResponse(null, false, 409, "Subscription already acknowledged."), 409);
        }
        try {
            $time = date('Y:m:d H:i:s', $purchase->getExpiryTimeMillis() / 1000);
            $this->updatePayment($this->transactionId, Payment::STATUS_SUCCESS, $purchase, false, new DateTime($time));
            $this->logger->info('Payment successful!');
            return response()->json(new JsonResponse(null, true, 200, "Success!"), 200);
        } catch (\Exception $e) {
            $this->logger->error('Error: '.$e->getMessage());
            return response()->json(new JsonResponse(null, false, 409, "Failed!"), 409);
        }
    }

    /**
     * @param PubSubPaymentNotification $notification
     * @return bool
     */
    public function renewSubscription(PubSubPaymentNotification $notification): bool
    {
        $purchase = $this->validate('rs.telego.fitness', $notification->subscriptionId, $notification->purchaseToken);

        $purchasedPayment = Payment::where('google_purchase_token', $notification->purchaseToken)->first();
        if (!$purchasedPayment) {
            $purchasedPayment = Payment::where('google_purchase_token', $purchase->getLinkedPurchaseToken())->first();
        }

        if (!$purchasedPayment) {
            $this->logger->error('Payment for renew not found in local database', [
                'subscriptionId' => $notification->subscriptionId,
                'purchaseToken' => $notification->purchaseToken,
                'linkedPurchaseToken' => $purchase->getLinkedPurchaseToken()
            ]);
            return false;
        }

        $userPackage = UserPackage::find($purchasedPayment->user_package_id);

        $this->setUser(User::find($userPackage->user_id));
        $this->setTransactionId($purchase->getOrderId());
        $this->setPackage(Package::where('android_store_package_id', $notification->subscriptionId)->first());
        $this->setGooglePurchaseToken($notification->purchaseToken);
        $this->setGoogleProductId($notification->subscriptionId);
        $this->setChargedValue($purchase->priceAmountMicros / 1000000);
        $this->setCurrencyCode($purchase->priceCurrencyCode);
        $this->initPayment();

        try {
            $time = date('Y:m:d H:i:s', $purchase->getExpiryTimeMillis() / 1000);
            $this->updatePayment(
                $purchase->getOrderId(),
                Payment::STATUS_SUCCESS, $purchase,
                false,
                new DateTime($time)
            );
            $this->logger->info('SUBSCRIPTION RENEWED');
        } catch (\Exception $e) {
            $this->logger->debug('Error: '.$e->getMessage());
            return false;
        }
        return true;
    }

    public function cancelSubscription(UserPackage $userPackage)
    {
        $payment = $userPackage->payment;
        return $this->googleService->purchases_subscriptions->cancel('rs.telego.fitness', $payment->google_product_id,
            $payment->google_purchase_token);
    }

    /**
     * @param PubSubPaymentNotification $notification
     * @return bool
     */
    public function unsubscribe(PubSubPaymentNotification $notification): bool
    {
        $purchasedPayment = Payment::where('google_purchase_token', $notification->purchaseToken)->first();
        if (!$purchasedPayment) {
            $purchase = $this->validate('rs.telego.fitness', $notification->subscriptionId,
                $notification->purchaseToken);
            $purchasedPayment = Payment::where('google_purchase_token', $purchase->getLinkedPurchaseToken())->first();
        }

        if (!$purchasedPayment) {
            $this->logger->error('Payment for unsubscribe not found in local database', [
                'subscriptionId' => $notification->subscriptionId,
                'purchaseToken' => $notification->purchaseToken,
                'linkedPurchaseToken' => $purchase->getLinkedPurchaseToken()
            ]);
            return false;
        }

        $userPackage = UserPackage::find($purchasedPayment->user_package_id);
        if (!$userPackage->renew_stopped) {
            $userPackage->renew_stopped = true;
            $userPackage->save();
            UserPackageNotification::sendUnsubscriptionEmail($userPackage);
        }

        return true;
    }
}
