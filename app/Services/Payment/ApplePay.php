<?php


namespace App\Services\Payment;

use App\Models\Package;
use App\Models\PackagePaymentMethod;
use App\Models\Payment;
use App\Models\User;
use App\Models\UserPackage as UserPackageModel;
use App\Services\PaymentNotification;
use App\Services\UserPackage as UserPackageService;
use App\Services\UserPackageNotification;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\HandlerStack;
use GuzzleRetry\GuzzleRetryMiddleware;
use Illuminate\Support\Facades\Log;
use ReceiptValidator\iTunes\PurchaseItem;
use ReceiptValidator\iTunes\Validator;


class ApplePay extends AbstractPayment
{
    const NOTIFICATION_TYPE_DID_CHANGE_RENEWAL_STATUS = 'DID_CHANGE_RENEWAL_STATUS';

    private $validator;
    private $sharedSecret;
    private $logger;

    public function __construct(
        UserPackageService $userPackageService
    ) {
        parent::__construct($userPackageService);
        $this->logger = Log::channel('payment-apple');
        $this->sharedSecret = config('infinity-fit.apple_pay.shared_secret');
        $this->validator = new Validator();
        $stack = HandlerStack::create();
        $stack->push(GuzzleRetryMiddleware::factory(['retry_on_timeout' => true]));
        $this->validator->setRequestOptions(['handler' => $stack]);
        $this->method = Payment::METHOD_APPLE_PAY;
    }

    /**
     * @param string $receipt
     * @param User $user
     * @throws InvalidStatusException
     * @throws GuzzleException
     * @throws Exception
     */
    public function validateReceipt(string $receipt, User $user)
    {
        try {
            $this->logger->debug('Receipt request', ['receipt' => $receipt]);
            $response = $this->validator
                ->setSharedSecret($this->sharedSecret)
                ->setReceiptData($receipt)
                ->validate();
            $this->logger->debug('Receipt response', $response->getReceipt());
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw $e;
        }
        if ($response->isValid()) {
            $this->validatePurchases($response->getPurchases(), $user);

        } else {
            throw new Exception('Apple receipt validation failed', $response->getResultCode());
        }
    }

    public function findUserByOriginalTransactionId(string $transactionId): ?User
    {
        /** @var Payment $payment */
        $payment = Payment::where('transaction_id', $transactionId)->first();
        if (!$payment) {
            return null;
        }

        $userPackage = $payment->userPackage;
        if (!$userPackage) {
            return null;
        }

        return $userPackage->user;
    }

    public function findUserPackageByOriginalTransactionId(string $transactionId): ?UserPackageModel
    {
        /** @var Payment $payment */
        $payment = Payment::where('transaction_id', $transactionId)->first();
        if (!$payment) {
            return null;
        }

        return $payment->userPackage;
    }

    /**
     * @param PurchaseItem[] $purchases
     * @param User $user
     * @throws InvalidStatusException
     */
    public function validatePurchases(array $purchases, User $user)
    {
        foreach ($purchases as $purchase) {
            $this->validatePurchase($purchase, $user);
        }
    }

    /**
     * @param PurchaseItem $purchase
     * @param User $user
     * @throws InvalidStatusException
     */
    public function validatePurchase(PurchaseItem $purchase, User $user)
    {
        $oldPayment = Payment::where('transaction_id', $purchase->getTransactionId())->first();
        if ($oldPayment !== null) {
            $this->logger->debug('Payment already exists: '.$purchase->getTransactionId());
            return;
        }
        if (time() > $purchase->getExpiresDate()->getTimestamp()) {
            return;
        }

        $package = Package::where('ios_store_package_id', $purchase->getProductId())->first();

        if (!$package) {
            $this->logger->error(sprintf('Package with ios_store_package_id(%s) not found',
                $purchase->getProductId()));
            return;
        }

        /** @var PackagePaymentMethod $paymentMethod */
        $paymentMethod = $package->paymentMethods->filter(function (
            PackagePaymentMethod $packagePaymentMethod
        ) {
            return $packagePaymentMethod->payment_method === PackagePaymentMethod::PAYMENT_METHOD_APPLE_PAY;
        })->first();

        if (!$paymentMethod) {
            $this->logger->error(sprintf('No apple payment method for Package(%s)', $package->id));
        }

        $this->setTransactionId($purchase->getTransactionId());
        $this->setPackage($package);
        $this->setUser($user);
        $this->setIsFreeTrial($purchase->isTrialPeriod());
        $this->logger->debug('Is trial period flag', [
            'flag' => $purchase->isTrialPeriod(),
            'rawPurchase' => $purchase->getRawResponse()
        ]);
        $this->setCurrencyCode($paymentMethod->currency_code);
        $this->setChargedValue($purchase->isTrialPeriod() ? 0 : $paymentMethod->price);

        $this->initPayment();
        $this->updatePayment($purchase->getTransactionId(), Payment::STATUS_SUCCESS,
            $purchase->getRawResponse(), false, $purchase->getExpiresDate());
        $this->logger->info('Payment successful!');
    }

    public function onChangeRenewalStatus(bool $autoRenewStatus, UserPackageModel $userPackage)
    {
        if (!$autoRenewStatus) {
            return $this->unsubscribe($userPackage->id);
        } else {
            $userPackage->renew_stopped = false;
            return $userPackage->save();
        }
    }


    /**
     * @param int $packageId
     * @return bool
     */
    public function unsubscribe(int $packageId)
    {
        /** @var UserPackageModel $package */
        $package = UserPackageModel::find($packageId);
        if (!$package) {
            $this->logger->debug(sprintf('UserPackage %s not found', $packageId));
            return false;
        } else {
            if (!$package->renew_stopped) {
                $package->renew_stopped = true;
                $package->save();
                UserPackageNotification::sendUnsubscriptionEmail($package);
            } else {
                $this->logger->debug(sprintf('UserPackage %s already deactivated', $packageId));
            }
        }

        return true;
    }
}
