<?php
declare(strict_types=1);

namespace App\Services\Payment;

use Throwable;

class InvalidStatusException extends \Exception
{
    public function __construct(
        $message = 'Payment status is not valid',
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
