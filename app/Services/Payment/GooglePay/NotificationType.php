<?php


namespace App\Services\Payment\GooglePay;

class NotificationType
{
    // A subscription was recovered from account hold.
    const SUBSCRIPTION_RECOVERED = 1;

    // An active subscription was renewed.
    const SUBSCRIPTION_RENEWED = 2;

    // A subscription was either voluntarily or involuntarily cancelled. For voluntary cancellation, sent when the user cancels.
    const SUBSCRIPTION_CANCELED = 3;

    // A new subscription was purchased.
    const SUBSCRIPTION_PURCHASED = 4;

    // A subscription has entered account hold (if enabled).
    const SUBSCRIPTION_ON_HOLD = 5;

    // A subscription has entered grace period (if enabled).
    const SUBSCRIPTION_IN_GRACE_PERIOD = 6;

    // User has reactivated their subscription from Play > Account > Subscriptions (requires opt-in for subscription restoration)
    const SUBSCRIPTION_RESTARTED = 7;

    // A subscription price change has successfully been confirmed by the user.
    const SUBSCRIPTION_PRICE_CHANGE_CONFIRMED = 8;

    // A subscription's recurrence time has been extended.
    const SUBSCRIPTION_DEFERRED = 9;

    // A subscription has been paused.
    const SUBSCRIPTION_PAUSED = 10;

    // A subscription pause schedule has been changed.
    const SUBSCRIPTION_PAUSE_SCHEDULE_CHANGED = 11;

    // A subscription has been revoked from the user before the expiration time.
    const SUBSCRIPTION_REVOKED = 12;

    // A subscription has expired.
    const SUBSCRIPTION_EXPIRED = 13;

}
