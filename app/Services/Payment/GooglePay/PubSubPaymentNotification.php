<?php


namespace App\Services\Payment\GooglePay;


class PubSubPaymentNotification
{
    public $version;
    public $notificationType;
    public $purchaseToken;
    public $subscriptionId;
    public $isTest;
}
