<?php

namespace App\Services\Payment;

use App\Models\Package;
use App\Models\Payment;
use App\Models\PaymentLog;
use App\Models\User;
use App\Models\UserPackage;
use App\Services\PaymentNotification;
use App\Services\UserPackage as UserPackageService;
use App\Services\UserPackageNotification;

abstract class AbstractPayment
{
    /**
     * @var Package
     */
    protected $package;

    /**
     * @var User
     */
    protected $user;

    protected $transactionId;

    protected $method;

    protected $userPackageService;

    protected $currencyCode = '';

    protected $chargedValue = 0;

    protected $googleProductId = '';

    protected $googlePurchaseToken = '';

    protected $isFreeTrial;

    public function __construct(
        UserPackageService $userPackageService
    ) {
        $this->userPackageService = $userPackageService;
    }

    public function setPackage(Package $package): void
    {
        $this->package = $package;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function setTransactionId(string $transactionId): void
    {
        $this->transactionId = $transactionId;
    }

    public function setCurrencyCode(string $currencyCode): void
    {
        $this->currencyCode = $currencyCode;
    }

    public function setChargedValue(string $chargedValue): void
    {
        $this->chargedValue = $chargedValue;
    }

    public function setGoogleProductId(string $googleProductId): void
    {
        $this->googleProductId = $googleProductId;
    }

    public function setGooglePurchaseToken(string $googlePurchaseToken): void
    {
        $this->googlePurchaseToken = $googlePurchaseToken;
    }

    public function isFreeTrial(): bool
    {
        return $this->isFreeTrial;
    }

    public function setIsFreeTrial(bool $isFreeTrial): void
    {
        $this->isFreeTrial = $isFreeTrial;
    }


    function initPayment(): Payment
    {
        $payment = new Payment();
        $payment->method = $this->method;
        $payment->transaction_id = $this->transactionId;
        $payment->status = Payment::STATUS_PROCESSING;
        $payment->currency_code = $this->currencyCode;
        $payment->charged_value = $this->chargedValue;
        $payment->google_purchase_token = $this->googlePurchaseToken;
        $payment->google_product_id = $this->googleProductId;
        $payment->save();

        return $payment;
    }

    /**
     * @param string $transactionId
     * @param string $status
     * @param $details
     * @param bool $testMode
     * @param \DateTime|null $validUntil
     * @return Payment
     * @throws InvalidStatusException
     */
    function updatePayment(
        string $transactionId,
        string $status,
        $details,
        bool $testMode,
        \DateTime $validUntil = null
    ): Payment {
        $validStatuses = [
            Payment::STATUS_FAILED, Payment::STATUS_SUCCESS, Payment::STATUS_PROCESSING, Payment::STATUS_VOID
        ];

        if (!in_array($status, $validStatuses)) {
            throw new InvalidStatusException();
        }

        /** @var Payment $payment */
        $payment = Payment::where('transaction_id', $transactionId)->first();

        $paymentLog = new PaymentLog();
        $paymentLog->method = $payment->method;
        $paymentLog->status = $payment->status;
        $paymentLog->transaction_id = $payment->transaction_id;
        $paymentLog->user_package_id = $payment->user_package_id;
        $paymentLog->details = $payment->details;
        $paymentLog->currency_code = $payment->currency_code;
        $paymentLog->charged_value = $payment->charged_value;
        $paymentLog->google_product_id = $payment->google_product_id;
        $paymentLog->google_purchase_token = $payment->google_purchase_token;
        $paymentLog->created_at = $payment->created_at;
        $paymentLog->updated_at = $payment->updated_at;

        $paymentLog->save();

        $payment->status = $status;
        $payment->details = $details;
        $payment->currency_code = $this->currencyCode;
        $payment->charged_value = $this->chargedValue;
        $payment->google_purchase_token = $this->googlePurchaseToken;
        $payment->google_product_id = $this->googleProductId;

        $payment->save();

        if ($payment->status === Payment::STATUS_SUCCESS && $paymentLog->status !== Payment::STATUS_SUCCESS) {
            $this->createUserPackage($payment, $testMode, $validUntil);
        }

        return $payment;
    }

    private function createUserPackage(Payment $payment, bool $testMode, \DateTime $validUntil = null): UserPackage
    {
        if ($this->isFreeTrial === true) {
            $userPackage = $this->userPackageService->createFreeTrialPackage($this->user, $this->package, $validUntil);
        } else {
            $userPackage = $this->userPackageService->createPackage($this->user, $this->package, $testMode,
                $validUntil);
        }
        $payment->user_package_id = $userPackage->id;
        $payment->save();
        $this->triggerNotifications($userPackage);
        return $userPackage;
    }

    private function triggerNotifications(UserPackage $userPackage)
    {
        PaymentNotification::sendConfirmationEmail($userPackage);
        UserPackageNotification::sendConfirmationEmail($userPackage);
    }
}
