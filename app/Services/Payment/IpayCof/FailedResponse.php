<?php
declare(strict_types=1);

namespace App\Services\Payment\IpayCof;

class FailedResponse extends Response
{
    public $ErrorMessage;

    public function __construct($data = [])
    {
        parent::__construct($data);
        foreach ($data as $key => $value) {
            if (property_exists(FailedResponse::class, $key)) {
                $this->$key = $value;
            }
        }
    }
}
