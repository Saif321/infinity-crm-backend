<?php
declare(strict_types=1);

namespace App\Services\Payment\IpayCof;

class SuccessfulResponse extends Response
{
    public $AVSResponse;
    public $ProcessorCCVResponse;
    public $ProcessorReferenceNumber;
    public $ProcessorResponseCode;
    public $ProcessorResponseMessage;
    public $TerminalId;

    public function __construct($data = [])
    {
        parent::__construct($data);
        foreach ($data as $key => $value) {
            if (property_exists(SuccessfulResponse::class, $key)) {
                $this->$key = $value;
            }
        }
    }
}
