<?php
declare(strict_types=1);

namespace App\Services\Payment\IpayCof;

class Response
{
    public $ApprovalCode;
    public $Brand;
    public $Country;
    public $CommercialServiceProvider;
    public $OrderId;
    public $IpgTransactionId;
    public $PaymentType;
    public $TDate;
    public $TDateFormatted;
    public $TransactionResult;
    public $TransactionTime;
    public $metadata;
    public $statusCode;

    public function __construct($data = [])
    {
        foreach ($data as $key => $value) {
            if (property_exists(Response::class, $key)) {
                $this->$key = $value;
            }
        }
    }
}
