<?php

namespace App\Services\Payment;

use App\Models\Package;
use App\Models\Payment;
use App\Models\User;
use App\Models\UserPackage as UserPackageModel;
use App\Services\PaymentNotification;
use App\Services\UserPackage as UserPackageService;
use App\Repositories\UserPackageRepository;
use App\Services\UserPackageNotification;

class Sms extends AbstractPayment
{
    private $userPackageRepository;
    private $shortNumber;
    private $bodyPattern;

    public function __construct(
        UserPackageService $userPackageService,
        UserPackageRepository $userPackageRepository
    ) {
        parent::__construct($userPackageService);
        $this->userPackageRepository = $userPackageRepository;
        $config = config('infinity-fit.sms_payment');
        $this->shortNumber = $config['short_number'];
        $this->bodyPattern = $config['body_pattern'];

        $this->method = Payment::METHOD_SMS;
    }

    public function getShortNumber(): string
    {
        return $this->shortNumber;
    }

    public function generateSmsPaymentMessageBody(User $user, Package $package): string
    {
        return str_replace(['{user_id}', '{package_id}'], [$user->id, $package->id], $this->bodyPattern);
    }

    public function unsubscribe(): string
    {
        $activePackage = $this->userPackageRepository->getCurrentActiveUserPackage($this->user->id);

        if (!$activePackage) {
            return 'No active package for user';
        }
        if ($activePackage->package_id != $this->package->id) {
            return 'Package is not active';
        }
        if ($activePackage->renew_stopped) {
            return 'Already unsubscribed';
        }

        $activePackage->renew_stopped = true;
        $result = $activePackage->update();

        if($result === true){
            $package = UserPackageModel::find($activePackage->id);
            if($package) UserPackageNotification::sendUnsubscriptionEmail($package);
        }

        return $result;
    }
}
