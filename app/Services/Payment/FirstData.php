<?php

namespace App\Services\Payment;

use App\Models\Package;
use App\Models\PackagePaymentMethod;
use App\Models\Payment;
use App\Models\User;
use App\Repositories\FirstDataVaultRepository;
use App\Services\Payment\FirstData\FailedRedirectResponse;
use App\Services\Payment\FirstData\RedirectResponse;
use App\Services\Payment\FirstData\SuccessfulRedirectResponse;
use App\Services\UserPackage as UserPackageService;
use Illuminate\Support\Facades\Log;

class FirstData extends AbstractPayment
{
    private $storeName;
    private $sharedSecret;
    private $gatewayUrl;
    private $creditCardAuthorizationPrice;
    private $firstDataVaultRepository;
    private $testMode;
    private $logger;

    public function __construct(
        UserPackageService $userPackageService,
        FirstDataVaultRepository $firstDataVaultRepository
    ) {
        parent::__construct($userPackageService);
        $this->logger = Log::channel('payment-first-data');
        $config = config('infinity-fit.first_data');
        $this->storeName = $config['store_name'];
        $this->sharedSecret = $config['shared_secret'];
        $this->gatewayUrl = $config['gateway_url'];
        $this->creditCardAuthorizationPrice = $config['credit_card_authorization_price'];
        $this->testMode = $config['test_mode'];

        $this->firstDataVaultRepository = $firstDataVaultRepository;

        $this->method = Payment::METHOD_CREDIT_CARD;
    }

    public function getRedirectionData()
    {
        /** @var PackagePaymentMethod $packagePaymentMethod */
        $packagePaymentMethod = $this->package->paymentMethods->filter(function (
            PackagePaymentMethod $packagePaymentMethod
        ) {
            return $packagePaymentMethod->payment_method === PackagePaymentMethod::PAYMENT_METHOD_CREDIT_CARD;
        })->first();

        if ($this->userPackageService->isUserEligibleForFreeTrial($this->user)) {
            $chargeTotal = number_format($this->creditCardAuthorizationPrice, 2);
            $transactionType = 'preauth';
        } else {
            $chargeTotal = number_format($packagePaymentMethod->price, 2);
            $transactionType = 'sale';
        }

        $rsdCurrencyCode = 941;
        $dateTime = date("Y:m:d-H:i:s");
        $timezone = date_default_timezone_get();
        $orderId = sprintf("PCC-%s-%s-%s", date('Ymd-His'), $this->package->id, $this->user->id);

        $this->setTransactionId($orderId);
        $this->setCurrencyCode($packagePaymentMethod->currency_code);
        $this->setChargedValue($packagePaymentMethod->price);

        $redirectionData = [
            'hash' => $this->createHash($chargeTotal, $rsdCurrencyCode, $dateTime),
            'dateTime' => $dateTime,
            'transactionType' => $transactionType,
            'mode' => 'payonly',
            'timezone' => $timezone,
            'gatewayUrl' => $this->gatewayUrl,
            'hashAlgorithm' => 'SHA256',
            'storeName' => $this->storeName,
            'chargeTotal' => $chargeTotal,
            'currency' => $rsdCurrencyCode,
            'successfulPaymentUrl' => route('firstDataSuccessfulPayment'),
            'failedPaymentUrl' => route('firstDataFailedPayment'),
            'comments' => 'Subscription vault',
            'customerId' => $this->user->id,
            'language' => 'en_GB',
            'mobileMode' => 'true',
            'orderId' => $orderId,
            'hostedDataId' => md5($orderId)
        ];

        $this->logger->debug('Redirection data', $redirectionData);

        return $redirectionData;
    }

    /**
     * @param array $data
     * @return RedirectResponse
     * @throws InvalidStatusException
     */
    public function handleRedirectionData(array $data): RedirectResponse
    {
        $this->logger->debug('Redirection response', $data);
        if (RedirectResponse::STATUS_APPROVED === $data['status']) {
            $redirectionData = new SuccessfulRedirectResponse($data);

            $orderData = $this->parseOrderId($redirectionData->oid);
            $this->setTransactionId($redirectionData->oid);
            $this->setUser(User::find($orderData['userId']));
            $this->setPackage(Package::find($orderData['packageId']));
            $this->setCurrencyCode('RSD');
            $this->setChargedValue($redirectionData->chargetotal);

            $payment = $this->updatePayment(
                $redirectionData->oid,
                Payment::STATUS_SUCCESS,
                $redirectionData,
                $this->testMode
            );

            $this->firstDataVaultRepository->createVaultItem(
                $this->user->id,
                $payment->id,
                $redirectionData->hosteddataid,
                $redirectionData->cardnumber
            );
        } else {
            $redirectionData = new FailedRedirectResponse($data);
            $this->updatePayment($redirectionData->oid, Payment::STATUS_FAILED, $redirectionData, $this->testMode);
        }
        $this->logger->debug('Redirection response handled', (array) $redirectionData);
        return $redirectionData;
    }

    public function generateRedirectionUrl(User $user, Package $package): string
    {
        return route('firstDataRedirect', ['user_id' => $user, 'package_id' => $package->id]);
    }

    public function getTestRedirectionData($testCaseId = 'TC1')
    {
        $dateTime = date("Y:m:d-H:i:s");
        $timezone = date_default_timezone_get();
        $orderId = sprintf("iPayTestCase-%s-%s", $testCaseId, date('Ymd-His'));
        $transactionType = 'sale';
        $rsdCurrencyCode = 941;
        $chargeTotal = 10;
        $trxOrigin = 'ECI';
        switch ($testCaseId) {
            case 'TC1':
            case 'TC3':
                $chargeTotal = 10;
                break;
            case 'TC2':
                $chargeTotal = 11;
                break;
            case 'TC4':
                $chargeTotal = 13;
                break;
            case 'TC5':
                $chargeTotal = 15;
                break;
            case 'TC6':
                $transactionType = 'refund';
                $chargeTotal = 15;
                throw new \Exception('Unsupported');
                break;
            case 'TC7':
                $transactionType = 'preauth';
                $chargeTotal = 16;
                break;
            case 'TC8':
                $transactionType = 'postauth';
                $chargeTotal = 16;
                break;
            case 'TC9':
                $transactionType = 'sale';
                $chargeTotal = 99;
                break;
            case 'TC10':
                $transactionType = 'void';
                $chargeTotal = 99;
                break;
            case 'TC11':
                $transactionType = 'sale';
                $chargeTotal = 18;
                break;
            case 'TC12':
                $transactionType = 'refund';
                $chargeTotal = 18;
                throw new \Exception('Unsupported');
                break;
            case 'TC13':
                $transactionType = 'sale';
                $chargeTotal = 2.99;
                $rsdCurrencyCode = 978;
                break;
            case 'TC14':
                $transactionType = 'preauth';
                $chargeTotal = 19;
                break;
            case 'TC15':
                $transactionType = 'postauth';
                $chargeTotal = 30;
                break;
            case 'TC16':
                $transactionType = 'sale';
                $chargeTotal = 20;
                break;
            case 'TC17':
                $transactionType = 'sale';
                $chargeTotal = 21;
                break;
            case 'TC18':
                $transactionType = 'sale';
                $chargeTotal = 17;
                $trxOrigin = 'MOTO';
                break;
        }
        return [
            'hash' => $this->createHash($chargeTotal, $rsdCurrencyCode, $dateTime),
            'dateTime' => $dateTime,
            'transactionType' => $transactionType,
            'mode' => 'payonly',
            'timezone' => $timezone,
            'gatewayUrl' => $this->gatewayUrl,
            'hashAlgorithm' => 'SHA256',
            'storeName' => $this->storeName,
            'chargeTotal' => $chargeTotal,
            'currency' => $rsdCurrencyCode,
            'successfulPaymentUrl' => route('firstDataSuccessfulPaymentTest'),
            'failedPaymentUrl' => route('firstDataFailedPaymentTest'),
            'comments' => 'iPayTestCase',
//            'customerId' => $this->user->id,
            'language' => 'en_GB',
            'orderId' => $orderId,
            'trxOrigin' => $trxOrigin
        ];
    }

    private function createHash(string $chargetotal, string $currency, string $dateTime): string
    {
        $stringToHash = $this->storeName.$dateTime.$chargetotal.$currency.$this->sharedSecret;
        $ascii = bin2hex($stringToHash);
        return hash('sha256', $ascii);
    }

    private function parseOrderId(string $orderId)
    {
        $segments = explode('-', $orderId);
        return [
            'date' => $segments[1],
            'time' => $segments[2],
            'packageId' => $segments[3],
            'userId' => $segments[4],
        ];
    }
}
