<?php
declare(strict_types=1);

namespace App\Services\Payment\FirstData;

class RedirectResponse
{
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_DECLINED = 'DECLINED';
    const STATUS_FAILED = 'FAILED';

    public $approval_code;
    public $comments;
    public $txndate_processed;
    public $timezone;
    public $response_hash;
    public $fail_rc;
    public $oid;
    public $tdate;
    public $installments_interest;
    public $cccountry;
    public $ccbrand;
    public $customerid;
    public $hash_algorithm;
    public $txntype;
    public $currency;
    public $txndatetime;
    public $ipgTransactionId;
    public $fail_reason;
    public $chargetotal;
    public $status;

    public function __construct($data = [])
    {
        foreach ($data as $key => $value) {
            if (property_exists(RedirectResponse::class, $key)) {
                $this->$key = $value;
            }
        }
    }
}

