<?php
declare(strict_types=1);

namespace App\Services\Payment\FirstData;

class SuccessfulRedirectResponse extends RedirectResponse
{
    public $ccbin;
    public $expmonth;
    public $endpointTransactionId;
    public $processor_response_code;
    public $terminal_id;
    public $expyear;
    public $response_code_3dsecure;
    public $bname;
    public $refnumber;
    public $paymentMethod;
    public $cardnumber;
    public $hosteddataid;

    public function __construct($data = [])
    {
        parent::__construct($data);
        foreach ($data as $key => $value) {
            if (property_exists(SuccessfulRedirectResponse::class, $key)) {
                $this->$key = $value;
            }
        }
    }
}
