<?php
declare(strict_types=1);

namespace App\Services\Payment\FirstData;

class FailedRedirectResponse extends RedirectResponse
{
    public $fail_rc;
    public $fail_reason;

    public function __construct($data = [])
    {
        parent::__construct($data);
        foreach ($data as $key => $value) {
            if (property_exists(FailedRedirectResponse::class, $key)) {
                $this->$key = $value;
            }
        }
    }
}
