<?php

namespace App\Services\Payment;

use App\Models\FirstDataVault;
use App\Models\Package;
use App\Models\PackagePaymentMethod;
use App\Models\Payment;
use App\Models\User;
use App\Services\Payment\IpayCof\FailedResponse;
use App\Services\Payment\IpayCof\Response;
use App\Services\Payment\IpayCof\SuccessfulResponse;
use App\Services\UserPackage as UserPackageService;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;

class IPayCof extends AbstractPayment
{

    private $gatewayUrl;
    private $merchantUsername;
    private $hostedDataStoreId;
    private $currency;
    private $testMode;
    private $secret;

    private $client;

    private $logger;

    public function __construct(
        UserPackageService $userPackageService
    ) {
        parent::__construct($userPackageService);
        $config = config('infinity-fit.ipay_cof');
        $this->gatewayUrl = $config['gateway_url'];
        $this->merchantUsername = $config['merchant_username'];
        $this->hostedDataStoreId = $config['hosted_data_store_id'];
        $this->currency = $config['currency'];
        $this->testMode = $config['test_mode'];
        $this->secret = $config['secret'];

        $this->client = new Client();

        $this->logger = Log::channel('payment-ipay-cof');

        $this->method = Payment::METHOD_CREDIT_CARD;
    }

    public function chargeCreditCard(string $customerId, string $chargeTotalRaw, string $hostedDataId): Response
    {
        $chargeTotal = 1 * $chargeTotalRaw;
        $signature = md5($this->merchantUsername.$hostedDataId.$chargeTotal.$this->secret);
        $data = [
            'MerchantUsername' => $this->merchantUsername,
            'CustomerID' => $customerId,
            'HostedDataID' => $hostedDataId,
            'HostedDataStoreID' => $this->hostedDataStoreId,
            'ChargeTotal' => $chargeTotal,
            'Currency' => $this->currency,
            'trxType' => 'sale',
            'metadata' => '',
            'signature' => $signature
        ];

        $this->logger->debug('Request', $data);

        $response = $this->client->post($this->gatewayUrl, [RequestOptions::JSON => $data]);
        $body = $response->getBody();
        $statusCode = $response->getStatusCode();

        $this->logger->debug('Response Status Code -> '.$statusCode);
        $this->logger->debug('Response -> '.$body);

        if ($statusCode == 200) {
            $responseData = json_decode($body, true);
            if ($responseData['statusCode'] == 200) {
                $transactionResponse = new SuccessfulResponse($responseData);
            } else {
                $transactionResponse = new FailedResponse($responseData);
            }
        } else {
            $transactionResponse = new FailedResponse();
            $transactionResponse->statusCode = $statusCode;
        }
        return $transactionResponse;
    }

    /**
     * @param User $user
     * @param Package $package
     * @param FirstDataVault $firstDataVault
     * @return Payment
     * @throws InvalidStatusException
     * @throws \Exception
     */
    public function processRenew(User $user, Package $package, FirstDataVault $firstDataVault): Payment
    {
        /** @var PackagePaymentMethod $packagePaymentMethod */
        $packagePaymentMethod = $package->paymentMethods->filter(function (PackagePaymentMethod $packagePaymentMethod) {
            return $packagePaymentMethod->payment_method === PackagePaymentMethod::PAYMENT_METHOD_CREDIT_CARD;
        })->first();

        $orderId = sprintf("PCOF-%s-%s-%s", date('Ymd-His'), $package->id, $user->id);

        $this->setUser($user);
        $this->setPackage($package);
        $this->setTransactionId($orderId);
        $this->setCurrencyCode($packagePaymentMethod->currency_code);
        $this->setChargedValue($packagePaymentMethod->price);

        $this->initPayment();
        $response = $this->chargeCreditCard($user->id, $packagePaymentMethod->price, $firstDataVault->hosted_id);

        if ($response instanceof SuccessfulResponse) {
            return $this->updatePayment($orderId, Payment::STATUS_SUCCESS, $response, $this->testMode);
        } elseif ($response instanceof FailedResponse) {
            return $this->updatePayment($orderId, Payment::STATUS_FAILED, $response, $this->testMode);
        } else {
            throw new \Exception('Invalid COF response');
        }
    }
}
