<?php
declare(strict_types=1);

namespace App\Services;

use App\Repositories\PaymentLogRepository;
use App\Repositories\PaymentRepository;
use App\Services\Dto\PaymentReportRow;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;

class Payment
{
    private $paymentRepository;
    private $paymentLogRepository;

    public function __construct(PaymentRepository $paymentRepository, PaymentLogRepository $paymentLogRepository)
    {
        $this->paymentRepository = $paymentRepository;
        $this->paymentLogRepository = $paymentLogRepository;
    }

    public function getPaymentReport(): Builder
    {
        return $this->paymentRepository->getPaymentReport();
    }

    public function getPaymentHistory(string $transactionId): Builder
    {
        return $this->paymentLogRepository->getPaymentHistory($transactionId);
    }

    public function mapPaymentReportRow(object $data): PaymentReportRow
    {
        $row = new PaymentReportRow();
        $row->method = $data->method;
        $row->status = $data->status;
        $row->transaction_id = $data->transaction_id;
        $row->details = is_string($data->details) ? json_decode($data->details) : $data->details;
        $row->charged_value = $data->charged_value;
        $row->currency_code = $data->currency_code;
        $row->package_name = $data->package_name;
        $row->number_of_history_items = isset($data->number_of_history_items) ? $data->number_of_history_items : 0;
        $row->updated_at = $data->updated_at ? (new Carbon($data->updated_at))->format('c') : null;
        $row->created_at = $data->created_at ? (new Carbon($data->created_at))->format('c') : null;
        return $row;
    }

}
