<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\Goal;
use App\Models\User;
use App\Models\UserProgram;
use App\Models\UserProgramWeek;
use App\Models\UserTraining;
use App\Repositories\ExerciseRepository;
use App\Repositories\LevelRepository;
use App\Repositories\ProgramRepository;
use App\Repositories\UserProgramRepository;
use App\Repositories\UserProgramWeekRepository;
use App\Repositories\UserTrainingRepository;
use App\Services\Dto\ProgramStatistics;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Models\Program as ProgramModel;
use App\Models\UserProgram as UserProgramModel;
use App\Services\Dto\Program as ProgramDto;
use App\Services\Dto\PurchasedProgram;
use App\Services\Dto\UserProgress;
use Illuminate\Database\Eloquent\Collection;

class Program
{
    private $programRepository;
    private $userProgramRepository;
    private $levelRepository;
    private $userProgramWeekRepository;
    private $userTrainingRepository;
    private $exerciseRepository;

    public function __construct(
        ProgramRepository $programRepository,
        UserProgramRepository $userProgramRepository,
        LevelRepository $levelRepository,
        UserProgramWeekRepository $userProgramWeekRepository,
        UserTrainingRepository $userTrainingRepository,
        ExerciseRepository $exerciseRepository
    ) {
        $this->programRepository = $programRepository;
        $this->userProgramRepository = $userProgramRepository;
        $this->levelRepository = $levelRepository;
        $this->userProgramWeekRepository = $userProgramWeekRepository;
        $this->userTrainingRepository = $userTrainingRepository;
        $this->exerciseRepository = $exerciseRepository;
    }

    /**
     * @return ProgramDto[]\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public function getProgramsForHomePage()
    {
        /** @var User $user */
        $user = Auth::user();
        $level = $user->level;
        $levelId = $level ? $level->getId() : null;
        $environmentId = ($user->questionnaire && $user->questionnaire->environment) ? $user->questionnaire->environment->getId() : null;
        $goalIds = null;

        if ($user->questionnaire && $user->questionnaire->goals->count() > 0) {
            $goalIds = $user->questionnaire->goals->map(function (Goal $goal) {
                return $goal->getId();
            })->toArray();
        }

        return $this->programRepository
            ->getProgramsForHomePage($user->id, null, $user->gender, $levelId, $environmentId, $goalIds)
            ->get()
            ->map(function (ProgramModel $programModel): ProgramDto {
                return $this->mapProgramModelToProgram($programModel);
            });
    }

    /**
     * @return PurchasedProgram[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public function getPurchasedPrograms()
    {
        return $this->getUserPrograms()
            ->map(function (UserProgramModel $programModel): PurchasedProgram {
                return $this->mapUserProgramToPurchasedProgram($programModel);
            });
    }

    /**
     * @return UserProgram[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public function getUserPrograms()
    {
        return $this->userProgramRepository
            ->getPurchasedPrograms(Auth::user()->getAuthIdentifier())
            ->get();
    }

    /**
     * @param ProgramModel $programModel
     *
     * @return ProgramDto
     */
    public function mapProgramModelToProgram(ProgramModel $programModel): ProgramDto
    {
        $program = new ProgramDto();
        $program->object_id = $programModel->getAttribute('object_id');
        $program->name = $programModel->getAttribute('name');
        $program->average_rating = $programModel->getAttribute('avg_rating');
        $program->level_name = $this->getLevelName($programModel->getAttribute('object_id'));
        $program->number_of_reviews = $programModel->getAttribute('review_count');
        $program->image_url = $programModel->image()->get('thumbnail')->first()['thumbnail'];
        $program->is_free = $programModel->getAttribute('is_free');

        return $program;
    }

    /**
     * @param UserProgramModel $programModel
     *
     * @return PurchasedProgram
     */
    public function mapUserProgramToPurchasedProgram(UserProgramModel $programModel): PurchasedProgram
    {
        $objectId = $programModel->getAttribute('object_id');
        $objectCacheKey = $this->getCacheKeyForPurchasedProgram($objectId);
        $program = new PurchasedProgram();
        if (Cache::has($objectCacheKey)) {
            return Cache::get($objectCacheKey);
        }

        $program->object_id = $objectId;
        $program->name = $programModel->getAttribute('name');
        $program->image_url = $programModel->image()->get('thumbnail')->first()['thumbnail'];
        $program->level_name = $this->getLevelName($programModel->getAttribute('source_program_id'));


        /** @var UserProgramWeek[] $weeks */
        $weeks = $this->userProgramWeekRepository
            ->getProgramWeeks($program->object_id)
            ->get()
            ->reverse();

        $lastWeek = true;
        foreach ($weeks as $week) {
            $trainings = $this->userTrainingRepository
                ->getTrainingsForWeek($week->getAttribute('id'))
                ->get()
                ->reverse();

            $completedTrainings = 0;
            $totalWeekTrainings = 0;

            $lastTrainingInWeekIsCompleted = false;
            $lastTrainingInWeek = null;

            foreach ($trainings as $training) {
                if ($training->is_rest_day == 1) {
                    continue;
                }

                if (null === $lastTrainingInWeek) {
                    $lastTrainingInWeek = $training;
                }

                $totalWeekTrainings++;
                if ($this->userTrainingRepository->isTrainingCompleted($training->object_id)) {
                    $completedTrainings++;
                    if ($lastTrainingInWeek === $training) {
                        $lastTrainingInWeekIsCompleted = true;
                    }
                }
            }

            if (($lastTrainingInWeekIsCompleted && !$lastWeek) ||
                (!$lastWeek && $completedTrainings === $totalWeekTrainings)) {
                break;
            }

            $program->current_week = $week->getAttribute('order');
            $program->current_week_finished_trainings = $completedTrainings;
            $program->current_week_trainings = $totalWeekTrainings;

            if (($completedTrainings > 0 && $completedTrainings !== $totalWeekTrainings) ||
                ($lastWeek && $completedTrainings === $totalWeekTrainings)) {
                break;
            }
            $lastWeek = false;
        }

        Cache::put($objectCacheKey, $program, now()->addSeconds(3600));

        return $program;
    }

    /**
     * @return UserProgress
     */
    public function getUserProgress(): UserProgress
    {
        $userId = Auth::user()->getAuthIdentifier();
        $objectCacheKey = $this->getCacheKeyForUserProgress($userId);
        if (Cache::has($objectCacheKey)) {
            return Cache::get($objectCacheKey);
        }

        $purchasedPrograms = $this->userProgramRepository->getPurchasedPrograms($userId)->get()->sort();
        $progress = new UserProgress();

        foreach ($purchasedPrograms as $program) {
            /** @var UserProgramWeek[] $weeks */
            $weeks = $this->userProgramWeekRepository
                ->getProgramWeeks($program->object_id)
                ->get()
                ->sort();

            $lastWeek = true;
            foreach ($weeks as $week) {
                $trainings = $this->userTrainingRepository
                    ->getTrainingsForWeek($week->getAttribute('id'))
                    ->get()
                    ->sort();

                $completedTrainings = 0;
                $totalWeekTrainings = 0;

                $lastTrainingInWeekIsCompleted = false;
                $lastTrainingInWeek = null;

                foreach ($trainings as $training) {
                    if ($training->is_rest_day == 1) {
                        continue;
                    }

                    if (null === $lastTrainingInWeek) {
                        $lastTrainingInWeek = $training;
                    }

                    $totalWeekTrainings++;
                    if ($this->userTrainingRepository->isTrainingCompleted($training->object_id)) {
                        $completedTrainings++;
                        $progress->workouts++;
                        if ($lastTrainingInWeek === $training) {
                            $lastTrainingInWeekIsCompleted = true;
                        }
                    }
                }

                if ($lastTrainingInWeekIsCompleted) {
                    $progress->weeks++;
                }

                if (($lastTrainingInWeekIsCompleted && !$lastWeek) ||
                    (!$lastWeek && $completedTrainings === $totalWeekTrainings)) {
                    //break;
                }

                if (($completedTrainings > 0 && $completedTrainings !== $totalWeekTrainings) ||
                    ($lastWeek && $completedTrainings === $totalWeekTrainings)) {
                    //break;
                }

                $lastWeek = false;
            }

            if (isset($lastTrainingInWeekIsCompleted) && $lastTrainingInWeekIsCompleted && $completedTrainings === $totalWeekTrainings) {
                $progress->programs++;
            }
        }

        Cache::put($objectCacheKey, $progress, now()->addSeconds(3600));

        return $progress;
    }

    /**
     * @param string $year
     * @param string $month
     *
     * @return string[]
     */
    public function getUserActivityDatesForMonth($year, $month)
    {
        $month = str_pad($month, 2, "0", STR_PAD_LEFT);
        $userId = Auth::user()->getAuthIdentifier();
        /*
        $objectCacheKey = $this->getCacheKeyForUserActivityDates( $userId, $year, $month );
        if ( Cache::has( $objectCacheKey ) ) {
            return Cache::get( $objectCacheKey );
        }
        */

        $activityDates = $this->userProgramRepository->getActivityDates($userId, $year, $month);

        /*
        Cache::put( $objectCacheKey, $activityDates, now()->addSeconds( 60 * 60 * 24) );
        */
        return $activityDates;
    }

    /**
     * @param $objectId
     */
    public function clearPurchasedProgramCache($objectId)
    {
        $objectCacheKey = $this->getCacheKeyForPurchasedProgram($objectId);
        Cache::forget($objectCacheKey);
    }

    /**
     * @param $objectId
     */
    public function clearUserProgressCache($objectId)
    {
        $objectCacheKey = $this->getCacheKeyForUserProgress($objectId);
        Cache::forget($objectCacheKey);
    }

    /**
     * @param int $userId
     */
    public function clearUserActivityDatesCache(int $userId)
    {
        $objectCacheKey = $this->getCacheKeyForUserActivityDates($userId, date('Y'), date('m'));
        Cache::forget($objectCacheKey);
    }

    /**
     * @param int $programId
     *
     * @return UserTraining|null
     */
    public function getNextTraining(int $programId)
    {
        /** @var UserProgramWeek[] $weeks */
        $weeks = $this->userProgramWeekRepository
            ->getProgramWeeks($programId)
            ->get()
            ->reverse();

        $lastIncompleteTraining = null;

        foreach ($weeks as $week) {
            $trainings = $this->userTrainingRepository
                ->getTrainingsForWeek($week->getAttribute('id'))
                ->get()
                ->reverse();

            foreach ($trainings as $training) {
                if ($training->is_rest_day == 1) {
                    continue;
                }

                if (!$this->userTrainingRepository->isTrainingCompleted($training->object_id)) {
                    $lastIncompleteTraining = $training;
                } else {
                    break 2;
                }
            }
        }

        return $lastIncompleteTraining;
    }

    public function getProgramDayWeekForTraining(UserProgram $userProgram, UserTraining $userTraining)
    {
        /** @var UserProgramWeek[]|Collection $weeks */
        $weeks = $this->userProgramWeekRepository
            ->getProgramWeeks($userProgram->getId())
            ->get();

        $lastIncompleteTraining = null;

        $dayWeek = [
            'day' => 0,
            'order' => 0,
            'last_training_in_program' => false
        ];

        foreach ($weeks as $week) {
            /** @var UserTraining[]|Collection $trainings */
            $trainings = $this->userTrainingRepository
                ->getTrainingsForWeek($week->getAttribute('id'))
                ->get()
                ->reverse();

            $trainingsWithoutPauses = $trainings->filter(function (UserTraining $training) {
                return !$training->is_rest_day;
            });

            $isLastWeek = $weeks->last() === $week;

            foreach ($trainings as $training) {
                if ($training->getId() === $userTraining->getId()) {
                    $dayWeek['day'] = $trainings->search($training) + 1;
                    $dayWeek['order'] = $week->getAttribute('order');
                    $dayWeek['last_training_in_program'] = $trainingsWithoutPauses->first() === $training && $isLastWeek;
                    break 2;
                }
            }
        }

        return $dayWeek;
    }

    /**
     * @param UserProgramModel $userProgram
     *
     * @return ProgramStatistics
     */
    public function getProgramStatistics(UserProgram $userProgram): ProgramStatistics
    {
        /** @var UserProgramWeek[]|Collection $weeks */
        $weeks = $this->userProgramWeekRepository
            ->getProgramWeeks($userProgram->getId())
            ->get();

        $statistics = new ProgramStatistics();
        $statistics->programName = $userProgram->name;
        $statistics->weeks = $weeks->count();

        foreach ($weeks as $week) {
            /** @var UserTraining[]|Collection $trainings */
            $trainings = $this->userTrainingRepository
                ->getTrainingsForWeek($week->getAttribute('id'))
                ->get();

            $trainingsWithoutPauses = $trainings->filter(function (UserTraining $training) {
                return !$training->is_rest_day;
            });
            $statistics->trainings += $trainingsWithoutPauses->count();
        }

        return $statistics;
    }

    public function canShowExerciseForProgram(int $exerciseId, int $programId): bool
    {
        if (!$this->getPurchasedPrograms()->some('object_id', '=', $programId)) {
            return false;
        }

        return $this->userProgramRepository->countExercisesWithId($programId, $exerciseId) > 0;
    }

    public function getUserActivityOnDate(int $userId, string $date)
    {
        $result = $this->userProgramRepository->getUserActivityOnDate($userId, $date);

        $activity = [];

        foreach ($result as $row) {
            if (!isset($activity[$row->user_program_id])) {
                $userProgram = $this->userProgramRepository->findById($row->user_program_id);
                $activity[$row->user_program_id]['program'] = [
                    'program_name' => $userProgram->name,
                    'training_id' => $row->user_training_id,
                    'week' => $row->week,
                    'day' => $row->day
                ];
            }
            $exercise = $this->exerciseRepository->findById($row->exercise_id);
            $activity[$row->user_program_id]['stats'][$row->exercise_id]['excercise_name'] = $exercise->name;
            $activity[$row->user_program_id]['stats'][$row->exercise_id]['stats'][] = [
                'completed_times' => $row->completed_times * 1,
                'number_of_reps' => $row->number_of_reps * 1,
                'strength_percentage' => $row->strength_percentage * 1,
                'strength_in_kilograms' => $row->strength_in_kilograms * 1,
                'duration_in_seconds' => $row->duration_in_seconds * 1
            ];
        }

        $repackedActivity = [];
        foreach ($activity as $programId => $program) {
            $repackedProgram = $program['program'];
            $repackedProgram['stats'] = [];
            foreach ($program['stats'] as $exerciseId => $exercise) {
                $repackedProgram['stats'][] = $exercise;
            }
            $repackedActivity[] = $repackedProgram;
        }
        return $repackedActivity;
    }

    /**
     * @param $programId
     *
     * @return string
     */
    private function getLevelName($programId)
    {
        return $this->levelRepository->getLevelsForProgram($programId)
            ->get(['name'])
            ->implode('name', ' / ');
    }

    /**
     * @param $objectId
     *
     * @return string
     */
    private function getCacheKeyForPurchasedProgram($objectId): string
    {
        return 'purchased-program-'.$objectId;
    }

    /**
     * @param $objectId
     *
     * @return string
     */
    private function getCacheKeyForUserProgress($objectId): string
    {
        return 'user-progress-'.$objectId;
    }

    /**
     * @param $objectId
     *
     * @return string
     */
    private function getCacheKeyForUserActivityDates($userId, $year, $month): string
    {
        return sprintf('user-activity-dates-%s-%s-%s', $userId, $year, $month);
    }
}
