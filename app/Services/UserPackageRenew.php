<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\Payment;
use App\Models\UserPackage as UserPackageModel;
use App\Repositories\FirstDataVaultRepository;
use App\Repositories\UserPackageRepository;
use App\Services\Payment\GooglePay;
use App\Services\Payment\IPayCof;
use Exception;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class UserPackageRenew
{
    private $userPackageRepository;
    private $firstDataVaultRepository;
    private $iPayCof;
    private $googlePayService;
    private $fitConfig;

    public function __construct(
        UserPackageRepository $userPackageRepository,
        FirstDataVaultRepository $firstDataVaultRepository,
        IPayCof $iPayCof,
        GooglePay $googlePayService
    ) {
        $this->userPackageRepository = $userPackageRepository;
        $this->firstDataVaultRepository = $firstDataVaultRepository;
        $this->iPayCof = $iPayCof;
        $this->googlePayService = $googlePayService;
        $this->fitConfig = config('infinity-fit');
    }

    public function renewPackage(UserPackageModel $userPackage)
    {
        $vaultMethod = $this->firstDataVaultRepository->getLatestForUser($userPackage->user->id);
        if (!$vaultMethod) {
            Log::warning(sprintf(
                'Vault method not found for User(%s) and UserPackage(%s)',
                $userPackage->user->id,
                $userPackage->id
            ));
            return;
        }
        Log::debug(sprintf(
            'Renew UserPackage(%s) for User(%s)',
            $userPackage->id,
            $userPackage->user->id
        ));

        $payment = $this->iPayCof->processRenew($userPackage->user, $userPackage->package, $vaultMethod);

        Log::debug(sprintf(
            'UserPackage(%s) renew status %s',
            $userPackage->id,
            $payment->status
        ));

        $userPackage->last_renewal_attempt = Carbon::now();
        $userPackage->renew_attempts += 1;
        $userPackage->save();

        return $payment;
    }

    public function renewBulk()
    {
        $packagesForRenew = $this->userPackageRepository->getExpiringUserPackagesPaidWithCreditCard(Payment::METHOD_CREDIT_CARD)
            ->filter(function (UserPackageModel $package) {
                return !$package->renew_stopped;
            });
        return $packagesForRenew->map(function (UserPackageModel $package) {
            return $this->renewPackage($package);
        });
    }

    /**
     * @param int $packageId
     * @return array
     * @throws Exception
     */
    public function unsubscribe(int $packageId)
    {
        /** @var UserPackageModel $package */
        $package = UserPackageModel::find($packageId);
        if (!$package) {
            throw new Exception(sprintf('Package(%s) not found', $packageId));
        }
        if ($package->user->id !== Auth::id()) {
            throw new Exception('Package is not owned by current user');
        }

        if ($package->payment->method == Payment::METHOD_APPLE_PAY) {
            return [
                'payment_method' => Payment::METHOD_APPLE_PAY,
                'sms_deactivation' => null,
                'package' => $package
            ];
        }

        if ($package->payment->method == Payment::METHOD_SMS) {
            return [
                'payment_method' => Payment::METHOD_SMS,
                'sms_deactivation' => [
                    'short_number' => $this->fitConfig['sms_payment']['deactivation_short_number'],
                    'sms_content' => $this->fitConfig['sms_payment']['deactivation_sms']
                ],
                'package' => $package
            ];
        }

        if ($package->payment->method == Payment::METHOD_CREDIT_CARD) {
            $package->renew_stopped = true;
            $package->save();
            UserPackageNotification::sendUnsubscriptionEmail($package);
            return [
                'payment_method' => Payment::METHOD_CREDIT_CARD,
                'sms_deactivation' => null,
                'package' => $package
            ];
        }

        if ($package->payment->method == Payment::METHOD_GOOGLE_PAY) {
            try {
                $this->googlePayService->cancelSubscription($package);
                $package->renew_stopped = true;
                $package->save();
                UserPackageNotification::sendUnsubscriptionEmail($package);
                return [
                    'payment_method' => Payment::METHOD_GOOGLE_PAY,
                    'sms_deactivation' => null,
                    'package' => $package
                ];
            } catch (Exception $e) {
                Log::error('Google Pay unsubscribe error: '.$e->getMessage());
                throw $e;
            }
        }
    }
}
