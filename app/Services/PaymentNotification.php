<?php
declare(strict_types=1);

namespace App\Services;

use App\Mail\PaymentConfirmationMail;
use App\Models\UserPackage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class PaymentNotification
{
    public static function sendConfirmationEmail(UserPackage $userPackage)
    {
        try {
            $carbon = new Carbon($userPackage->payment->created_at);
            $paymentTime = $carbon->toDayDateTimeString() . ' ' . $carbon->tzName;
            $orderId = $userPackage->payment->transaction_id;
            $value = $userPackage->payment->charged_value;
            $currencyCode = $userPackage->payment->currency_code;
            $mailTo = $userPackage->user->email;
            Mail::to($mailTo)->send(new PaymentConfirmationMail([
                'packageName' => $userPackage->package->name,
                'orderId' => $orderId,
                'currencyCode' => $currencyCode,
                'value' => $value,
                'paymentTime' => $paymentTime
            ]));
        } catch (\Exception $e) {
            Log::error(sprintf('Error while sending %s. Error message: %s', 'emails.payment-confirmation-email',
                $e->getMessage()));
        }
    }
}
