<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\Goal;
use App\Models\NutritionProgram as NutritionProgramModel;
use App\Models\User;
use App\Repositories\GoalRepository;
use App\Repositories\NutritionProgramRepository;
use App\Repositories\PurchasedNutritionProgramRepository;
use App\Services\Dto\NutritionProgram as NutritionProgramDto;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

class NutritionProgram
{
    private $nutritionProgramRepository;
    private $purchasedNutritionProgramRepository;
    private $goalRepository;

    public function __construct(
        NutritionProgramRepository $nutritionProgramRepository,
        PurchasedNutritionProgramRepository $purchasedNutritionProgramRepository,
        GoalRepository $goalRepository
    ) {
        $this->nutritionProgramRepository = $nutritionProgramRepository;
        $this->purchasedNutritionProgramRepository = $purchasedNutritionProgramRepository;
        $this->goalRepository = $goalRepository;
    }

    /**
     * @return NutritionProgramDto[]|Builder[]|Collection|\Illuminate\Support\Collection
     */
    public function getProgramsForHomePage()
    {
        /** @var User $user */
        $user = Auth::user();
        $goalIds = null;

        if ($user->questionnaire && $user->questionnaire->goals->count() > 0) {
            $goalIds = $user->questionnaire->goals->map(function (Goal $goal) {
                return $goal->getId();
            })->toArray();
        }

        $programs = $this->nutritionProgramRepository
            ->getProgramsForHomePage($user->id, null, $goalIds)
            ->get();

        if ($programs->count() === 0) {
            $programs = $this->nutritionProgramRepository->getProgramsForHomePage($user->id, null, null)
                ->get();
        }

        return $programs->map(function (
            NutritionProgramModel $nutritionProgramModel
        ): NutritionProgramDto {
            return $this->mapNutritionProgramModelToNutritionProgram($nutritionProgramModel);
        });
    }

    /**
     * @param NutritionProgramModel $nutritionProgramModel
     *
     * @return NutritionProgramDto
     */
    public function mapNutritionProgramModelToNutritionProgram(
        NutritionProgramModel $nutritionProgramModel
    ): NutritionProgramDto {
        $goals = $this->goalRepository->getTranslatedGoalsForNutrition($nutritionProgramModel->getId())
            ->get(['name'])
            ->implode('name', ' / ');

        $nutritionProgram = new NutritionProgramDto();
        $nutritionProgram->object_id = $nutritionProgramModel->getAttribute('object_id');
        $nutritionProgram->name = $nutritionProgramModel->getAttribute('name');
        $nutritionProgram->is_free = $nutritionProgramModel->getAttribute('is_free');
        $nutritionProgram->image_url = $nutritionProgramModel->image()->get('thumbnail')->first()['thumbnail'];
        $nutritionProgram->number_of_reviews = $nutritionProgramModel->getNumberOfReviews();
        $nutritionProgram->average_rating = $nutritionProgramModel->getAverageRating();
        $nutritionProgram->goals = $goals;

        return $nutritionProgram;
    }

    public function hasUserBoughtProgramWithId(User $user, int $programId): bool
    {
        return $this->purchasedNutritionProgramRepository->countByNutritionProgramIdAndUserId($programId,
                $user->id) > 0;
    }
}
