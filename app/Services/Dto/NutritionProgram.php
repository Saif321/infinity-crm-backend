<?php
declare(strict_types = 1);
namespace App\Services\Dto;

class NutritionProgram {
	public $object_id;
	public $name = '';
	public $image_url = '';
	public $goals = '';
	public $average_rating = 0;
	public $number_of_reviews = 0;
	public $is_free = false;
}
