<?php

namespace App\Services\Dto;

class CustomerReportRow
{
    public $first_name;
    public $last_name;
    public $level;
    public $goals;
    public $environment;
    public $age;
    public $gender;
}
