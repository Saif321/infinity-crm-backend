<?php
declare( strict_types=1 );

namespace App\Services\Dto;

class TrainingStatistics {
	public $sets = 0;
	public $exercises = 0;
	public $calories_formatted = '';
}
