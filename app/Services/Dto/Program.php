<?php
declare(strict_types = 1);
namespace App\Services\Dto;

class Program {
	public $object_id;
	public $name;
	public $image_url;
	public $level_name;
	public $average_rating;
	public $number_of_reviews;
	public $is_free;
}
