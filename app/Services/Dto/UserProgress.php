<?php
declare( strict_types=1 );

namespace App\Services\Dto;

class UserProgress {
    public $programs = 0;
    public $weeks = 0;
    public $workouts = 0;
}
