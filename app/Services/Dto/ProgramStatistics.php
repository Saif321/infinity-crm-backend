<?php
declare( strict_types=1 );

namespace App\Services\Dto;

class ProgramStatistics {
	public $program_name = '';
	public $weeks = 0;
	public $trainings = 0;
}
