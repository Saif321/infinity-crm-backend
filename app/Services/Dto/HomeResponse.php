<?php

namespace App\Services\Dto;

class HomeResponse {
	public $programs;
	public $purchased_programs;
	public $nutrition_programs;
}