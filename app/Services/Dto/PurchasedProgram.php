<?php
declare(strict_types = 1);
namespace App\Services\Dto;

class PurchasedProgram {
	public $object_id;
	public $name = '';
	public $image_url = '';
	public $level_name = '';
	public $current_week = 0;
	public $current_week_trainings = 0;
	public $current_week_finished_trainings = 0;
}