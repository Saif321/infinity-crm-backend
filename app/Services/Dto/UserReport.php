<?php


namespace App\Services\Dto;


class UserReport{
    public $firstName;
    public $lastName;
    public $levels;
    public $goals;
    public $environments;
    public $gender;
    public $age;
}
