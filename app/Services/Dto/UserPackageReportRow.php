<?php

namespace App\Services\Dto;

class UserPackageReportRow
{
    public $user_id;
    public $email;
    public $package_name;
    public $valid_until;
    public $last_renewal_attempt;
    public $renew_attempts;
    public $renew_stopped;
    public $payment_method;
    public $charged_value;
    public $currency_code;
    public $updated_at;
    public $created_at;
    public $is_free_trial;
}
