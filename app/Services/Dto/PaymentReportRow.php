<?php

namespace App\Services\Dto;

class PaymentReportRow
{
    public $method;
    public $status;
    public $transaction_id;
    public $details;
    public $charged_value;
    public $currency_code;
    public $package_name;
    public $updated_at;
    public $created_at;
    public $number_of_history_items;
}
