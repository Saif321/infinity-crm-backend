<?php
declare( strict_types=1 );

namespace App\Services\Dto;

class ProfileInfo {
    public $posts;
    public $followers;
    public $following;
}
