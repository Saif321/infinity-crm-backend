<?php
declare( strict_types=1 );

namespace App\Services\Dto;

class UserStatistics {
    public $progress;
    public $weight;
    public $profile;
}
