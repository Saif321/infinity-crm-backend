<?php
declare( strict_types=1 );

namespace App\Services\Dto;

class WeightChange {
    public $initial;
    public $current;
    public $difference;
}
