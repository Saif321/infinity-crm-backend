<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\Package;
use App\Models\User;
use App\Models\UserPackage as UserPackageModel;
use App\Repositories\UserPackageRepository;
use App\Services\Dto\UserPackageReportRow;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;

class UserPackage
{
    private $userPackageRepository;

    public function __construct(
        UserPackageRepository $userPackageRepository
    ) {
        $this->userPackageRepository = $userPackageRepository;
    }

    public function createPackage(
        User $user,
        Package $package,
        bool $testMode,
        \DateTime $validUntil = null
    ): UserPackageModel {
        if ($this->isUserEligibleForFreeTrial($user)) {
            $freeTrial = true;
            $days = $package->free_trial_in_days + 1;
        } else {
            $freeTrial = false;
            $days = $package->recurring_period_in_days + 1;
        }
        return $this->storePackage($user, $package, $days, $testMode, $freeTrial, $validUntil);
    }

    public function createFreeTrialPackage($user, $package, $validUntil)
    {
        $validUntil = Carbon::instance($validUntil);
        return $this->userPackageRepository->createPackage($validUntil, $user, $package, true);
    }

    public function isUserEligibleForFreeTrial(User $user): bool
    {
        return $this->userPackageRepository->countNumberOfPackegesForUser($user->id) < 1;
    }

    /**
     * @param string $search
     * @return Builder
     */
    public function getUserPackageReport(
        $search = null,
        bool $onlyActive = false,
        bool $includeFreeTrial = false
    ): Builder {
        return $this->userPackageRepository->getUserPackageReport($search, $onlyActive, $includeFreeTrial);
    }

    public function mapToUserPackageReportRow(object $data): UserPackageReportRow
    {
        $row = new UserPackageReportRow();
        $row->user_id = $data->user_id;
        $row->email = $data->email;
        $row->package_name = $data->package_name;
        $row->valid_until = (new Carbon($data->valid_until))->format('c');
        $row->last_renewal_attempt = $data->last_renewal_attempt ? (new Carbon($data->last_renewal_attempt))->format('c') : null;
        $row->renew_attempts = $data->renew_attempts;
        $row->payment_method = $data->payment_method ?? 'MANUAL';
        $row->charged_value = $data->charged_value;
        $row->currency_code = $data->currency_code;
        $row->renew_stopped = $data->renew_stopped;
        $row->is_free_trial = $data->is_free_trial;
        $row->updated_at = $data->updated_at ? (new Carbon($data->updated_at))->format('c') : null;
        $row->created_at = $data->created_at ? (new Carbon($data->created_at))->format('c') : null;
        return $row;
    }

    public function getUserPackageByPaymentId(int $paymentId): ?UserPackageModel
    {
        return $this->userPackageRepository->getUserPackageByPaymentId($paymentId);
    }

    public function getCurrentActiveUserPackage(int $userId): ?UserPackageModel
    {
        return $this->userPackageRepository->getCurrentActiveUserPackage($userId);
    }

    public function storePackage(
        User $user,
        Package $package,
        int $days,
        bool $testMode,
        bool $freeTrial,
        \DateTime $validUntil = null
    ): UserPackageModel {
        if ($validUntil === null) {
            if ($testMode) {
                $validUntil = Carbon::now()->addMinutes(intval($days / 3));
            } else {
                $validUntil = Carbon::now()->addDays($days);
            }
        } else {
            $validUntil = Carbon::instance($validUntil);
        }
        return $this->userPackageRepository->createPackage($validUntil, $user, $package, $freeTrial);
    }
}
