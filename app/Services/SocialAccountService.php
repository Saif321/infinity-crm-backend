<?php

namespace App\Services;

use App\Models\User;
use App\Repositories\GalleryRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Storage;
use Laravel\Socialite\Two\User as ProviderUser;
use Prettus\Repository\Exceptions\RepositoryException;
use Prettus\Validator\Exceptions\ValidatorException;
use Spatie\Permission\Models\Role;

class SocialAccountService
{

    private $userRepository;
    private $galleryRepository;

    public function __construct(UserRepository $userRepository, GalleryRepository $galleryRepository)
    {
        $this->userRepository = $userRepository;
        $this->galleryRepository = $galleryRepository;
    }

    /**
     * Find or create user instance by provider user instance and provider name.
     *
     * @param ProviderUser $providerUser
     * @param string $provider
     *
     * @return User
     * @throws RepositoryException
     * @throws ValidatorException
     */
    public function findOrCreate(ProviderUser $providerUser, string $provider): User
    {
        $user = $this->userRepository->findByField('email', $providerUser->getEmail())->first();
        if (!$user) {
            $user = $this->createUserFromProviderUser($providerUser);
        }
        if ($provider === 'google') {
            $user->google_id = $providerUser->getId();
        } else if ($provider === 'apple') {
            $user->apple_id = $providerUser->getId();
        } else if ($provider === 'facebook' || $provider === 'Facebook') {
            $user->fb_id = $providerUser->getId();
        } else {
            return null;
        }
        $user->save();
        $user->load('roles', 'image');
        return $user;
    }

    /**
     * @param ProviderUser $providerUser
     * @return User
     * @throws ValidatorException
     * @throws RepositoryException
     */
    public function createUserFromProviderUser(ProviderUser $providerUser): User
    {
        $user = $this->userRepository->create([
            'first_name' => $providerUser->getName(),
            'last_name' => $providerUser->getNickname(),
            'email' => $providerUser->getEmail(),
            'is_active' => true
        ]);
        $user->assignRole(Role::findByName('customer'));
        $this->saveAvatar($providerUser->getAvatar(), $user);
        return $user;
    }


    /**
     * @param $avatar
     * @param User $user
     * @throws RepositoryException
     */
    public function saveAvatar($avatar, User $user)
    {
        if ($avatar) {
            $picture = file_get_contents($avatar);
            $imageData = getimagesize($avatar);
            $extension = preg_replace('/^image\//is', '', $imageData['mime']);
            $fileName = md5(microtime());
            $directory = $user->id.'/'.$fileName[0].'/'.$fileName[1].'/'.$fileName[2];
            Storage::put('public/avatars/'.$directory.'/'.$fileName.'.'.$extension, $picture);
            if ($user && $user->image_id) {
                $gallery = $this->galleryRepository->find($user->image_id);
            } else {
                $gallery = $this->galleryRepository->makeModel();
            }
            $gallery->name = pathinfo($avatar)['filename'];
            $gallery->mime_type = $imageData['mime'];
            $gallery->extension = $extension;
            $gallery->size = 0;
            $gallery->url = 'public/avatars/'.$directory.'/'.$fileName.'.'.$extension;
            $gallery->type = 'image';
            $gallery->is_user_avatar = true;
            $gallery->save();
            $user->image_id = $gallery->id;
            $user->save();
        }
    }

}
