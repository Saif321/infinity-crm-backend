<?php
declare( strict_types=1 );

namespace App\Services;

use App\Repositories\LanguageRepository;
use Illuminate\Database\Eloquent\Model;

class Translation
{
    private $languageRepository;

    public function __construct(LanguageRepository $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }

    /**
     * @param Model $item
     * @param mixed $translations
     *
     * @return mixed
     */
    public function translateObject( Model $item, array $translations )
    {
        $translatedItems = [];
        $sentLanguageIds = [];

        $itemArray = $item->getOriginal();

        $modelClassNameParts = explode('\\', get_class($item));
        $modelClassName = array_pop($modelClassNameParts);
        $repositoryClassName = 'App\\Repositories\\' . $modelClassName . 'Repository';
        $repository = app()->make($repositoryClassName);

        unset($itemArray['id']);

        foreach($translations as $translation) {
            $itemArray['object_id'] = $item->id;
            foreach($translation as $field => $value) {
                if(is_object($value) || is_array($value)) {
                    unset($translation[$field]);
                    continue;
                }

                $itemArray[$field] = $value;
            }

            $sentLanguageIds[] = $translation['language_id'];
            $translatedItems[] = $repository->saveTranslation($item->id, $translation['language_id'], $itemArray);

            if($translation['language_id'] == $this->languageRepository->getDefaultLanguage()->id) {
                foreach($translation as $field => $value) {
                    if(in_array($field, ['object_id', 'language_id', 'id'])) {
                        continue;
                    }
                    $item->setAttribute($field, $value);
                }
			    $item->save();
		    }
        }

        $unpopulatedLanguages = $this->languageRepository->findWhereNotIn('id', $sentLanguageIds);
        foreach($unpopulatedLanguages as $unpopulatedLanguage) {
            $itemArray['object_id'] = $item->id;
            $itemArray['language_id'] = $unpopulatedLanguage->id;
            $translatedItems[] = $repository->saveTranslation($item->id, $unpopulatedLanguage->id, $itemArray);
        }

        return $translatedItems;
    }

}
