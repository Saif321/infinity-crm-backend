<?php

namespace App\Services;

use App\Http\Responses\JsonResponse;
use Illuminate\Support\Facades\Log;

class Notification
{
    protected $allowedObjectTypes;
    protected $platforms;
    protected $config;

    public function __construct()
    {
        $this->allowedObjectTypes = ['Blog', 'Program', 'NutritionProgram'];
        $this->platforms = ['iOS', 'Android'];
        $this->config = config('infinity-fit.notifications');
    }

    /**
     * @param $objectId
     * @param $objectType
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function sendNotification($objectId, $objectType)
    {
        $objectClass = 'App\Models\\'.$objectType;
        if (!in_array($objectType, $this->allowedObjectTypes) || !class_exists($objectClass)) {
            return response()->json(
                new JsonResponse(null, false, 6, trans('errors.6'))
            );
        }

        $objectExists = $objectClass::where('id', $objectId)->first();
        if (!$objectExists) {
            return response()->json(
                new JsonResponse(null, false, 6, trans('errors.6'))
            );
        }

        $repositoryType = 'App\Repositories\\'.$objectType.'Repository';
        $repository = app()->make($repositoryType);
        $object = $repository->find($objectId);

        $objectTitle = isset($object->name) ? $object->name : $object->title;
        $objectImage = $object->image->url;

        $result = [];
        foreach ($this->platforms as $platform) {
            $result[] = $this->fireBase($platform, $objectId, $objectType, $objectTitle, $objectImage);
        }

        return response()->json(
            new JsonResponse($result)
        );
    }

    /**
     * @param string $platform
     * @param integer $id
     * @param string $type
     * @param string $title
     * @param string $image
     *
     * @return string
     */
    private function fireBase($platform, $id, $type, $title, $image)
    {
        $headers = [
            'Authorization: key='.$this->config['firebase']['api_key'],
            'Content-Type: application/json'
        ];

        $topic = sprintf('/topics/%s-%s', $platform, $type);
        $body = [
            'to' => $topic
        ];
        $type_rn = 0;
        switch ($type) {
            case 'Blog' :
                $type = 'New Blog';
                $type_rn = 1;
                break;
            case 'Program' :
                $type = 'New Workout program';
                $type_rn = 2;
                break;
            case 'NutritionProgram' :
                $type = 'New Nutrition program';
                $type_rn = 3;
                break;
        }

        if ($platform == 'iOS') {
            $body['notification'] = [
                'title' => $type,
                'body' => $title,
            ];
            $body['data'] = [
                'topic' => $topic,
                'id' => $id,
                'image' => $image,
                'type' => $type_rn,
            ];
        } else {
            $body['data'] = [
                'id' => $id,
                'title' => $type,
                'message' => $title,
                'image' => $image,
                'type' => $type_rn,
            ];
        }

        $jsonBody = json_encode($body);
        Log::debug('Notification with body: '.PHP_EOL.$jsonBody);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->config['firebase']['url']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
        $result = curl_exec($ch);

        curl_close($ch);

        Log::debug('Notification response: '.PHP_EOL.$result);

        json_decode($result);
        if (json_last_error() !== JSON_ERROR_NONE) {
            Log::error(json_last_error_msg().' -> '.$result);
        }

        return $result;
    }
}
