<?php
declare( strict_types=1 );

namespace App\Services;

use App\Models\ExerciseRoundItem;
use App\Models\ExerciseSet;
use App\Models\UserExerciseRoundItem;
use App\Models\UserExerciseSet;
use App\Models\UserRound;
use App\Models\UserTraining;
use App\Repositories\ExerciseRoundItemRepository;
use App\Repositories\PurchasedProgramRepository;
use App\Repositories\RoundRepository;
use App\Repositories\UserExerciseRoundItemRepository;
use App\Repositories\UserExerciseSetRepository;
use App\Repositories\UserProgramRepository;
use App\Repositories\UserRoundRepository;
use App\Repositories\UserTrainingRepository;
use App\Services\Dto\TrainingStatistics;
use App\Services\Exceptions\NotCurrentUserProgramException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class Training {
	private $programService;
	private $userTrainingRepository;
	private $userProgramRepository;
	private $purchasedProgramRepository;
	Private $userExerciseRoundItemRepository;
	private $userExerciseSetRepository;
	private $userRoundRepository;

	private $roundRepository;
	private $exerciseRoundItemRepository;

	public function __construct(
		Program $programService,
		UserTrainingRepository $userTrainingRepository,
		UserProgramRepository $userProgramRepository,
		PurchasedProgramRepository $purchasedProgramRepository,
		UserExerciseRoundItemRepository $userExerciseRoundItemRepository,
		UserExerciseSetRepository $userExerciseSetRepository,
		UserRoundRepository $userRoundRepository,

        RoundRepository $roundRepository,
        ExerciseRoundItemRepository $exerciseRoundItemRepository
	) {
		$this->programService                  = $programService;
		$this->userTrainingRepository          = $userTrainingRepository;
		$this->userProgramRepository           = $userProgramRepository;
		$this->purchasedProgramRepository      = $purchasedProgramRepository;
		$this->userExerciseRoundItemRepository = $userExerciseRoundItemRepository;
		$this->userExerciseSetRepository       = $userExerciseSetRepository;
		$this->userRoundRepository             = $userRoundRepository;

		$this->roundRepository                 = $roundRepository;
		$this->exerciseRoundItemRepository     = $exerciseRoundItemRepository;
	}

	/**
	 * @param $trainingId
	 * @param int $tillUserExerciseRoundItemId
	 *
	 * @throws NotCurrentUserProgramException
	 */
	public function finishTraining( $trainingId, $tillUserExerciseRoundItemId = 0 ) {
		$training         = $this->userTrainingRepository
			->findTranslation( $trainingId, Config::get( 'language' )['id'] );
		$program          = $this->userProgramRepository->getProgramFromTraining( $training->object_id );
		$purchasedProgram = $this->purchasedProgramRepository->findByField( 'user_program_id', $program->id )->first();

		if ( $purchasedProgram->user_id != Auth::user()->getAuthIdentifier() ) {
			throw new NotCurrentUserProgramException();
		}
		$this->programService->clearPurchasedProgramCache( $program->id );

		if ( $tillUserExerciseRoundItemId !== 0 ) {
			$userExerciseRoundItem              = $this->userExerciseRoundItemRepository->find( $tillUserExerciseRoundItemId );
			$userExerciseRoundItem->is_finished = true;
			$userExerciseRoundItem->finished_at = date( 'Y-m-d H:i:s' );

			$userExerciseRoundItem->save();
		}

		$exerciseSets         = $this->userExerciseSetRepository->getSetsForTraining( $training->object_id )->get();
		foreach ( $exerciseSets as $exerciseSet ) {
			$exerciseSetRounds = $this->userRoundRepository->getRoundsForExerciseSet( $exerciseSet->object_id )->get();
			foreach ( $exerciseSetRounds as $round ) {
				$roundItems = $this->userExerciseRoundItemRepository->getRoundItems($round->id)->get();
				foreach ( $roundItems as $item ) {
					if (! $item->is_finished ) {
						$item->is_finished = true;
						$item->finished_at = date( 'Y-m-d H:i:s' );
						$item->save();
					}

					if ( isset( $userExerciseRoundItem ) && $item->id == $userExerciseRoundItem->id ) {
						break 3;
					}
				}
			}
		}
	}

	/**
	 * @param $trainingId
	 *
	 * @throws NotCurrentUserProgramException
	 */
	public function resetTraining( $trainingId ) {
		$training         = $this->userTrainingRepository
			->findTranslation( $trainingId, Config::get( 'language' )['id'] );
		$program          = $this->userProgramRepository->getProgramFromTraining( $training->object_id );
		$purchasedProgram = $this->purchasedProgramRepository->findByField( 'user_program_id', $program->id )->first();

		if ( $purchasedProgram->user_id != Auth::user()->getAuthIdentifier() ) {
			throw new NotCurrentUserProgramException();
		}
		$this->programService->clearPurchasedProgramCache( $program->id );

		$exerciseSets = $this->userExerciseSetRepository->getSetsForTraining( $training->object_id )->get();
		foreach ( $exerciseSets as $exerciseSet ) {
			$exerciseSetRounds = $this->userRoundRepository->getRoundsForExerciseSet( $exerciseSet->object_id )->get();
			foreach ( $exerciseSetRounds as $round ) {
				$roundItems = $this->userExerciseRoundItemRepository->findWhere( [ 'user_round_id' => $round->id ] );
				foreach ( $roundItems as $item ) {
					$item->is_finished = false;
					$item->finished_at = null;
					$item->save();
				}
			}
		}
	}


	/**
	 * @param $userTrainingId
	 *
	 * @return UserTraining|null
	 */
	public function getNextTrainingForProgramWithTraining( $userTrainingId ) {
		$userProgram = $this->userProgramRepository->getProgramFromTraining( $userTrainingId );

		return $this->programService->getNextTraining( $userProgram->id );
	}

	/**
	 * @param $trainingId
	 * @param $roundItemId
	 * @param $userNumberOfReps
	 * @param $userStrengthKilos
	 *
	 * @throws NotCurrentUserProgramException
	 */
	public function setRoundItemUserData( $trainingId, $roundItemId, $userNumberOfReps, $userStrengthKilos ) {
		$training         = $this->userTrainingRepository
			->findTranslation( $trainingId, Config::get( 'language' )['id'] );
		$program          = $this->userProgramRepository->getProgramFromTraining( $training->object_id );
		$purchasedProgram = $this->purchasedProgramRepository->findByField( 'user_program_id', $program->id )->first();

		if ( $purchasedProgram->user_id != Auth::user()->getAuthIdentifier() ) {
			throw new NotCurrentUserProgramException();
		}

		$userExerciseRoundItem = $this->userExerciseRoundItemRepository->find( $roundItemId );

		$userExerciseRoundItem->user_number_of_reps = $userNumberOfReps;
		$userExerciseRoundItem->user_strength_kilos = $userStrengthKilos;

		$userExerciseRoundItem->save();
	}

	/**
	 * @param UserTraining $training
	 *
	 * @return TrainingStatistics
	 */
	public function getTrainingStatistics( UserTraining $training ): TrainingStatistics {
		$statistics                     = new TrainingStatistics();
		$statistics->calories_formatted = $this->formatCalories( $training->number_of_calories );

		/** @var UserExerciseSet|Collection $exerciseSets */
		$exerciseSets     = $this->userExerciseSetRepository->getSetsForTraining( $training->getId() )->get();
		$statistics->sets += $exerciseSets->count();
		foreach ( $exerciseSets as $exerciseSet ) {
			/** @var UserRound[]|Collection $exerciseSetRounds */
			$exerciseSetRounds = $this->userRoundRepository->getRoundsForExerciseSet( $exerciseSet->object_id )->get();
			foreach ( $exerciseSetRounds as $round ) {
				/** @var UserExerciseRoundItem[]|Collection $roundItems */
				$roundItems            = $this->userExerciseRoundItemRepository->findWhere( [ 'user_round_id' => $round->id ] );
				$onlyExercises         = $roundItems->filter( function ( UserExerciseRoundItem $item ) {
					return $item->type === 'exercise';
				} );
				$statistics->exercises += $onlyExercises->count();
			}
		}

		return $statistics;
	}

    /**
     * @param Training $training
     * @param Collection|ExerciseSet $exerciseSets
     *
     * @return TrainingStatistics
     */
    public function getFreeTrainingStatistics(
        \App\Models\Training $training,
        Collection $exerciseSets
    ): TrainingStatistics {
        $statistics                     = new TrainingStatistics();
        $statistics->calories           = $training->number_of_calories;
        $statistics->calories_formatted = $this->formatCalories( $statistics->calories );

        /** @var ExerciseSet|Collection $exerciseSets */
        $statistics->sets += $exerciseSets->count();
        foreach ( $exerciseSets as $exerciseSet ) {
            /** @var Round[]|Collection $exerciseSetRounds */
            $exerciseSetRounds = $this->roundRepository->getRoundsForExerciseSet( $exerciseSet->object_id )->get();
            foreach ( $exerciseSetRounds as $round ) {
                /** @var ExerciseRoundItem[]|Collection $roundItems */
                $roundItems            = $this->exerciseRoundItemRepository->findWhere( [ 'round_id' => $round->id ] );
                $onlyExercises         = $roundItems->filter( function ( ExerciseRoundItem $item ) {
                    return $item->type === 'exercise';
                } );
                $statistics->exercises += $onlyExercises->count();
            }
        }

        return $statistics;
    }

	/**
	 * @param integer $calories
	 *
	 * @return string
	 */
	private function formatCalories( $calories ) {
		$percentage = $calories * 5 / 100;
		$min        = round( $calories - $percentage );
		$max        = round( $calories + $percentage );

		return "{$min} - {$max}";
	}
}
