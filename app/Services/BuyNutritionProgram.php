<?php

namespace App\Services;

use App\Models\NutritionProgram;
use App\Models\PurchasedNutritionProgram;
use App\Models\User;
use App\Repositories\NutritionProgramRepository;
use App\Repositories\PurchasedNutritionProgramRepository;
use App\Repositories\DeactivatedNutritionProgramRepository;
use App\Repositories\UserPackageRepository;
use App\Services\Exceptions\NotFreeProgramException;
use App\Services\Exceptions\PackageLimitsReachedException;
use App\Services\Exceptions\ProgramAlreadyPurchasedException;
use Illuminate\Support\Facades\Auth;
use Prettus\Validator\Exceptions\ValidatorException;

class BuyNutritionProgram
{
    const NUTRITION_PROGRAM_STATE_CAN_ACTIVATE = 'CAN_ACTIVATE';
    const NUTRITION_PROGRAM_STATE_BUY_PLAN = 'BUY_PLAN';
    const NUTRITION_PROGRAM_STATE_CHANGE_PLAN = 'CHANGE_PLAN';
    const NUTRITION_PROGRAM_STATE_ALREADY_ACTIVE = 'ALREADY_ACTIVE';

    private $nutritionProgramRepository;
    private $purchasedNutritionProgramRepository;
    private $deactivatedNutritionProgramRepository;
    private $userPackageRepository;
    private $usePackages = false;

    public function __construct(
        NutritionProgramRepository $nutritionProgramRepository,
        PurchasedNutritionProgramRepository $purchasedNutritionProgramRepository,
        DeactivatedNutritionProgramRepository $deactivatedNutritionProgramRepository,
        UserPackageRepository $userPackageRepository
    ) {
        $this->nutritionProgramRepository = $nutritionProgramRepository;
        $this->purchasedNutritionProgramRepository = $purchasedNutritionProgramRepository;
        $this->deactivatedNutritionProgramRepository = $deactivatedNutritionProgramRepository;
        $this->userPackageRepository = $userPackageRepository;

        $this->usePackages = config('infinity-fit.use_packages', false);
    }

    private function hasEnoughLimitsToActivateProgram(
        User $user,
        \App\Models\UserPackage $userPackage,
        int $nutritionProgramId
    ): bool {
        $deactivatedPrograms = $this->deactivatedNutritionProgramRepository->getDistinctDeactivatedProgramsForPackage($user->id,
            $userPackage->id);
        if ($userPackage->free_trial) {
            if ($userPackage->package->number_of_free_trial_nutrition_programs <= $userPackage->purchasedNutritionPrograms->count() + count($deactivatedPrograms) && !in_array($nutritionProgramId,
                    $deactivatedPrograms)) {
                return false;
            }
        } else {
            if ($userPackage->package->number_of_nutrition_programs <= $userPackage->purchasedNutritionPrograms->count() + count($deactivatedPrograms) && !in_array($nutritionProgramId,
                    $deactivatedPrograms)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param NutritionProgram $nutritionProgram
     * @param User $user
     * @param bool $preventUsePackages
     * @return PurchasedNutritionProgram
     * @throws NotFreeProgramException
     * @throws PackageLimitsReachedException
     * @throws ProgramAlreadyPurchasedException
     * @throws ValidatorException
     */
    public function buy(
        NutritionProgram $nutritionProgram,
        User $user,
        bool $preventUsePackages = false
    ): PurchasedNutritionProgram {
        $userPackage = null;
        if ($this->usePackages && !$preventUsePackages) {
            $userPackage = $this->userPackageRepository->getCurrentActiveUserPackage($user->id);
            if (!$nutritionProgram->is_free && !$userPackage) {
                throw new NotFreeProgramException();
            }

            if (!$nutritionProgram->is_free && $userPackage) {
                if (!$this->hasEnoughLimitsToActivateProgram($user, $userPackage, $nutritionProgram->id)) {
                    throw new PackageLimitsReachedException();
                }
            }
        }

        if ($this->nutritionProgramRepository->findIsPurchased($user->id, $nutritionProgram->id)) {
            throw new ProgramAlreadyPurchasedException();
        }

        return $this->purchasedNutritionProgramRepository->create([
            'user_id' => $user->id,
            'nutrition_program_id' => $nutritionProgram->id,
            'user_package_id' => !$nutritionProgram->is_free && $userPackage ? $userPackage->id : null
        ]);
    }

    public function getCanActivateState(NutritionProgram $program, User $user): string
    {
        $nutritionProgramId = $program->object_id ?? $program->id;
        if ($this->isPurchased($user->id, $nutritionProgramId)) {
            return static::NUTRITION_PROGRAM_STATE_ALREADY_ACTIVE;
        }

        if (!$this->usePackages || $program->is_free) {
            return static::NUTRITION_PROGRAM_STATE_CAN_ACTIVATE;
        }

        $userPackage = null;
        if ($this->usePackages) {
            $userPackage = $this->userPackageRepository->getCurrentActiveUserPackage($user->id);
            if (!$userPackage) {
                return static::NUTRITION_PROGRAM_STATE_BUY_PLAN;
            }
            if (!$this->hasEnoughLimitsToActivateProgram($user, $userPackage, $nutritionProgramId)) {
                return static::NUTRITION_PROGRAM_STATE_CHANGE_PLAN;
            }
        }

        return static::NUTRITION_PROGRAM_STATE_CAN_ACTIVATE;
    }

    public function isPurchased(int $userId, int $nutritionProgramId): bool
    {
        return $this->nutritionProgramRepository->findIsPurchased($userId, $nutritionProgramId);
    }
}
