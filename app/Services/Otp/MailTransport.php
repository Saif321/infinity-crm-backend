<?php

namespace App\Services\Otp;

use App\Mail\OtpMail;
use App\Models\VerificationCode;
use Illuminate\Support\Facades\Mail;

class MailTransport implements TransportInterface
{
    public function send(VerificationCode $verificationCode)
    {
        try {
            Mail::to($verificationCode->destination)->send(new OtpMail([
                'code' => $verificationCode
            ]));
        } catch (\Exception $e) {
            throw $e;
        }

        return true;
    }
}
