<?php

namespace App\Services\Otp;


class ExceptionDestinationRequired extends \Exception
{
    public function __construct()
    {
        parent::__construct('Destination is required.');
    }
}