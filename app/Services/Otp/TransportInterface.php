<?php

namespace App\Services\Otp;

use App\Models\VerificationCode;

interface TransportInterface
{
    public function send(VerificationCode $verificationCode);
}