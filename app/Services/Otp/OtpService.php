<?php

namespace App\Services\Otp;

use App\Repositories\VerificationCodeRepository;

class OtpService
{
    private $destination;
    private $verificationCodeRepository;
    private $verificationCode;
    private $transport = null;

    public function __construct(VerificationCodeRepository $verificationCodeRepository)
    {
        $this->verificationCodeRepository = $verificationCodeRepository;
    }

    public function setDestination($destination)
    {
        if(!preg_match('/^(.*?)\@(.*?)\.(.*?)$/', $destination)) {
            throw new ExceptionDestinationRequired();
        }

        $this->destination = $destination;
        return $this;
    }

    public function setTransportForDestination()
    {
        if(!$this->destination) {
            throw new ExceptionDestinationRequired();
        }

        $this->transport = app()->make(MailTransport::class);

        return $this;
    }

    public function generate()
    {
        if(!$this->destination) {
            throw new ExceptionDestinationRequired();
        }

        $code = rand(1000, 9999);

        $this->verificationCode = $this->verificationCodeRepository->create([
            'destination' => $this->destination,
            'code' => $code
        ]);

        return $this;
    }

    public function getVerificationCode()
    {
        return $this->verificationCode;
    }

    public function send()
    {
        if(!$this->destination) {
            throw new ExceptionDestinationRequired();
        }

        return $this->transport->send($this->verificationCode);
    }

    public function verify($destination, $code)
    {
        return $this->verificationCodeRepository->getCodeForDestination($destination, $code)->count();
    }

    public function invalidate($destination, $code)
    {
        $verificationCode = $this->verificationCodeRepository->findWhere(['destination' => $destination, 'code' => $code])->first();
        $verificationCode->is_used = true;
        $verificationCode->save();

        return $verificationCode;
    }
}
