<?php

namespace App\Providers;

use App\Auth\Grant\RefreshTokenGrant;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Bridge\RefreshTokenRepository;
use Laravel\Passport\Passport;
use League\OAuth2\Server\AuthorizationServer;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes(null, ['prefix' => 'api/v1.0/oauth']);
        Passport::tokensExpireIn(now()->addMinutes(config('infinity-fit.access_token_duration')));
        Passport::refreshTokensExpireIn(now()->addMinutes(config('infinity-fit.refresh_token_duration')));

        if (!app()->runningInConsole()) {
            app(AuthorizationServer::class)->enableGrantType(
                $this->makeRefreshTokenGrant(), Passport::tokensExpireIn()
            );
        }
    }

    protected function makeRefreshTokenGrant()
    {
        $repository = $this->app->make(RefreshTokenRepository::class);

        return tap(new RefreshTokenGrant($repository), function (RefreshTokenGrant $grant) {
            $grant->setRefreshTokenTTL(Passport::refreshTokensExpireIn());
        });
    }
}
