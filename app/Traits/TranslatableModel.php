<?php
namespace App\Traits;

trait TranslatableModel
{
    public function normalisedTranslation()
    {
//        $data = $this->getAttributes();

//        foreach($data as $key => $value) {
//            if(preg_match('/^__/', $key)) {
//                $data[str_replace('__', '', $key)] = $value;
//                unset($data[$key]);
//            }
//
//            if($key == 'object_id') {
//                $data['id'] = $value;
//                unset($data[$key]);
//            }
//
//            if($key == 'language_id') {
//                unset($data[$key]);
//            }
//        }

//        $this->setRawAttributes($data, true);

        return $this;
    }

    /**
     * @return integer|null
     */
    public function getId()
    {
        if(empty($this->attributes)) {
            return null;
        }

        return array_key_exists('object_id', $this->attributes) ? $this->attributes['object_id'] : $this->attributes['id'];
    }
}
