<?php
namespace App\Traits;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @property Model|Builder $model
 */
trait TranslatableRepository
{
	private $baseTableName;
	private $translatableTableName;

    public function findTranslatable($id, $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();
        //$model = $this->model->findOrFail($id, $columns);

        $results = $this->setTranslatableTable()->where('object_id', $id)->select($columns)->first();
        $this->resetModel();

        return $this->parserResult($results);
    }

    public function getTranslatable($columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();

        $model = $this->setTranslatableTable();
        $results = $model->get($columns);

        $this->resetModel();
        $this->resetScope();

        return $this->parserResult($results);
    }

	/**
	 * @return Builder
	 */
    public function setTranslatableTable()
    {
        $this->model->setTable($this->getTranslatableTableName());
        $model = $this->model->where('language_id', Config::get('language')['id']);

        return $model;
    }

	/**
	 * @return string
	 */
    public function getTranslatableTableName(){
    	if(!$this->translatableTableName){
		    $this->baseTableName = $this->model->getTable();
		    $this->translatableTableName = '__' . ltrim($this->baseTableName, '_');
	    }
		return $this->translatableTableName;
    }

	/**
	 * @return Builder|Model
	 */
    public function makeTranslatableModel()
    {
        $this->model->setTable($this->getTranslatableTableName());
        return $this->model;
    }

    public function parserResult($result)
    {
        $result = parent::parserResult($result);

        if ($result instanceof Collection || $result instanceof LengthAwarePaginator) {
            foreach ($result as &$item) {
                $item->normalisedTranslation();
            }
        } else {
            if(empty($result)) {
                throw new NotFoundHttpException('No data found');
            }

            $result = $result->normalisedTranslation();
        }

        return $result;
    }

    public function saveTranslation($itemId, $languageId, $data, $debug = false)
    {
        $table = $this->model->getTable();
        $translatableTable = '__' . ltrim($table, '_');

        $fields = [];
        $values = [];
        foreach($data as $key => $value) {
            if(is_array($value)) {
                continue;
            }

            $fields[] = $key;
            $values[] = $value;
        }

        try {
            $item = DB::select("SELECT * FROM " . $translatableTable . " WHERE object_id = ? AND language_id = ?", [$itemId, $languageId])[0];
            $sql = "UPDATE " . $translatableTable . " SET ";
            $isFirst = true;
            foreach($data as $key => $value) {
                if(is_array($value)) {
                    continue;
                }

                $sql .= (!$isFirst ? ", " : '') . "`" . $key . "`=?";

                $isFirst = false;
            }
            $sql .= " WHERE id = ?;";

            $values[] = $item->id;

            DB::update($sql, $values);
        } catch(\Exception $e) {
            $sql = "INSERT INTO " . $translatableTable . " SET ";
            $isFirst = true;
            foreach($data as $key => $value) {
                if(is_array($value)) {
                    continue;
                }

                $sql .= (!$isFirst ? ", " : '') . "`" . $key . "`=?";

                $isFirst = false;
            }

//            if($debug) {
//                echo $sql; print_r($values); exit;
//            }

            DB::insert($sql, $values);
        }

        return DB::select("SELECT * FROM " . $translatableTable . " WHERE object_id = ? AND language_id = ?", [$itemId, $languageId])[0];
    }

    public function deleteTranslations($itemId, $languageId = null)
    {
        $table = $this->model->getTable();
        $translatableTable = '__' . ltrim($table, '_');

        $sql = "DELETE FROM " . $translatableTable . " WHERE object_id = ?";
        $bindings = [];
        $bindings[] = $itemId;

        if($languageId) {
            $sql .= " AND language_id = ?";
            $bindings[] = $languageId;
        }

        return DB::delete($sql, $bindings);
    }

    public function deleteTranslationsWhere($where = [], $languageId = null)
    {
        if(empty($where)) {
            throw new \Exception('Where is empty!');
        }

        $table = $this->model->getTable();
        $translatableTable = '__' . ltrim($table, '_');

        $sql = "DELETE FROM " . $translatableTable . " WHERE ";
        $bindings = [];

        $isFirst = true;
        foreach($where as $key => $value) {
            $bindings[] = $value;
            if($isFirst) {
                $isFirst = false;
                $sql .= $key . " = ?";
            } else {
                $sql .= " AND " . $key . " = ?";
            }
        }

        if($languageId) {
            $sql .= " AND language_id = ?";
            $bindings[] = $languageId;
        }

        return DB::delete($sql, $bindings);
    }

    public function getTranslations($itemId)
    {
        $table = $this->model->getTable();
        $translatableTable = '__' . ltrim($table, '_');

        return $this->model
            ->select('' . $translatableTable . '.*')
            ->join($translatableTable, $translatableTable . '.object_id', '=', $table . '.id')
            ->where($translatableTable . '.object_id', $itemId)
            ->orderBy('language_id');
    }

	/**
	 * @param $itemId
	 * @param $languageId
	 *
	 * @return Model
	 */
    public function findTranslation($itemId, $languageId)
    {
        $table = $this->model->getTable();
        $translatableTable = '__' . ltrim($table, '_');

        return $this->model
            ->select('' . $translatableTable . '.*')
            ->join($translatableTable, $translatableTable . '.object_id', '=', $table . '.id')
            ->where($translatableTable . '.object_id', $itemId)
            ->where($translatableTable . '.language_id', $languageId)
            ->first();
    }
}
