<?php

namespace App\Exceptions;

use App\Http\Responses\JsonResponse;
use Exception;
use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\Exceptions\InvalidAuthTokenException;
use Laravel\Passport\Exceptions\MissingScopeException;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Spatie\Permission\Exceptions\UnauthorizedException;

class Handler extends ExceptionHandler
{
    protected $oauthExceptions = [
        OAuthServerException::class,
        UniqueTokenIdentifierConstraintViolationException::class,
        InvalidAuthTokenException::class,
        MissingScopeException::class,
        \Laravel\Passport\Exceptions\OAuthServerException::class
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * @param Throwable $e
     * @throws Throwable
     */
    public function report(Throwable $e)
    {
        if ($this->shouldntReport($e)) {
            return;
        }

        if (is_callable($reportCallable = [$e, 'report'])) {
            $this->container->call($reportCallable);

            return;
        }

        try {
            if ($this->isOauthException($e)) {
                $logger = Log::channel('oauth');
            } else {
                $logger = $this->container->make(LoggerInterface::class);
            }
        } catch (Exception $ex) {
            throw $e;
        }

        $logger->error(
            $e->getMessage(),
            array_merge(
                $this->exceptionContext($e),
                $this->context(),
                ['exception' => $e]
            )
        );
    }

    /**
     * @param Request $request
     * @param Throwable $exception
     * @return \Illuminate\Http\JsonResponse|Response
     * @throws Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($request->wantsJson()) {
            $message = 'Something went wrong';

            if (config('app.debug')) {
                $message = $exception->getMessage();
            }

            // Default response of 400
            $status = 400;

            // If this exception is an instance of HttpException
            if ($this->isHttpException($exception)) {
                // Grab the HTTP status code from the Exception
                $status = $exception->getStatusCode();
            }

            if ($status == 429) {
                $message = $exception->getMessage();
            }

            // Return a JSON response with the response array and status code
            return response()->json(new JsonResponse(null, false, $status, $message), $status);
        } else {
            if ($exception instanceof UnauthorizedException) {
                return response()->json(new JsonResponse(null, false, 401, $exception->getMessage()), 401);
            }
        }

        return parent::render($request, $exception);
    }


    /**
     * Determine if the exception is in oAuth exception list
     *
     * @param \Throwable $e
     * @return bool
     */
    protected function isOauthException(Throwable $e)
    {
        return !is_null(Arr::first($this->oauthExceptions, function ($type) use ($e) {
            return $e instanceof $type;
        }));
    }
}
