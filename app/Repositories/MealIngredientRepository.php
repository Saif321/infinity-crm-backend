<?php

namespace App\Repositories;

use App\Models\MealIngredient;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class MealIngredientRepository.
 *
 * @package namespace App\Repositories;
 */
class MealIngredientRepository extends BaseRepository
{
    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */

    public function model()
    {
        return MealIngredient::class;
    }

    public function getTranslatedForMealId($mealId, $languageId)
    {
        $this->setTranslatableTable();

        return $this->model
            ->where('language_id', $languageId)
            ->where('meal_id', $mealId)
            ->orderBy('name');
    }
}
