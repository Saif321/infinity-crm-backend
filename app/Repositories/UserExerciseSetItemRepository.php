<?php

namespace App\Repositories;

use App\Models\UserExerciseSetItem;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class UserExerciseSetItemRepository.
 *
 * @package namespace App\Repositories;
 */
class UserExerciseSetItemRepository extends BaseRepository
{
    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserExerciseSetItem::class;
    }

    
    
    
}
