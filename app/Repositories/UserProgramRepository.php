<?php

namespace App\Repositories;

use App\Models\UserProgram;
use App\Traits\TranslatableRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class UserProgramRepository.
 *
 * @package namespace App\Repositories;
 */
class UserProgramRepository extends BaseRepository
{
    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserProgram::class;
    }

    public function getPurchasedPrograms($userId, $search = null)
    {
        $model = $this->setTranslatableTable()
            ->select('__user_programs.*', 'source_program_id')
            ->join('purchased_programs', 'purchased_programs.user_program_id', '=',
                '__user_programs.object_id')
            ->where('is_published', true)
            ->where('purchased_programs.user_id', $userId)
            ->orderBy('__user_programs.object_id', 'DESC');

        if ($search) {
            $model = $model->where(function ($query) use ($search) {
                $query->where('__user_programs.name', 'LIKE', '%'.$search.'%')
                    ->orWhere('__user_programs.description', 'LIKE', '%'.$search.'%');
            });
        }

        return $model;
    }

    /**
     * @param $programId
     *
     * @return UserProgram
     */
    public function getPurchasedProgram($programId): ?UserProgram
    {
        return $this->setTranslatableTable()
            ->select('__user_programs.*', 'source_program_id')
            ->join('purchased_programs', 'purchased_programs.user_program_id', '=',
                '__user_programs.object_id')
            ->where('is_published', true)
            ->where('__user_programs.object_id', $programId)
            ->where('purchased_programs.user_id', Auth::user()->id)
            ->orderBy('created_at', 'DESC')
            ->first();
    }


    /**
     * @param $trainingId
     *
     * @return UserProgram
     */
    public function getProgramFromTraining($trainingId)
    {
        return $this->model
            ->select('user_programs.*')
            ->join('user_program_weeks', 'user_program_weeks.user_program_id', '=', 'user_programs.id')
            ->join('user_program_week_items', 'user_program_week_items.user_program_week_id', '=',
                'user_program_weeks.id')
            ->where('user_program_week_items.user_training_id', $trainingId)
            ->first();
    }

    public function resetProgress($programId)
    {
        $progress = DB::update("UPDATE user_exercise_round_items SET is_finished = 0 WHERE is_finished = 1 AND user_round_id IN (
            select user_rounds.id
            from user_program_weeks
            inner join user_program_week_items on user_program_week_items.user_program_week_id = user_program_weeks.id
            inner join user_exercise_sets on user_exercise_sets.user_training_id = user_program_week_items.user_training_id
            inner join user_rounds on user_rounds.user_exercise_set_id = user_exercise_sets.id
            where user_program_weeks.user_program_id = ?
        )", [$programId]);

        return $progress;
    }

    public function countExercisesWithId($programId, $exerciseId)
    {
        $noOfExercises = DB::select("SELECT COUNT(exercise_id) as no_of_exercises FROM user_exercise_round_items WHERE user_round_id IN (
            select user_rounds.id
            from user_program_weeks
            inner join user_program_week_items on user_program_week_items.user_program_week_id = user_program_weeks.id
            inner join user_exercise_sets on user_exercise_sets.user_training_id = user_program_week_items.user_training_id
            inner join user_rounds on user_rounds.user_exercise_set_id = user_exercise_sets.id
            where user_program_weeks.user_program_id = ?
        ) AND user_exercise_round_items.exercise_id = ?", [$programId, $exerciseId]);

        return $noOfExercises[0]->no_of_exercises;
    }

    /**
     * @param integer $programId
     *
     * @return string[]
     */
    public function getActivityDates(int $userId, string $year, string $month)
    {
        $from = Carbon::create($year, $month)->format('Y-m-d 00:00:00');
        $to = Carbon::create($year, $month)->addMonth()->format('Y-m-d 00:00:00');
        $dates = DB::select("
        SELECT DATE(ueri.finished_at) as activity_date
        FROM purchased_programs
                 INNER JOIN user_programs on purchased_programs.user_program_id = user_programs.id
                 INNER JOIN user_program_weeks ON user_program_weeks.user_program_id = user_programs.id
                 INNER JOIN user_program_week_items ON user_program_week_items.user_program_week_id = user_program_weeks.id
                 INNER JOIN user_exercise_sets ON user_exercise_sets.user_training_id = user_program_week_items.user_training_id
                 INNER JOIN user_rounds ON user_rounds.user_exercise_set_id = user_exercise_sets.id
                 INNER JOIN user_exercise_round_items ueri ON ueri.user_round_id = user_rounds.id
        WHERE ueri.finished_at IS NOT NULL
          AND ueri.is_finished = 1
          AND ueri.type = 'exercise'
          AND purchased_programs.user_id = ?
          AND ueri.finished_at BETWEEN ? AND ?
        GROUP BY activity_date
        ORDER BY activity_date ASC
        ", [$userId, $from, $to]);

        $result = [];
        foreach ($dates as $date) {
            array_push($result, $date->activity_date);
        }
        return $result;
    }

    public function getUserActivityOnDate(int $userId, string $date)
    {
        $time = strtotime($date);
        $from = date('Y-m-d 00:00:00', $time);
        $to = date('Y-m-d 23:59:59', $time);

        $result = DB::select("
    SELECT user_programs.id as user_program_id,
           user_program_week_items.user_training_id,
           user_program_weeks.`order` as week,
           user_program_week_items.`order` as day,
           ueri.exercise_id,
           COUNT(ueri.exercise_id) as completed_times,
           SUM(IFNULL(ueri.user_number_of_reps, ueri.number_of_reps)) AS number_of_reps,
           ueri.strength_percentage,
           ueri.user_strength_kilos as strength_in_kilograms,
           SUM(ueri.duration_in_seconds) as duration_in_seconds
    FROM purchased_programs
             INNER JOIN user_programs on purchased_programs.user_program_id = user_programs.id
             INNER JOIN user_program_weeks ON user_program_weeks.user_program_id = user_programs.id
             INNER JOIN user_program_week_items ON user_program_week_items.user_program_week_id = user_program_weeks.id
             INNER JOIN user_exercise_sets ON user_exercise_sets.user_training_id = user_program_week_items.user_training_id
             INNER JOIN user_rounds ON user_rounds.user_exercise_set_id = user_exercise_sets.id
             INNER JOIN user_exercise_round_items ueri ON ueri.user_round_id = user_rounds.id
    WHERE
        ueri.finished_at IS NOT NULL AND
        ueri.is_finished = 1 AND
        ueri.type = 'exercise' AND
        purchased_programs.user_id = ? AND
        ueri.finished_at BETWEEN ? AND ?
    GROUP BY user_programs.id, user_program_week_items.user_training_id, ueri.exercise_id, ueri.strength_percentage, ueri.user_strength_kilos
	    ", [$userId, $from, $to]);

        return $result;
    }

    /**
     * @param int $sourceProgramId
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function getPurchasedProgramsBySourceProgramId(int $sourceProgramId): Builder
    {
        return $this->model
            ->select('user_programs.*')
            ->join('purchased_programs', 'purchased_programs.user_program_id', '=', 'user_programs.id')
            ->where('purchased_programs.source_program_id', $sourceProgramId);
    }

    /**
     * @param int $id
     * @return UserProgram|null
     */
    public function findById(int $id): ?UserProgram
    {
        return $this->find($id);
    }
}
