<?php

namespace App\Repositories;

use App\Models\UserWeightHistory;
use Prettus\Repository\Eloquent\BaseRepository;

class UserWeightHistoryRepository extends BaseRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return UserWeightHistory::class;
    }

}
