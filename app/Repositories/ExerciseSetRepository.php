<?php

namespace App\Repositories;

use App\Models\ExerciseSet;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class ExerciseSetRepository.
 *
 * @package namespace App\Repositories;
 */
class ExerciseSetRepository extends BaseRepository
{

    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ExerciseSet::class;
    }

    public function getSetsForTraining($trainingId)
    {
        return $this->setTranslatableTable()
            ->where('training_id', $trainingId)
            ->orderBy('order');
    }
}
