<?php

namespace App\Repositories;

use App\Models\Round;
use Prettus\Repository\Eloquent\BaseRepository;



/**
 * Class RoundRepository.
 *
 * @package namespace App\Repositories;
 */
class RoundRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Round::class;
    }

    public function getRoundsForExerciseSet($exerciseSetId)
    {
        return $this->model
            ->where('exercise_set_id', $exerciseSetId)
            ->orderBy('order');
    }
}
