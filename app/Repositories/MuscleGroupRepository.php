<?php

namespace App\Repositories;

use App\Models\MuscleGroup;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class MuscleGroupRepository.
 *
 * @package namespace App\Repositories;
 */
class MuscleGroupRepository extends BaseRepository
{
    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MuscleGroup::class;
    }

    public function getTranslatedMuscleGroups($search)
    {
        $model = $this->setTranslatableTable()->orderBy('name', 'desc');

        if($search) {
            $model = $model->where(function($query) use($search) {
                $query->where('name', 'LIKE', '%' . $search . '%');
            });
        }

        return $model;
    }

    public function getMuscleGroups($search)
    {
        $model = $this->model->orderBy('name', 'asc');

        if($search) {
            $model = $model->where('name', 'LIKE', '%' . $search . '%');
        }

        return $model;
    }
}
