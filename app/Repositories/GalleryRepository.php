<?php

namespace App\Repositories;

use App\Models\Gallery;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class GalleryRepository.
 *
 * @package namespace App\Repositories;
 */
class GalleryRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Gallery::class;
    }

    public function getFiles($search = null, $type = null, $getUserAvatars = false)
    {
        $model = $this->model
            ->where('is_user_avatar', $getUserAvatars)
            ->orderBy('created_at', 'desc');

        if($search) {
            $model = $model->where(function($query) use($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('mime_type', 'LIKE', '%' . $search . '%');
            });
        }

        if($type && in_array($type, ['image', 'video'])) {
            $model = $model->where('type', $type);
        }

        return $model;
    }
}
