<?php

namespace App\Repositories;

use App\Models\NutritionProgram;
use App\Models\NutritionProgramMealGroup;
use App\Traits\TranslatableRepository;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;


class NutritionProgramMealGroupRepository extends BaseRepository
{
    public function model()
    {
        return NutritionProgramMealGroup::class;
    }
}
