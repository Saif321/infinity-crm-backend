<?php

namespace App\Repositories;

use App\Models\UserProgramWeekItem;
use App\Traits\TranslatableRepository;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;


class UserProgramWeekItemRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserProgramWeekItem::class;
    }
}
