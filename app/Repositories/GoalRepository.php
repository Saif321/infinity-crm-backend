<?php

namespace App\Repositories;

use App\Models\Goal;
use App\Traits\TranslatableRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class BasicQuestionnaireRepository.
 *
 * @package namespace App\Repositories;
 */
class GoalRepository extends BaseRepository
{
	/**
	 * @var Goal $model
	 */
	protected $model;

    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Goal::class;
    }

	/**
	 * @param null $search
	 *
	 * @return Builder
	 */
    public function getGoals($search = null)
    {
 	    /** @var Builder $model */
        $model = $this->model->orderBy('order', 'asc');

        if($search) {
            $model = $model->where(function(Builder $query) use($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('description', 'LIKE', '%' . $search . '%');
            });
        }
        return $model;
    }

    public function getGoalsTranslations()
    {
        return $this->model
            ->select('__goals.*')
            ->join('__goals', 'goals.id', '=', '__goals.object_id')
            ->where('language_id', Config::get('language')['id'])
            ->orderBy('order');
    }

    public function getTranslatedGoalsForNutrition($nutritionProgramId)
    {
        return $this->setTranslatableTable()
            ->select('__goals.*')
            ->join('goal_nutrition_program_items', 'goal_nutrition_program_items.goal_id', '=', '__goals.object_id')
            ->where('goal_nutrition_program_items.nutrition_program_id', $nutritionProgramId)
            ->orderBy('order');
    }
}
