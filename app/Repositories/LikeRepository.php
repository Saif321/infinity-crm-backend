<?php

namespace App\Repositories;

use App\Models\Like;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class LikeRepository.
 *
 * @package namespace App\Repositories;
 */
class LikeRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Like::class;
    }

    public function getLikes($objectId, $objectType)
    {
        return $this->model
            ->where('object_id', $objectId)
            ->where('object_type', $objectType);
    }

    public function getUserLikes($objectId, $objectType, $userId)
    {
        return $this->model
            ->where('user_id', $userId)
            ->where('object_id', $objectId)
            ->where('object_type', $objectType);
    }

    public function getUsersBlogLike($userId, $objectId, $objectType)
    {
        return $this->model
            ->where('user_id', $userId)
            ->where('object_id', $objectId)
            ->where('object_type', $objectType);
    }

    public function isUserLiked(Model $item, User $user)
    {
        return $this->model
            ->where('object_type', get_class($item))
            ->where('object_id', $item->getId())
            ->where('user_id', $user->id)
            ->count() > 0 ? true : false;
    }
}
