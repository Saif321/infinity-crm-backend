<?php

namespace App\Repositories;

use App\Models\NutritionProgramItem;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class NutritionProgramItemRepository.
 *
 * @package namespace App\Repositories;
 */
class NutritionProgramItemRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return NutritionProgramItem::class;
    }

}
