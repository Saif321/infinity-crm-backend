<?php

namespace App\Repositories;

use App\Models\BasicQuestionnaire;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class BasicQuestionnaireRepository.
 *
 * @package namespace App\Repositories;
 */
class BasicQuestionnaireRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BasicQuestionnaire::class;
    }

    public function getForUser($userId)
    {
        return $this->model
            ->where('user_id', $userId);
    }
}
