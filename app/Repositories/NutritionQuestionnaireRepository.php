<?php

namespace App\Repositories;

use App\Models\NutritionQuestionnaire;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class NutritionQuestionnaireRepository.
 *
 * @package namespace App\Repositories;
 */
class NutritionQuestionnaireRepository extends BaseRepository
{

    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return NutritionQuestionnaire::class;
    }

    public function getForUser($userId)
    {
        return $this->model
            ->where('user_id', $userId);
    }
}
