<?php

namespace App\Repositories;

use App\Models\Meat;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;



/**
 * Class MeatRepository.
 *
 * @package namespace App\Repositories;
 */
class MeatRepository extends BaseRepository
{
    use TranslatableRepository;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Meat::class;
    }


}
