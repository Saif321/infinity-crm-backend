<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Traits\TranslatableRepository;
use App\Models\Training;


/**
 * Class TrainingRepository.
 *
 * @package namespace App\Repositories;
 */
class TrainingRepository extends BaseRepository
{
    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Training::class;
    }

    public function getTrainingsForWeek($weekId)
    {
        $model = $this->makeTranslatableModel()
            ->select(['__trainings.*', 'week_items.is_locked', 'week_items.is_rest_day'])
            ->rightJoin('week_items', '__trainings.object_id', '=', 'week_items.training_id')
            ->where('program_week_id', $weekId)
            ->where( function ( Builder $builder ) {
	            $builder->where( 'language_id', Config::get( 'language' )['id'] )
	                    ->orWhereNull( 'language_id' );
            } )
            ->orderBy('order');

        return $model;
    }

    public function getTraining($search = null)
    {
        $model = $this->model
             ->where(function($query) use($search) {
                 $query->whereNull('parent_id')
                       ->orWhere('parent_id', 0);
             })
            ->orderBy('created_at', 'desc');

        if($search) {
            $model = $model->where(function($query) use($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('description', 'LIKE', '%' . $search . '%');
            });
        }

        return $model;
    }
}
