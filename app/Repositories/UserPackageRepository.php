<?php

namespace App\Repositories;

use App\Models\Package;
use App\Models\User;
use App\Models\UserPackage;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class PurchasedProgramRepository.
 *
 * @package namespace App\Repositories;
 *
 * @property Model|Builder $model
 */
class UserPackageRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserPackage::class;
    }

    public function getCurrentActiveUserPackage(int $userId): ?UserPackage
    {
        $date = date('Y-m-d H:i:s');
        return $this->orderBy('id', 'desc')->findWhere([
            ['user_id', '=', $userId],
            ['valid_until', '>', $date]
        ])->first();
    }

    /**
     * @param int $userId
     * @return int
     */
    public function countNumberOfPackegesForUser(int $userId): int
    {
        return $this->count([['user_id', '=', $userId]]);
    }

    public function createPackage(Carbon $validUntil, User $user, Package $package, bool $freeTrial): UserPackage
    {
        $userPackage = new UserPackage();
        $userPackage->valid_until = $validUntil;
        $userPackage->free_trial = $freeTrial;
        $userPackage->user()->associate($user);
        $userPackage->package()->associate($package);
        $userPackage->save();

        return $userPackage;
    }

    /**
     * @param string $method
     * @param int $limit
     * @return Collection
     */
    public function getExpiringUserPackagesPaidWithCreditCard(string $method, $limit = 20): Collection
    {
        $tomorrow = Carbon::now()->addHours(6)->format('Y-m-d H:i:s');
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $lastRenewalAttemptBefore = Carbon::now()->subHours(2)->format('Y-m-d H:i:s');

        $builder = $this->model->select('user_packages.*')
            ->join('payments', 'payments.user_package_id', '=', 'user_packages.id')
            ->where('renew_attempts', '<', 1)
            ->where(function (Builder $q) use ($lastRenewalAttemptBefore) {
                $q->where('last_renewal_attempt', '<', $lastRenewalAttemptBefore)
                    ->orWhereNull('last_renewal_attempt');
            })
            ->where('valid_until', '<=', $tomorrow)
            ->where('valid_until', '>=', $now)
            ->where('payments.method', '=', $method)
            ->groupBy('user_packages.user_id')
            ->orderBy('user_packages.id', 'desc')
            ->limit($limit);

        return $builder->get();
    }

    /**
     * @param string|null $search
     * @param bool $onlyActive
     * @param bool $includeFreeTrial
     * @return \Illuminate\Database\Query\Builder
     */
    public function getUserPackageReport(
        string $search = null,
        bool $onlyActive = false,
        bool $includeFreeTrial = false
    ): \Illuminate\Database\Query\Builder {
        $data = DB::table('user_packages')
            ->select('user_id', 'users.email', 'packages.name as package_name',
                'user_packages.free_trial as is_free_trial', 'valid_until', 'last_renewal_attempt',
                'renew_attempts', 'renew_stopped', 'payments.method as payment_method', 'payments.charged_value',
                'payments.currency_code', 'user_packages.updated_at', 'user_packages.created_at')
            ->leftJoin('payments', 'payments.user_package_id', '=', 'user_packages.id')
            ->leftJoin('packages', 'packages.id', '=', 'user_packages.package_id')
            ->leftJoin('users', 'user_packages.user_id', '=', 'users.id')
            ->orderBy('user_packages.id', 'desc');

        if ($search) {
            //$data = $data->where('users.email', 'LIKE', $search.'%');
            $search = trim($search, '*+-()@<>~" ');
            if (strlen($search)) {
                $data = $data->whereRaw('MATCH (users.email) AGAINST (? in boolean mode)', [$search.'*']);
            }
        }

        if ($onlyActive) {
            $currentDateTime = date('Y-m-d H:i:s');
            $data->where('valid_until', '>', $currentDateTime);
        }

        if (!$includeFreeTrial) {
            $data->where('user_packages.free_trial', '=', 0);
        }

        return $data;
    }

    public function getUserPackageByPaymentId(int $paymentId): ?UserPackage
    {
        return $this->findByField('payment_id', $paymentId)->first();
    }
}
