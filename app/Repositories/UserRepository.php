<?php

namespace App\Repositories;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
use Spatie\Permission\Models\Role;

class UserRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return User::class;
    }

    public function getAllAdministrators($search = null)
    {
        $model = $this->model->role('administrator');

        if ($search) {
            $model = $model->where(function ($query) use ($search) {
                $query->where('first_name', 'LIKE', '%'.$search.'%')
                    ->orWhere('last_name', 'LIKE', '%'.$search.'%')
                    ->orWhere('email', 'LIKE', '%'.$search.'%');
            });
        }

        return $model;
    }

    public function getAllCustomer($search = null)
    {
        $model = $this->model->role(Role::findByName('customer'));

        if ($search) {
            $model = $model->where(function ($query) use ($search) {
                $query->where('first_name', 'LIKE', '%'.$search.'%')
                    ->orWhere('last_name', 'LIKE', '%'.$search.'%')
                    ->orWhere('email', 'LIKE', '%'.$search.'%');
            });
        }

        return $model;
    }

    public function findByCredentials(array $credentials)
    {
        $model = $this->model;
        foreach ($credentials as $key => $value) {
            $model = $model->where($key, $value);
        }

        return $model->first();
    }

    public function getUsersForProgram($programId)
    {
        return $this->model
            ->select('users.*')
            ->join('purchased_programs', 'purchased_programs.user_id', '=', 'users.id')
            ->where('source_program_id', $programId);
    }

    public function getUsersForNutritionProgram($nutritionProgramId)
    {
        return $this->model
            ->select('users.*')
            ->join('purchased_nutrition_programs', 'purchased_nutrition_programs.user_id', '=', 'users.id')
            ->where('nutrition_program_id', $nutritionProgramId);
    }

    public function getUserByEmail(string $email): ?User
    {
        return $this->findByField('email', $email)->first();
    }

    /**
     * @param int[] $levels
     * @param int[] $goals
     * @param int[] $environments
     * @param string $gender
     * @param int $ageFrom
     * @param int $ageTo
     * @return Builder
     */
    public function getCustomerReport(
        $levels = null,
        $goals = null,
        $environments = null,
        $gender = null,
        $ageFrom = null,
        $ageTo = null
    ): Builder {
        /** @var Builder $data */
        $data = $this->model
            ->role(Role::findByName('customer'))
            ->join('basic_questionnaires', 'basic_questionnaires.user_id', '=', 'users.id')
            ->join('goal_basic_questionnaire_items', 'goal_basic_questionnaire_items.basic_questionnaire_id', '=',
                'basic_questionnaires.id')
            ->join('goals', 'goal_basic_questionnaire_items.goal_id', '=', 'goals.id')
            ->join('levels', 'users.level_id', '=', 'levels.id')
            ->join('exercise_environments', 'basic_questionnaires.exercise_environment_id', '=',
                'exercise_environments.id')
            ->select(DB::raw('
               users.id                                      as id,
               users.first_name,
               users.last_name,
               users.gender,
               levels.name                                   as level,
               group_concat(distinct goals.name)             as goals,
               exercise_environments.name                    as environment,
               TIMESTAMPDIFF(YEAR, date_of_birth, DATE_FORMAT(NOW(),\'%Y-12-31\')) as age
        '));
        $data->groupBy('users.id');

        if ($levels) {
            $data = $data->whereIn('levels.id', $levels);
        }
        if ($goals) {
            $data = $data->whereIn('goals.id', $goals);
        };
        if ($gender) {
            $data = $data->where('gender', $gender);
        }
        if ($environments) {
            $data = $data->whereIn('exercise_environments.id', $environments);
        }
        if ($ageFrom) {
            $data = $data->where('date_of_birth', '<=', Carbon::now()->subYears($ageFrom)->format('Y-12-31'));
        }
        if ($ageTo) {
            $data = $data->where('date_of_birth', '>=', Carbon::now()->subYears($ageTo)->format('Y-01-01'));
        }

        return $data;
    }
}
