<?php

namespace App\Repositories;

use App\Models\MealRecipeStep;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class RecipeStepRepository.
 *
 * @package namespace App\Repositories;
 */
class MealRecipeStepRepository extends BaseRepository
{
    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MealRecipeStep::class;
    }

    public function getTranslatedForMealId($mealId, $languageId)
    {
        $this->setTranslatableTable();

        return $this->model
            ->where('language_id', $languageId)
            ->where('meal_id', $mealId)
            ->orderBy('order');
    }
}
