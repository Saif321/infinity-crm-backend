<?php
namespace App\Repositories;

use App\Models\Language;
use App\Traits\TranslatableRepository;
use Illuminate\Database\Eloquent\Collection;
use Prettus\Repository\Eloquent\BaseRepository;

class LanguageRepository extends BaseRepository {

//    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return Language::class;
    }

    public function getDefaultLanguage(): Language
    {
        return $this->model->where('is_default', true)->first();
    }

    /**
     * @return Language[]
     */
    public function getLanguages(): Collection {
        return $this->model->orderBy('language')->get();
    }
}
