<?php

namespace App\Repositories;

use App\Models\Fruit;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;



/**
 * Class FruitRepository.
 *
 * @package namespace App\Repositories;
 */
class FruitRepository extends BaseRepository
{
    use TranslatableRepository;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Fruit::class;
    }


}
