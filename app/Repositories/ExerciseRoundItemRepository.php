<?php

namespace App\Repositories;

use App\Models\ExerciseRoundItem;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class ExerciseRoundItemRepository.
 *
 * @package namespace App\Repositories;
 */
class ExerciseRoundItemRepository extends BaseRepository
{
    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ExerciseRoundItem::class;
    }

    
    
    
}
