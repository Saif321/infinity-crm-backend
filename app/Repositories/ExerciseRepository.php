<?php

namespace App\Repositories;

use App\Models\Exercise;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Config;


/**
 * Class ExerciseRepository.
 *
 * @package namespace App\Repositories;
 */
class ExerciseRepository extends BaseRepository
{

    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Exercise::class;
    }

    public function getExercises($search = null, $muscleGroupIds = null, $environmentIds = null, $isPublished = null, $isFree = null)
    {
        $model = $this->model->select('exercises.*')->orderBy('created_at', 'desc');

        if($search) {
            $model = $model->where(function($query) use($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('description', 'LIKE', '%' . $search . '%')
                    ->orWhere('image_id', 'LIKE', '%' . $search . '%')
                    ->orWhere('video_id', 'LIKE', '%' . $search . '%');
            });
        }

        if($muscleGroupIds) {
            $model = $model->join('exercise_muscle_group_items', 'exercise_muscle_group_items.exercise_id', 'exercises.id')
                ->whereIn('exercise_muscle_group_items.muscle_group_id', $muscleGroupIds);
        }

        if($environmentIds) {
            $model = $model->join('exercise_environment_items', 'exercise_environment_items.exercise_id', 'exercises.id')
                ->whereIn('exercise_environment_items.exercise_environment_id', $environmentIds);
        }

	    if($isPublished !== null) {
		    $isPublished = $isPublished == 'true' ? true : false;

		    $model = $model->where('is_published', $isPublished);
	    }

	    if($isFree !== null) {
		    $isFree = $isFree == 'true' ? true : false;

		    $model = $model->where('is_free', $isFree);
	    }

        $model->groupBy('exercises.id');

        return $model;
    }

    public function getFreeExercisesForMuscleGroupAndEnvironment($muscleGroupId, $exerciseEnvironmentId, $search = null)
    {
        $model = $this->model
            ->select('__exercises.*')
            ->join('__exercises', '__exercises.object_id', '=', 'exercises.id')
            ->join('exercise_muscle_group_items', 'exercise_muscle_group_items.exercise_id', '=', 'exercises.id')
            ->join('exercise_environment_items', 'exercise_environment_items.exercise_id', '=', 'exercises.id')
            ->where('exercise_muscle_group_items.muscle_group_id', $muscleGroupId)
            ->where('exercise_environment_items.exercise_environment_id', $exerciseEnvironmentId)
            ->where('__exercises.is_free', true)
            ->where('exercises.is_published', true);

        if($search) {
            $model = $model->where('__exercises.name', 'LIKE', '%' . $search . '%');
        }

        $model = $model->where('__exercises.language_id', Config::get('language')['id']);
        $model = $model->orderBy('__exercises.name');
        $model = $model->groupBy('exercises.id');

        return $model;
    }

    public function findTranslated($id)
    {
        return $this->setTranslatableTable()
            ->where('object_id', $id)
            ->with('image')->first();
    }
    /**
     * @param int $id
     * @return Exercise|null
     */
    public function findById(int $id): ?Exercise
    {
        return $this->find($id);
    }
}
