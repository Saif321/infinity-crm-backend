<?php

namespace App\Repositories;

use App\Models\UserProgramItem;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class UserProgramItemRepository.
 *
 * @package namespace App\Repositories;
 */
class UserProgramItemRepository extends BaseRepository
{
    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserProgramItem::class;
    }

    
    
    
}
