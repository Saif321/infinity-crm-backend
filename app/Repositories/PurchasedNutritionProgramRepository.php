<?php

namespace App\Repositories;

use App\Models\PurchasedNutritionProgram;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class PurchasedNutritionProgramRepository.
 *
 * @package namespace App\Repositories;
 */
class PurchasedNutritionProgramRepository extends BaseRepository
{

    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PurchasedNutritionProgram::class;
    }

    public function countByNutritionProgramIdAndUserId(int $programId, int $userId): int
    {
        return $this->model
            ->where('nutrition_program_id', '=', $programId)
            ->where('user_id', '=', $userId)
            ->count('id');
    }
}
