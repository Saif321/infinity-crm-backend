<?php

namespace App\Repositories;

use App\Models\HomeFeed;
use App\Traits\TranslatableRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Auth;


/**
 * Class HomeFeedRepository.
 *
 * @package namespace App\Repositories;
 */
class HomeFeedRepository extends BaseRepository {
	use TranslatableRepository;

	/**
	 * Specify Model class name
	 *
	 * @return string
	 */
	public function model() {
		return HomeFeed::class;
	}
	
    public function getHomeFeed($search = null)
    {
        $user = Auth::user();
        $model = $this->model->whereNotExists( function ($query) use ($user) {
                                    $query->select(DB::raw(1))
                                    ->from('users_blocks')
                                    ->whereRaw('users_blocks.blocked_users_id = home_feed.users_id')
                                    ->where('users_blocks.users_id', '=', $user->id);
                                })
                            ->select('home_feed.*')
                            ->orderBy('home_feed.created_at', 'desc');

        if($search) {
            $model = $model->where(function($query) use($search) {
                $query->where('title', 'LIKE', '%' . $search . '%')
                    ->orWhere('description', 'LIKE', '%' . $search . '%');
            });
        }

        return $model;
    }


}
