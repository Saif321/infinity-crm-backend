<?php

namespace App\Repositories;

use App\Models\UserRound;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class UserRoundsRepository.
 *
 * @package namespace App\Repositories;
 */
class UserRoundRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserRound::class;
    }

    public function getRoundsForExerciseSet($userExerciseSetId)
    {
        return $this->model
            ->where('user_exercise_set_id', $userExerciseSetId)
            ->orderBy('order');
    }
}
