<?php

namespace App\Repositories;

use App\Models\BlogTag;
use App\Traits\TranslatableRepository;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class BlogTagRepository.
 *
 * @package namespace App\Repositories;
 */
class BlogTagRepository extends BaseRepository
{
    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */

    public function model()
    {
        return BlogTag::class;
    }

    public function getTags($objectId, $objectType)
    {
        return $this->model->blogs();
    }
}
