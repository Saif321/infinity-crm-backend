<?php

namespace App\Repositories;

use App\Models\Payment;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;

class PaymentRepository extends BaseRepository
{
    public function model()
    {
        return Payment::class;
    }

    public function getPaymentReport(): Builder
    {
        return DB::table('payments')
            ->select('payments.method', 'payments.status', 'payments.transaction_id', 'payments.details', 'payments.charged_value',
                'payments.currency_code', 'payments.updated_at', 'payments.created_at', 'packages.name as package_name', DB::raw('COUNT(payment_logs.id) as number_of_history_items'))
            ->leftJoin('user_packages', 'user_packages.id', '=', 'payments.user_package_id')
            ->leftJoin('packages', 'packages.id', '=', 'user_packages.package_id')
            ->leftJoin('payment_logs', 'payments.transaction_id', '=', 'payment_logs.transaction_id')
            ->groupBy('payments.id')
            ->orderBy('payments.id', 'desc');
    }

}
