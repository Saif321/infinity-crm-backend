<?php

namespace App\Repositories;

use App\Models\ExerciseEnvironment;
use App\Traits\TranslatableRepository;
use Illuminate\Support\Facades\Config;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class ExerciseEnvironmentRepository.
 *
 * @package namespace App\Repositories;
 */
class ExerciseEnvironmentRepository extends BaseRepository
{
    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ExerciseEnvironment::class;
    }

    public function getExerciseEnvironments($search = null)
    {
        $model = $this->model->orderBy('order', 'asc');

        if($search) {
            $model = $model->where('name', 'LIKE', '%' . $search . '%');
        }

        return $model;
    }

    public function getTranslatedExerciseEnvironments($search = null)
    {
        $model = $this->setTranslatableTable()
            ->select('__exercise_environments.*')
            ->orderBy('order', 'asc');

        if($search) {
            $model = $model->where('name', 'LIKE', '%' . $search . '%');
        }

        return $model;
    }

    public function getEnvironmentTranslations()
    {
        return $this->model
            ->select('__exercise_environments.*')
            ->join('__exercise_environments', 'exercise_environments.id', '=', '__exercise_environments.object_id')
            ->where('language_id', Config::get('language')['id'])
            ->orderBy('exercise_environments.order', 'asc');

    }
}
