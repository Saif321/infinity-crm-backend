<?php

namespace App\Repositories;

use App\Models\UserProgram;
use App\Models\UserProgramWeek;
use App\Traits\TranslatableRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;


class UserProgramWeekRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserProgramWeek::class;
    }

	/**
	 * @param $programId
	 *
	 * @return Builder
	 */
    public function getProgramWeeks($programId)
    {
        return $this->model->where('user_program_id', $programId)->orderBy('order', 'asc');
    }

    public function getProgress($programId)
    {
        $progress = DB::select("
            select COUNT(user_exercise_round_items.id) AS 'round_count', SUM(case when user_exercise_round_items.is_finished = 1 then 1 else 0 end) as 'completed_rounds'
            from user_program_weeks
            inner join user_program_week_items on user_program_week_items.user_program_week_id = user_program_weeks.id
            inner join user_exercise_sets on user_exercise_sets.user_training_id = user_program_week_items.user_training_id
            inner join user_rounds on user_rounds.user_exercise_set_id = user_exercise_sets.id
            inner join user_exercise_round_items on user_exercise_round_items.user_round_id = user_rounds.id
            where user_program_weeks.user_program_id = ?", [$programId])[0];

        if(!$progress->completed_rounds) {
            $progress->completed_rounds = 0;
        }

        return $progress;
    }

    public function getActiveWeek($programId)
    {

    }
}
