<?php

namespace App\Repositories;

use App\Models\MealGroup;
use App\Traits\TranslatableRepository;
use Illuminate\Support\Facades\Config;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class MealGroupRepository.
 *
 * @package namespace App\Repositories;
 */
class MealGroupRepository extends BaseRepository
{

    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MealGroup::class;
    }


    public function searchTranslatedMealGroups($nutritionProgramId)
    {
        $model = $this->setTranslatableTable()
            ->select('__meal_groups.*')
            ->join('nutrition_program_meal_groups', 'nutrition_program_meal_groups.meal_group_id', '=', '__meal_groups.object_id')
            ->where('nutrition_program_id', $nutritionProgramId)
            ->groupBy('__meal_groups.object_id')
            ->orderBy('order', 'asc');

        return $model;
    }

	public function searchMealGroups($search = null)
	{
		$model = $this->model
			->orderBy('name', 'desc');

		if($search) {
			$model = $model->where(function($query) use($search) {
				$query->where('name', 'LIKE', '%' . $search . '%')
				      ->orWhere('eating_time', 'LIKE', '%' . $search . '%');
			});
		}

		return $model;
	}

}
