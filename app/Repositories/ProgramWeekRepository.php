<?php

namespace App\Repositories;

use App\Models\ProgramWeek;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class BasicQuestionnaireRepository.
 *
 * @package namespace App\Repositories;
 */

class ProgramWeekRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProgramWeek::class;
    }

    public function getProgramWeeks($programId)
    {
        return $this->model->where('program_id', $programId)->orderBy('order');
    }
}
