<?php
declare( strict_types=1 );

namespace App\Repositories;

use App\Models\NutritionProgram;
use App\Traits\TranslatableRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class NutritionProgramRepository.
 *
 * @package namespace App\Repositories;
 */
class NutritionProgramRepository extends BaseRepository
{

    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return NutritionProgram::class;
    }

    /**
     * @param int $userId
     * @param string|null $search
     * @param array|null $goalIds
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function getProgramsForHomePage(
        int $userId,
        string $search = null,
        array $goalIds = null
    )
    {
        $userPrograms = DB::table( 'purchased_nutrition_programs' )
            ->select( 'nutrition_program_id' )
            ->where( 'user_id', $userId );

        $model = $this->model
            ->select('__nutrition_programs.*')
            ->join('__nutrition_programs', '__nutrition_programs.object_id', '=', 'nutrition_programs.id')
            ->where( '__nutrition_programs.language_id', Config::get( 'language' )['id'] )
            ->where('nutrition_programs.is_published', true)
            ->whereNotIn('nutrition_programs.id', $userPrograms)
            ->orderBy('nutrition_programs.created_at', 'DESC')
            ->limit(5);

        if ( $search ) {
            $model = $model->where( function ( Builder $query ) use ( $search ) {
                $query->where( '__nutrition_programs.name', 'LIKE', '%' . $search . '%' )
                    ->orWhere( '__nutrition_programs.description', 'LIKE', '%' . $search . '%' );
            } );
        }

        if( $goalIds ) {
            $model = $model->where(function(Builder $query) use ( $goalIds ) {
                $query->whereHas( 'goals', function ( Builder $query ) use ( $goalIds ) {
                    $query->whereIn( 'goals.id', $goalIds );
                } )->orWhereDoesntHave('goals');
            });
        }

        return $model;
    }

    public function getNutritionProgram($userId, $search = null)
    {
        $model = $this->setTranslatableTable()
            ->select('__nutrition_programs.*')
            //->join('nutrition_program_items', 'nutrition_program_items.nutrition_program_id', '=', '__nutrition_programs.object_id')
            ->where('__nutrition_programs.is_published', true)
            ->whereNotIn('__nutrition_programs.object_id', DB::table('purchased_nutrition_programs')->select('nutrition_program_id')->where('user_id', $userId))
            ->orderBy('created_at', 'desc');

        if($search) {
            $model = $model->where(function($query) use($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('description', 'LIKE', '%' . $search . '%');
            });
        }

        //$model = $model->groupBy('nutrition_program_items.nutrition_program_id');

        return $model;
    }

    public function getPurchasedNutritionProgram($userId, $search = null)
    {
        $model = $this->setTranslatableTable()
            ->select('__nutrition_programs.*')
            ->join('purchased_nutrition_programs', 'purchased_nutrition_programs.nutrition_program_id', '=', '__nutrition_programs.object_id')
            ->where('purchased_nutrition_programs.user_id', $userId)
            ->where('__nutrition_programs.is_published', true)
            ->orderBy('created_at', 'desc');

        if($search) {
            $model = $model->where(function($query) use($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('description', 'LIKE', '%' . $search . '%');
            });
        }

        $model = $model->groupBy('__nutrition_programs.object_id');

        return $model;
    }

    public function findIsPurchased($userId, $programId)
    {
        return $this->model
            ->join('purchased_nutrition_programs', 'purchased_nutrition_programs.nutrition_program_id', '=', 'nutrition_programs.id')
            ->where('purchased_nutrition_programs.user_id', $userId)
            ->where('purchased_nutrition_programs.nutrition_program_id', $programId)
            ->count() ? true : false;
    }

	public function searchNutritionPrograms($search = null)
	{
		$model = $this->model
            ->select([
                'nutrition_programs.*',
                DB::raw('(SELECT COUNT(*) FROM purchased_nutrition_programs WHERE nutrition_program_id = nutrition_programs.id) AS activated_count'),
                DB::raw('(SELECT COUNT(*) FROM deactivated_nutrition_programs WHERE nutrition_program_id = nutrition_programs.id) AS deactivated_count')
            ])
			->orderBy('created_at', 'desc');

		if($search) {
			$model = $model->where(function($query) use($search) {
				$query->where('name', 'LIKE', '%' . $search . '%')
				      ->orWhere('description', 'LIKE', '%' . $search . '%');
			});
		}

		return $model;
	}

    public function getPurchasedNutritionProgramsForAdmin($userId)
    {
        $model = $this->model
            ->select('nutrition_programs.*')
            ->join('purchased_nutrition_programs', 'purchased_nutrition_programs.nutrition_program_id', '=', 'nutrition_programs.id')
            ->where('purchased_nutrition_programs.user_id', $userId)
            ->where('nutrition_programs.is_published', true)
            ->orderBy('nutrition_programs.created_at', 'DESC');

        return $model;
    }

}
