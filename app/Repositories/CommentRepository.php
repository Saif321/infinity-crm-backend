<?php

namespace App\Repositories;

use App\Models\Comment;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class CommentRepository.
 *
 * @package namespace App\Repositories;
 */
class CommentRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */

    public function model()
    {
        return Comment::class;
    }

    public function getPublishedComments($objectId, $objectType)
    {
        return $this->model
            ->where('object_id', $objectId)
            ->where('object_type', $objectType)
            ->where('is_published', true)
            ->orderBy('published_at', 'desc');
    }

    public function getComments($search = null)
    {
        $model = $this->model
            ->orderBy('published_at', 'asc')
            ->orderBy('created_at', 'desc');

        if($search) {
            $model = $model->where('comment', 'LIKE', '%' . $search . '%');
        }

        return $model;
    }
}
