<?php

namespace App\Repositories;

use App\Models\DeactivatedProgram;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class DeactivatedProgramRepository.
 *
 * @package namespace App\Repositories;
 */
class DeactivatedProgramRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return DeactivatedProgram::class;
    }

    /**
     * @param integer $userId
     * @param integer $packageId
     *
     * @return integer[]
     */
    public function getDistinctDeactivatedProgramsForPackage(int $userId, int $packageId)
    {
        $programs = DB::select("
        SELECT DISTINCT source_program_id
        FROM deactivated_programs
        WHERE user_id = ? AND user_package_id = ?", [$userId, $packageId]);

        $result = [];
        foreach ($programs as $program) {
            array_push($result, $program->source_program_id);
        }
        return $result;
    }

}
