<?php

namespace App\Repositories;

use App\Models\FirstDataVault;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class PurchasedProgramRepository.
 *
 * @package namespace App\Repositories;
 * @property Model|Builder $model
 */
class FirstDataVaultRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return FirstDataVault::class;
    }

    public function createVaultItem(
        int $userId,
        int $paymentId,
        string $hostedId,
        string $creditCardNumber
    ): FirstDataVault {
        $model = new FirstDataVault();
        $model->hosted_id = $hostedId;
        $model->credit_card_number = $creditCardNumber;
        $model->user()->associate($userId);
        $model->payment()->associate($paymentId);
        $model->save();
        return $model;
    }

    public function getLatestForUser(int $userId): ?FirstDataVault
    {
        return $this->model->where('user_id', '=', $userId)
            ->orderBy('id', 'desc')
            ->limit(1)
            ->get()
            ->first();
    }

}
