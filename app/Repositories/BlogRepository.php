<?php

namespace App\Repositories;

use App\Models\Blog;
use App\Traits\TranslatableRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class ExerciseRepository.
 *
 * @package namespace App\Repositories;
 */
class BlogRepository extends BaseRepository
{
    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Blog::class;
    }

    public function getPosts($search = null)
    {
        $model = $this->model->orderBy('created_at', 'desc');

        if($search) {
            $model = $model->where(function($query) use($search) {
                $query->where('title', 'LIKE', '%' . $search . '%')
                    ->orWhere('description', 'LIKE', '%' . $search . '%');
            });
        }

        return $model;
    }

	public function getTranslationsForPost($postId)
	{
		return $this->model
			->select('__blogs.*')
			->join('__blogs', '__blogs.object_id', '=', 'blogs.id')
			->where('__blogs.object_id', $postId)
			->orderBy('language_id');
	}

    public function findTranslationForPost($postId, $languageId)
    {
        return $this->model
            ->select('__blogs.*')
            ->join('__blogs', '__blogs.object_id', '=', 'blogs.id')
            ->where('language_id', $languageId)
            ->where('object_id', $postId)
            ->first();
    }

    public function getPublishedPosts($search = null)
    {
        $model = $this->setTranslatableTable();

        $model = $model
            ->select('__blogs.*')
            ->where('is_published', true)
            ->orderBy('published_at', 'desc');

        if($search) {
            $model = $model->where('title', 'LIKE', '%' . $search . '%');
        }

        return $model;
    }

    public function findTranslated($id)
    {
        return $this->setTranslatableTable()
            ->where('object_id', $id)
            ->with('image')->first();
    }
}
