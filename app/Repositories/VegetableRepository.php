<?php

namespace App\Repositories;

use App\Models\Vegetable;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;



/**
 * Class VegetableRepository.
 *
 * @package namespace App\Repositories;
 */
class VegetableRepository extends BaseRepository
{
    use TranslatableRepository;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Vegetable::class;
    }


}
