<?php

namespace App\Repositories;

use App\Models\PaymentLog;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;

class PaymentLogRepository extends BaseRepository
{
    public function model()
    {
        return PaymentLog::class;
    }

    public function getPaymentHistory(string $transactionId): Builder
    {
        return DB::table('payment_logs')
            ->select('method', 'status', 'transaction_id', 'details', 'payment_logs.charged_value',
                'payment_logs.currency_code', 'payment_logs.updated_at', 'payment_logs.created_at', 'packages.name as package_name')
            ->leftJoin('user_packages', 'user_packages.id', '=', 'payment_logs.user_package_id')
            ->leftJoin('packages', 'packages.id', '=', 'user_packages.package_id')
            ->where('transaction_id', '=', $transactionId)
            ->orderBy('payment_logs.id', 'desc');
    }

}
