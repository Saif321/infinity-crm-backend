<?php

namespace App\Repositories;

use App\Models\UserTraining;
use App\Traits\TranslatableRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class UserTrainingRepository.
 *
 * @package namespace App\Repositories;
 */
class UserTrainingRepository extends BaseRepository {
	use TranslatableRepository;

	/**
	 * Specify Model class name
	 *
	 * @return string
	 */
	public function model() {
		return UserTraining::class;
	}

	/**
	 * @param $weekId
	 *
	 * @return Builder
	 */
	public function getTrainingsForWeek( $weekId ) {
		$model = $this->makeTranslatableModel()
		              ->select( [ '__user_trainings.*', 'is_locked', 'is_rest_day' ] )
		              ->rightJoin( 'user_program_week_items', '__user_trainings.object_id', '=',
			              'user_program_week_items.user_training_id' )
		              ->where( 'user_program_week_id', $weekId )
		              ->where( function ( Builder $builder ) {
			              $builder->where( 'language_id', Config::get( 'language' )['id'] )
			                      ->orWhereNull( 'language_id' );
		              } )
		              ->orderBy( 'order' );

		return $model;
	}

	/**
	 * @param $trainingId
	 *
	 * @return bool
	 */
	public function isTrainingCompleted( $trainingId ) {
		/** @var Builder $db */
		$db = $this->model
			->select( [
				DB::raw( 'SUM(case when is_finished = 1 then 1 else 0 end) AS completed_count' ),
				DB::raw( 'SUM(case when is_finished = 0 then 1 else 0 end) AS not_completed_count' )
			] )
			->join( 'user_exercise_sets', 'user_exercise_sets.user_training_id', '=', '__user_trainings.object_id' )
			->join( 'user_rounds', 'user_rounds.user_exercise_set_id', '=', 'user_exercise_sets.id' )
			->join( 'user_exercise_round_items', 'user_exercise_round_items.user_round_id', 'user_rounds.id' )
			->where( 'user_exercise_round_items.type', 'exercise' )
			->where( '__user_trainings.object_id', $trainingId )
			->where( '__user_trainings.language_id', Config::get( 'language' )['id'] );

		return $db->first()->not_completed_count == 0;
	}
}
