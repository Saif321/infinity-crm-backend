<?php

namespace App\Repositories;

use App\Models\WeekItem;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class ProgramItemRepository.
 *
 * @package namespace App\Repositories;
 */
class WeekItemRepository extends BaseRepository
{

    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return WeekItem::class;
    }

    public function getWeekItems($weekId)
    {
        return $this->model->with('training')->where('program_week_id', $weekId)->orderBy('order');
    }
}
