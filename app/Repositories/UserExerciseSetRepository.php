<?php

namespace App\Repositories;

use App\Models\UserExerciseSet;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class UserExerciseSetRepository.
 *
 * @package namespace App\Repositories;
 */
class UserExerciseSetRepository extends BaseRepository
{
    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserExerciseSet::class;
    }

    public function getSetsForTraining($userTrainingId)
    {
        return $this->setTranslatableTable()
            ->where('user_training_id', $userTrainingId)
            ->orderBy('order');
    }
}
