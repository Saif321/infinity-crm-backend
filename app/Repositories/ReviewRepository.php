<?php

namespace App\Repositories;

use App\Models\Review;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class ReviewRepository.
 *
 * @package namespace App\Repositories;
 */
class ReviewRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Review::class;
    }

    public function getReview($search = null)
    {
        $model = $this->model
            ->orderBy('published_at', 'asc')
            ->orderBy('created_at', 'desc');

        if($search) {
            $model = $model->where('description', 'LIKE', '%' . $search . '%');
        }

        return $model;
    }

    public function getUserReview($objectId, $objectType, $userId)
    {
        return $this->model
            ->where('user_id', $userId)
            ->where('object_id', $objectId)
            ->where('object_type', $objectType);
    }

    public function getPublishedReviews($objectId, $objectType)
    {
        return $this->model
            ->where('object_id', $objectId)
            ->where('object_type', $objectType)
            ->where('is_published', true)
            ->orderBy('published_at');
    }
}
