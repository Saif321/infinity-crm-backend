<?php

namespace App\Repositories;

use App\Models\Level;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Config;


/**
 * Class LevelRepository.
 *
 * @package namespace App\Repositories;
 */
class LevelRepository extends BaseRepository
{

    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Level::class;
    }

    public function getLevelTranslations()
    {
        return $this->model
            ->select('__levels.*')
            ->join('__levels', 'levels.id', '=', '__levels.object_id')
            ->where('language_id', Config::get('language')['id'])
            ->orderBy('levels.order', 'asc');

    }

    public function getLevels($search = null)
    {
        $model = $this->model->orderBy('order', 'asc');

        if($search) {
            $model = $model->where(function($query) use($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('description', 'LIKE', '%' . $search . '%');
            });
        }

        return $model;
    }

    public function getLevelsForProgram($programId)
    {
        return $this->model
            ->select('__levels.*')
            ->join('__levels', 'levels.id', '=', '__levels.object_id')
            ->join('program_level_items', 'program_level_items.level_id', '=', '__levels.object_id')
            ->where('__levels.language_id', Config::get('language')['id'])
            ->where('program_level_items.program_id', $programId)
            ->orderBy('__levels.order');
    }

    public function getLevelsForExercise($exerciseId)
    {
        return $this->model
            ->select('__levels.*')
            ->join('__levels', 'levels.id', '=', '__levels.object_id')
            ->join('exercise_level_items', 'exercise_level_items.level_id', '=', '__levels.object_id')
            ->where('__levels.language_id', Config::get('language')['id'])
            ->where('exercise_level_items.exercise_id', $exerciseId)
            ->orderBy('__levels.order');
    }
}
