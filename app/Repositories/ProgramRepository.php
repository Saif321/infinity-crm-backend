<?php

namespace App\Repositories;

use App\Models\Program;
use App\Traits\TranslatableRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class ProgramRepository.
 *
 * @package namespace App\Repositories;
 */
class ProgramRepository extends BaseRepository {
	use TranslatableRepository;

	/**
	 * Specify Model class name
	 *
	 * @return string
	 */
	public function model() {
		return Program::class;
	}

	public function getPrograms( $search = null ) {
		$model = $this->model
			->select( [
				'programs.*',
				DB::raw( '(SELECT COUNT(*) FROM purchased_programs WHERE source_program_id = programs.id) AS activated_count' ),
				DB::raw( '(SELECT COUNT(*) FROM deactivated_programs WHERE source_program_id = programs.id) AS deactivated_count' ),
			] )
			->orderBy( 'created_at', 'desc' );

		if ( $search ) {
			$model = $model->where( 'name', 'LIKE', '%' . $search . '%' );
		}

		return $model;
	}

	public function getProgramsAvailableForPurchase( $userId, $search = null ) {
		$model = $this->setTranslatableTable()
		              ->select( '__programs.*' )
		              ->where( '__programs.is_published', true )
		              ->whereNotIn( '__programs.object_id',
			              DB::table( 'purchased_programs' )->select( 'source_program_id' )->where( 'user_id',
				              $userId ) )
		              ->orderBy( '__programs.created_at', 'desc' );

		if ( $search ) {
			$model = $model->where( function ( $query ) use ( $search ) {
				$query->where( '__programs.name', 'LIKE', '%' . $search . '%' )
				      ->orWhere( '__programs.description', 'LIKE', '%' . $search . '%' );
			} );
		}

		return $model;
	}

	public function getProgramsForHomePage(
		int $userId,
		string $search = null,
		string $gender = null,
        int $levelId = null,
        int $environmentId = null,
		array $goalIds = null
	) {
		$userPrograms = DB::table( 'purchased_programs' )
		                  ->select( 'source_program_id' )
		                  ->where( 'user_id', $userId );

        $model = $this->model
            ->select('__programs.*')
            ->join('__programs', '__programs.object_id', '=', 'programs.id')
            ->where( 'programs.is_published', true )
            ->where( '__programs.language_id', Config::get( 'language' )['id'] )
            ->whereNotIn( 'programs.id', $userPrograms )
            ->orderBy( 'programs.created_at', 'DESC' )
            ->limit( 5 );

        if ( $search ) {
            $model = $model->where( function ( Builder $query ) use ( $search ) {
                $query->where( '__programs.name', 'LIKE', '%' . $search . '%' )
                    ->orWhere( '__programs.description', 'LIKE', '%' . $search . '%' );
            } );
        }

        if ( $gender ) {
            $model = $model->where( function ( Builder $query ) use ( $gender ) {
                $query->where( 'programs.gender', $gender )
                    ->orWhereNull( 'programs.gender' )
                    ->orWhere( 'programs.gender', '' );
            } );
        }

        if( $levelId ) {
            $model = $model->where(function(Builder $query) use ( $levelId ) {
                $query->whereHas( 'levels', function ( Builder $query ) use ( $levelId ) {
                    $query->where( 'levels.id', $levelId );
                } )->orWhereDoesntHave('levels');
            });
        }

        if( $environmentId ) {
            $model = $model->where(function(Builder $query) use ( $environmentId ) {
                $query->whereHas( 'exerciseEnvironments', function ( Builder $query ) use ( $environmentId ) {
                    $query->where( 'exercise_environments.id', $environmentId );
                } )->orWhereDoesntHave('exerciseEnvironments');
            });
        }

        if( $goalIds ) {
            $model = $model->where(function(Builder $query) use ( $goalIds ) {
                $query->whereHas( 'goals', function ( Builder $query ) use ( $goalIds ) {
                    $query->whereIn( 'goals.id', $goalIds );
                } )->orWhereDoesntHave('goals');
            });
        }

        $model = $model->groupBy('__programs.object_id');

		return $model;
	}

	public function findIsPurchased($userId, $programId)
    {
        return $this->model
            ->join('purchased_programs', 'purchased_programs.source_program_id', '=', 'programs.id')
            ->where('purchased_programs.user_id', $userId)
            ->where('purchased_programs.source_program_id', $programId)
            ->count() ? true : false;
    }

	public function getPurchasedProgramsForAdmin( $userId ) {
		$model = $this->model
			->select( 'programs.*' )
			->join( 'purchased_programs', 'purchased_programs.source_program_id', '=', 'programs.id' )
			->where( 'purchased_programs.user_id', $userId )
			->where( 'programs.is_published', true )
			->orderBy( 'programs.created_at', 'DESC' );

		return $model;
	}
}
