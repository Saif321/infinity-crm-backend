<?php

namespace App\Repositories;

use App\Models\PurchasedProgram;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class PurchasedProgramRepository.
 *
 * @package namespace App\Repositories;
 */
class PurchasedProgramRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PurchasedProgram::class;
    }

    public function getPurchasedPrograms($search = null)
    {
        $model = $this->model
            ->select([
                'purchased_programs.*',
                DB::raw('(SELECT COUNT(*) FROM programs WHERE user_id = programs.id) AS source_program_id')
            ])
            ->orderBy('created_at', 'desc');

        if ($search) {
            $model = $model->where('user_program_id', 'LIKE', '%'.$search.'%');
        }

        return $model;
    }

    public function getPurchasedProgramByUserProgramId(int $userProgramId): ?PurchasedProgram
    {
        return $this->model->select()->where('user_program_id', '=', $userProgramId)->first();
    }

}
