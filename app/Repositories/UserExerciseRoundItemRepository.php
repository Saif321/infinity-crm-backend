<?php

namespace App\Repositories;

use App\Models\UserRound;
use App\Models\UserExerciseRoundItem;
use Prettus\Repository\Eloquent\BaseRepository;

class UserExerciseRoundItemRepository extends BaseRepository {
	/**
	 * Specify Model class name
	 *
	 * @return string
	 */
	public function model() {
		return UserExerciseRoundItem::class;
	}

	/**
	 * @param $id
	 * @param array $columns
	 *
	 * @return UserExerciseRoundItem
	 */
	public function find( $id, $columns = [ '*' ] ) {
		return parent::find( $id, $columns );
	}

	/**
	 * @param $roundId
	 *
	 * @return mixed
	 */
	public function getRoundItems( $roundId ) {
		return $this->model
			->where( 'user_round_id', $roundId )
			->orderBy( 'order' );
	}
}
