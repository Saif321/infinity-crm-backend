<?php

namespace App\Repositories;

use App\Models\UserExercise;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class UserExerciseRepository.
 *
 * @package namespace App\Repositories;
 */
class UserExerciseRepository extends BaseRepository
{
    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserExercise::class;
    }
}
