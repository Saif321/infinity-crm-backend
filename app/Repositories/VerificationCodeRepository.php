<?php

namespace App\Repositories;

use App\Models\ChatGroup;
use App\Models\VerificationCode;
use Prettus\Repository\Eloquent\BaseRepository;

class VerificationCodeRepository extends BaseRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
     	return VerificationCode::class;
    }

    public function getCodeForDestination($destination, $code)
    {
     	return $this->model
            ->where('code', $code)
            ->where('destination', $destination)
            ->where('is_used', false)
            ->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-' . env('OTP_VALIDATION_TIME') . ' minute')));
    }

}
