<?php

namespace App\Repositories;

use App\Models\DeactivatedNutritionProgram;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class DeactivatedNutritionProgramRepository.
 *
 * @package namespace App\Repositories;
 */
class DeactivatedNutritionProgramRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return DeactivatedNutritionProgram::class;
    }

    /**
     * @param integer $userId
     * @param integer $packageId
     *
     * @return integer[]
     */
    public function getDistinctDeactivatedProgramsForPackage(int $userId, int $packageId)
    {
        $programs = DB::select("
        SELECT DISTINCT nutrition_program_id
        FROM deactivated_nutrition_programs
        WHERE user_id = ? AND user_package_id = ?", [$userId, $packageId]);

        $result = [];
        foreach ($programs as $program) {
            array_push($result, $program->nutrition_program_id);
        }
        return $result;
    }

}
