<?php

namespace App\Repositories;

use App\Models\Meal;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class MealRepository.
 *
 * @package namespace App\Repositories;
 */
class MealRepository extends BaseRepository
{

    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Meal::class;
    }

    public function getMeal($nutritionProgramId, $mealGroupId, $search = null)
    {
        $model = $this->setTranslatableTable()
            ->select(['__meals.*', 'nutrition_program_meal_groups.meal_group_id' => 'meal_group_id'])
            ->join('nutrition_program_items', 'nutrition_program_items.meal_id', '=', '__meals.object_id')
            ->join('nutrition_program_meal_groups', 'nutrition_program_meal_groups.id', '=', 'nutrition_program_items.nutrition_program_meal_group_id')
            ->where('nutrition_program_meal_groups.meal_group_id', $mealGroupId)
            ->where('nutrition_program_meal_groups.nutrition_program_id', $nutritionProgramId)
//            ->where('__meals.is_published', true)
            ->orderBy('name', 'desc');

        if($search) {
            $model = $model->where(function($query) use($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('description', 'LIKE', '%' . $search . '%');

            });
        }

        $model = $model->groupBy('__meals.object_id');

        return $model;
    }

    public function getMealsForAdmin($search = null)
    {
        $model = $this->model->orderBy('id', 'desc');

        if($search) {
            $model = $model->where(function($query) use($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('description', 'LIKE', '%' . $search . '%');

            });
        }

        return $model;
    }
}
