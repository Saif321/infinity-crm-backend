<?php

namespace App\Repositories;

use App\Models\ExerciseTag;
use App\Traits\TranslatableRepository;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class ExerciseTagRepository.
 *
 * @package namespace App\Repositories;
 */
class ExerciseTagRepository extends BaseRepository
{

    use TranslatableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ExerciseTag::class;
    }

    
    
    
}
