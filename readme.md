## Instalation instructions

### Edit hosts file add

```bash
127.0.0.1 infinity-fit.test
```

#### Clone the repository
`
git clone git@gitlab.com:tele-go/infinity-fit/api.git
`
#### Change working directory
`
cd api
`

#### Change branch
`
git checkout development
`

#### Copy configuration files
`
cp .env.example .env
`

`
cp docker/.env.example docker/.env
`

#### Copy configuration files

In docker/.env set UID and USER_NAME to match yours.
Find user ID with command
`id -u ubuntu`

#### Build and run docker containers
`
cd docker
`

`
docker-compose up -d
`

#### Execute composer install inside app container
`
docker-compose exec app composer install
`

#### Generate application key
`
docker-compose exec app php artisan key:generate
`
#### Run migrations and seeders
`
docker-compose exec app php artisan migrate --seed
`
#### Install passport
`
docker-compose exec app php artisan passport:install
`
#### Link storage
`
docker-compose exec app php artisan storage:link
`

#### Link to swagger
`
http://infinity-fit.test:8080/api/documentation
`

#### Change permissions (if not working)
`sudo chmod -R 777 ../storage`

#### Testing credentials
- admin `administrator:testeras`
- user `test@server.com:testeras`

#### Console debug

```shell script
PHP_IDE_CONFIG=serverName=infinity-fit.test \
php \
-dxdebug.remote_enable=1 \
-dxdebug.remote_port=9000 \
-dxdebug.remote_host=host.docker.internal \
-dxdebug.remote_connect_back=0 \
-dxdebug.remote_autostart=1 \
-dxdebug.idekey=fit_debug_id \
artisam user-package:renew

```

#### Watch jobs queue

```
docker-compose exec app php artisan queue:listen
```

### Start queue worker 
```
docker-compose exec app php artisan queue:work
```
### To list failed jobs
```
docker-compose exec app php artisan queue:failed  
```
### Log to specific channel
```
 Log::channel('google')->debug('test google log');
```

### Check code
```docker-compose exec app vendor/bin/phpstan analyse app```
