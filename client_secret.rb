require 'jwt'

key_file = 'apple-signin-key.txt'
team_id = 'QB7WMAUMUD'
client_id = 'rs.infinity.fit'
key_id = '9G867KP5RC'

ecdsa_key = OpenSSL::PKey::EC.new IO.read key_file

headers = {
'kid' => key_id
}

claims = {
    'iss' => team_id,
    'iat' => Time.now.to_i,
    'exp' => Time.now.to_i + 86400*180,
    'aud' => 'https://appleid.apple.com',
    'sub' => client_id,
}

token = JWT.encode claims, ecdsa_key, 'ES256', headers

puts token
